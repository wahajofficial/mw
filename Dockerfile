#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

#Depending on the operating system of the host machines(s) that will build or run the containers, the image specified in the FROM statement may need to be changed.
#For more information, please see https://aka.ms/containercompat

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-nanoserver-1809 AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-nanoserver-1809 AS build
WORKDIR /src
COPY ["DigiBancMiddleware/DigiBancMiddleware.csproj", "DigiBancMiddleware/"]
COPY ["MoneyThorHandler/MoneyThorHandler.csproj", "MoneyThorHandler/"]
COPY ["CommonModel/CommonModel.csproj", "CommonModel/"]
COPY ["HttpHandler/HttpHandler.csproj", "HttpHandler/"]
COPY ["TMSHandler/TMSHandler.csproj", "TMSHandler/"]
COPY ["DMSHandler/DMSHandler.csproj", "DMSHandler/"]
COPY ["DBHandler/DBHandler.csproj", "DBHandler/"]
COPY ["ShipaHandler/ShipaHandler.csproj", "ShipaHandler/"]
COPY ["ECSFinHandler/ECSFinHandler.csproj", "ECSFinHandler/"]
COPY ["JCREDITHandler/JCREDITHandler.csproj", "JCREDITHandler/"]
COPY ["IMTFHandler/IMTFHandler.csproj", "IMTFHandler/"]
COPY ["LiveChat/LiveChat.csproj", "LiveChat/"]
COPY ["RMSHandler/RMSHandler.csproj", "RMSHandler/"]
COPY ["ERC321/RuleEngineHandler.csproj", "ERC321/"]
COPY ["ERCCorporateHandler/ERCCorporateHandler.csproj", "ERCCorporateHandler/"]
COPY ["CRMHandler/CRMHandler.csproj", "CRMHandler/"]
COPY ["NestHandler/NestHandler.csproj", "NestHandler/"]
COPY ["EuronetHandler/EuronetHandler.csproj", "EuronetHandler/"]
COPY ["ORDHandler/ORDHandler.csproj", "ORDHandler/"]
RUN dotnet restore "DigiBancMiddleware/DigiBancMiddleware.csproj"
COPY . .
WORKDIR "/src/DigiBancMiddleware"
RUN dotnet build "DigiBancMiddleware.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "DigiBancMiddleware.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "DigiBancMiddleware.dll"]