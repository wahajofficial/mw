﻿using CommonModel;
using System;
using System.ComponentModel.DataAnnotations;


namespace ShipaHandler.OrderManagement
{
    public class OrderManagementModel
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public RootObject RootObject { get; set; }        
    }

    public class Data
    {
        public string is_carry_now { get; set; }
        public string auto_confirm { get; set; }
        public string receiver_location { get; set; }
        public string receiver_address { get; set; }
        public string receiver_contact_name { get; set; }
        public string receiver_contact_number { get; set; }
        public string package_name { get; set; }
        public string package_description { get; set; }
        public string pickup_time { get; set; }
        public string dropoff_time { get; set; }
        public int amount_to_collect { get; set; }
        public int vehicle_type { get; set; }
        public string __invalid_name__weight { get; set; }
        public int quantity { get; set; }
        public string handling_type { get; set; }
    }

    public class RootObject
    {
        public int userid { get; set; }
        public string action { get; set; }
        public string externalid { get; set; }
        public Data data { get; set; }
    }



    public class ResponseObject
    {
        public string status { get; set; }
        public string carryid { get; set; }
        public int code { get; set; }
        public string error { get; set; }
    }
    //public class ResponseObject
    //{
    //    public string status { get; set; }
    //    public int carryid { get; set; }
    //    public int code { get; set; }
    //    public string error { get; set; }
    //    public string shipa_awb { get; set; }
    //}
}
