﻿using CommonModel;
using System;
using System.ComponentModel.DataAnnotations;

namespace ShipaHandler.StatusUpdate
{
    public class StatusUpdateModel
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public Status Status { get; set; }
    }

    public class Status
    {
        public string StatusCode { get; set; }
        public string AWBNumber { get; set; }
    }
}
