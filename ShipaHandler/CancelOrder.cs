﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace ShipaHandler.CancelOrder
{
    public class CancelOrderModel
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public RootObject RootObject { get; set; }
    }

    public class Data
    {
        public string cancellation_reason { get; set; }
    }

    public class RootObject
    {
        public int userid { get; set; }
        public string action { get; set; }
        public string externalid { get; set; }
        public Data data { get; set; }
    }
}
