﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SendGridHandler.SendMail
{
    //public class RequestSendMail
    //{
    //    public SendGridHandler.SendMail.Root RootObject { get; set; }
    //}
    public class To
    {
        public string email { get; set; }
        public string name { get; set; }
    }

    public class Personalization
    {
        public List<To> to { get; set; }
        public string subject { get; set; }
    }

    public class Content
    {
        public string type { get; set; }
        public string value { get; set; }
    }

    public class From
    {
        public string email { get; set; }
        public string name { get; set; }
    }

    public class ReplyTo
    {
        public string email { get; set; }
        public string name { get; set; }
    }

    public class RequestSendMail
    {
        public List<Personalization> personalizations { get; set; }
        public List<Content> content { get; set; }
        public From from { get; set; }
        public ReplyTo reply_to { get; set; }
    }

    public class Error
    {
        public string message { get; set; }
        public string field { get; set; }
        public object help { get; set; }
    }

    public class ResponseObject
    {
        public string errors { get; set; }
    }

    public class ErrorResponse
    {
        public List<Error> errors { get; set; }
    }

}
