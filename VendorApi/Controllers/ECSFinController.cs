﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DBHandler.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using DBHandler.Repositories;
using Microsoft.Extensions.Configuration;
using DBHandler.Enum;
using HttpHandler;
using VendorApi.Helper;
using CommonModel;
using ECSFinHandler;
using Swashbuckle.AspNetCore.Annotations;
using System.Text;

namespace VendorApi.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class ECSFinController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private readonly ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;
        private IHttpClientFactory _httpClientFactory;

        public ECSFinController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs)
        {
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
            _httpClientFactory = httpClientFactory;
        }

        [Route("validateSingleDebitSingleCredit")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.validateSingleDebitSingleCreditModel.RootObjectResponse> ", typeof(ActiveResponseSucces<ECSFinHandler.validateSingleDebitSingleCreditModel.RootObjectResponse>))]
        public async Task<IActionResult> validateSingleDebitSingleCredit([FromBody] ECSFinHandler.validateSingleDebitSingleCreditModel.validateSingleDebitSingleCreditModel Model)
        {
            ActiveResponseSucces<ECSFinHandler.validateSingleDebitSingleCreditModel.RootObjectResponse> response = new ActiveResponseSucces<ECSFinHandler.validateSingleDebitSingleCreditModel.RootObjectResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.RootObject,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:validateSingleDebitSingleCredit"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
            response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.validateSingleDebitSingleCreditModel.RootObjectResponse>(result.httpResult.httpResponse);

                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("singleDebitSingleCredit")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.validateSingleDebitSingleCreditModel.RootObjectResponse> ", typeof(ActiveResponseSucces<ECSFinHandler.validateSingleDebitSingleCreditModel.RootObjectResponse>))]
        public async Task<IActionResult> singleDebitSingleCredit([FromBody] ECSFinHandler.singleDebitSingleCreditModel.singleDebitSingleCreditModel Model)
        {
            ActiveResponseSucces<ECSFinHandler.validateSingleDebitSingleCreditModel.RootObjectResponse> response = new ActiveResponseSucces<ECSFinHandler.validateSingleDebitSingleCreditModel.RootObjectResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.RootObject,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:singleDebitSingleCredit"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.validateSingleDebitSingleCreditModel.RootObjectResponse>(result.httpResult.httpResponse);

                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        
        [Route("singleInstitutionalTransfer")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.singleInstitutionalTransferModel.RootObjectResponse>", typeof(ActiveResponseSucces<ECSFinHandler.singleInstitutionalTransferModel.RootObjectResponse>))]
        public async Task<IActionResult> singleInstitutionalTransfer([FromBody] ECSFinHandler.singleInstitutionalTransferModel.singleInstitutionalTransferModel Model)
        {
            ActiveResponseSucces<ECSFinHandler.singleInstitutionalTransferModel.RootObjectResponse> response = new ActiveResponseSucces<ECSFinHandler.singleInstitutionalTransferModel.RootObjectResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.RootObject,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:singleInstitutionalTransfer"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.singleInstitutionalTransferModel.RootObjectResponse>(result.httpResult.httpResponse);

                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("receiveFromIMS")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.receiveFromIMSModel.RootObjectResponse>", typeof(ActiveResponseSucces<ECSFinHandler.receiveFromIMSModel.RootObjectResponse>))]
        public async Task<IActionResult> receiveFromIMS([FromBody] ECSFinHandler.receiveFromIMSModel.receiveFromIMSModelModel Model)
        {
            ActiveResponseSucces<ECSFinHandler.receiveFromIMSModel.RootObjectResponse> response = new ActiveResponseSucces<ECSFinHandler.receiveFromIMSModel.RootObjectResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.RootObject,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:receiveFromIMS"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.receiveFromIMSModel.RootObjectResponse>(result.httpResult.httpResponse);

                if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("sentToIMS")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.sentToIMSModel.ResponseRootObject>", typeof(ActiveResponseSucces<ECSFinHandler.sentToIMSModel.ResponseRootObject>))]
        public async Task<IActionResult> sentToIMS([FromBody] ECSFinHandler.sentToIMSModel.sentToIMSModel Model)
        {
            ActiveResponseSucces<ECSFinHandler.sentToIMSModel.ResponseRootObject> response = new ActiveResponseSucces<ECSFinHandler.sentToIMSModel.ResponseRootObject>();

            if(Model != null )
            {
                if( Model.RootObject.addenda == null )
                {
                    Model.RootObject.addenda = new List<ECSFinHandler.sentToIMSModel.Addendum>();
                }
                ECSFinHandler.sentToIMSModel.Addendum ad = new ECSFinHandler.sentToIMSModel.Addendum();
                ad.key = "channelInfo";
                ad.value = Model.SignOnRq.ChannelId;
                Model.RootObject.addenda.Add(ad);
            }

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.RootObject,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:sentToIMS"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 2);
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.sentToIMSModel.ResponseRootObject>(result.httpResult.httpResponse);

                if (data != null && data.response.statusCode != null && data.response.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.response.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("custPaymentStatus")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ResponseStatus>", typeof(ActiveResponseSucces<ResponseStatus>))]
        public async Task<IActionResult> custPaymentStatus([FromBody] CommonRequest Model)
        {
            ActiveResponseSucces<ResponseStatus> response = new ActiveResponseSucces<ResponseStatus>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.Param,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:custPaymentStatus"].ToString().Replace("{unique-reference-id}", Model.Param),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ResponseStatus>(result.httpResult.httpResponse);

                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        
        [Route("institutionTransferStatus")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ResponseStatus>", typeof(ActiveResponseSucces<ResponseStatus>))]
        public async Task<IActionResult> institutionTransferStatus([FromBody] CommonRequest Model)
        {
            ActiveResponseSucces<ResponseStatus> response = new ActiveResponseSucces<ResponseStatus>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.Param,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:institutionTransferStatus"].ToString().Replace("{unique-reference-id}", Model.Param),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ResponseStatus>(result.httpResult.httpResponse);

                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("multipledebitmultiplecredit")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<ECSFinHandler.multipledebitmultiplecredit.RootObjectResponse>>", typeof(ActiveResponseSucces<List<ECSFinHandler.multipledebitmultiplecredit.RootObjectResponse>>))]
        public async Task<IActionResult> multipledebitmultiplecredit([FromBody] ECSFinHandler.multipledebitmultiplecredit.multipledebitmultiplecreditModel Model)
        {
            ActiveResponseSucces<List<ECSFinHandler.multipledebitmultiplecredit.RootObjectResponse>> response = new ActiveResponseSucces<List<ECSFinHandler.multipledebitmultiplecredit.RootObjectResponse>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.RootObject,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:multipledebitmultiplecredit"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<List<ECSFinHandler.multipledebitmultiplecredit.RootObjectResponse>>(result.httpResult.httpResponse);

                if (data != null && data.FirstOrDefault().statusCode != null && data.FirstOrDefault().statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.FirstOrDefault().statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        
        [Route("validateMultipleDebitMultipleCredit")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<ECSFinHandler.validateMultipleDebitMultipleCredit.RootObjectResponse>>", typeof(ActiveResponseSucces<List<ECSFinHandler.validateMultipleDebitMultipleCredit.RootObjectResponse>>))]
        public async Task<IActionResult> validateMultipleDebitMultipleCredit([FromBody] ECSFinHandler.validateMultipleDebitMultipleCredit.validateMultipleDebitMultipleCreditModel Model)
        {
            ActiveResponseSucces<List<ECSFinHandler.validateMultipleDebitMultipleCredit.RootObjectResponse>> response = new ActiveResponseSucces<List<ECSFinHandler.validateMultipleDebitMultipleCredit.RootObjectResponse>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.RootObject,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:validatemultipledebitmultiplecredit"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<List<ECSFinHandler.validateMultipleDebitMultipleCredit.RootObjectResponse>>(result.httpResult.httpResponse);
                if (data != null && data.FirstOrDefault().statusCode != null && data.FirstOrDefault().statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.FirstOrDefault().statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }

            }
            return BadRequest(response);
        }

        
        [Route("validateIBAN")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ResponseStatus>", typeof(ActiveResponseSucces<ResponseStatus>))]
        public async Task<IActionResult> validateIBAN([FromBody] CommonRequest Model)
        {
            ActiveResponseSucces<ResponseStatus> response = new ActiveResponseSucces<ResponseStatus>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.Param,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:validateIBAN"].ToString().Replace("{IBAN}", Model.Param),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ResponseStatus>(result.httpResult.httpResponse);

                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        
        [Route("validateBIC")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ResponseStatus> ", typeof(ActiveResponseSucces<ResponseStatus>))]
        public async Task<IActionResult> validateBIC([FromBody] CommonRequest Model)
        {
            ActiveResponseSucces<ResponseStatus> response = new ActiveResponseSucces<ResponseStatus>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.Param,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:validateBIC"].ToString().Replace("{BICS}", Model.Param),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ResponseStatus>(result.httpResult.httpResponse);

                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        
        [Route("initiateCancelPayment")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ResponseStatus>", typeof(ActiveResponseSucces<ResponseStatus>))]
        public async Task<IActionResult> initiateCancelPayment([FromBody] ECSFinHandler.initiateCancelPayment.initiateCancelPaymentModel Model)
        {
            ActiveResponseSucces<ResponseStatus> response = new ActiveResponseSucces<ResponseStatus>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.RootObject,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:initiateCancelPayment"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ResponseStatus>(result.httpResult.httpResponse);
                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("createCustomerSI")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.CustomerSI.Response>", typeof(ActiveResponseSucces<ECSFinHandler.CustomerSI.Response>))]
        public async Task<IActionResult> createCustomerSI([FromBody] ECSFinHandler.createCustomerSI.createCustomerSIModel Model)
        {
            ActiveResponseSucces<ECSFinHandler.CustomerSI.Response> response = new ActiveResponseSucces<ECSFinHandler.CustomerSI.Response>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.RootObject,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:createCustomerSI"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.CustomerSI.Response>(result.httpResult.httpResponse);
                if (data != null && data.statusCode != null && data.statusCode == 1)
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        
        [Route("modifyCustomerSI")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.CustomerSI.Response>", typeof(ActiveResponseSucces<ECSFinHandler.CustomerSI.Response>))]
        public async Task<IActionResult> modifyCustomerSI([FromBody] ECSFinHandler.modifyCustomerSI.modifyCustomerSIModel Model)
        {
            ActiveResponseSucces<ECSFinHandler.CustomerSI.Response> response = new ActiveResponseSucces<ECSFinHandler.CustomerSI.Response>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.RootObject,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:modifyCustomerSI"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2,3);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.CustomerSI.Response>(result.httpResult.httpResponse);
                if (data != null && data.statusCode != null && data.statusCode == 1)
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        //
        
        [Route("modifyCustomerSIInstance")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.CustomerSI.Response>", typeof(ActiveResponseSucces<ECSFinHandler.CustomerSI.Response>))]
        public async Task<IActionResult> modifyCustomerSIInstance([FromBody] ECSFinHandler.modifyCustomerSIInstance.modifyCustomerSIInstanceModel Model)
        {
            ActiveResponseSucces<ECSFinHandler.CustomerSI.Response> response = new ActiveResponseSucces<ECSFinHandler.CustomerSI.Response>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.RootObject,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:modifyCustomerSIInstance"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2, 3);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.CustomerSI.Response>(result.httpResult.httpResponse);
                if (data != null && data.statusCode != null && data.statusCode == 1)
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("cancelCustomerSIInstance")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.CustomerSI.RootObject>", typeof(ActiveResponseSucces<ECSFinHandler.CustomerSI.RootObject>))]
        public async Task<IActionResult> cancelCustomerSIInstance([FromBody] CommonRequest Model)
        {
            ActiveResponseSucces<ECSFinHandler.CustomerSI.RootObject> response = new ActiveResponseSucces<ECSFinHandler.CustomerSI.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.Param,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:cancelCustomerSIInstance"].ToString().Replace("{unique-SIReference-id}", Model.Param),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2, 3);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.CustomerSI.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        
        [Route("getBICInformation")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.getBICInformation.RootObject>", typeof(ActiveResponseSucces<ECSFinHandler.getBICInformation.RootObject>))]
        public async Task<IActionResult> getBICInformation([FromBody] CommonRequest Model)
        {
            ActiveResponseSucces<ECSFinHandler.getBICInformation.RootObject> response = new ActiveResponseSucces<ECSFinHandler.getBICInformation.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.Param,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:getBicInformation"].ToString().Replace("{bic}", Model.Param),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.getBICInformation.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        
        [Route("holdCustomerSIInstance")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.CustomerSI.RootObj>", typeof(ActiveResponseSucces<ECSFinHandler.CustomerSI.RootObj>))]
        public async Task<IActionResult> holdCustomerSIInstance([FromBody] CommonRequest Model)
        {
            ActiveResponseSucces<ECSFinHandler.CustomerSI.RootObject> response = new ActiveResponseSucces<ECSFinHandler.CustomerSI.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.Param,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:holdCustomerSIInstance"].ToString().Replace("{unique-SIReference-id}", Model.Param),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2, 3);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.CustomerSI.RootObject>(result.httpResult.httpResponse);

                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        
        [Route("resumeCustomerSIInstance")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.CustomerSI.RootObj>", typeof(ActiveResponseSucces<ECSFinHandler.CustomerSI.RootObj>))]
        public async Task<IActionResult> resumeCustomerSIInstance([FromBody] CommonRequest Model)
        {
            ActiveResponseSucces<ECSFinHandler.CustomerSI.RootObject> response = new ActiveResponseSucces<ECSFinHandler.CustomerSI.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.Param,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:resumeCustomerSIInstance"].ToString().Replace("{unique-SIReference-id}", Model.Param),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2, 3);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
            response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.CustomerSI.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        //
        
        [Route("getExecutedCustomerSIInstances")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.getExecutedCustomerSIInstances.RootObject>", typeof(ActiveResponseSucces<ECSFinHandler.getExecutedCustomerSIInstances.RootObject>))]
        public async Task<IActionResult> getExecutedCustomerSIInstances([FromBody] CommonRequest Model)
        {
            ActiveResponseSucces<ECSFinHandler.getExecutedCustomerSIInstances.RootObject> response = new ActiveResponseSucces<ECSFinHandler.getExecutedCustomerSIInstances.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.Param,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:getExecutedCustomerSIInstances"].ToString().Replace("{unique-SIReference- id}", Model.Param),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.getExecutedCustomerSIInstances.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("getFutureCustomerSIInstances")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.getExecutedCustomerSIInstances.RootObject>", typeof(ActiveResponseSucces<ECSFinHandler.getExecutedCustomerSIInstances.RootObject>))]
        public async Task<IActionResult> getFutureCustomerSIInstances([FromBody] CommonRequest Model)
        {
            ActiveResponseSucces<ECSFinHandler.getExecutedCustomerSIInstances.RootObject> response = new ActiveResponseSucces<ECSFinHandler.getExecutedCustomerSIInstances.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.Param,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:getFutureCustomerSIInstances"].ToString().Replace("{unique-SIReference-id}", Model.Param),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.getExecutedCustomerSIInstances.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("getCustomerSIList")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.getCustomerSIList.RootObject>", typeof(ActiveResponseSucces<ECSFinHandler.getCustomerSIList.RootObject>))]
        public async Task<IActionResult> getCustomerSIList([FromBody] CommonRequest Model)
        {
            ActiveResponseSucces<ECSFinHandler.getCustomerSIList.RootObject> response = new ActiveResponseSucces<ECSFinHandler.getCustomerSIList.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.Param,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:getCustomerSIList"].ToString().Replace("(cifNumber)", Model.Param),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.getCustomerSIList.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("createCustomerSweep")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ResponseStatus>", typeof(ActiveResponseSucces<ResponseStatus>))]
        public async Task<IActionResult> createCustomerSweep([FromBody] ECSFinHandler.createCustomerSweep.createCustomerSweepModel Model)
        {
            ActiveResponseSucces<ResponseStatus> response = new ActiveResponseSucces<ResponseStatus>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.RootObject,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:createCustomerSweep"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ResponseStatus>(result.httpResult.httpResponse);

                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("modifyCustomerSweep")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ResponseStatus>", typeof(ActiveResponseSucces<ResponseStatus>))]
        public async Task<IActionResult> modifyCustomerSweep([FromBody] ECSFinHandler.modifyCustomerSweep.modifyCustomerSweepModel Model)
        {
            ActiveResponseSucces<ResponseStatus> response = new ActiveResponseSucces<ResponseStatus>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.RootObject,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:modifyCustomerSweep"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2, 3);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ResponseStatus>(result.httpResult.httpResponse);
                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("modifyCustomerSweepInstance")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ResponseStatus>", typeof(ActiveResponseSucces<ResponseStatus>))]
        public async Task<IActionResult> modifyCustomerSweepInstance([FromBody] ECSFinHandler.modifyCustomerSweep.modifyCustomerSweepModel Model)
        {
            ActiveResponseSucces<ResponseStatus> response = new ActiveResponseSucces<ResponseStatus>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.RootObject,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:modifyCustomerSweepInstance"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2, 3);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ResponseStatus>(result.httpResult.httpResponse);
                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("cancelCustomerSweepInstance")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ResponseStatus>", typeof(ActiveResponseSucces<ResponseStatus>))]
        public async Task<IActionResult> cancelCustomerSweepInstance([FromBody] CommonRequest Model)
        {
            ActiveResponseSucces<ResponseStatus> response = new ActiveResponseSucces<ResponseStatus>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.Param,
                                                                       _config["ECSFin:BaseUrl"].ToString(),
                                                                       _config["ECSFin:cancelCustomerSweepInstance"].ToString().Replace("{unique-SweepReference- id}", Model.Param),
                                                                       level,
                                                                       _logs,
                                                                       _userChannels,
                                                                          this,
                                                                       _config,
                                                                       httpHandler, 2, 3);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ResponseStatus>(result.httpResult.httpResponse);
                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        
        [Route("holdCustomerSweepInstance")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ResponseStatus>", typeof(ActiveResponseSucces<ResponseStatus>))]
        public async Task<IActionResult> holdCustomerSweepInstance([FromBody] CommonRequest Model)
        {
            ActiveResponseSucces<ResponseStatus> response = new ActiveResponseSucces<ResponseStatus>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.Param,
                                                                       _config["ECSFin:BaseUrl"].ToString(),
                                                                       _config["ECSFin:holdCustomerSweepInstance"].ToString().Replace("{unique-SweepReference- id}", Model.Param),
                                                                       level,
                                                                       _logs,
                                                                       _userChannels,
                                                                          this,
                                                                       _config,
                                                                       httpHandler, 2, 3);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ResponseStatus>(result.httpResult.httpResponse);
                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("resumeCustomerSweepInstance")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ResponseStatus> ", typeof(ActiveResponseSucces<ResponseStatus>))]
        public async Task<IActionResult> resumeCustomerSweepInstance([FromBody] CommonRequest Model)
        {
            ActiveResponseSucces<ResponseStatus> response = new ActiveResponseSucces<ResponseStatus>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.Param,
                                                                       _config["ECSFin:BaseUrl"].ToString(),
                                                                       _config["ECSFin:resumeCustomerSweepInstance"].ToString().Replace("{unique-SweepReference- id}", Model.Param),
                                                                       level,
                                                                       _logs,
                                                                       _userChannels,
                                                                          this,
                                                                       _config,
                                                                       httpHandler, 2, 3);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ResponseStatus>(result.httpResult.httpResponse);
                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("getExecutedCustomerSweepInstances")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.getExecutedCustomerSIInstances.RootObject>", typeof(ActiveResponseSucces<ECSFinHandler.getExecutedCustomerSIInstances.RootObject>))]
        public async Task<IActionResult> getExecutedCustomerSweepInstances([FromBody] CommonRequest Model)
        {
            ActiveResponseSucces<ECSFinHandler.getExecutedCustomerSIInstances.RootObject> response = new ActiveResponseSucces<ECSFinHandler.getExecutedCustomerSIInstances.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.Param,
                                                                       _config["ECSFin:BaseUrl"].ToString(),
                                                                       _config["ECSFin:getExecutedCustomerSweepInstances"].ToString().Replace("{unique-sweep- id}", Model.Param),
                                                                       level,
                                                                       _logs,
                                                                       _userChannels,
                                                                          this,
                                                                       _config,
                                                                       httpHandler, 2, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.getExecutedCustomerSIInstances.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("getCustomerSweepList")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.getCustomerSweepList.RootObject> ", typeof(ActiveResponseSucces<ECSFinHandler.getCustomerSweepList.RootObject>))]
        public async Task<IActionResult> getCustomerSweepList([FromBody] CommonRequest Model)
        {
            ActiveResponseSucces<ECSFinHandler.getCustomerSweepList.RootObject> response = new ActiveResponseSucces<ECSFinHandler.getCustomerSweepList.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.Param,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:getCustomerSweepList"].ToString().Replace("(cifNumber)", Model.Param),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.getCustomerSweepList.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        //new
        
        [Route("complianceStatus")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.complianceStatus.RootObjectResponse>", typeof(ActiveResponseSucces<ECSFinHandler.complianceStatus.RootObjectResponse>))]
        public async Task<IActionResult> complianceStatus([FromBody] ECSFinHandler.complianceStatus.complianceStatusModel Model)
        {
            ActiveResponseSucces<ECSFinHandler.complianceStatus.RootObjectResponse> response = new ActiveResponseSucces<ECSFinHandler.complianceStatus.RootObjectResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.RootObject,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:complianceStatus"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2, 3);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.complianceStatus.RootObjectResponse>(result.httpResult.httpResponse);
                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("validatesingledebitmultiplecredit")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.validatesingledebitmultiplecredit.RootObjectResponse>", typeof(ActiveResponseSucces<ECSFinHandler.validatesingledebitmultiplecredit.RootObjectResponse>))]
        public async Task<IActionResult> validatesingledebitmultiplecredit([FromBody] ECSFinHandler.validatesingledebitmultiplecredit.validatesingledebitmultiplecreditModel Model)
        {
            ActiveResponseSucces<ECSFinHandler.validatesingledebitmultiplecredit.RootObjectResponse> response = new ActiveResponseSucces<ECSFinHandler.validatesingledebitmultiplecredit.RootObjectResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.RootObject,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:validatesingledebitmultiplecredit"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.validatesingledebitmultiplecredit.RootObjectResponse>(result.httpResult.httpResponse);
                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        
        [Route("validateinstitudesingledebitsinglecredit")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.validateinstitudesingledebitsinglecredit.RootObjectResponse>", typeof(ActiveResponseSucces<ECSFinHandler.validateinstitudesingledebitsinglecredit.RootObjectResponse>))]
        public async Task<IActionResult> validateinstitudesingledebitsinglecredit([FromBody] ECSFinHandler.validateinstitudesingledebitsinglecredit.validateinstitudesingledebitsinglecreditModel Model)
        {
            ActiveResponseSucces<ECSFinHandler.validateinstitudesingledebitsinglecredit.RootObjectResponse> response = new ActiveResponseSucces<ECSFinHandler.validateinstitudesingledebitsinglecredit.RootObjectResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.RootObject,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:validateinstitudesingledebitsinglecredit"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.validateinstitudesingledebitsinglecredit.RootObjectResponse>(result.httpResult.httpResponse);
                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("singledebitmultiplecredit")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.singledebitmultiplecredit.RootObjectResponse>", typeof(ActiveResponseSucces<ECSFinHandler.singledebitmultiplecredit.RootObjectResponse>))]
        public async Task<IActionResult> singledebitmultiplecredit([FromBody] ECSFinHandler.singledebitmultiplecredit.singledebitmultiplecreditModel Model)
        {
            ActiveResponseSucces<ECSFinHandler.singledebitmultiplecredit.RootObjectResponse> response = new ActiveResponseSucces<ECSFinHandler.singledebitmultiplecredit.RootObjectResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.RootObject,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:singledebitmultiplecredit"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.singledebitmultiplecredit.RootObjectResponse>(result.httpResult.httpResponse);
                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        
        [Route("getCharges")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.GetCharges.RootObjectResponse>", typeof(ActiveResponseSucces<ECSFinHandler.GetCharges.RootObjectResponse>))]
        public async Task<IActionResult> getCharges([FromBody] ECSFinHandler.GetCharges.Parameter Model)
        {
            ActiveResponseSucces<ECSFinHandler.GetCharges.RootObjectResponse> response = new ActiveResponseSucces<ECSFinHandler.GetCharges.RootObjectResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.requireddata,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:getCharges"].ToString()
                                                                        .Replace("{bankCode}", Model.requireddata.bankCode)
                                                                        .Replace("{amount}", Model.requireddata.amount)
                                                                        .Replace("{currency}", Model.requireddata.currency)
                                                                        .Replace("{segmentType}", Model.requireddata.segmentType)
                                                                        .Replace("{pymntChannel}", Model.requireddata.pymntChannel)
                                                                        .Replace("{purposeCode}", Model.requireddata.purposeCode)
                                                                        .Replace("{channelInfo}", Model.requireddata.channelInfo)
                                                                        .Replace("{pymntDir}", Model.requireddata.pymntDir)
                                                                        .Replace("{cifNumber}", Model.requireddata.cifNumber)
                                                                        .Replace("{accNo}", Model.requireddata.accNo),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2, 1);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.GetCharges.RootObjectResponse>(result.httpResult.httpResponse);
                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        //queryPaymentInfo
        
        [Route("queryPaymentInfo")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.queryPaymentInfo.RootObjectResponse>", typeof(ActiveResponseSucces<ECSFinHandler.queryPaymentInfo.RootObjectResponse>))]
        public async Task<IActionResult> queryPaymentInfo([FromBody] ECSFinHandler.queryPaymentInfo.queryPaymentInfoModel Model)
        {
            ActiveResponseSucces<ECSFinHandler.queryPaymentInfo.RootObjectResponse> response = new ActiveResponseSucces<ECSFinHandler.queryPaymentInfo.RootObjectResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.RootObject,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:queryPaymentInfo"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2, 1);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.queryPaymentInfo.RootObjectResponse>(result.httpResult.httpResponse);
                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        //
        
        [Route("GetPossibleRoute")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.GetPossibleRoute.RootObjectResponse>", typeof(ActiveResponseSucces<ECSFinHandler.GetPossibleRoute.RootObjectResponse>))]
        public async Task<IActionResult> GetPossibleRoute([FromBody] ECSFinHandler.GetPossibleRoute.Parameter Model)
        {
            ActiveResponseSucces<ECSFinHandler.GetPossibleRoute.RootObjectResponse> response = new ActiveResponseSucces<ECSFinHandler.GetPossibleRoute.RootObjectResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.requireddata,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:GetPossibleRoute"].ToString()
                                                                        .Replace("{bankCode}", Model.requireddata.bankCode)
                                                                        .Replace("{amount}", Model.requireddata.amount)
                                                                        .Replace("{currency}", Model.requireddata.currency)
                                                                        .Replace("{pymntChannel}", Model.requireddata.pymntChannel)
                                                                        .Replace("{channelInfo}", Model.requireddata.channelInfo),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2, 1);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.GetPossibleRoute.RootObjectResponse>(result.httpResult.httpResponse);
                if (data != null && data.response.statusCode != null && data.response.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.response.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        //GetExchangeRate
        
        [Route("GetExchangeRate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.GetExchangeRate.RootObjectResponse>", typeof(ActiveResponseSucces<ECSFinHandler.GetExchangeRate.RootObjectResponse>))]
        public async Task<IActionResult> GetExchangeRate([FromBody] ECSFinHandler.GetExchangeRate.Parameter Model)
        {
            ActiveResponseSucces<ECSFinHandler.GetExchangeRate.RootObjectResponse> response = new ActiveResponseSucces<ECSFinHandler.GetExchangeRate.RootObjectResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.requireddata,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:GetExchangeRate"].ToString()
                                                                        .Replace("{base-currency}", Model.requireddata.basecurrency)
                                                                        .Replace("{counter-currency}", Model.requireddata.countercurrency),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.GetExchangeRate.RootObjectResponse>(result.httpResult.httpResponse);
                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        //GetSpecialExchangeRates
        
        [Route("GetSpecialExchangeRates")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.GetSpecialExchangeRates.RootObjectResponse>", typeof(ActiveResponseSucces<ECSFinHandler.GetSpecialExchangeRates.RootObjectResponse>))]
        public async Task<IActionResult> GetSpecialExchangeRates([FromBody] ECSFinHandler.GetSpecialExchangeRates.Parameter Model)
        {
            ActiveResponseSucces<ECSFinHandler.GetSpecialExchangeRates.RootObjectResponse> response = new ActiveResponseSucces<ECSFinHandler.GetSpecialExchangeRates.RootObjectResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.requireddata,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:GetSpecialExchangeRates"].ToString()
                                                                        .Replace("{baseCur}", Model.requireddata.baseCur)
                                                                        .Replace("{counterCur}", Model.requireddata.counterCur)
                                                                        .Replace("{custId}", Model.requireddata.custId),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.GetSpecialExchangeRates.RootObjectResponse>(result.httpResult.httpResponse);
                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        //CancelCustomerSI
        
        [Route("CancelCustomerSI")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.CancelCustomerSI.RootObjectResponse>", typeof(ActiveResponseSucces<ECSFinHandler.CancelCustomerSI.RootObjectResponse>))]
        public async Task<IActionResult> CancelCustomerSI([FromBody] ECSFinHandler.CancelCustomerSI.Parameter Model)
        {
            ActiveResponseSucces<ECSFinHandler.CancelCustomerSI.RootObjectResponse> response = new ActiveResponseSucces<ECSFinHandler.CancelCustomerSI.RootObjectResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.requireddata,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:CancelCustomerSI"].ToString()
                                                                        .Replace("{unique-SIReference-id}", Model.requireddata.uniqueSIReferenceid),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2, 3);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.CancelCustomerSI.RootObjectResponse>(result.httpResult.httpResponse);
                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("CancelCustomerSweep")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.CancelCustomerSweep.RootObjectResponse>", typeof(ActiveResponseSucces<ECSFinHandler.CancelCustomerSweep.RootObjectResponse>))]
        public async Task<IActionResult> CancelCustomerSweep([FromBody] ECSFinHandler.CancelCustomerSweep.Parameter Model)
        {
            ActiveResponseSucces<ECSFinHandler.CancelCustomerSweep.RootObjectResponse> response = new ActiveResponseSucces<ECSFinHandler.CancelCustomerSweep.RootObjectResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.requireddata,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:CancelCustomerSweep"].ToString()
                                                                        .Replace("{unique-SweepReference-id}", Model.requireddata.uniqueSweepReferenceid),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2, 3);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.CancelCustomerSweep.RootObjectResponse>(result.httpResult.httpResponse);
                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("IMTFTransactionScreening")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>", typeof(ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>))]
        public async Task<IActionResult> IMTFTransactionScreening([FromBody] ECSFinHandler.INTER_FT_AED_AED_Outbound.INTER_FT_AED_AED_OutboundModel Model)
        {
            IMTFHandler.IncomingFinTrx.RootObject mainOBJ = new IMTFHandler.IncomingFinTrx.RootObject();
            mainOBJ.RequestMetaData = new IMTFHandler.IncomingFinTrx.RequestMetaData();
            mainOBJ.RequestMetaData.type = Model.RootObject.actionType;
            mainOBJ.RequestMetaData.sendTime = DateTime.Now.ToString();
            mainOBJ.RequestMetaData.timeZone = "4";
                //TimeZone.CurrentTimeZone.GetUtcOffset(new DateTime()).ToString();
            mainOBJ.RequestMetaData.userId = "SYSTEM";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestMetaData.requestUid = Guid.NewGuid().ToString();
            mainOBJ.RequestMetaData.orgUnit = Model.RootObject.clientType.ToUpper()=="CORP"? "ORG_GLOBAL/ORG_AE/AE_CORP" : "ORG_GLOBAL/ORG_AE/AE_RETAIL";
            mainOBJ.RequestMetaData.branchCompanyId = "784100";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestMetaData.branchLocationId = "784101";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestMetaData.systemId = "CIF";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestMetaData.serviceId = Model.RootObject.actionType;

            mainOBJ.RequestDataObject = new IMTFHandler.IncomingFinTrx.RequestDataObject();
            mainOBJ.RequestDataObject.DoiTAccountTransaction = new IMTFHandler.IncomingFinTrx.DoiTAccountTransaction();
            mainOBJ.RequestDataObject.DoiTAccountTransaction.entityType = "2001";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestDataObject.DoiTAccountTransaction.originId = Model.RootObject.correlationId;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.listId= Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/TRX" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/TRX";
            if (Model.RootObject.sourceType == "INTRA-SC" || Model.RootObject.sourceType == "INTRA-DC")
            {
                mainOBJ.RequestDataObject.DoiTAccountTransaction.originalAccountFrom = Model.RootObject.sourceAccount;
            }
            else if((Model.RootObject.sourceType == "INTER-FT" || Model.RootObject.sourceType == "INTER-SW" || Model.RootObject.sourceType == "INTER-OTH") && Model.RootObject.destinationType.ToUpper() == "OUTGOING")
            {
                mainOBJ.RequestDataObject.DoiTAccountTransaction.originalAccountFrom = Model.RootObject.sourceAccount;
               // mainOBJ.RequestDataObject.DoiTAccountTransaction.originalCtptyAcc2 = Model.RootObject.destinationAccount;
            }


            if ((Model.RootObject.sourceType == "INTRA-SC" || Model.RootObject.sourceType == "INTRA-DC") && Model.RootObject.destinationType.ToUpper() == "OUTGOING")
            {
                mainOBJ.RequestDataObject.DoiTAccountTransaction.originalAccountTo = Model.RootObject.destinationAccount;
            }

            if (Model.RootObject.destinationType.ToUpper() == "INCOMING")
            {
               // mainOBJ.RequestDataObject.DoiTAccountTransaction.originalCtptyAcc1 = Model.RootObject.sourceAccount;
               // mainOBJ.RequestDataObject.DoiTAccountTransaction.originalCtptyAcc2 = Model.RootObject.destinationAccount;
            }


            //mainOBJ.RequestDataObject.DoiTAccountTransaction.originalCtptyAcc1 = mainOBJ.RequestDataObject.DoiTAccountTransaction.originalCtptyAcc1 == null ? "" : mainOBJ.RequestDataObject.DoiTAccountTransaction.originalCtptyAcc1;
            //mainOBJ.RequestDataObject.DoiTAccountTransaction.originalCtptyAcc2 = mainOBJ.RequestDataObject.DoiTAccountTransaction.originalCtptyAcc2 == null ? "" : mainOBJ.RequestDataObject.DoiTAccountTransaction.originalCtptyAcc2;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.transactionDirection = "C";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestDataObject.DoiTAccountTransaction.originalAmount = Model.RootObject.transactionAmount;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.originalCurrency = Model.RootObject.transactionCurrency;
            //mainOBJ.RequestDataObject.DoiTAccountTransaction.originalInstructingBankBranch = Model.RootObject.transactionRouting == null ?"": Model.RootObject.transactionRouting.instructingBank.branch;
            //mainOBJ.RequestDataObject.DoiTAccountTransaction.originalInstructingBranchCity = Model.RootObject.transactionRouting == null ? "" : Model.RootObject.transactionRouting.instructingBank.city;
            //mainOBJ.RequestDataObject.DoiTAccountTransaction.uidInstructingBank = Model.RootObject.transactionRouting == null ? "" : Model.RootObject.transactionRouting.instructingBank.uid;
            //mainOBJ.RequestDataObject.DoiTAccountTransaction.instructingBankCountry = Model.RootObject.transactionRouting == null ? "" : Model.RootObject.transactionRouting.instructingBank.country;
            //mainOBJ.RequestDataObject.DoiTAccountTransaction.uidSenderCorrespBank = Model.RootObject.transactionRouting == null ? "" : Model.RootObject.transactionRouting.senderCorrespondantBank.uid;
            //mainOBJ.RequestDataObject.DoiTAccountTransaction.senderCorrespBankCountry = Model.RootObject.transactionRouting == null ? "" : Model.RootObject.transactionRouting.senderCorrespondantBank.country;
            //mainOBJ.RequestDataObject.DoiTAccountTransaction.uidIntermediaryBank = Model.RootObject.transactionRouting == null ? "" : Model.RootObject.transactionRouting.intermediaryBank.uid;
            //mainOBJ.RequestDataObject.DoiTAccountTransaction.intermediaryBankCountry = Model.RootObject.transactionRouting == null ? "" : Model.RootObject.transactionRouting.intermediaryBank.country;
            //mainOBJ.RequestDataObject.DoiTAccountTransaction.uidReceiverCorrespBank = Model.RootObject.transactionRouting == null ? "" : Model.RootObject.transactionRouting.receiverBank.uid;
            //mainOBJ.RequestDataObject.DoiTAccountTransaction.receiverCorrespBankCountry = Model.RootObject.transactionRouting == null ? "" : Model.RootObject.transactionRouting.receiverBank.country;
            //mainOBJ.RequestDataObject.DoiTAccountTransaction.uidAccountWithInstitution = Model.RootObject.transactionRouting == null ? "" : Model.RootObject.transactionRouting.accountWithInstitution.uid;
            //mainOBJ.RequestDataObject.DoiTAccountTransaction.accountWithInstitutionCountry = Model.RootObject.transactionRouting == null ? "" : Model.RootObject.transactionRouting.accountWithInstitution.country;

            //DO I TAccount Transactions.
          //  mainOBJ.RequestDataObject.DoiTAccountTransaction.transactionReference = Model.RootObject.transactionReference;
          //  mainOBJ.RequestDataObject.DoiTAccountTransaction.relatedReference = Model.RootObject.relatedReference;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.transactionType = Model.RootObject.transactionType;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.transactionPurpose = Model.RootObject.purpose;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.transactionDtg = Model.RootObject.transactionDate;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.transactionTz = "4";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestDataObject.DoiTAccountTransaction.baseAmount = Model.RootObject.accountAmount;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.baseCurrency = Model.RootObject.accountCurrency;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.localAmount = Model.RootObject.aedAmount;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.localCurrency = "AED";
            mainOBJ.RequestDataObject.DoiTAccountTransaction.localCommission = Model.RootObject.commission;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.localFees = Model.RootObject.fees;

            //why to add multiple  object and when ?
            IMTFHandler.IncomingFinTrx.PaymentDetail paymentDetailSource = new IMTFHandler.IncomingFinTrx.PaymentDetail();
            paymentDetailSource.entityType = "2009";//TODO: we need to  read this via soft configuration.
            paymentDetailSource.originId = Model.RootObject.correlationId+"_1";
            paymentDetailSource.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/TRX" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/TRX";
            paymentDetailSource.paymentType = "PAYIN";//TODO: we need to  read this via soft configuration.
            paymentDetailSource.paymentMode= "ACCT";//TODO: we need to  read this via soft configuration.
            paymentDetailSource.payTransactionDtg = Model.RootObject.transactionDate;
            paymentDetailSource.payTransactionTz = "4";//TODO: we need to  read this via soft configuration.
            paymentDetailSource.payBaseAmount = Model.RootObject.debitLeg == null ?"":Model.RootObject.debitLeg.amount;
            paymentDetailSource.payBaseCurrency = Model.RootObject.debitLeg == null ? "" : Model.RootObject.debitLeg.accountCurrency;
            paymentDetailSource.payLocalAmount = Model.RootObject.debitLeg == null ? "" : Model.RootObject.debitLeg.aedAmount;
            paymentDetailSource.payLocalCurrency = "AED";//TODO: we need to  read this via soft configuration.
            paymentDetailSource.payOriginalAmount = Model.RootObject.debitLeg == null ? "" : Model.RootObject.debitLeg.tranasctionAmount;
            paymentDetailSource.payOriginalCurrency = Model.RootObject.debitLeg == null ? "" : Model.RootObject.debitLeg.transactionCurrency;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.PaymentDetails = new List<IMTFHandler.IncomingFinTrx.PaymentDetail>();
            mainOBJ.RequestDataObject.DoiTAccountTransaction.PaymentDetails.Add(paymentDetailSource);

            IMTFHandler.IncomingFinTrx.PaymentDetail paymentDetailDestination = new IMTFHandler.IncomingFinTrx.PaymentDetail();
            paymentDetailDestination.entityType = "2009";//TODO: we need to  read this via soft configuration.
            paymentDetailDestination.originId = Model.RootObject.correlationId + "_2";
            paymentDetailDestination.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/TRX" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/TRX";
            paymentDetailDestination.paymentType = "PAYOUT";//TODO: we need to  read this via soft configuration.
            paymentDetailDestination.paymentMode = "ACCT";//TODO: we need to  read this via soft configuration.
            paymentDetailDestination.payTransactionDtg = "25/04/2019 09:37:36.036629";//TODO: we need to  read this via soft configuration.
            paymentDetailDestination.payTransactionTz = "4";//TODO: we need to  read this via soft configuration.
            paymentDetailDestination.payBaseAmount = Model.RootObject.creditLeg == null ? "" :  Model.RootObject.creditLeg.amount;
            paymentDetailDestination.payBaseCurrency = Model.RootObject.creditLeg == null ? "" : Model.RootObject.creditLeg.accountCurrency;
            paymentDetailDestination.payLocalAmount = Model.RootObject.creditLeg == null ? "" : Model.RootObject.creditLeg.aedAmount;
            paymentDetailDestination.payLocalCurrency = "AED";//TODO: we need to  read this via soft configuration.
            paymentDetailDestination.payOriginalAmount = Model.RootObject.creditLeg == null ? "" : Model.RootObject.creditLeg.tranasctionAmount;
            paymentDetailDestination.payOriginalCurrency = Model.RootObject.creditLeg == null ? "" : Model.RootObject.creditLeg.transactionCurrency;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.PaymentDetails.Add(paymentDetailDestination);

            IMTFHandler.IncomingFinTrx.DoiPerson personSource = new IMTFHandler.IncomingFinTrx.DoiPerson();
            personSource.entityType = "3001";//TODO: we need to  read this via soft configuration.
            personSource.originId = Model.RootObject.cif;
            personSource.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CUS" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CUS";

          //  IMTFHandler.IncomingFinTrx.DoiPersonPhysChar personPhysCharSource = new IMTFHandler.IncomingFinTrx.DoiPersonPhysChar();
          ////  personPhysCharSource.originId = Model.RootObject.sourceType == "INTRA-SC" ? Model.RootObject.cif : Model.RootObject.destinationAccount;
           // personPhysCharSource.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CUS";
           // personPhysCharSource.nationality = Model.RootObject.beneficiaryDetails == null ? "" : Model.RootObject.beneficiaryDetails.countryCode;
          //  personSource.DoiPersonPhysChar = new List<IMTFHandler.IncomingFinTrx.DoiPersonPhysChar>();
           // personSource.DoiPersonPhysChar.Add(personPhysCharSource);

            //personSource.DoiPersonPhysChar.Add(personPhysCharSource);
            mainOBJ.RequestDataObject.DoiPerson = new List<IMTFHandler.IncomingFinTrx.DoiPerson>();
            mainOBJ.RequestDataObject.DoiPerson.Add(personSource);


            IMTFHandler.IncomingFinTrx.DoiAccountEntity accountEntity1 = new IMTFHandler.IncomingFinTrx.DoiAccountEntity();
            accountEntity1.entityType = "4";//TODO: we need to  read this via soft configuration.
            accountEntity1.originId = Model.RootObject.sourceAccount;
            accountEntity1.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CUS_ACC" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CUS_ACC";
            personSource.DoiAccountEntity = new List<IMTFHandler.IncomingFinTrx.DoiAccountEntity>();
            personSource.DoiAccountEntity.Add(accountEntity1);
            
            IMTFHandler.IncomingFinTrx.DoiPerson personDestination = new IMTFHandler.IncomingFinTrx.DoiPerson();
            personDestination.entityType = "3001";//TODO: we need to  read this via soft configuration.
            personDestination.originId = (Model.RootObject.sourceType == "INTRA-SC" || Model.RootObject.sourceType == "INTRA-DC") ? Model.RootObject.destinationAccount.Substring(5,9) : Model.RootObject.destinationAccount;

            if(Model.RootObject.clientType.ToUpper() == "CORP" )
            {
                if((Model.RootObject.sourceType == "INTRA-SC" || Model.RootObject.sourceType == "INTRA-DC"))
                {
                    personDestination.listId = "ORG_GLOBAL/ORG_AE/AE_CORP/CUS";
                }
                else
                {
                    personDestination.listId = "ORG_GLOBAL/ORG_AE/AE_CORP/CUS_ACC";
                }
            }
            else
            {
                if ((Model.RootObject.sourceType == "INTRA-SC" || Model.RootObject.sourceType == "INTRA-DC"))
                {
                    personDestination.listId = "ORG_GLOBAL/ORG_AE/AE_RETAIL/CUS";
                }
                else
                {
                    personDestination.listId = "ORG_GLOBAL/ORG_AE/AE_RETAIL/CUS_ACC";
                }
            }
            //personDestination.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CUS_ACC" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CUS_ACC";
            //personDestination.legalStatus = Model.RootObject.beneficiaryDetails ==null ?"": Model.RootObject.beneficiaryDetails.beneficaryType;
         //   personDestination.fullName = Model.RootObject.beneficiaryDetails == null ? "" : Model.RootObject.beneficiaryDetails.fullName;

          //  IMTFHandler.IncomingFinTrx.DoiPersonPhysChar personPhysChar = new IMTFHandler.IncomingFinTrx.DoiPersonPhysChar();
          //  personPhysChar.originId = Model.RootObject.sourceType == "INTRA-SC" ? Model.RootObject.cif : Model.RootObject.destinationAccount;
          //  personPhysChar.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CUS";
           // personPhysChar.nationality = Model.RootObject.beneficiaryDetails == null ? "" : Model.RootObject.beneficiaryDetails.countryCode;
         //   personDestination.DoiPersonPhysChar = new List<IMTFHandler.IncomingFinTrx.DoiPersonPhysChar>();
          //  personDestination.DoiPersonPhysChar.Add(personPhysChar);

            IMTFHandler.IncomingFinTrx.DoiLocation location = new IMTFHandler.IncomingFinTrx.DoiLocation();
            location.entityType = "21";//TODO: we need to  read this via soft configuration.
            location.originId = Model.RootObject.destinationAccount;
            location.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CUS_ADR" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CUS_ADR";
           // location.fullAddressText = Model.RootObject.beneficiaryDetails == null ? "" : Model.RootObject.beneficiaryDetails.fullAddress;
           // location.country = Model.RootObject.beneficiaryDetails == null ? "" : Model.RootObject.beneficiaryDetails.countryCode;
          //  personDestination.DoiLocation = new List<IMTFHandler.IncomingFinTrx.DoiLocation>();
          //  personDestination.DoiLocation.Add(location);

            IMTFHandler.IncomingFinTrx.DoiAccountEntity accountEntity = new IMTFHandler.IncomingFinTrx.DoiAccountEntity();
            accountEntity.entityType = "4";//TODO: we need to  read this via soft configuration.
            accountEntity.originId = Model.RootObject.destinationAccount;
            accountEntity.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CUS_ACC" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CUS_ACC";
         //   accountEntity.accountType = "";
          //  accountEntity.accountNumber = Model.RootObject.destinationAccount;
          //  accountEntity.bic = Model.RootObject.beneficiaryDetails == null ? "" : Model.RootObject.beneficiaryDetails.bic;
          //  accountEntity.bankIban = Model.RootObject.beneficiaryDetails == null ? "" : Model.RootObject.beneficiaryDetails.bankIban;
           // accountEntity.bankName = Model.RootObject.beneficiaryDetails == null ? "" : Model.RootObject.beneficiaryDetails.bankName;
           // accountEntity.branchName = Model.RootObject.beneficiaryDetails == null ? "" : Model.RootObject.beneficiaryDetails.branchName;
          //  accountEntity.branchCity = Model.RootObject.beneficiaryDetails == null ? "" : Model.RootObject.beneficiaryDetails.branchCity;
         //  accountEntity.branchCountry = Model.RootObject.beneficiaryDetails == null ? "" : Model.RootObject.beneficiaryDetails.branchCountry;
            personDestination.DoiAccountEntity = new List<IMTFHandler.IncomingFinTrx.DoiAccountEntity>();
            personDestination.DoiAccountEntity.Add(accountEntity);

            mainOBJ.RequestDataObject.DoiPerson.Add(personDestination);

            ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject> response = new ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, mainOBJ,
                                                                        _config["IMTF:BaseUrl"].ToString(),
                                                                        _config["IMTF:transaction"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 
                                                                        3);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<IMTFHandler.CommonResponse.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.responseDataObject.responseCode != null && data.responseDataObject.responseCode == "SUC")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);

                }
                else
                {
                    if (data == null)
                    {
                        response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = "FAIL" };
                    }
                    else
                    {
                        response.Status = new Status { Severity = Severity.Error, Code = data.responseDataObject.responseCode, StatusMessage = data.responseDataObject.responseMessage };
                    }
                }
            }
            return BadRequest(response);
        }

        [Route("StatusUpdate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ResponseObject>", typeof(ActiveResponseSucces<ResponseObject>))]
        public async Task<IActionResult> StatusUpdate([FromBody] VendorProcessStatusModel Model)
        {
            ActiveResponseSucces<ResponseObject> response = new ActiveResponseSucces<ResponseObject>();

            string retailChannelID = _config["GlobalSettings:RetailChannel"];
            string[] corporateChannelID = _config["GlobalSettings:CorporateCannel"].Split(",");
            string ECSCannel = _config["GlobalSettings:ECSCannel"];
            string IMTFCannel = _config["GlobalSettings:IMTFCannel"];
            string WPSPortal = _config["GlobalSettings:WPSPortal"];

            InternalResult result = new InternalResult();

            //ECS fin to Retail
            //IMTF to Retail
            if ((retailChannelID == Model.VendorProcessStatus.channelID && Model.SignOnRq.ChannelId == ECSCannel) ||
                (Model.SignOnRq.ChannelId == IMTFCannel &&  Model.VendorProcessStatus.statusType.ToUpper() == "NAMECHECK" && Model.VendorProcessStatus.provCode.ToUpper() == "ORG_GLOBAL/ORG_AE/AE_RETAIL/CUS"))
            {
                result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                            _config["Retail:BaseUrl"].ToString(),
                                                                            _config["Retail:StatusUpdate"].ToString(),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                               this,
                                                                            _config,
                                                                            httpHandler, 5);

            }
            //ECS fin to Corporate
            //IMTF to Corporate
            else if ((corporateChannelID.Contains(Model.VendorProcessStatus.channelID)  && Model.SignOnRq.ChannelId == ECSCannel) ||
               (Model.SignOnRq.ChannelId == IMTFCannel && Model.VendorProcessStatus.statusType.ToUpper() == "NAMECHECK" && Model.VendorProcessStatus.provCode.ToUpper() == "ORG_GLOBAL/ORG_AE/AE_CORP/CUS"))
            {
                result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.VendorProcessStatus,
                                                                            _config["ERCCorporate:BaseUrl"].ToString(),
                                                                            _config["ERCCorporate:UpdatePaymentStatus"].ToString(),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                               this,
                                                                            _config,
                                                                            httpHandler, 4);
            }
            //IMTF to ECSFIN
            else if (Model.SignOnRq.ChannelId == IMTFCannel && Model.VendorProcessStatus.statusType.ToUpper() == "PAYMENTS")
            {
                ECSFinHandler.updateComplianceStatus.updateComplianceStatusModel IMTFModel = new ECSFinHandler.updateComplianceStatus.updateComplianceStatusModel();
                IMTFModel.SignOnRq = Model.SignOnRq;
                IMTFModel.RootObject = new ECSFinHandler.updateComplianceStatus.RootObject();
                IMTFModel.RootObject.dataHeader = new ECSFinHandler.updateComplianceStatus.DataHeader();
                IMTFModel.RootObject.dataHeader.uniqueId = Guid.NewGuid().ToString();
                IMTFModel.RootObject.dataHeader.timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")+ ".003";// "2019-09-15 05:45:28.003";
                IMTFModel.RootObject.data = new ECSFinHandler.updateComplianceStatus.Data();
                IMTFModel.RootObject.data.date = DateTime.Now.ToString("yyyy-MM-dd"); //"2019-09-15";
                IMTFModel.RootObject.data.reference = Model.VendorProcessStatus.transReference;
                IMTFModel.RootObject.data.status = Model.VendorProcessStatus.statusCode;
                IMTFModel.RootObject.data.comment = Model.VendorProcessStatus.statusDesc;

                result = await APIRequestHandler.GetResponse(Model == null ? null : IMTFModel.SignOnRq, IMTFModel.RootObject,
                                                                            _config["ECSFin:BaseUrl"].ToString(),
                                                                            _config["ECSFin:updateComplianceStatus"].ToString(),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                               this,
                                                                            _config,
                                                                            httpHandler, 2,3);
            }
            else if( Model.SignOnRq.ChannelId == ECSCannel && Model.VendorProcessStatus.channelID == WPSPortal)
            {
                NestHandler.Update_NonMOLStatus.Update_NonMOLStatusModel request = new NestHandler.Update_NonMOLStatus.Update_NonMOLStatusModel();
                request.RootObject = new NestHandler.Update_NonMOLStatus.RootObject();
                request.SignOnRq = Model.SignOnRq;
                request.RootObject.OrgReferenceNo = Model.VendorProcessStatus.transReference;
                request.RootObject.ReferenceID = Guid.NewGuid().ToString();
                request.RootObject.Status = Model.VendorProcessStatus.provStatusCode;
                request.RootObject.Status_Description = Model.VendorProcessStatus.provStatusDesc;
                return Update_NonMOLStatus(request).Result;
            }

            else
            {
                throw new Exception("Invalid request.");
            }

            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
            response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.httpResult == null || result.httpResult.severity == "failure")
            {
                if (retailChannelID == Model.VendorProcessStatus.channelID)
                {
                    ERCCorporateHandler.StatusUpdateResponseRetail.RootObject res = JsonConvert.DeserializeObject<ERCCorporateHandler.StatusUpdateResponseRetail.RootObject>(result.httpResult.httpResponse);
                    if (res == null)
                    {
                        response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = result.httpResult.severity.ToString() };
                    }
                    else
                    {
                        response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = res.response.message };
                    }
                }
                else
                {
                    var data = JsonConvert.DeserializeObject<ResponseObject>(result.httpResult.httpResponse);
                    if (data == null)
                    {
                        response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = result.httpResult.severity.ToString() };
                    }
                    else
                    {
                        response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.Message };
                    }
                }
            }
            else
            {
                if (retailChannelID == Model.VendorProcessStatus.channelID)
                {
                    ERCCorporateHandler.StatusUpdateResponseRetail.RootObject res = JsonConvert.DeserializeObject<ERCCorporateHandler.StatusUpdateResponseRetail.RootObject>(result.httpResult.httpResponse);
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                    response.Content = res.response.content;
                }
                else if (Model.SignOnRq.ChannelId == IMTFCannel && Model.VendorProcessStatus.statusType.ToUpper() == "PAYMENTS")
                {
                    var data = JsonConvert.DeserializeObject<ECSFinHandler.getCustomerSIList.RootObject>(result.httpResult.httpResponse);
                    ResponseObject r = new ResponseObject() { Code = data.statusCode, Message = data.statusComment };
                    if (r.Code == "0")
                    {
                        response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                    }
                    else
                    {
                        response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = Severity .Error.ToString()};
                    }
                    response.Content = r;
                }
                else
                {
                    var data = JsonConvert.DeserializeObject<ResponseObject>(result.httpResult.httpResponse);
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                    response.Content = data;
                }
              
                return Ok(response);
            }
      
            return BadRequest(response);
        }

        [Route("ExchangeRatesForAllCurrencies")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.ExchangeRatesForAllCurrencies.RootObject>", typeof(ActiveResponseSucces<ECSFinHandler.ExchangeRatesForAllCurrencies.RootObject>))]
        public async Task<IActionResult> ExchangeRatesForAllCurrencies([FromBody] CommonRequest Model)
        {
            ActiveResponseSucces<ECSFinHandler.ExchangeRatesForAllCurrencies.RootObject> response = new ActiveResponseSucces<ECSFinHandler.ExchangeRatesForAllCurrencies.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.Param,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:ExchangeRatesForAllCurrencies"].ToString().Replace("{baseCur}", Model.Param).Replace("{type}", Model.type),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 2, 2);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.ExchangeRatesForAllCurrencies.RootObject>(result.httpResult.httpResponse);

                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }


        [Route("DebitAuthorizationStatus")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.DebitAuthorizationStatus.ResponseRootObject>", typeof(ActiveResponseSucces<ECSFinHandler.DebitAuthorizationStatus.ResponseRootObject>))]
        public async Task<IActionResult> DebitAuthorizationStatus([FromBody] ECSFinHandler.DebitAuthorizationStatus.DebitAuthorizationStatusModel Model)
        {
            ActiveResponseSucces<ECSFinHandler.DebitAuthorizationStatus.ResponseRootObject> response = new ActiveResponseSucces<ECSFinHandler.DebitAuthorizationStatus.ResponseRootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.RootObject,
                                                                        _config["ECSFin:BaseUrl"].ToString(),
                                                                        _config["ECSFin:DebitAuthorizationStatus"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2, 3);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<ECSFinHandler.DebitAuthorizationStatus.ResponseRootObject>(result.httpResult.httpResponse);
                if (data != null && data.statusCode != null && data.statusCode == "1")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = data.statusComment };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("INTERFT_OUTBOUND")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>", typeof(ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>))]
        public async Task<IActionResult> INTERFT_OUTBOUND([FromBody] ECSFinHandler.INTER_FT_AED_AED_Outbound.INTER_FT_AED_AED_OutboundModel Model)
        {
            IMTFHandler.INTER_FT_AED_AED_Outbound.RootObject mainOBJ = new IMTFHandler.INTER_FT_AED_AED_Outbound.RootObject();
            mainOBJ.RequestMetaData = new IMTFHandler.INTER_FT_AED_AED_Outbound.RequestMetaData();
            mainOBJ.RequestMetaData.type = Model.RootObject.actionType;
            mainOBJ.RequestMetaData.sendTime = DateTime.Now.ToString();
            mainOBJ.RequestMetaData.timeZone = "4";
            //TimeZone.CurrentTimeZone.GetUtcOffset(new DateTime()).ToString();
            mainOBJ.RequestMetaData.userId = "SYSTEM";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestMetaData.requestUid = Guid.NewGuid().ToString();
            mainOBJ.RequestMetaData.orgUnit = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP" : "ORG_GLOBAL/ORG_AE/AE_RETAIL";
            mainOBJ.RequestMetaData.branchCompanyId = "784100";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestMetaData.branchLocationId = "784101";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestMetaData.systemId = "CIF";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestMetaData.serviceId = Model.RootObject.actionType;

            mainOBJ.RequestDataObject = new IMTFHandler.INTER_FT_AED_AED_Outbound.RequestDataObject();
            mainOBJ.RequestDataObject.DoiTAccountTransaction = new IMTFHandler.INTER_FT_AED_AED_Outbound.DoiTAccountTransaction();
            mainOBJ.RequestDataObject.DoiTAccountTransaction.entityType = "2001";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestDataObject.DoiTAccountTransaction.originId = Model.RootObject.transactionReference;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/TRX" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/TRX";
            if (Model.RootObject.sourceType == "INTRA-SC" || Model.RootObject.sourceType == "INTRA-DC")
            {
                mainOBJ.RequestDataObject.DoiTAccountTransaction.originalAccountFrom = Model.RootObject.sourceAccount;
            }
            else if ((Model.RootObject.sourceType == "INTER-FT" || Model.RootObject.sourceType == "INTER-SW" || Model.RootObject.sourceType == "INTER-OTH") && Model.RootObject.destinationType.ToUpper() == "OUTGOING")
            {
                mainOBJ.RequestDataObject.DoiTAccountTransaction.originalAccountFrom = Model.RootObject.sourceAccount;
                mainOBJ.RequestDataObject.DoiTAccountTransaction.originalCtptyAcc2 = Model.RootObject.destinationAccount;
            }

            mainOBJ.RequestDataObject.DoiTAccountTransaction.transactionDirection = "C";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestDataObject.DoiTAccountTransaction.transactionType = "OUT";
            mainOBJ.RequestDataObject.DoiTAccountTransaction.transactionPurpose = "CQP002";
            mainOBJ.RequestDataObject.DoiTAccountTransaction.transactionDtg = Model.RootObject.transactionDate;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.transactionTz = "4";
            mainOBJ.RequestDataObject.DoiTAccountTransaction.baseAmount = Model.RootObject.accountAmount;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.baseCurrency = Model.RootObject.accountCurrency;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.baseCurrency = Model.RootObject.accountCurrency;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.localAmount = Model.RootObject.aedAmount;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.localCurrency = "AED";
            mainOBJ.RequestDataObject.DoiTAccountTransaction.originalAmount = Model.RootObject.transactionAmount;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.originalCurrency = Model.RootObject.transactionCurrency;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.localCommission = Model.RootObject.commission;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.localFees = Model.RootObject.fees;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.originalInstructingBankBranch = Model.RootObject.transactionRouting.instructingBank.branch;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.originalInstructingBranchCity = Model.RootObject.transactionRouting.instructingBank.city;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.uidInstructingBank = Model.RootObject.transactionRouting.instructingBank.uid;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.instructingBankCountry = Model.RootObject.transactionRouting.instructingBank.country;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.uidAccountWithInstitution = Model.RootObject.transactionRouting.receiverBank.uid;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.accountWithInstitutionCountry = Model.RootObject.transactionRouting.receiverBank.country;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.localFees = Model.RootObject.fees;

            mainOBJ.RequestDataObject.DoiPerson = new List<IMTFHandler.INTER_FT_AED_AED_Outbound.DoiPerson>();

            IMTFHandler.INTER_FT_AED_AED_Outbound.DoiPerson doIPersonSource = new IMTFHandler.INTER_FT_AED_AED_Outbound.DoiPerson();
            doIPersonSource.DoiAccountEntity = new List<IMTFHandler.INTER_FT_AED_AED_Outbound.DoiAccountEntity>();
            doIPersonSource.entityType = "3001";
            doIPersonSource.originId = Model.RootObject.cif;
            doIPersonSource.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CUS" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CUS";
            IMTFHandler.INTER_FT_AED_AED_Outbound.DoiAccountEntity doIAccoutSourcet = new IMTFHandler.INTER_FT_AED_AED_Outbound.DoiAccountEntity();
            doIAccoutSourcet.entityType = "4";
            doIAccoutSourcet.originId = Model.RootObject.sourceAccount;
            doIAccoutSourcet.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CUS_ACC" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CUS_ACC";
            doIPersonSource.DoiAccountEntity.Add(doIAccoutSourcet);
            mainOBJ.RequestDataObject.DoiPerson.Add(doIPersonSource);

            IMTFHandler.INTER_FT_AED_AED_Outbound.DoiPerson doIPersonDestination= new IMTFHandler.INTER_FT_AED_AED_Outbound.DoiPerson();
            doIPersonDestination.DoiAccountEntity = new List<IMTFHandler.INTER_FT_AED_AED_Outbound.DoiAccountEntity>();
            doIPersonDestination.entityType = "3001";
            doIPersonDestination.originId = Model.RootObject.destinationAccount;
            doIPersonDestination.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CTPTY" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CTPTY";
            doIPersonDestination.fullName = Model.RootObject.beneficiaryDetails.fullName;

            doIPersonDestination.DoiPersonPhysChar  = new List<IMTFHandler.INTER_FT_AED_AED_Outbound.DoiPersonPhysChar>();
            IMTFHandler.INTER_FT_AED_AED_Outbound.DoiPersonPhysChar DoiPersonPhysCharDestination = new IMTFHandler.INTER_FT_AED_AED_Outbound.DoiPersonPhysChar();
            DoiPersonPhysCharDestination.originId = Model.RootObject.destinationAccount;
            DoiPersonPhysCharDestination.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CTPTY" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CTPTY";
            DoiPersonPhysCharDestination.nationality = Model.RootObject.beneficiaryDetails.countryCode;
            doIPersonDestination.DoiPersonPhysChar.Add(DoiPersonPhysCharDestination);


            doIPersonDestination.DoiLocation = new List<IMTFHandler.INTER_FT_AED_AED_Outbound.DoiLocation>();
            IMTFHandler.INTER_FT_AED_AED_Outbound.DoiLocation DoiLocationDestination = new IMTFHandler.INTER_FT_AED_AED_Outbound.DoiLocation();
            DoiLocationDestination.entityType = "21";
            DoiLocationDestination.originId = Model.RootObject.destinationAccount;
            DoiLocationDestination.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CTPTY_ADR" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CTPTY_ADR";
            DoiLocationDestination.fullStreetAddress = Model.RootObject.beneficiaryDetails.fullAddress;
            DoiLocationDestination.country = Model.RootObject.beneficiaryDetails.addressCountry;
            doIPersonDestination.DoiLocation.Add(DoiLocationDestination);

            IMTFHandler.INTER_FT_AED_AED_Outbound.DoiAccountEntity doIAccountDestination = new IMTFHandler.INTER_FT_AED_AED_Outbound.DoiAccountEntity();
            doIAccountDestination.entityType = "4";
            doIAccountDestination.originId = Model.RootObject.destinationAccount;
            doIAccountDestination.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CTPTY_ACC" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CTPTY_ACC";
            doIAccountDestination.accountNumber = Model.RootObject.destinationAccount;
            doIAccountDestination.bic = Model.RootObject.beneficiaryDetails.bic;
            doIAccountDestination.bankIban = Model.RootObject.beneficiaryDetails.bankIban;
            doIAccountDestination.bankName = Model.RootObject.beneficiaryDetails.bankName;
            doIAccountDestination.branchName = Model.RootObject.beneficiaryDetails.branchName;
            doIAccountDestination.branchCity = Model.RootObject.beneficiaryDetails.branchCity;
            doIAccountDestination.branchCountry = Model.RootObject.beneficiaryDetails.branchCountry;
            doIPersonDestination.DoiAccountEntity.Add(doIAccountDestination);
            mainOBJ.RequestDataObject.DoiPerson.Add(doIPersonDestination);


            var body = JsonConvert.SerializeObject(mainOBJ).Trim();
            body = body.Replace(",\"fullName\":null,", "");
            body = body.Replace("\"accountNumber\":null,","");
            body = body.Replace("\"bic\":null,", "");
            body = body.Replace("\"bankIban\":null,", "");
            body = body.Replace("\"bankName\":null,", "");
            body = body.Replace("\"branchName\":null,", "");
            body = body.Replace("\"branchCity\":null,", "");
            body = body.Replace("\branchCountry\":null,", "");
            body = body.Replace("\"DoiPersonPhysChar\":null,", "");
            body = body.Replace("\"DoiLocation\":null", "");
            body = body.Replace(",\"branchCountry\":null", "");

            ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject> response = new ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, mainOBJ,
                                                                        _config["IMTF:BaseUrl"].ToString(),
                                                                        _config["IMTF:transaction"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler,
                                                                        3,1,body);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<IMTFHandler.CommonResponse.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.responseDataObject.responseCode != null && data.responseDataObject.responseCode == "SUC")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);

                }
                else
                {
                    if (data == null)
                    {
                        response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = "FAIL" };
                    }
                    else
                    {
                        response.Status = new Status { Severity = Severity.Error, Code = data.responseDataObject.responseCode, StatusMessage = data.responseDataObject.responseMessage };
                    }
                }
            }
            return BadRequest(response);
        }

        [Route("INTERFT_INBOUND")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>", typeof(ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>))]
        public async Task<IActionResult> INTERFT_INBOUND([FromBody] ECSFinHandler.INTER_FT_AED_AED_Outbound.INTER_FT_AED_AED_OutboundModel Model)
        {
            IMTFHandler.INTERFT_INBOUND.RootObject mainOBJ = new IMTFHandler.INTERFT_INBOUND.RootObject();
            mainOBJ.RequestMetaData = new IMTFHandler.INTERFT_INBOUND.RequestMetaData();
            mainOBJ.RequestMetaData.type = Model.RootObject.actionType;
            mainOBJ.RequestMetaData.sendTime = DateTime.Now.ToString();
            mainOBJ.RequestMetaData.timeZone = "4";
            //TimeZone.CurrentTimeZone.GetUtcOffset(new DateTime()).ToString();
            mainOBJ.RequestMetaData.userId = "SYSTEM";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestMetaData.requestUid = Guid.NewGuid().ToString();
            mainOBJ.RequestMetaData.orgUnit = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP" : "ORG_GLOBAL/ORG_AE/AE_RETAIL";
            mainOBJ.RequestMetaData.branchCompanyId = "784100";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestMetaData.branchLocationId = "784101";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestMetaData.systemId = "CIF";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestMetaData.serviceId = Model.RootObject.actionType;

            mainOBJ.RequestDataObject = new IMTFHandler.INTERFT_INBOUND.RequestDataObject();
            mainOBJ.RequestDataObject.DoiTAccountTransaction = new IMTFHandler.INTERFT_INBOUND.DoiTAccountTransaction();
            mainOBJ.RequestDataObject.DoiTAccountTransaction.entityType = "2001";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestDataObject.DoiTAccountTransaction.originId = Model.RootObject.transactionReference;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/TRX" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/TRX";
            mainOBJ.RequestDataObject.DoiTAccountTransaction.originalAccountTo = Model.RootObject.destinationAccount;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.originalCtptyAcc1 = Model.RootObject.sourceAccount;

            mainOBJ.RequestDataObject.DoiTAccountTransaction.transactionDirection = "C";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestDataObject.DoiTAccountTransaction.transactionType = "IN";
            mainOBJ.RequestDataObject.DoiTAccountTransaction.transactionPurpose = "CQP002";
            mainOBJ.RequestDataObject.DoiTAccountTransaction.transactionDtg = Model.RootObject.transactionDate;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.transactionTz = "4";
            mainOBJ.RequestDataObject.DoiTAccountTransaction.baseAmount = Model.RootObject.accountAmount;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.baseCurrency = Model.RootObject.accountCurrency;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.localAmount = Model.RootObject.aedAmount;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.localCurrency = "AED";
            mainOBJ.RequestDataObject.DoiTAccountTransaction.originalAmount = Model.RootObject.transactionAmount;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.originalCurrency = Model.RootObject.transactionCurrency;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.localCommission = Model.RootObject.commission;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.localFees = Model.RootObject.fees;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.originalInstructingBankBranch = Model.RootObject.transactionRouting.instructingBank.branch;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.originalInstructingBranchCity = Model.RootObject.transactionRouting.instructingBank.city;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.uidInstructingBank = Model.RootObject.transactionRouting.instructingBank.uid;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.instructingBankCountry = Model.RootObject.transactionRouting.instructingBank.country;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.uidAccountWithInstitution = Model.RootObject.transactionRouting.receiverBank.uid;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.accountWithInstitutionCountry = Model.RootObject.transactionRouting.receiverBank.country;

            mainOBJ.RequestDataObject.DoiPerson = new List<IMTFHandler.INTERFT_INBOUND.DoiPerson>();

            IMTFHandler.INTERFT_INBOUND.DoiPerson doIPersonSource = new IMTFHandler.INTERFT_INBOUND.DoiPerson();
            doIPersonSource.DoiAccountEntity = new List<IMTFHandler.INTERFT_INBOUND.DoiAccountEntity>();
            doIPersonSource.entityType = "3001";
            doIPersonSource.originId = Model.RootObject.cif;
            doIPersonSource.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CUS" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CUS";
            IMTFHandler.INTERFT_INBOUND.DoiAccountEntity doIAccoutSourcet = new IMTFHandler.INTERFT_INBOUND.DoiAccountEntity();
            doIAccoutSourcet.entityType = "4";
            doIAccoutSourcet.originId = Model.RootObject.destinationAccount;
            doIAccoutSourcet.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CUS_ACC" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CUS_ACC";
            doIPersonSource.DoiAccountEntity.Add(doIAccoutSourcet);
            mainOBJ.RequestDataObject.DoiPerson.Add(doIPersonSource);

            IMTFHandler.INTERFT_INBOUND.DoiPerson doIPersonDestination = new IMTFHandler.INTERFT_INBOUND.DoiPerson();
            doIPersonDestination.DoiAccountEntity = new List<IMTFHandler.INTERFT_INBOUND.DoiAccountEntity>();
            doIPersonDestination.entityType = "3001";
            doIPersonDestination.originId = Model.RootObject.sourceAccount;
            doIPersonDestination.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CTPTY" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CTPTY";
            doIPersonDestination.fullName = Model.RootObject.beneficiaryDetails.fullName;

            doIPersonDestination.DoiPersonPhysChar = new List<IMTFHandler.INTERFT_INBOUND.DoiPersonPhysChar>();
            IMTFHandler.INTERFT_INBOUND.DoiPersonPhysChar DoiPersonPhysCharDestination = new IMTFHandler.INTERFT_INBOUND.DoiPersonPhysChar();
            DoiPersonPhysCharDestination.originId = Model.RootObject.sourceAccount;
            DoiPersonPhysCharDestination.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CTPTY" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CTPTY";
            DoiPersonPhysCharDestination.nationality = Model.RootObject.beneficiaryDetails.countryCode;
            doIPersonDestination.DoiPersonPhysChar.Add(DoiPersonPhysCharDestination);


            doIPersonDestination.DoiLocation = new List<IMTFHandler.INTERFT_INBOUND.DoiLocation>();
            IMTFHandler.INTERFT_INBOUND.DoiLocation DoiLocationDestination = new IMTFHandler.INTERFT_INBOUND.DoiLocation();
            DoiLocationDestination.entityType = "21";
            DoiLocationDestination.originId = Model.RootObject.sourceAccount;
            DoiLocationDestination.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CTPTY_ADR" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CTPTY_ADR";
            DoiLocationDestination.fullStreetAddress = Model.RootObject.beneficiaryDetails.fullAddress;
            DoiLocationDestination.country = Model.RootObject.beneficiaryDetails.addressCountry;
            doIPersonDestination.DoiLocation.Add(DoiLocationDestination);

            IMTFHandler.INTERFT_INBOUND.DoiAccountEntity doIAccountDestination = new IMTFHandler.INTERFT_INBOUND.DoiAccountEntity();
            doIAccountDestination.entityType = "4";
            doIAccountDestination.originId = Model.RootObject.sourceAccount;
            doIAccountDestination.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CTPTY_ACC" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CTPTY_ACC";
            doIAccountDestination.accountNumber = Model.RootObject.sourceAccount;
            doIAccountDestination.bic = Model.RootObject.beneficiaryDetails.bic;
            doIAccountDestination.bankIban = Model.RootObject.beneficiaryDetails.bankIban;
            doIAccountDestination.bankName = Model.RootObject.beneficiaryDetails.bankName;
            doIAccountDestination.branchName = Model.RootObject.beneficiaryDetails.branchName;
            doIAccountDestination.branchCity = Model.RootObject.beneficiaryDetails.branchCity;
            doIAccountDestination.branchCountry = Model.RootObject.beneficiaryDetails.branchCountry;
            doIPersonDestination.DoiAccountEntity.Add(doIAccountDestination);
            mainOBJ.RequestDataObject.DoiPerson.Add(doIPersonDestination);

            var body = JsonConvert.SerializeObject(mainOBJ).Trim();

            body = body.Replace(",\"fullName\":null,", "");
            body = body.Replace("\"accountNumber\":null,", "");
            body = body.Replace("\"bic\":null,", "");
            body = body.Replace("\"bankIban\":null,", "");
            body = body.Replace("\"bankName\":null,", "");
            body = body.Replace("\"branchName\":null,", "");
            body = body.Replace("\"branchCity\":null,", "");
            body = body.Replace("\branchCountry\":null,", "");
            body = body.Replace("\"DoiPersonPhysChar\":null,", "");
            body = body.Replace("\"DoiLocation\":null", "");
            body = body.Replace(",\"branchCountry\":null", "");

            ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject> response = new ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, mainOBJ,
                                                                        _config["IMTF:BaseUrl"].ToString(),
                                                                        _config["IMTF:transaction"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler,
                                                                        3, 1, body);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<IMTFHandler.CommonResponse.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.responseDataObject.responseCode != null && data.responseDataObject.responseCode == "SUC")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                    response.Content = data;
                    return Ok(response);
                }
                else
                {
                    if (data == null)
                    {
                        response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = "FAIL" };
                    }
                    else
                    {
                        response.Status = new Status { Severity = Severity.Error, Code = data.responseDataObject.responseCode, StatusMessage = data.responseDataObject.responseMessage };
                    }
                }
            }
            return BadRequest(response);
        }


        //[Route("INTERFT_OUTBOUND")]
        [Route("INTERSW_OUTBOUND")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>", typeof(ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>))]
        public async Task<IActionResult> INTERSW_OUTBOUND([FromBody] ECSFinHandler.INTER_FT_AED_AED_Outbound.INTER_FT_AED_AED_OutboundModel Model)
        {
            IMTFHandler.INTER_FT_AED_AED_Outbound.RootObject mainOBJ = new IMTFHandler.INTER_FT_AED_AED_Outbound.RootObject();
            mainOBJ.RequestMetaData = new IMTFHandler.INTER_FT_AED_AED_Outbound.RequestMetaData();
            mainOBJ.RequestMetaData.type = Model.RootObject.actionType;
            mainOBJ.RequestMetaData.sendTime = DateTime.Now.ToString();
            mainOBJ.RequestMetaData.timeZone = "4";
            //TimeZone.CurrentTimeZone.GetUtcOffset(new DateTime()).ToString();
            mainOBJ.RequestMetaData.userId = "SYSTEM";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestMetaData.requestUid = Guid.NewGuid().ToString();
            mainOBJ.RequestMetaData.orgUnit = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP" : "ORG_GLOBAL/ORG_AE/AE_RETAIL";
            mainOBJ.RequestMetaData.branchCompanyId = "784100";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestMetaData.branchLocationId = "784101";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestMetaData.systemId = "CIF";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestMetaData.serviceId = Model.RootObject.actionType;

            mainOBJ.RequestDataObject = new IMTFHandler.INTER_FT_AED_AED_Outbound.RequestDataObject();
            mainOBJ.RequestDataObject.DoiTAccountTransaction = new IMTFHandler.INTER_FT_AED_AED_Outbound.DoiTAccountTransaction();
            mainOBJ.RequestDataObject.DoiTAccountTransaction.entityType = "2001";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestDataObject.DoiTAccountTransaction.originId = Model.RootObject.transactionReference;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/TRX" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/TRX";

            mainOBJ.RequestDataObject.DoiTAccountTransaction.originalAccountFrom = Model.RootObject.sourceAccount;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.originalCtptyAcc2 = Model.RootObject.destinationAccount;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.relatedReference = Model.RootObject.relatedReference;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.transactionDirection = "C";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestDataObject.DoiTAccountTransaction.transactionType = "OUT";
            mainOBJ.RequestDataObject.DoiTAccountTransaction.transactionPurpose = "CQP002";
            mainOBJ.RequestDataObject.DoiTAccountTransaction.transactionDtg = Model.RootObject.transactionDate;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.transactionTz = "4";
            mainOBJ.RequestDataObject.DoiTAccountTransaction.baseAmount = Model.RootObject.accountAmount;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.baseCurrency = Model.RootObject.accountCurrency;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.localAmount = Model.RootObject.aedAmount;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.localCurrency = "AED";
            mainOBJ.RequestDataObject.DoiTAccountTransaction.originalAmount = Model.RootObject.transactionAmount;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.originalCurrency = Model.RootObject.transactionCurrency;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.localCommission = Model.RootObject.commission;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.localFees = Model.RootObject.fees;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.originalInstructingBankBranch = Model.RootObject.transactionRouting.instructingBank.branch;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.originalInstructingBranchCity = Model.RootObject.transactionRouting.instructingBank.city;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.uidInstructingBank = Model.RootObject.transactionRouting.instructingBank.uid;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.instructingBankCountry = Model.RootObject.transactionRouting.instructingBank.uid;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.senderCorrespBankCountry = Model.RootObject.transactionRouting.senderCorrespondantBank.uid;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.uidSenderCorrespBank = Model.RootObject.transactionRouting.senderCorrespondantBank.country;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.uidIntermediaryBank = Model.RootObject.transactionRouting.intermediaryBank.uid;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.intermediaryBankCountry = Model.RootObject.transactionRouting.intermediaryBank.country;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.uidReceiverCorrespBank = Model.RootObject.transactionRouting.receiverBank.uid;

            mainOBJ.RequestDataObject.DoiTAccountTransaction.receiverCorrespBankCountry = Model.RootObject.transactionRouting.receiverBank.country;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.receiverCorrespBankCountry = Model.RootObject.transactionRouting.receiverBank.country;

            mainOBJ.RequestDataObject.DoiTAccountTransaction.uidAccountWithInstitution = Model.RootObject.transactionRouting.receiverBank.uid;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.accountWithInstitutionCountry = Model.RootObject.transactionRouting.receiverBank.country;

            mainOBJ.RequestDataObject.DoiPerson = new List<IMTFHandler.INTER_FT_AED_AED_Outbound.DoiPerson>();

            IMTFHandler.INTER_FT_AED_AED_Outbound.DoiPerson doIPersonSource = new IMTFHandler.INTER_FT_AED_AED_Outbound.DoiPerson();
            doIPersonSource.DoiAccountEntity = new List<IMTFHandler.INTER_FT_AED_AED_Outbound.DoiAccountEntity>();
            doIPersonSource.entityType = "3001";
            doIPersonSource.originId = Model.RootObject.cif;
            doIPersonSource.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CUS" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CUS";
            IMTFHandler.INTER_FT_AED_AED_Outbound.DoiAccountEntity doIAccoutSourcet = new IMTFHandler.INTER_FT_AED_AED_Outbound.DoiAccountEntity();
            doIAccoutSourcet.entityType = "4";
            doIAccoutSourcet.originId = Model.RootObject.sourceAccount;
            doIAccoutSourcet.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CUS_ACC" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CUS_ACC";
            doIPersonSource.DoiAccountEntity.Add(doIAccoutSourcet);
            mainOBJ.RequestDataObject.DoiPerson.Add(doIPersonSource);

            IMTFHandler.INTER_FT_AED_AED_Outbound.DoiPerson doIPersonDestination = new IMTFHandler.INTER_FT_AED_AED_Outbound.DoiPerson();
            doIPersonDestination.DoiAccountEntity = new List<IMTFHandler.INTER_FT_AED_AED_Outbound.DoiAccountEntity>();
            doIPersonDestination.entityType = "3001";
            doIPersonDestination.originId = Model.RootObject.destinationAccount;
            doIPersonDestination.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CTPTY" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CTPTY";
            doIPersonDestination.fullName = Model.RootObject.beneficiaryDetails.fullName;

            doIPersonDestination.DoiPersonPhysChar = new List<IMTFHandler.INTER_FT_AED_AED_Outbound.DoiPersonPhysChar>();
            IMTFHandler.INTER_FT_AED_AED_Outbound.DoiPersonPhysChar DoiPersonPhysCharDestination = new IMTFHandler.INTER_FT_AED_AED_Outbound.DoiPersonPhysChar();
            DoiPersonPhysCharDestination.originId = Model.RootObject.destinationAccount;
            DoiPersonPhysCharDestination.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CTPTY" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CTPTY";
            DoiPersonPhysCharDestination.nationality = Model.RootObject.beneficiaryDetails.countryCode;
            doIPersonDestination.DoiPersonPhysChar.Add(DoiPersonPhysCharDestination);


            doIPersonDestination.DoiLocation = new List<IMTFHandler.INTER_FT_AED_AED_Outbound.DoiLocation>();
            IMTFHandler.INTER_FT_AED_AED_Outbound.DoiLocation DoiLocationDestination = new IMTFHandler.INTER_FT_AED_AED_Outbound.DoiLocation();
            DoiLocationDestination.entityType = "21";
            DoiLocationDestination.originId = Model.RootObject.destinationAccount;
            DoiLocationDestination.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CTPTY_ADR" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CTPTY_ADR";
            DoiLocationDestination.fullStreetAddress = Model.RootObject.beneficiaryDetails.fullAddress;
            DoiLocationDestination.country = Model.RootObject.beneficiaryDetails.addressCountry;
            doIPersonDestination.DoiLocation.Add(DoiLocationDestination);

            IMTFHandler.INTER_FT_AED_AED_Outbound.DoiAccountEntity doIAccountDestination = new IMTFHandler.INTER_FT_AED_AED_Outbound.DoiAccountEntity();
            doIAccountDestination.entityType = "4";
            doIAccountDestination.originId = Model.RootObject.destinationAccount;
            doIAccountDestination.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CTPTY_ACC" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CTPTY_ACC";
            doIAccountDestination.accountNumber = Model.RootObject.destinationAccount;
            doIAccountDestination.bic = Model.RootObject.beneficiaryDetails.bic;
            doIAccountDestination.bankIban = Model.RootObject.beneficiaryDetails.bankIban;
            doIAccountDestination.bankName = Model.RootObject.beneficiaryDetails.bankName;
            doIAccountDestination.branchName = Model.RootObject.beneficiaryDetails.branchName;
            doIAccountDestination.branchCity = Model.RootObject.beneficiaryDetails.branchCity;
            doIAccountDestination.branchCountry = Model.RootObject.beneficiaryDetails.branchCountry;
            doIPersonDestination.DoiAccountEntity.Add(doIAccountDestination);
            mainOBJ.RequestDataObject.DoiPerson.Add(doIPersonDestination);


            var body = JsonConvert.SerializeObject(mainOBJ).Trim();
            body = body.Replace(",\"fullName\":null,", "");
            body = body.Replace("\"accountNumber\":null,", "");
            body = body.Replace("\"bic\":null,", "");
            body = body.Replace("\"bankIban\":null,", "");
            body = body.Replace("\"bankName\":null,", "");
            body = body.Replace("\"branchName\":null,", "");
            body = body.Replace("\"branchCity\":null,", "");
            body = body.Replace("\branchCountry\":null,", "");
            body = body.Replace("\"DoiPersonPhysChar\":null,", "");
            body = body.Replace("\"DoiLocation\":null", "");
            body = body.Replace(",\"branchCountry\":null", "");

            ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject> response = new ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, mainOBJ,
                                                                        _config["IMTF:BaseUrl"].ToString(),
                                                                        _config["IMTF:transaction"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler,
                                                                        3, 1, body);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<IMTFHandler.CommonResponse.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.responseDataObject.responseCode != null && data.responseDataObject.responseCode == "SUC")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);

                }
                else
                {
                    if (data == null)
                    {
                        response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = "FAIL" };
                    }
                    else
                    {
                        response.Status = new Status { Severity = Severity.Error, Code = data.responseDataObject.responseCode, StatusMessage = data.responseDataObject.responseMessage };
                    }
                }
            }
            return BadRequest(response);
        }

        //[Route("INTERFT_OUTBOUND")]
        [Route("INTERSW_INBOUND")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>", typeof(ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>))]
        public async Task<IActionResult> INTERSW_INBOUND([FromBody] ECSFinHandler.INTER_FT_AED_AED_Outbound.INTER_FT_AED_AED_OutboundModel Model)
        {
            IMTFHandler.INTER_FT_AED_AED_Outbound.RootObject mainOBJ = new IMTFHandler.INTER_FT_AED_AED_Outbound.RootObject();
            mainOBJ.RequestMetaData = new IMTFHandler.INTER_FT_AED_AED_Outbound.RequestMetaData();
            mainOBJ.RequestMetaData.type = Model.RootObject.actionType;
            mainOBJ.RequestMetaData.sendTime = DateTime.Now.ToString();
            mainOBJ.RequestMetaData.timeZone = "4";
            //TimeZone.CurrentTimeZone.GetUtcOffset(new DateTime()).ToString();
            mainOBJ.RequestMetaData.userId = "SYSTEM";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestMetaData.requestUid = Guid.NewGuid().ToString();
            mainOBJ.RequestMetaData.orgUnit = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP" : "ORG_GLOBAL/ORG_AE/AE_RETAIL";
            mainOBJ.RequestMetaData.branchCompanyId = "784100";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestMetaData.branchLocationId = "784101";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestMetaData.systemId = "CIF";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestMetaData.serviceId = Model.RootObject.actionType;

            mainOBJ.RequestDataObject = new IMTFHandler.INTER_FT_AED_AED_Outbound.RequestDataObject();
            mainOBJ.RequestDataObject.DoiTAccountTransaction = new IMTFHandler.INTER_FT_AED_AED_Outbound.DoiTAccountTransaction();
            mainOBJ.RequestDataObject.DoiTAccountTransaction.entityType = "2001";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestDataObject.DoiTAccountTransaction.originId = Model.RootObject.correlationId;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/TRX" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/TRX";

            mainOBJ.RequestDataObject.DoiTAccountTransaction.originalAccountTo = Model.RootObject.destinationAccount;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.originalCtptyAcc1 = Model.RootObject.sourceAccount;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.relatedReference = Model.RootObject.relatedReference;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.transactionDirection = "C";//TODO: we need to  read this via soft configuration.
            mainOBJ.RequestDataObject.DoiTAccountTransaction.transactionReference = Model.RootObject.transactionReference;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.transactionType = "IN";
            mainOBJ.RequestDataObject.DoiTAccountTransaction.transactionPurpose = "CQP002";
            mainOBJ.RequestDataObject.DoiTAccountTransaction.transactionDtg = Model.RootObject.transactionDate;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.transactionTz = "4";
            mainOBJ.RequestDataObject.DoiTAccountTransaction.baseAmount = Model.RootObject.accountAmount;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.baseCurrency = Model.RootObject.accountCurrency;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.localAmount = Model.RootObject.aedAmount;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.localCurrency = "AED";
            mainOBJ.RequestDataObject.DoiTAccountTransaction.originalAmount = Model.RootObject.transactionAmount;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.originalCurrency = Model.RootObject.transactionCurrency;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.localCommission = Model.RootObject.commission;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.localFees = Model.RootObject.fees;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.originalInstructingBankBranch = Model.RootObject.transactionRouting.instructingBank.branch;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.originalInstructingBranchCity = Model.RootObject.transactionRouting.instructingBank.city;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.uidInstructingBank = Model.RootObject.transactionRouting.instructingBank.uid;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.instructingBankCountry = Model.RootObject.transactionRouting.instructingBank.uid;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.senderCorrespBankCountry = Model.RootObject.transactionRouting.senderCorrespondantBank.uid;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.uidSenderCorrespBank = Model.RootObject.transactionRouting.senderCorrespondantBank.country;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.uidIntermediaryBank = Model.RootObject.transactionRouting.intermediaryBank.uid;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.intermediaryBankCountry = Model.RootObject.transactionRouting.intermediaryBank.country;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.uidReceiverCorrespBank = Model.RootObject.transactionRouting.receiverBank.uid;

            mainOBJ.RequestDataObject.DoiTAccountTransaction.receiverCorrespBankCountry = Model.RootObject.transactionRouting.receiverBank.country;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.receiverCorrespBankCountry = Model.RootObject.transactionRouting.receiverBank.country;

            mainOBJ.RequestDataObject.DoiTAccountTransaction.uidAccountWithInstitution = Model.RootObject.transactionRouting.receiverBank.uid;
            mainOBJ.RequestDataObject.DoiTAccountTransaction.accountWithInstitutionCountry = Model.RootObject.transactionRouting.receiverBank.country;

            mainOBJ.RequestDataObject.DoiPerson = new List<IMTFHandler.INTER_FT_AED_AED_Outbound.DoiPerson>();

            IMTFHandler.INTER_FT_AED_AED_Outbound.DoiPerson doIPersonSource = new IMTFHandler.INTER_FT_AED_AED_Outbound.DoiPerson();
            doIPersonSource.DoiAccountEntity = new List<IMTFHandler.INTER_FT_AED_AED_Outbound.DoiAccountEntity>();
            doIPersonSource.entityType = "3001";
            doIPersonSource.originId = Model.RootObject.cif;
            doIPersonSource.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CUS" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CUS";
            IMTFHandler.INTER_FT_AED_AED_Outbound.DoiAccountEntity doIAccoutSourcet = new IMTFHandler.INTER_FT_AED_AED_Outbound.DoiAccountEntity();
            doIAccoutSourcet.entityType = "4";
            doIAccoutSourcet.originId = Model.RootObject.destinationAccount;
            doIAccoutSourcet.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CUS_ACC" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CUS_ACC";
            doIPersonSource.DoiAccountEntity.Add(doIAccoutSourcet);
            mainOBJ.RequestDataObject.DoiPerson.Add(doIPersonSource);

            IMTFHandler.INTER_FT_AED_AED_Outbound.DoiPerson doIPersonDestination = new IMTFHandler.INTER_FT_AED_AED_Outbound.DoiPerson();
            doIPersonDestination.DoiAccountEntity = new List<IMTFHandler.INTER_FT_AED_AED_Outbound.DoiAccountEntity>();
            doIPersonDestination.entityType = "3001";
            doIPersonDestination.originId = Model.RootObject.sourceAccount;
            doIPersonDestination.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CTPTY" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CTPTY";
            doIPersonDestination.fullName = Model.RootObject.beneficiaryDetails.fullName;

            doIPersonDestination.DoiPersonPhysChar = new List<IMTFHandler.INTER_FT_AED_AED_Outbound.DoiPersonPhysChar>();
            IMTFHandler.INTER_FT_AED_AED_Outbound.DoiPersonPhysChar DoiPersonPhysCharDestination = new IMTFHandler.INTER_FT_AED_AED_Outbound.DoiPersonPhysChar();
            DoiPersonPhysCharDestination.originId = Model.RootObject.sourceAccount;
            DoiPersonPhysCharDestination.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CTPTY" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CTPTY";
            DoiPersonPhysCharDestination.nationality = Model.RootObject.beneficiaryDetails.countryCode;
            doIPersonDestination.DoiPersonPhysChar.Add(DoiPersonPhysCharDestination);


            doIPersonDestination.DoiLocation = new List<IMTFHandler.INTER_FT_AED_AED_Outbound.DoiLocation>();
            IMTFHandler.INTER_FT_AED_AED_Outbound.DoiLocation DoiLocationDestination = new IMTFHandler.INTER_FT_AED_AED_Outbound.DoiLocation();
            DoiLocationDestination.entityType = "21";
            DoiLocationDestination.originId = Model.RootObject.sourceAccount;
            DoiLocationDestination.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CTPTY_ADR" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CTPTY_ADR";
            DoiLocationDestination.fullStreetAddress = Model.RootObject.beneficiaryDetails.fullAddress;
            DoiLocationDestination.country = Model.RootObject.beneficiaryDetails.addressCountry;
            doIPersonDestination.DoiLocation.Add(DoiLocationDestination);

            IMTFHandler.INTER_FT_AED_AED_Outbound.DoiAccountEntity doIAccountDestination = new IMTFHandler.INTER_FT_AED_AED_Outbound.DoiAccountEntity();
            doIAccountDestination.entityType = "4";
            doIAccountDestination.originId = Model.RootObject.sourceAccount;
            doIAccountDestination.listId = Model.RootObject.clientType.ToUpper() == "CORP" ? "ORG_GLOBAL/ORG_AE/AE_CORP/CTPTY_ACC" : "ORG_GLOBAL/ORG_AE/AE_RETAIL/CTPTY_ACC";
            doIAccountDestination.accountNumber = Model.RootObject.destinationAccount;
            doIAccountDestination.bic = Model.RootObject.beneficiaryDetails.bic;
            doIAccountDestination.bankIban = Model.RootObject.beneficiaryDetails.bankIban;
            doIAccountDestination.bankName = Model.RootObject.beneficiaryDetails.bankName;
            doIAccountDestination.branchName = Model.RootObject.beneficiaryDetails.branchName;
            doIAccountDestination.branchCity = Model.RootObject.beneficiaryDetails.branchCity;
            doIAccountDestination.branchCountry = Model.RootObject.beneficiaryDetails.branchCountry;
            doIPersonDestination.DoiAccountEntity.Add(doIAccountDestination);
            mainOBJ.RequestDataObject.DoiPerson.Add(doIPersonDestination);


            var body = JsonConvert.SerializeObject(mainOBJ).Trim();
            body = body.Replace(",\"fullName\":null,", "");
            body = body.Replace("\"accountNumber\":null,", "");
            body = body.Replace("\"bic\":null,", "");
            body = body.Replace("\"bankIban\":null,", "");
            body = body.Replace("\"bankName\":null,", "");
            body = body.Replace("\"branchName\":null,", "");
            body = body.Replace("\"branchCity\":null,", "");
            body = body.Replace("\branchCountry\":null,", "");
            body = body.Replace("\"DoiPersonPhysChar\":null,", "");
            body = body.Replace("\"DoiLocation\":null", "");
            body = body.Replace(",\"branchCountry\":null", "");

            ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject> response = new ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, mainOBJ,
                                                                        _config["IMTF:BaseUrl"].ToString(),
                                                                        _config["IMTF:transaction"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler,
                                                                        3, 1, body);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<IMTFHandler.CommonResponse.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.responseDataObject.responseCode != null && data.responseDataObject.responseCode == "SUC")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);

                }
                else
                {
                    if (data == null)
                    {
                        response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = "FAIL" };
                    }
                    else
                    {
                        response.Status = new Status { Severity = Severity.Error, Code = data.responseDataObject.responseCode, StatusMessage = data.responseDataObject.responseMessage };
                    }
                }
            }
            return BadRequest(response);
        }


        [Route("TransactionScreening")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>", typeof(ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>))]
        public async Task<IActionResult> TransactionScreening([FromBody] ECSFinHandler.INTER_FT_AED_AED_Outbound.INTER_FT_AED_AED_OutboundModel Model)
        {

            if ((Model.RootObject.sourceType == "INTRA-DC" || Model.RootObject.sourceType == "INTRA-SC") && Model.RootObject.destinationType == "OUTGOING")
            {
                return IMTFTransactionScreening(Model).Result;
            }
            else if ((Model.RootObject.sourceType == "INTER-FT") && Model.RootObject.destinationType == "OUTGOING")
            {
                return INTERFT_OUTBOUND(Model).Result;
            }
            else if ((Model.RootObject.sourceType == "INTER-FT") && Model.RootObject.destinationType == "INCOMING")
            {
                return INTERFT_INBOUND(Model).Result;
            }
            else if ((Model.RootObject.sourceType == "INTER-SW") && Model.RootObject.destinationType == "OUTGOING")
            {
                return INTERSW_OUTBOUND(Model).Result;
            }
            else if ((Model.RootObject.sourceType == "INTER-SW") && Model.RootObject.destinationType == "INCOMING")
            {
                return INTERSW_INBOUND(Model).Result;
            }

            return BadRequest();
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> Update_NonMOLStatus([FromBody] NestHandler.Update_NonMOLStatus.Update_NonMOLStatusModel Model)
        {
            ActiveResponseSucces<NestHandler.Update_NonMOLStatus.ResponseObject> response = new ActiveResponseSucces<NestHandler.Update_NonMOLStatus.ResponseObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObject,
                                                                        _config["NEST:BaseUrl"].ToString(),
                                                                        _config["NEST:Update_NonMOLStatus"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 2);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;

            response.Status = result.Status;
            if (result.httpResult != null && result.httpResult.severity == "Success")
            {
                var data = JsonConvert.DeserializeObject<NestHandler.Update_NonMOLStatus.ResponseObject>(result.httpResult.httpResponse);
                if (data != null && data.Update_NonMOLStatusResult != null && data.Update_NonMOLStatusResult.Status == "FAILED")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = data.Update_NonMOLStatusResult.ReferenceID, StatusMessage = data.Update_NonMOLStatusResult.ERROR_DESCRIPTION };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
    }
}