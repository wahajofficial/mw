﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DBHandler.Enum;
using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using DBHandler.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MoneyThorHandler;
using MoneyThorHandler.Model;
using Newtonsoft.Json;
using VendorApi.Helper;
using HttpHandler;

namespace VendorApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoneyThorController : ControllerBase
    {
        private Exception excetionForLog;
        private Level level = Level.Info;

        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private readonly ILogRepository _log;
        private readonly ITransactionImageRepository _imageTransaction;
        private readonly EncryptionHelper _encr;
        private readonly IConfiguration _config;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly MoneyThor _moneyThor;
        private readonly IHostingEnvironment _env;
        private readonly ILinkAccountRepository _linkAccountRepository;
        public MoneyThorController(IUserChannelsRepository userChannels, ILinkAccountRepository linkAccountRepository, ITransactionImageRepository imageTransaction, IHostingEnvironment env, UserManager<ApplicationUser> userManager, IHttpClientFactory httpClientFactory, IConfiguration config, ILogRepository logs)
        {
            _userChannels = userChannels;
            _linkAccountRepository = linkAccountRepository;
            _imageTransaction = imageTransaction;
            _env = env;
            _httpClientFactory = httpClientFactory;
            _moneyThor = new MoneyThorHandler.MoneyThor(httpClientFactory);
            _userManager = userManager;
            _log = logs;
            _config = config;
            _encr = new EncryptionHelper();
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
        }

        //GetBalance
        [Route("GetBalance")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> GetBalance([FromBody] GetBalance Model)
        {
            DateTime starTime = DateTime.Now;
            IActionResult Result = null;
            ActiveResponseSucces<RootObjectBalance> ResponseSuccess = new ActiveResponseSucces<RootObjectBalance>();
            DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, _log);
            HttpContext ctx = Request.HttpContext;
            var id = HttpContext.User.Claims.First().Value;
            try
            {
                bool isModelValid = TryValidateModel(Model);
                if (!isModelValid || !helper.ValidateChannel(id, Model.SignOnRq.ChannelId, ctx))
                {
                    if (!isModelValid)
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidModel"].ToString(), Code = "ERROR-01" };
                    }
                    else
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidChannel"].ToString(), Code = "ERROR-01" };
                    }

                    level = Level.Error;
                    ResponseSuccess.LogId = Model.SignOnRq.LogId;
                    excetionForLog = new Exception(ResponseSuccess.Status.StatusMessage);
                    Result = BadRequest(ResponseSuccess);
                }
                else
                {
                    var result = _moneyThor.GetBalance(_config["MoneyThor:BaseUrl"], _config["MoneyThor:GetBalance"],Model.AccountType, Model.AccountKeys, Model.Currency, Model.min_date, Model.max_date, Model.CustomerId ).Result;
                    if (result != null && result.header.success == true)
                    {
                        ResponseSuccess.LogId = Model.SignOnRq.LogId;
                        ResponseSuccess.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                        ResponseSuccess.Content = result;
                        Result = Ok(ResponseSuccess);

                    }
                    else
                    {
                        level = Level.Error;
                        ResponseSuccess.Content = result;
                        ResponseSuccess.LogId = Model.SignOnRq.LogId;
                        ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                        Result = BadRequest(ResponseSuccess);
                    }
                }

            }
            catch (Exception ex)
            {
                level = Level.Error;
                ResponseSuccess.Content = null;
                ResponseSuccess.LogId = Model.SignOnRq.LogId;
                ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                excetionForLog = ex;
                Result = BadRequest(ResponseSuccess);
            }

            ResponseSuccess.LogId = Model.SignOnRq.LogId;
            ResponseSuccess.RequestDateTime = Model.SignOnRq.DateTime;
            var userID = id;
            string res = JsonConvert.SerializeObject(Model);
            string req = JsonConvert.SerializeObject(Result);
            DateTime Endtime = DateTime.Now;
            HttpContext Ctx = Request.HttpContext;
            _log.Logs(level, ResponseSuccess.Status.Code.ToString(), Ctx, _config, excetionForLog, starTime, Endtime, res, req, userID, Model.SignOnRq.ChannelId, ResponseSuccess.LogId, Model.SignOnRq.DateTime);
            return Result;
        }

        //SearchTransactions
        [Route("SearchTransactions")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> SearchTransactions([FromBody] SearchTransactionsDto Model)
        {
            DateTime starTime = DateTime.Now;
            IActionResult Result = null;
            ActiveResponseSucces<RootObjectSearchtransaction> ResponseSuccess = new ActiveResponseSucces<RootObjectSearchtransaction>();
            DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, _log);
            HttpContext ctx = Request.HttpContext;
            var id = HttpContext.User.Claims.First().Value;
            try
            {
                bool isModelValid = TryValidateModel(Model);
                if (!isModelValid || !helper.ValidateChannel(id, Model.SignOnRq.ChannelId, ctx))
                {
                    if (!isModelValid)
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidModel"].ToString(), Code = "ERROR-01" };
                    }
                    else
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidChannel"].ToString(), Code = "ERROR-01" };
                    }

                    level = Level.Error;
                    ResponseSuccess.LogId = Model.SignOnRq.LogId;
                    excetionForLog = new Exception(ResponseSuccess.Status.StatusMessage);
                    Result = BadRequest(ResponseSuccess);
                }
                else
                {
                    var result = _moneyThor.SeacrhTransaction(_config["MoneyThor:BaseUrl"], _config["MoneyThor:searchtransactions"], Model).Result;
                    if (result != null && result.header.success == true)
                    {
                        ResponseSuccess.LogId = Model.SignOnRq.LogId;
                        ResponseSuccess.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                        ResponseSuccess.Content = result;
                        Result = Ok(ResponseSuccess);

                    }
                    else
                    {
                        level = Level.Error;
                        ResponseSuccess.Content = result;
                        ResponseSuccess.LogId = Model.SignOnRq.LogId;
                        ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                        Result = BadRequest(ResponseSuccess);
                    }
                }

            }
            catch (Exception ex)
            {
                level = Level.Error;
                ResponseSuccess.Content = null;
                ResponseSuccess.LogId = Model.SignOnRq.LogId;
                ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                excetionForLog = ex;
                Result = BadRequest(ResponseSuccess);
            }

            ResponseSuccess.LogId = Model.SignOnRq.LogId;
            ResponseSuccess.RequestDateTime = Model.SignOnRq.DateTime;
            var userID = id;
            string res = JsonConvert.SerializeObject(Model);
            string req = JsonConvert.SerializeObject(Result);
            DateTime Endtime = DateTime.Now;
            HttpContext Ctx = Request.HttpContext;
            _log.Logs(level, ResponseSuccess.Status.Code.ToString(), Ctx, _config, excetionForLog, starTime, Endtime, res, req, userID, Model.SignOnRq.ChannelId, ResponseSuccess.LogId, Model.SignOnRq.DateTime);
            return Result;
        }

        //TrackEvents
        [Route("TrackEvents")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> TrackEvents([FromBody] TrackEventDto Model)
        {
            DateTime starTime = DateTime.Now;
            IActionResult Result = null;
            ActiveResponseSucces<MoneyThorHandler.Model.TrackEventResponse.RootObject> ResponseSuccess = new ActiveResponseSucces<MoneyThorHandler.Model.TrackEventResponse.RootObject>();
            DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, _log);
            HttpContext ctx = Request.HttpContext;
            var id = HttpContext.User.Claims.First().Value;
            try
            {
                bool isModelValid = TryValidateModel(Model);
                if (!isModelValid || !helper.ValidateChannel(id, Model.SignOnRq.ChannelId, ctx))
                {
                    if (!isModelValid)
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidModel"].ToString(), Code = "ERROR-01" };
                    }
                    else
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidChannel"].ToString(), Code = "ERROR-01" };
                    }

                    level = Level.Error;
                    ResponseSuccess.LogId = Model.SignOnRq.LogId;
                    excetionForLog = new Exception(ResponseSuccess.Status.StatusMessage);
                    Result = BadRequest(ResponseSuccess);
                }
                else
                {
                    var result = _moneyThor.TrackEvents(_config["MoneyThor:BaseUrl"], _config["MoneyThor:TrackEvents"], Model.Root, Model.CustomerId).Result;
                    if (result != null && result.header.success == true)
                    {
                        ResponseSuccess.LogId = Model.SignOnRq.LogId;
                        ResponseSuccess.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                        ResponseSuccess.Content = result;
                        Result = Ok(ResponseSuccess);
                    }
                    else
                    {
                        level = Level.Error;
                        ResponseSuccess.LogId = Model.SignOnRq.LogId;
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage =result.header.message, Code = "ERROR-03" };
                        Result = BadRequest(ResponseSuccess);
                    }
                }
            }
            catch (Exception ex)
            {
                level = Level.Error;
                ResponseSuccess.Content = null;
                ResponseSuccess.LogId = Model.SignOnRq.LogId;
                ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                excetionForLog = ex;
                Result = BadRequest(ResponseSuccess);
            }

            ResponseSuccess.LogId = Model.SignOnRq.LogId;
            ResponseSuccess.RequestDateTime = Model.SignOnRq.DateTime;
            var userID = id;
            string res = JsonConvert.SerializeObject(Model);
            string req = JsonConvert.SerializeObject(Result);
            DateTime Endtime = DateTime.Now;
            HttpContext Ctx = Request.HttpContext;
            _log.Logs(level, ResponseSuccess.Status.Code.ToString(), Ctx, _config, excetionForLog, starTime, Endtime, res, req, userID, Model.SignOnRq.ChannelId, ResponseSuccess.LogId, Model.SignOnRq.DateTime);
            return Result;
        }

        //synccustomfield
        [Route("synccustomfield")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> synccustomfields([FromBody] synccustomfieldDto Model)
        {
            DateTime starTime = DateTime.Now;
            IActionResult Result = null;
            ActiveResponseSucces<MoneyThorHandler.Model.responsesynccustomField> ResponseSuccess = new ActiveResponseSucces<MoneyThorHandler.Model.responsesynccustomField>();
            DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, _log);
            HttpContext ctx = Request.HttpContext;
            var id = HttpContext.User.Claims.First().Value;
            try
            {
                bool isModelValid = TryValidateModel(Model);
                if (!isModelValid || !helper.ValidateChannel(id, Model.SignOnRq.ChannelId, ctx))
                {
                    if (!isModelValid)
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidModel"].ToString(), Code = "ERROR-01" };
                    }
                    else
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidChannel"].ToString(), Code = "ERROR-01" };
                    }

                    level = Level.Error;
                    ResponseSuccess.LogId = Model.SignOnRq.LogId;
                    excetionForLog = new Exception(ResponseSuccess.Status.StatusMessage);
                    Result = BadRequest(ResponseSuccess);
                }
                else
                {
                    var result = _moneyThor.synccustomfield(_config["MoneyThor:BaseUrl"], _config["MoneyThor:SyncCustomfields"], Model.synccustomfield, Model.CustomerId).Result;
                    if (result != null && result.header.success == true)
                    {
                        ResponseSuccess.LogId = Model.SignOnRq.LogId;
                        ResponseSuccess.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                        ResponseSuccess.Content = result;
                        Result = Ok(ResponseSuccess);

                    }
                    else
                    {
                        level = Level.Error;
                        ResponseSuccess.Content = result;
                        ResponseSuccess.LogId = Model.SignOnRq.LogId;
                        ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                        Result = BadRequest(ResponseSuccess);
                    }
                }

            }
            catch (Exception ex)
            {
                level = Level.Error;
                ResponseSuccess.Content = null;
                ResponseSuccess.LogId = Model.SignOnRq.LogId;
                ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                excetionForLog = ex;
                Result = BadRequest(ResponseSuccess);
            }

            ResponseSuccess.LogId = Model.SignOnRq.LogId;
            ResponseSuccess.RequestDateTime = Model.SignOnRq.DateTime;
            var userID = id;
            string res = JsonConvert.SerializeObject(Model);
            string req = JsonConvert.SerializeObject(Result);
            DateTime Endtime = DateTime.Now;
            HttpContext Ctx = Request.HttpContext;
            _log.Logs(level, ResponseSuccess.Status.Code.ToString(), Ctx, _config, excetionForLog, starTime, Endtime, res, req, userID, Model.SignOnRq.ChannelId, ResponseSuccess.LogId, Model.SignOnRq.DateTime);
            return Result;
        }

        [Route("GetTimeLine")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> GetTimeLine([FromBody] TimeLineDTO Model)
        {
            DateTime starTime = DateTime.Now;
            IActionResult Result = null;
            ActiveResponseSucces<MoneyThorHandler.Model.TimeLine.RootObject> ResponseSuccess = new ActiveResponseSucces<MoneyThorHandler.Model.TimeLine.RootObject>();
            DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, _log);
            HttpContext ctx = Request.HttpContext;
            var id = HttpContext.User.Claims.First().Value;

            try
            {
                bool isModelValid = TryValidateModel(Model);
                if (!isModelValid || !helper.ValidateChannel(id, Model.SignOnRq.ChannelId, ctx))
                {
                    if (!isModelValid)
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidModel"].ToString(), Code = "ERROR-01" };
                    }
                    else
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidChannel"].ToString(), Code = "ERROR-01" };
                    }

                    level = Level.Error;
                    ResponseSuccess.LogId = Model.SignOnRq.LogId;
                    excetionForLog = new Exception(ResponseSuccess.Status.StatusMessage);
                    Result = BadRequest(ResponseSuccess);
                }
                else
                {

                    DateTime endDate = DateTime.Today;
                    DateTime startDate = new DateTime();
                    var currentMonthStart = DateTime.Today.AddDays(-((DateTime.Now.Date.Day) - 1));
                    if (string.IsNullOrEmpty(Model.Months))
                    {
                        startDate = currentMonthStart.AddMonths(-6);
                    }
                    else
                    {
                        int month = int.Parse(Model.Months);
                        startDate = currentMonthStart.AddMonths(-month);
                    }
                    if(Model.StartDate != null && Model.EndDate != null)
                    {
                        startDate = DateTime.Parse(Model.StartDate.ToString());
                        endDate = DateTime.Parse(Model.EndDate.ToString());
                    }

                    var charRounding = int.Parse(_config["GlobalSettings:DescriptionLimit"]);
                    var result = _moneyThor.GetTimeLine(_config["MoneyThor:BaseUrl"], _config["MoneyThor:TimeLine"], startDate, endDate, 9999, Model.CustomerAccount, Model.CustomerId,Model.query,Model.minAmount,Model.maxAmount).Result;
                    if (result != null && result.header.success == true)
                    {
                        ResponseSuccess.LogId = Model.SignOnRq.LogId;
                        ResponseSuccess.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                        ResponseSuccess.Content = result;
                        Result = Ok(ResponseSuccess);
                    }
                    else
                    {
                        level = Level.Error;
                        ResponseSuccess.Content = result;
                        ResponseSuccess.LogId = Model.SignOnRq.LogId;
                        ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                        Result = BadRequest(ResponseSuccess);
                    }
                }
            }
            catch (Exception ex)
            {
                level = Level.Error;
                //ResponseSuccess.Status.Code =Status.ACTVETErroCode.Failed;
                ResponseSuccess.Content = null;
                ResponseSuccess.LogId = Model.SignOnRq.LogId;
                ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                excetionForLog = ex;
                Result = BadRequest(ResponseSuccess);
            }

            ResponseSuccess.LogId = Model.SignOnRq.LogId;
            ResponseSuccess.RequestDateTime = Model.SignOnRq.DateTime;

            //var userID = HttpContext.User.Claims.First().Value;
            var userID = id;
            string res = JsonConvert.SerializeObject(Model);
            string req = JsonConvert.SerializeObject(Result);
            DateTime Endtime = DateTime.Now;
            HttpContext Ctx = Request.HttpContext;
            _log.Logs(level, ResponseSuccess.Status.Code.ToString(), Ctx, _config, excetionForLog, starTime, Endtime, res, req, userID, Model.SignOnRq.ChannelId, ResponseSuccess.LogId, Model.SignOnRq.DateTime);
            return Result;
        }


        //[Route("UploadImage")]
        //[Authorize(AuthenticationSchemes = "BasicAuthentication")]
        //[HttpPost]
        //public async Task<IActionResult> UploadImage([FromBody] UploadImageDTO em)
        //{
        //    HttpContext ctx = Request.HttpContext;
        //    DateTime startime = DateTime.Now;
        //    string requestParameter = JsonConvert.SerializeObject(em);
        //    DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, _log);
        //    var id = HttpContext.User.Claims.First().Value;
        //    // Return if model is not validated
        //    if (!TryValidateModel(em))
        //    {
        //        if (!ModelState.IsValid)
        //        {
        //            return helper.Response("err-Model", Level.Success, helper.GetErrors(ModelState), ActiveResponse.ACTVETErroCode.Failed, startime, _logs, HttpContext, _config, "", id, ActiveResponse.ReturnResponse.BadRequest, null);
        //        }

        //    }

        //    // Return if ID null
        //    if (id == null)
        //    {
        //        return helper.Response("Error", Level.Success, "Id is null", ActiveResponse.ACTVETErroCode.UnAuthorise, startime, _log, HttpContext, _config, "", id, ActiveResponse.ReturnResponse.Unauthorized, null);

        //    }
        //    //return if channel not validated
        //    if (!helper.ValidateChannel(id, em.ChannelId, ctx))
        //    {
        //        return helper.Response("Error", Level.Error, _config["Errors:InvalidChannel"].ToString(), ActiveResponse.ACTVETErroCode.Failed, startime, _logs, HttpContext, _config, "", id, ActiveResponse.ReturnResponse.BadRequest, null);

        //    }
        //    //API logic here

        //    try
        //    {

        //        id = "9adffee3-0496-4b6f-bb4f-db243cc84aec";
        //        var lastTransaction = _imageTransaction.Find(x => x.UserId == id).Count();
        //        var imageKey = "image_1";
        //        if (lastTransaction > 0)
        //        {
        //            imageKey = "image_" + (lastTransaction + 1);
        //        }
        //        var iDD = Guid.NewGuid().ToString();
        //        var webRoot = _env.WebRootPath;
        //        var filename = iDD + "" + DateTime.Now.ToString("dd-MM-yyyy_hhmmsstt") + ".png";
        //        var file = System.IO.Path.Combine(webRoot, filename);

        //        await System.IO.File.WriteAllBytesAsync(file, Convert.FromBase64String(em.Image));
        //        var result = _moneyThor.UploadImageToTransaction(_config["MoneyThor:BaseUrl"], _config["MoneyThor:UploadIamge"], filename, em.TrxKey, em.CustomerId, em.Note, imageKey).Result;
        //        if (result.header.success == true)
        //        {

        //            return helper.Response("suc-001", Level.Success, result, ActiveResponse.ACTVETErroCode.Success, startime, _log, HttpContext, _config, requestParameter, id, ActiveResponse.ReturnResponse.Success, null);

        //        }
        //        else
        //        {
        //            return helper.Response("kerr-112", Level.Error, result, ActiveResponse.ACTVETErroCode.Success, startime, _log, HttpContext, _config, requestParameter, id, ActiveResponse.ReturnResponse.BadRequest, null);
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        return helper.Response("kerr-114", Level.Error, null, ActiveResponse.ACTVETErroCode.Failed, startime, _log, HttpContext, _config, requestParameter, id, ActiveResponse.ReturnResponse.BadRequest, ex);

        //    }
        //}

        [Route("CreateCustomer")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> CreateCustomer([FromBody] MoneyThorCustomer Model)
        {
            DateTime starTime = DateTime.Now;
            IActionResult Result = null;
            ActiveResponseSucces<CreateCustomerResponse> ResponseSuccess = new ActiveResponseSucces<CreateCustomerResponse>();
            DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, _log);
            HttpContext ctx = Request.HttpContext;
            var id = HttpContext.User.Claims.First().Value;

            try
            {
                bool isModelValid = TryValidateModel(Model);
                if (!isModelValid || !helper.ValidateChannel(id, Model.SignOnRq.ChannelId, ctx))
                {
                    if (!isModelValid)
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidModel"].ToString(), Code = "ERROR-01" };
                    }
                    else
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidChannel"].ToString(), Code = "ERROR-01" };
                    }

                    level = Level.Error;
                    ResponseSuccess.LogId = Model.SignOnRq.LogId;
                    excetionForLog = new Exception(ResponseSuccess.Status.StatusMessage);
                    Result = BadRequest(ResponseSuccess);
                }
                else
                {
                    var result = _moneyThor.CreateCustomer(_config["MoneyThor:BaseUrl"], _config["MoneyThor:CreateCustomer"], Model).Result;
                    if (result != null && result.header.success == true)
                    {
                        ResponseSuccess.LogId = Model.SignOnRq.LogId;
                        ResponseSuccess.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                        ResponseSuccess.Content = result;
                        Result = Ok(ResponseSuccess);

                    }
                    else
                    {
                        level = Level.Error;
                        ResponseSuccess.Content = result;
                        ResponseSuccess.LogId = Model.SignOnRq.LogId;
                        ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                        Result = BadRequest(ResponseSuccess);
                    }
                }

            }
            catch (Exception ex)
            {
                level = Level.Error;
                //ResponseSuccess.Status.Code =Status.ACTVETErroCode.Failed;
                ResponseSuccess.Content = null;
                ResponseSuccess.LogId = Model.SignOnRq.LogId;
                ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                excetionForLog = ex;
                Result = BadRequest(ResponseSuccess);
            }

            ResponseSuccess.LogId = Model.SignOnRq.LogId;
            ResponseSuccess.RequestDateTime = Model.SignOnRq.DateTime;

            //var userID = HttpContext.User.Claims.First().Value;
            var userID = id;
            string res = JsonConvert.SerializeObject(Model);
            string req = JsonConvert.SerializeObject(Result);
            DateTime Endtime = DateTime.Now;
            HttpContext Ctx = Request.HttpContext;
            _log.Logs(level, ResponseSuccess.Status.Code.ToString(), Ctx, _config, excetionForLog, starTime, Endtime, res, req, userID, Model.SignOnRq.ChannelId, ResponseSuccess.LogId, Model.SignOnRq.DateTime);


            return Result;
        }


        [Route("GetBudgets")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> GetBudgets([FromBody] BudgetRequest Model)
        {
            DateTime starTime = DateTime.Now;
            IActionResult Result = null;
            ActiveResponseSucces<RootObject> ResponseSuccess = new ActiveResponseSucces<RootObject>();
            DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, _log);
            HttpContext ctx = Request.HttpContext;
            var id = HttpContext.User.Claims.First().Value;

            try
            {
                bool isModelValid = TryValidateModel(Model);
                if (!isModelValid || !helper.ValidateChannel(id, Model.SignOnRq.ChannelId, ctx))
                {
                    if (!isModelValid)
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidModel"].ToString(), Code = "ERROR-01" };
                    }
                    else
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidChannel"].ToString(), Code = "ERROR-01" };
                    }

                    level = Level.Error;
                    ResponseSuccess.LogId = Model.SignOnRq.LogId;
                    excetionForLog = new Exception(ResponseSuccess.Status.StatusMessage);
                    Result = BadRequest(ResponseSuccess);
                }
                else
                {
                    var result = _moneyThor.GetBudgets(_config["MoneyThor:BaseUrl"], _config["MoneyThor:GetBudgets"], Model).Result;
                    if (result != null && result.header.success == true)
                    {
                        ResponseSuccess.LogId = Model.SignOnRq.LogId;
                        ResponseSuccess.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                        ResponseSuccess.Content = result;
                        Result = Ok(ResponseSuccess);

                    }
                    else
                    {
                        level = Level.Error;
                        ResponseSuccess.Content = result;
                        ResponseSuccess.LogId = Model.SignOnRq.LogId;
                        ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                        Result = BadRequest(ResponseSuccess);
                    }
                }

            }
            catch (Exception ex)
            {
                level = Level.Error;
                //ResponseSuccess.Status.Code =Status.ACTVETErroCode.Failed;
                ResponseSuccess.Content = null;
                ResponseSuccess.LogId = Model.SignOnRq.LogId;
                ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                excetionForLog = ex;
                Result = BadRequest(ResponseSuccess);
            }

            ResponseSuccess.LogId = Model.SignOnRq.LogId;
            ResponseSuccess.RequestDateTime = Model.SignOnRq.DateTime;

            //var userID = HttpContext.User.Claims.First().Value;
            var userID = id;
            string res = JsonConvert.SerializeObject(Model);
            string req = JsonConvert.SerializeObject(Result);
            DateTime Endtime = DateTime.Now;
            HttpContext Ctx = Request.HttpContext;
            _log.Logs(level, ResponseSuccess.Status.Code.ToString(), Ctx, _config, excetionForLog, starTime, Endtime, res, req, userID, Model.SignOnRq.ChannelId, ResponseSuccess.LogId, Model.SignOnRq.DateTime);


            return Result;
        }

        //[Route("GetBudgets")]
        //[Authorize(AuthenticationSchemes = "BasicAuthentication")]
        //[HttpPost]
        //public async Task<IActionResult> GetBudgets([FromBody] TimeLineDTO em)
        //{
        //    HttpContext ctx = Request.HttpContext;
        //    DateTime startime = DateTime.Now;
        //    string requestParameter = JsonConvert.SerializeObject(em);
        //    DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, _logs);


        //    var id = HttpContext.User.Claims.First().Value;
        //    if (!TryValidateModel(em))
        //    {
        //        if (!ModelState.IsValid)
        //        {
        //            return helper.Response("err-Model", Level.Success, helper.GetErrors(ModelState), ActiveResponse.ACTVETErroCode.Failed, startime, _logs, HttpContext, _config, "", id, ActiveResponse.ReturnResponse.BadRequest, null);
        //        }

        //    }

        //    // Return if ID null
        //    if (id == null)
        //    {
        //        return helper.Response("Error", Level.Success, "Id is null", ActiveResponse.ACTVETErroCode.UnAuthorise, startime, _logs, HttpContext, _config, "", id, ActiveResponse.ReturnResponse.Unauthorized, null);

        //    }
        //    //return if channel not validated
        //    if (!helper.ValidateChannel(id, em.ChannelId, ctx))
        //    {
        //        return helper.Response("Error", Level.Error, _config["Errors:InvalidChannel"].ToString(), ActiveResponse.ACTVETErroCode.Failed, startime, _logs, HttpContext, _config, "", id, ActiveResponse.ReturnResponse.BadRequest, null);

        //    }
        //    //API logic here
        //    var clientIp = HttpContext.Features.Get<IHttpConnectionFeature>()?.RemoteIpAddress?.ToString();
        //    try
        //    {
        //        var currentMonthStart = DateTime.Today.AddDays(-((DateTime.Now.Date.Day) - 1));
        //        var startDate = currentMonthStart.AddMonths(-4);
        //        var charRounding = int.Parse(_config["GlobalSettings:DescriptionLimit"]);
        //        var result = _moneyThor.GetTimeLine(_config["MoneyThor:BaseUrl"], _config["MoneyThor:TimeLine"], startDate, DateTime.Today, 9999, em.CustomerAccount, em.CustomerId).Result;
        //        if (result.payLoad.Count > 0)
        //        {
        //            return helper.Response("suc-001", Level.Success, result, ActiveResponse.ACTVETErroCode.Success, startime, _logs, HttpContext, _config, requestParameter, id, ActiveResponse.ReturnResponse.Success, null);

        //        }
        //        else
        //        {
        //            return helper.Response("suc-001", Level.Success, result, ActiveResponse.ACTVETErroCode.Success, startime, _logs, HttpContext, _config, requestParameter, id, ActiveResponse.ReturnResponse.Success, null);
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        return helper.Response("kerr-113", Level.Error, null, ActiveResponse.ACTVETErroCode.Failed, startime, _logs, HttpContext, _config, requestParameter, id, ActiveResponse.ReturnResponse.BadRequest, ex);

        //    }
        //}

        [Route("AddTransaction")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> AddTransaction([FromBody] MoneyThorTransfer Model)
        {
            DateTime starTime = DateTime.Now;
            IActionResult Result = null;
            ActiveResponseSucces<CreateCustomerResponse> ResponseSuccess = new ActiveResponseSucces<CreateCustomerResponse>();
            DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, _log);
            HttpContext ctx = Request.HttpContext;
            var id = HttpContext.User.Claims.First().Value;

            try
            {
                bool isModelValid = TryValidateModel(Model);
                if (!isModelValid || !helper.ValidateChannel(id, Model.SignOnRq.ChannelId, ctx))
                {
                    if (!isModelValid)
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidModel"].ToString(), Code = "ERROR-01" };
                    }
                    else
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidChannel"].ToString(), Code = "ERROR-01" };
                    }

                    level = Level.Error;
                    ResponseSuccess.LogId = Model.SignOnRq.LogId;
                    excetionForLog = new Exception(ResponseSuccess.Status.StatusMessage);
                    Result = BadRequest(ResponseSuccess);
                }
                else
                {
                    var result = _moneyThor.Transfer(_config["MoneyThor:BaseUrl"], _config["MoneyThor:CreateCustomer"], Model).Result;
                    if (result != null && result.header.success == true)
                    {
                        DBHandler.Model.Version version = _userChannels.GetAPIVersion();
                        ResponseSuccess.LogId = Model.SignOnRq.LogId;
                        ResponseSuccess.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                        ResponseSuccess.Content = result;
                        Result = Ok(ResponseSuccess);

                    }
                    else
                    {
                        level = Level.Error;
                        ResponseSuccess.Content = result;
                        ResponseSuccess.LogId = Model.SignOnRq.LogId;
                        ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                        Result = BadRequest(ResponseSuccess);
                    }
                }
            }
            catch (Exception ex)
            {
                level = Level.Error;
                //ResponseSuccess.Status.Code =Status.ACTVETErroCode.Failed;
                ResponseSuccess.Content = null;
                ResponseSuccess.LogId = Model.SignOnRq.LogId;
                ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                excetionForLog = ex;
                Result = BadRequest(ResponseSuccess);
            }

            ResponseSuccess.LogId = Model.SignOnRq.LogId;
            ResponseSuccess.RequestDateTime = Model.SignOnRq.DateTime;

            //var userID = HttpContext.User.Claims.First().Value;
            var userID = id;
            string res = JsonConvert.SerializeObject(Model);
            string req = JsonConvert.SerializeObject(Result);
            DateTime Endtime = DateTime.Now;
            HttpContext Ctx = Request.HttpContext;
            _log.Logs(level, ResponseSuccess.Status.Code.ToString(), Ctx, _config, excetionForLog, starTime, Endtime, res, req, userID, Model.SignOnRq.ChannelId, ResponseSuccess.LogId, Model.SignOnRq.DateTime);

            return Result;
        }


        [Route("CreateBudget")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> CreateBudget([FromBody] CreateBudgetModel Model)
        {
            DateTime starTime = DateTime.Now;
            IActionResult Result = null;
            ActiveResponseSucces<BudgetResponse> ResponseSuccess = new ActiveResponseSucces<BudgetResponse>();
            DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, _log);
            HttpContext ctx = Request.HttpContext;
            var id = HttpContext.User.Claims.First().Value;

            try
            {
                bool isModelValid = TryValidateModel(Model);
                if (!isModelValid || !helper.ValidateChannel(id, Model.SignOnRq.ChannelId, ctx))
                {
                    if (!isModelValid)
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidModel"].ToString(), Code = "ERROR-01" };
                    }
                    else
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidChannel"].ToString(), Code = "ERROR-01" };
                    }

                    level = Level.Error;
                    ResponseSuccess.LogId = Model.SignOnRq.LogId;
                    excetionForLog = new Exception(ResponseSuccess.Status.StatusMessage);
                    Result = BadRequest(ResponseSuccess);
                }
                else
                {
                    var result = _moneyThor.CreateBudgets(_config["MoneyThor:BaseUrl"], _config["MoneyThor:createbudget"], Model.budget,Model.CustomerId).Result;
                    if (result != null && result.header.success == true)
                    {
                        ResponseSuccess.LogId = Model.SignOnRq.LogId;
                        ResponseSuccess.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                        ResponseSuccess.Content = result;
                        Result = Ok(ResponseSuccess);

                    }
                    else
                    {
                        level = Level.Error;
                        ResponseSuccess.Content = result;
                        ResponseSuccess.LogId = Model.SignOnRq.LogId;
                        ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                        Result = BadRequest(ResponseSuccess);
                    }
                }
            }
            catch (Exception ex)
            {
                level = Level.Error;
                //ResponseSuccess.Status.Code =Status.ACTVETErroCode.Failed;
                ResponseSuccess.Content = null;
                ResponseSuccess.LogId = Model.SignOnRq.LogId;
                ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                excetionForLog = ex;
                Result = BadRequest(ResponseSuccess);
            }

            ResponseSuccess.LogId = Model.SignOnRq.LogId;
            ResponseSuccess.RequestDateTime = Model.SignOnRq.DateTime;

            //var userID = HttpContext.User.Claims.First().Value;
            var userID = id;
            string res = JsonConvert.SerializeObject(Model);
            string req = JsonConvert.SerializeObject(Result);
            DateTime Endtime = DateTime.Now;
            HttpContext Ctx = Request.HttpContext;
            _log.Logs(level, ResponseSuccess.Status.Code.ToString(), Ctx, _config, excetionForLog, starTime, Endtime, res, req, userID, Model.SignOnRq.ChannelId, ResponseSuccess.LogId, Model.SignOnRq.DateTime);

            return Result;
        }


        [Route("DeleteBudget")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> DeleteBudget([FromBody] DeleteBudgetModel Model)
        {
            DateTime starTime = DateTime.Now;
            IActionResult Result = null;
            ActiveResponseSucces<BudgetResponse> ResponseSuccess = new ActiveResponseSucces<BudgetResponse>();
            DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, _log);
            HttpContext ctx = Request.HttpContext;
            var id = HttpContext.User.Claims.First().Value;

            try
            {
                bool isModelValid = TryValidateModel(Model);
                if (!isModelValid || !helper.ValidateChannel(id, Model.SignOnRq.ChannelId, ctx))
                {
                    if (!isModelValid)
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidModel"].ToString(), Code = "ERROR-01" };
                    }
                    else
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidChannel"].ToString(), Code = "ERROR-01" };
                    }

                    level = Level.Error;
                    ResponseSuccess.LogId = Model.SignOnRq.LogId;
                    excetionForLog = new Exception(ResponseSuccess.Status.StatusMessage);
                    Result = BadRequest(ResponseSuccess);
                }
                else
                {
                    var result = _moneyThor.DeleteBudget(_config["MoneyThor:BaseUrl"], _config["MoneyThor:deletebudget"], Model.budget, Model.CustomerId).Result;
                    if (result != null && result.header.success == true)
                    {
                        ResponseSuccess.LogId = Model.SignOnRq.LogId;
                        ResponseSuccess.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                        ResponseSuccess.Content = result;
                        Result = Ok(ResponseSuccess);

                    }
                    else
                    {
                        level = Level.Error;
                        ResponseSuccess.Content = result;
                        ResponseSuccess.LogId = Model.SignOnRq.LogId;
                        ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                        Result = BadRequest(ResponseSuccess);
                    }
                }
            }
            catch (Exception ex)
            {
                level = Level.Error;
                //ResponseSuccess.Status.Code =Status.ACTVETErroCode.Failed;
                ResponseSuccess.Content = null;
                ResponseSuccess.LogId = Model.SignOnRq.LogId;
                ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                excetionForLog = ex;
                Result = BadRequest(ResponseSuccess);
            }

            ResponseSuccess.LogId = Model.SignOnRq.LogId;
            ResponseSuccess.RequestDateTime = Model.SignOnRq.DateTime;

            //var userID = HttpContext.User.Claims.First().Value;
            var userID = id;
            string res = JsonConvert.SerializeObject(Model);
            string req = JsonConvert.SerializeObject(Result);
            DateTime Endtime = DateTime.Now;
            HttpContext Ctx = Request.HttpContext;
            _log.Logs(level, ResponseSuccess.Status.Code.ToString(), Ctx, _config, excetionForLog, starTime, Endtime, res, req, userID, Model.SignOnRq.ChannelId, ResponseSuccess.LogId, Model.SignOnRq.DateTime);

            return Result;
        }

        [Route("Updatebudget")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> Updatebudget([FromBody] UpdateBudgetModel Model)
        {
            DateTime starTime = DateTime.Now;
            IActionResult Result = null;
            ActiveResponseSucces<BudgetResponse> ResponseSuccess = new ActiveResponseSucces<BudgetResponse>();
            DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, _log);
            HttpContext ctx = Request.HttpContext;
            var id = HttpContext.User.Claims.First().Value;

            try
            {
                bool isModelValid = TryValidateModel(Model);
                if (!isModelValid || !helper.ValidateChannel(id, Model.SignOnRq.ChannelId, ctx))
                {
                    if (!isModelValid)
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidModel"].ToString(), Code = "ERROR-01" };
                    }
                    else
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidChannel"].ToString(), Code = "ERROR-01" };
                    }

                    level = Level.Error;
                    ResponseSuccess.LogId = Model.SignOnRq.LogId;
                    excetionForLog = new Exception(ResponseSuccess.Status.StatusMessage);
                    Result = BadRequest(ResponseSuccess);
                }
                else
                {
                    var result = _moneyThor.UpdateBudget(_config["MoneyThor:BaseUrl"], _config["MoneyThor:Updatebudget"], Model.budget, Model.CustomerId).Result;
                    if (result != null && result.header.success == true)
                    {
                        ResponseSuccess.LogId = Model.SignOnRq.LogId;
                        ResponseSuccess.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                        ResponseSuccess.Content = result;
                        Result = Ok(ResponseSuccess);

                    }
                    else
                    {
                        level = Level.Error;
                        ResponseSuccess.Content = result;
                        ResponseSuccess.LogId = Model.SignOnRq.LogId;
                        ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                        Result = BadRequest(ResponseSuccess);
                    }
                }
            }
            catch (Exception ex)
            {
                level = Level.Error;
                //ResponseSuccess.Status.Code =Status.ACTVETErroCode.Failed;
                ResponseSuccess.Content = null;
                ResponseSuccess.LogId = Model.SignOnRq.LogId;
                ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                excetionForLog = ex;
                Result = BadRequest(ResponseSuccess);
            }

            ResponseSuccess.LogId = Model.SignOnRq.LogId;
            ResponseSuccess.RequestDateTime = Model.SignOnRq.DateTime;

            //var userID = HttpContext.User.Claims.First().Value;
            var userID = id;
            string res = JsonConvert.SerializeObject(Model);
            string req = JsonConvert.SerializeObject(Result);
            DateTime Endtime = DateTime.Now;
            HttpContext Ctx = Request.HttpContext;
            _log.Logs(level, ResponseSuccess.Status.Code.ToString(), Ctx, _config, excetionForLog, starTime, Endtime, res, req, userID, Model.SignOnRq.ChannelId, ResponseSuccess.LogId, Model.SignOnRq.DateTime);

            return Result;
        }

        [Route("AddTransactionDetail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> AddTransactionDetail([FromBody] TransactionDetailDTO Model)
        {
            DateTime starTime = DateTime.Now;
            IActionResult Result = null;
            ActiveResponseSucces<CustomFieldResponse> ResponseSuccess = new ActiveResponseSucces<CustomFieldResponse>();
            DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, _log);
            HttpContext ctx = Request.HttpContext;
            var id = HttpContext.User.Claims.First().Value;

            try
            {
                bool isModelValid = TryValidateModel(Model);
                if (!isModelValid || !helper.ValidateChannel(id, Model.SignOnRq.ChannelId, ctx))
                {
                    if (!isModelValid)
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidModel"].ToString(), Code = "ERROR-01" };
                    }
                    else
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidChannel"].ToString(), Code = "ERROR-01" };
                    }

                    level = Level.Error;
                    ResponseSuccess.LogId = Model.SignOnRq.LogId;
                    excetionForLog = new Exception(ResponseSuccess.Status.StatusMessage);
                    Result = BadRequest(ResponseSuccess);
                }
                else
                {
                    var result = _moneyThor.AddTransactionDetail(_config["MoneyThor:BaseUrl"], _config["MoneyThor:AddTransactionDetail"],
                                                                 Model.transactionDetail, Model.CustomerId).Result;
                    if (result != null && result.header.success == true)
                    {
                        ResponseSuccess.LogId = Model.SignOnRq.LogId;
                        ResponseSuccess.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                        ResponseSuccess.Content = result;
                        Result = Ok(ResponseSuccess);

                    }
                    else
                    {
                        level = Level.Error;
                        ResponseSuccess.Content = result;
                        ResponseSuccess.LogId = Model.SignOnRq.LogId;
                        ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                        Result = BadRequest(ResponseSuccess);
                    }
                }
            }
            catch (Exception ex)
            {
                level = Level.Error;
                //ResponseSuccess.Status.Code =Status.ACTVETErroCode.Failed;
                ResponseSuccess.Content = null;
                ResponseSuccess.LogId = Model.SignOnRq.LogId;
                ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                excetionForLog = ex;
                Result = BadRequest(ResponseSuccess);
            }

            ResponseSuccess.LogId = Model.SignOnRq.LogId;
            ResponseSuccess.RequestDateTime = Model.SignOnRq.DateTime;

            //var userID = HttpContext.User.Claims.First().Value;
            var userID = id;
            string res = JsonConvert.SerializeObject(Model);
            string req = JsonConvert.SerializeObject(Result);
            DateTime Endtime = DateTime.Now;
            HttpContext Ctx = Request.HttpContext;
            _log.Logs(level, ResponseSuccess.Status.Code.ToString(), Ctx, _config, excetionForLog, starTime, Endtime, res, req, userID, Model.SignOnRq.ChannelId, ResponseSuccess.LogId, Model.SignOnRq.DateTime);

            return Result;
        }

        [Route("GetPromotions")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> GetPromotions([FromBody] MoneyThorHandler.Model.Promotion.PromotionRequest Model)
        {
            DateTime starTime = DateTime.Now;
            IActionResult Result = null;
            ActiveResponseSucces<MoneyThorHandler.Model.Promotion.RootObject> ResponseSuccess = new ActiveResponseSucces<MoneyThorHandler.Model.Promotion.RootObject>();
            DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, _log);
            HttpContext ctx = Request.HttpContext;
            var id = HttpContext.User.Claims.First().Value;

            try
            {
                bool isModelValid = TryValidateModel(Model);
                if (!isModelValid || !helper.ValidateChannel(id, Model.SignOnRq.ChannelId, ctx))
                {
                    if (!isModelValid)
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidModel"].ToString(), Code = "ERROR-01" };
                    }
                    else
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidChannel"].ToString(), Code = "ERROR-01" };
                    }

                    level = Level.Error;
                    ResponseSuccess.LogId = Model.SignOnRq.LogId;
                    excetionForLog = new Exception(ResponseSuccess.Status.StatusMessage);
                    Result = BadRequest(ResponseSuccess);
                }
                else
                {
                    var result = _moneyThor.GetPromotions(_config["MoneyThor:BaseUrl"], _config["MoneyThor:GetPromotions"],
                                                                 Model.min_date, Model.max_date,Model.type,Model.family,Model.CustomerId).Result;
                    if (result != null && result.header.success == true)
                    {
                        ResponseSuccess.LogId = Model.SignOnRq.LogId;
                        ResponseSuccess.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                        ResponseSuccess.Content = result;
                        Result = Ok(ResponseSuccess);

                    }
                    else
                    {
                        level = Level.Error;
                        ResponseSuccess.Content = result;
                        ResponseSuccess.LogId = Model.SignOnRq.LogId;
                        ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                        Result = BadRequest(ResponseSuccess);
                    }
                }
            }
            catch (Exception ex)
            {
                level = Level.Error;
                //ResponseSuccess.Status.Code =Status.ACTVETErroCode.Failed;
                ResponseSuccess.Content = null;
                ResponseSuccess.LogId = Model.SignOnRq.LogId;
                ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                excetionForLog = ex;
                Result = BadRequest(ResponseSuccess);
            }

            ResponseSuccess.LogId = Model.SignOnRq.LogId;
            ResponseSuccess.RequestDateTime = Model.SignOnRq.DateTime;

            //var userID = HttpContext.User.Claims.First().Value;
            var userID = id;
            string res = JsonConvert.SerializeObject(Model);
            string req = JsonConvert.SerializeObject(Result);
            DateTime Endtime = DateTime.Now;
            HttpContext Ctx = Request.HttpContext;
            _log.Logs(level, ResponseSuccess.Status.Code.ToString(), Ctx, _config, excetionForLog, starTime, Endtime, res, req, userID, Model.SignOnRq.ChannelId, ResponseSuccess.LogId, Model.SignOnRq.DateTime);

            return Result;
        }

        [Route("GetClientCategories")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> GetClientCategories([FromBody] CategoryModel Model)
        {
            DateTime starTime = DateTime.Now;
            IActionResult Result = null;
            ActiveResponseSucces<MoneyThorHandler.Model.Category.RootObject> ResponseSuccess = new ActiveResponseSucces<MoneyThorHandler.Model.Category.RootObject>();
            DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, _log);
            HttpContext ctx = Request.HttpContext;
            var id = HttpContext.User.Claims.First().Value;

            try
            {
                bool isModelValid = TryValidateModel(Model);
                if (!isModelValid || !helper.ValidateChannel(id, Model.SignOnRq.ChannelId, ctx))
                {
                    if (!isModelValid)
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidModel"].ToString(), Code = "ERROR-01" };
                    }
                    else
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidChannel"].ToString(), Code = "ERROR-01" };
                    }

                    level = Level.Error;
                    ResponseSuccess.LogId = Model.SignOnRq.LogId;
                    excetionForLog = new Exception(ResponseSuccess.Status.StatusMessage);
                    Result = BadRequest(ResponseSuccess);
                }
                else
                {
                    var result = _moneyThor.GetClientCategories(_config["MoneyThor:BaseUrl"], _config["MoneyThor:getcategories"],
                                                                 Model.CustomerId).Result;
                    if (result != null && result.header.success == true)
                    {
                        ResponseSuccess.LogId = Model.SignOnRq.LogId;
                        ResponseSuccess.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                        ResponseSuccess.Content = result;
                        Result = Ok(ResponseSuccess);

                    }
                    else
                    {
                        level = Level.Error;
                        ResponseSuccess.Content = result;
                        ResponseSuccess.LogId = Model.SignOnRq.LogId;
                        ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                        Result = BadRequest(ResponseSuccess);
                    }
                }
            }
            catch (Exception ex)
            {
                level = Level.Error;
                //ResponseSuccess.Status.Code =Status.ACTVETErroCode.Failed;
                ResponseSuccess.Content = null;
                ResponseSuccess.LogId = Model.SignOnRq.LogId;
                ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                excetionForLog = ex;
                Result = BadRequest(ResponseSuccess);
            }

            ResponseSuccess.LogId = Model.SignOnRq.LogId;
            ResponseSuccess.RequestDateTime = Model.SignOnRq.DateTime;

            //var userID = HttpContext.User.Claims.First().Value;
            var userID = id;
            string res = JsonConvert.SerializeObject(Model);
            string req = JsonConvert.SerializeObject(Result);
            DateTime Endtime = DateTime.Now;
            HttpContext Ctx = Request.HttpContext;
            _log.Logs(level, ResponseSuccess.Status.Code.ToString(), Ctx, _config, excetionForLog, starTime, Endtime, res, req, userID, Model.SignOnRq.ChannelId, ResponseSuccess.LogId, Model.SignOnRq.DateTime);

            return Result;
        }

        [Route("GetTotalsByCategory")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponseSucces<List<TransactionDescription>>), 200)]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        public async Task<IActionResult> GetTotalsByCategory([FromBody] TotalCategoryRequest Model)
        {
            ActiveResponseSucces<MoneyThorHandler.Model.TotalCategory.RootObject> response = new ActiveResponseSucces<MoneyThorHandler.Model.TotalCategory.RootObject>();

            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Content-Type", "application/x-www-form-urlencoded");
            headers.Add("Customer", Model.CustomerId);
            List<string> accounts = new List<string>();
            accounts.Add(Model.MtAccount);
            var mtData= JsonConvert.SerializeObject(accounts);
            Dictionary<string, string> body = new Dictionary<string, string>();
            body.Add("min_date", Model.StartDate.ToString("yyyy-MM-dd"));
            body.Add("max_date", Model.EndDate.ToString("yyyy-MM-dd"));
            body.Add("movement", Model.movement);
            body.Add("accounts", mtData);

            InternalResult result = await APIRequestHandler.GetResponseX(Model.SignOnRq, Model,
                                                                        _config["MoneyThor:BaseUrl"].ToString(),
                                                                        _config["MoneyThor:gettotalsbycategory"].ToString(),
                                                                        level,
                                                                        _log,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler,
                                                                        headers,
                                                                        body);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<MoneyThorHandler.Model.TotalCategory.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.header.success )
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                    response.Content = data;

                    return Ok(response);
                }
                else
                {
                    response.Status = new Status { Severity = Severity.Error, Code = data.header.code.ToString(), StatusMessage = data.header.message };
                }
            }
            return BadRequest(response);
        }



        [Route("GetTransactionDetails")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponseSucces<List<TransactionDescription>>), 200)]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        public async Task<IActionResult> GetTransactionDetails([FromBody] TransactionSearchModel Model)
        {
            ActiveResponseSucces<MoneyThorHandler.Model.SingleTransactionDetail.RootObject> response = new ActiveResponseSucces<MoneyThorHandler.Model.SingleTransactionDetail.RootObject>();

            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Content-Type", "application/json");
            headers.Add("Customer", Model.CustomerId);

            InternalResult result = await APIRequestHandler.GetResponseX(Model.SignOnRq, Model.GetTransactionDetails,
                                                                        _config["MoneyThor:BaseUrl"].ToString(),
                                                                        _config["MoneyThor:GetTransactionDetails"].ToString(),
                                                                        level,
                                                                        _log,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler,
                                                                        headers,
                                                                        null,
                                                                        2);
            response.Status = result.Status;
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<MoneyThorHandler.Model.SingleTransactionDetail.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.header.success)
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                    response.Content = data;

                    return Ok(response);
                }
                else
                {
                    response.Status = new Status { Severity = Severity.Error, Code = data.header.code.ToString(), StatusMessage = data .header.message};
                }
            }
            return BadRequest(response);
        }



        [Route("GetTipsByPattern")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponseSucces<List<TransactionDescription>>), 200)]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        public async Task<IActionResult> GetTipsByPattern([FromBody] TotalCategoryRequest Model)
        {
            ActiveResponseSucces<MoneyThorHandler.Model.TipsByPattern.RootObject> response = new ActiveResponseSucces<MoneyThorHandler.Model.TipsByPattern.RootObject>();

            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Content-Type", "application/x-www-form-urlencoded");
            headers.Add("Customer", Model.CustomerId);
            Dictionary<string, string> body = new Dictionary<string, string>();
            body.Add("min_date", Model.StartDate.ToString("yyyy-MM-dd"));
            body.Add("max_date", Model.EndDate.ToString("yyyy-MM-dd"));
            body.Add("type", "regexp");
            body.Add("match", ".*");
            body.Add("target", Model.taget);
            InternalResult result = await APIRequestHandler.GetResponseX(Model.SignOnRq, Model,
                                                                        _config["MoneyThor:BaseUrl"].ToString(),
                                                                        _config["MoneyThor:gettipsbypattern"].ToString(),
                                                                        level,
                                                                        _log,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler,
                                                                        headers,
                                                                        body);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<MoneyThorHandler.Model.TipsByPattern.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.header.success)
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                    response.Content = data;

                    return Ok(response);
                }
                else
                {
                    response.Status = new Status { Severity = Severity.Error, Code = data.header.code.ToString(), StatusMessage = data.header.message };
                }
            }
            return BadRequest(response);
        }

        [Route("GetTip")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponseSucces<List<TransactionDescription>>), 200)]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        public async Task<IActionResult> GetTip([FromBody] TipRequest Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Content-Type", "application/x-www-form-urlencoded");
            headers.Add("Customer", Model.CustomerId);

            InternalResult result = await APIRequestHandler.GetResponseX(Model.SignOnRq, Model,
                                                                        _config["MoneyThor:BaseUrl"].ToString() +"tip?tip_key="+Model.tip,
                                                                       "",
                                                                        level,
                                                                        _log,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler,
                                                                        headers,
                                                                        null,3);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = result.httpResult.httpResponse;
                if (data != "")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                    response.Content = data;

                    return Ok(response);
                }
                else
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "0", StatusMessage = ""};
                }
            }
            return BadRequest(response);
        }
    }
}