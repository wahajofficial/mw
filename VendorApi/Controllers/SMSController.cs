﻿using System.Threading.Tasks;
using DBHandler.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using VendorApi.Helper;
using Swashbuckle.AspNetCore.Annotations;
using DBHandler.Repositories;
using Microsoft.Extensions.Configuration;
using DBHandler.Enum;
using System.Net.Http;

namespace VendorApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SMSController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private readonly ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;

        public SMSController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs)
        {
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
        }

        [Route("SendSMS")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CommonModel.Email.RootObject>", typeof(ActiveResponseSucces<CommonModel.Email.RootObject>))]
        public async Task<IActionResult> SendSMS([FromBody] CommonModel.SMS.SMSModel Model)
        {
            ActiveResponseSucces<CommonModel.SMS.RootObject> response = new ActiveResponseSucces<CommonModel.SMS.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.RootObject,
                                                                        _config["SMS:BaseUrl"].ToString(),
                                                                        _config["SMS:SendSMS"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2);

            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;

            response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

            response.Content = null;
            return Ok(response);
        }
    }
}