﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CoreHandler;
using DBHandler.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using DBHandler.Repositories;
using Microsoft.Extensions.Configuration;
using DBHandler.Enum;
using HttpHandler;
using VendorApi.Helper;
using CommonModel;
using Newtonsoft.Json.Linq;

namespace VendorApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DigibancStatementController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private readonly ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;
        private Exception myEx;

        private Exception excetionForLog;

        public DigibancStatementController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs)
        {
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
        }

        [Route("CustStatDateWise")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> CustStatDateWise([FromBody] CustomerStatementDto Model)
        {
            ActiveResponseSucces<DBHandler.Model.StatementSmart.CustStatementDateWiseResponseOut> response = new ActiveResponseSucces<DBHandler.Model.StatementSmart.CustStatementDateWiseResponseOut>();

            InternalResult result = await APIRequestHandler.GetResponse(Model.signOnRq == null ? null : Model.signOnRq, Model,
                                                                        _config["Statement:BaseUrl"].ToString(),
                                                                        _config["Statement:CustStatDateWise"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.StatementSmart.CustStatementDateWiseResponseOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("TrxListLastNTrx")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> TrxListLastNTrx([FromBody] TrxListLastNTrxDto Model)
        {
            ActiveResponseSucces<DBHandler.Model.StatementSmart.TrxListLastNTrxResponseOut> response = new ActiveResponseSucces<DBHandler.Model.StatementSmart.TrxListLastNTrxResponseOut>();

            InternalResult result = await APIRequestHandler.GetResponse(Model.signOnRq == null ? null : Model.signOnRq, Model,
                                                                        _config["Statement:BaseUrl"].ToString(),
                                                                        _config["Statement:TrxListLastNTrx"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.StatementSmart.TrxListLastNTrxResponseOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("CBStatementRequest")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> CBStatementRequest([FromBody] CBStatementReqDto Model)
        {
            ActiveResponseSucces<DBHandler.Model.StatementSmart.CBStatementResponseOut> response = new ActiveResponseSucces<DBHandler.Model.StatementSmart.CBStatementResponseOut>();

            InternalResult result = await APIRequestHandler.GetResponse(Model.signOnRq == null ? null : Model.signOnRq, Model,
                                                                        _config["Statement:BaseUrl"].ToString(),
                                                                        _config["Statement:CBStatementRequest"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.StatementSmart.CBStatementResponseOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("MonthlyStatement")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> MonthlyStatement([FromBody] MonthlyStatementDto Model)
        {
            ActiveResponseSucces<DBHandler.Model.StatementSmart.MonthlyStatementResponseOut> response = new ActiveResponseSucces<DBHandler.Model.StatementSmart.MonthlyStatementResponseOut>();

            InternalResult result = await APIRequestHandler.GetResponse(Model.signOnRq == null ? null : Model.signOnRq, Model,
                                                                        _config["Statement:BaseUrl"].ToString(),
                                                                        _config["Statement:MonthlyStatement"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.StatementSmart.MonthlyStatementResponseOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("VATAdvice")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> VATAdvice([FromBody] VATAdviceDto Model)
        {
            ActiveResponseSucces<DBHandler.Model.StatementSmart.VATAdviceResponseOut> response = new ActiveResponseSucces<DBHandler.Model.StatementSmart.VATAdviceResponseOut>();

            InternalResult result = await APIRequestHandler.GetResponse(Model.signOnRq == null ? null : Model.signOnRq, Model,
                                                                        _config["Statement:BaseUrl"].ToString(),
                                                                        _config["Statement:VATAdvice"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.StatementSmart.VATAdviceResponseOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("MonthlyVATAdvice")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> MonthlyVATAdvice([FromBody] MonthlyVATAdviceDto Model)
        {
            ActiveResponseSucces<DBHandler.Model.StatementSmart.MonthlyVATAdviceResponseOut> response = new ActiveResponseSucces<DBHandler.Model.StatementSmart.MonthlyVATAdviceResponseOut>();

            InternalResult result = await APIRequestHandler.GetResponse(Model.signOnRq == null ? null : Model.signOnRq, Model,
                                                                        _config["Statement:BaseUrl"].ToString(),
                                                                        _config["Statement:MonthlyVATAdvice"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.StatementSmart.MonthlyVATAdviceResponseOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("TrxAdvice")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> TrxAdvice([FromBody] TrxAdviceDto Model)
        {
            ActiveResponseSucces<DBHandler.Model.StatementSmart.TrxAdviceResponseOut> response = new ActiveResponseSucces<DBHandler.Model.StatementSmart.TrxAdviceResponseOut>();

            InternalResult result = await APIRequestHandler.GetResponse(Model.signOnRq == null ? null : Model.signOnRq, Model,
                                                                        _config["Statement:BaseUrl"].ToString(),
                                                                        _config["Statement:TrxAdvice"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.StatementSmart.TrxAdviceResponseOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("DebitAdvice")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> DebitAdvice([FromBody] DebitAdviceDto Model)
        {
            ActiveResponseSucces<DBHandler.Model.StatementSmart.DebitAdviceResponseOut> response = new ActiveResponseSucces<DBHandler.Model.StatementSmart.DebitAdviceResponseOut>();

            InternalResult result = await APIRequestHandler.GetResponse(Model.signOnRq == null ? null : Model.signOnRq, Model,
                                                                        _config["Statement:BaseUrl"].ToString(),
                                                                        _config["Statement:DebitAdvice"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.StatementSmart.DebitAdviceResponseOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
    }
}
