﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CoreHandler;
using DBHandler.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using DBHandler.Repositories;
using DBHandler.Model;
using Microsoft.Extensions.Configuration;
using DBHandler.Model.Dtos;
using DBHandler.Enum;
using HttpHandler;
using VendorApi.Helper;
using CommonModel;
using Newtonsoft.Json.Linq;
using ECSFinHandler;
using Swashbuckle.AspNetCore.Annotations;


namespace VendorApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NestController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private readonly ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;

        public NestController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs)
        {
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
        }

        [Route("CreateUser")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<NestHandler.CommonResponse> ", typeof(ActiveResponseSucces<NestHandler.CommonResponse>))]
        public async Task<IActionResult> CreateUser([FromBody] NestHandler.CreateUser.CreateUserModel Model)
        {
            ActiveResponseSucces<NestHandler.CommonResponse> response = new ActiveResponseSucces<NestHandler.CommonResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObject,
                                                                        _config["NEST:BaseUrl"].ToString(),
                                                                        _config["NEST:CreateUser"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<NestHandler.CommonResponse>(result.httpResult.httpResponse);

                if (data != null && data.CreateUserResult != null && data.CreateUserResult.STATUS == "FAILED")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = data.CreateUserResult.RefereceId, StatusMessage = data.CreateUserResult.ERROR_DESCRIPTION};

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("UpdateUser")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<NestHandler.CommonResponse> ", typeof(ActiveResponseSucces<NestHandler.CommonResponse>))]
        public async Task<IActionResult> UpdateUser([FromBody] NestHandler.CreateUser.CreateUserModel Model)
        {
            ActiveResponseSucces<NestHandler.CommonResponse> response = new ActiveResponseSucces<NestHandler.CommonResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObject,
                                                                        _config["NEST:BaseUrl"].ToString(),
                                                                        _config["NEST:CreateUser"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<NestHandler.CommonResponse>(result.httpResult.httpResponse);

                if (data != null && data.CreateUserResult != null && data.CreateUserResult.STATUS == "FAILED")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = data.CreateUserResult.RefereceId, StatusMessage = data.CreateUserResult.ERROR_DESCRIPTION };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("DeleteUser")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<NestHandler.CommonResponse> ", typeof(ActiveResponseSucces<NestHandler.CommonResponse>))]
        public async Task<IActionResult> DeleteUser([FromBody] NestHandler.DeleteUser.DeleteUserModel Model)
        {
            ActiveResponseSucces<NestHandler.CommonResponse> response = new ActiveResponseSucces<NestHandler.CommonResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObject,
                                                                        _config["NEST:BaseUrl"].ToString(),
                                                                        _config["NEST:CreateUser"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<NestHandler.CommonResponse>(result.httpResult.httpResponse);

                if (data != null && data.CreateUserResult != null && data.CreateUserResult.STATUS == "FAILED")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = data.CreateUserResult.RefereceId, StatusMessage = data.CreateUserResult.ERROR_DESCRIPTION };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }


        [Route("GetWorkFlowForCorporate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<NestHandler.GetWorkFlowForCorporate.ResponseRootObject> ", typeof(ActiveResponseSucces<NestHandler.GetWorkFlowForCorporate.ResponseRootObject>))]
        public async Task<IActionResult> GetWorkFlowForCorporate([FromBody] NestHandler.GetWorkFlowForCorporate.GetWorkFlowForCorporateModel Model)
        {
            ActiveResponseSucces<NestHandler.GetWorkFlowForCorporate.ResponseRootObject> response = new ActiveResponseSucces<NestHandler.GetWorkFlowForCorporate.ResponseRootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObject,
                                                                        _config["NEST:BaseUrl"].ToString(),
                                                                        _config["NEST:GetWorkFlowForCorporate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<NestHandler.GetWorkFlowForCorporate.ResponseRootObject>(result.httpResult.httpResponse);

                if (data != null && data.GetWorkFlowForCorporateResult != null && data.GetWorkFlowForCorporateResult.STATUS.ToLower().StartsWith("failed"))
                {
                    response.Status = new Status { Severity = Severity.Error, Code = data.GetWorkFlowForCorporateResult.REFERENCE_ID, StatusMessage = data.GetWorkFlowForCorporateResult.ERROR_DESCRIPTION };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("Update002Response")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<NestHandler.Update002Response.ResponseObject> ", typeof(ActiveResponseSucces<NestHandler.Update002Response.ResponseObject>))]
        public async Task<IActionResult> Update002Response([FromBody] NestHandler.Update002Response.Update002ResponseModel Model)
        {
            ActiveResponseSucces<NestHandler.Update002Response.ResponseObject> response = new ActiveResponseSucces<NestHandler.Update002Response.ResponseObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObject,
                                                                        _config["NEST:Update002URL"].ToString(),
                                                                        _config["NEST:Update002Response"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 2);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<NestHandler.Update002Response.ResponseObject>(result.httpResult.httpResponse);

                if (data != null && data.ErrorCode != null && data.ErrorCode.ToLower().StartsWith("err"))
                {
                    response.Status = new Status { Severity = Severity.Error, Code = data.ErrorCode, StatusMessage = data.ErrorDescription};

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }


        [Route("AuthenticateSSO")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<NestHandler.AuthenticateSSOModel.RootObjectResponse> ", typeof(ActiveResponseSucces<NestHandler.AuthenticateSSOModel.RootObjectResponse>))]
        public async Task<IActionResult> AuthenticateSSO([FromBody] NestHandler.AuthenticateSSOModel.AuthenticateSSOModel Model)
        {
            ActiveResponseSucces<NestHandler.AuthenticateSSOModel.RootObjectResponse> response = new ActiveResponseSucces<NestHandler.AuthenticateSSOModel.RootObjectResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObject,
                                                                        _config["NEST:AuthSSOURL"].ToString(),
                                                                        _config["NEST:AuthenticateSSO"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 2);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;

            response.Status = result.Status;
            if (result.httpResult.severity== "Success")
            {
                var data = JsonConvert.DeserializeObject<NestHandler.AuthenticateSSOModel.RootObjectResponse>(result.httpResult.httpResponse);
                response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                response.Content = data;
                return Ok(response);
            }
            return BadRequest(response);
        }

        [Route("CreateCorporate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<NestHandler.CreateCorporate.ResponseObject> ", typeof(ActiveResponseSucces<NestHandler.CreateCorporate.ResponseObject>))]
        public async Task<IActionResult> CreateCorporate([FromBody] NestHandler.CreateCorporate.CreateCorporateModel Model)
        {
            ActiveResponseSucces<NestHandler.CreateCorporate.ResponseObject> response = new ActiveResponseSucces<NestHandler.CreateCorporate.ResponseObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObject,
                                                                        _config["NEST:BaseUrl"].ToString(),
                                                                        _config["NEST:CreateCorporate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 2);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;

            response.Status = result.Status;
            if (result.httpResult != null && result.httpResult.severity == "Success")
            {
                var data = JsonConvert.DeserializeObject<NestHandler.CreateCorporate.ResponseObject>(result.httpResult.httpResponse);
                if (data != null && data.CreateCorporateResult != null && data.CreateCorporateResult.STATUS == "FAILED")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = data.CreateCorporateResult.ReferenceId, StatusMessage = data.CreateCorporateResult.ERROR_DESCRIPTION };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("Update_NonMOLStatus")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<NestHandler.CreateCorporate.ResponseObject> ", typeof(ActiveResponseSucces<NestHandler.CreateCorporate.ResponseObject>))]
        public async Task<IActionResult> Update_NonMOLStatus([FromBody] NestHandler.Update_NonMOLStatus.Update_NonMOLStatusModel Model)
        {
            ActiveResponseSucces<NestHandler.Update_NonMOLStatus.ResponseObject> response = new ActiveResponseSucces<NestHandler.Update_NonMOLStatus.ResponseObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObject,
                                                                        _config["NEST:BaseUrl"].ToString(),
                                                                        _config["NEST:Update_NonMOLStatus"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 2);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;

            response.Status = result.Status;
            if (result.httpResult != null && result.httpResult.severity == "Success")
            {
                var data = JsonConvert.DeserializeObject<NestHandler.Update_NonMOLStatus.ResponseObject>(result.httpResult.httpResponse);
                if (data != null && data.Update_NonMOLStatusResult != null && data.Update_NonMOLStatusResult.Status == "FAILED")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = data.Update_NonMOLStatusResult.ReferenceID, StatusMessage = data.Update_NonMOLStatusResult.ERROR_DESCRIPTION };

                }
                else if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
    }
}