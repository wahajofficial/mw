﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CoreHandler;
using DBHandler.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using DBHandler.Repositories;
using DBHandler.Model;
using Microsoft.Extensions.Configuration;
using DBHandler.Model.Dtos;
using DBHandler.Enum;
using HttpHandler;
using VendorApi.Helper;
using CommonModel;
using Newtonsoft.Json.Linq;
using Swashbuckle.AspNetCore.Annotations;
using ERCCorporateHandler.ListOfWPSAccounts;
using ERCCorporateHandler;

namespace VendorApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ERCCorporateController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private readonly ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;

        public ERCCorporateController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs)
        {
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
        }

        [Route("ListOfWPSAccounts")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ERCCorporateHandler.ListOfWPSAccounts.RootObject>", typeof(ActiveResponseSucces<List<ERCCorporateHandler.ListOfWPSAccounts.RootObject>>))]
        public async Task<IActionResult> ListOfWPSAccounts([FromBody] ListOfWPSAccountsModelRequest Model)
        {
            ActiveResponseSucces<List<ERCCorporateHandler.ListOfWPSAccounts.RootObject>> response = new ActiveResponseSucces<List<ERCCorporateHandler.ListOfWPSAccounts.RootObject>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObjectRequest,
                                                                        _config["ERCCorporate:BaseUrl"].ToString(),
                                                                        _config["ERCCorporate:ListOfWPSAccounts"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler,4);
            response.LogId = Model.SignOnRq.LogId;
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.Status = result.Status;
            if (result.httpResult == null ||result.httpResult.severity == "failure")
            {
                if (result.httpResult != null  && result.httpResult.httpResponse != null  && result.httpResult.httpResponse.Contains("ModelState"))
                {
                    ActiveResponseSucces<ERCCorporateHandler.ListOfWPSAccounts.RootObject2> failResponse = new ActiveResponseSucces<ERCCorporateHandler.ListOfWPSAccounts.RootObject2>();
                    failResponse.LogId = Model.SignOnRq.LogId;
                    failResponse.RequestDateTime = Model.SignOnRq.DateTime;

                    var d = JsonConvert.DeserializeObject<ERCCorporateHandler.ListOfWPSAccounts.RootObject2>(result.httpResult.httpResponse);
                    failResponse.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = "ERROR" };

                    failResponse.Content = d;
                    return Ok(failResponse);
                }
                else if (response.Status != null && response.Status.Code != "")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = response.Status.Code, StatusMessage = response.Status.StatusMessage};
                }
                else
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = "ERROR" };
                }
            }
            else
            {
                if (result.httpResult.httpResponse.Contains("ModelState"))
                {
                    ActiveResponseSucces<ERCCorporateHandler.ListOfWPSAccounts.RootObject2> failResponse = new ActiveResponseSucces<ERCCorporateHandler.ListOfWPSAccounts.RootObject2>();
                    failResponse.LogId = Model.SignOnRq.LogId;
                    failResponse.RequestDateTime = Model.SignOnRq.DateTime;

                    var d = JsonConvert.DeserializeObject<ERCCorporateHandler.ListOfWPSAccounts.RootObject2>(result.httpResult.httpResponse);
                    failResponse.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = "ERROR"};

                    failResponse.Content = d;
                    return Ok(failResponse);
                }
                else
                {
                    var data = JsonConvert.DeserializeObject<List<ERCCorporateHandler.ListOfWPSAccounts.RootObject>>(result.httpResult.httpResponse);
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }


        [Route("QueueForAuth")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ERCCorporateHandler.QueueForAuth.RootObject>", typeof(ActiveResponseSucces<ERCCorporateHandler.QueueForAuth.RootObject>))]
        public async Task<IActionResult> QueueForAuth([FromBody] ERCCorporateHandler.QueueForAuth.QueueForAuthRequest Model)
        {
            ActiveResponseSucces<ERCCorporateHandler.QueueForAuth.RootObject> response = new ActiveResponseSucces<ERCCorporateHandler.QueueForAuth.RootObject>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObjectRequest,
                                                                        _config["ERCCorporate:BaseUrl"].ToString(),
                                                                        _config["ERCCorporate:QueueForAuth"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 4);
            response.LogId = Model.SignOnRq.LogId;
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.Status = result.Status;
            if (result.httpResult == null || result.httpResult.severity == "failure")
            {
                if (result.httpResult != null && result.httpResult.httpResponse != null && result.httpResult.httpResponse.Contains("ModelState"))
                {
                    ActiveResponseSucces<ERCCorporateHandler.ListOfWPSAccounts.RootObject2> failResponse = new ActiveResponseSucces<ERCCorporateHandler.ListOfWPSAccounts.RootObject2>();
                    failResponse.LogId = Model.SignOnRq.LogId;
                    failResponse.RequestDateTime = Model.SignOnRq.DateTime;

                    var d = JsonConvert.DeserializeObject<ERCCorporateHandler.ListOfWPSAccounts.RootObject2>(result.httpResult.httpResponse);
                    failResponse.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = "ERROR" };

                    failResponse.Content = d;
                    return Ok(failResponse);
                }
                else if (response.Status != null && response.Status.Code != "")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = response.Status.Code, StatusMessage = response.Status.StatusMessage };
                }
                else
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = "ERROR" };
                }
            }
            else
            {
                if (result.httpResult.httpResponse.Contains("ModelState"))
                {
                    ActiveResponseSucces<ERCCorporateHandler.ListOfWPSAccounts.RootObject2> failResponse = new ActiveResponseSucces<ERCCorporateHandler.ListOfWPSAccounts.RootObject2>();
                    failResponse.LogId = Model.SignOnRq.LogId;
                    failResponse.RequestDateTime = Model.SignOnRq.DateTime;

                    var d = JsonConvert.DeserializeObject<ERCCorporateHandler.ListOfWPSAccounts.RootObject2>(result.httpResult.httpResponse);
                    failResponse.Status = new Status { Severity = Severity.Success, Code = "Err-0001", StatusMessage = "ERROR" };

                    failResponse.Content = d;
                    return Ok(failResponse);
                }
                else
                {
                    var data = JsonConvert.DeserializeObject<ERCCorporateHandler.QueueForAuth.RootObject>(result.httpResult.httpResponse);
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("ValidateTOTP")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ERCCorporateHandler.QueueForAuth.RootObject>", typeof(ActiveResponseSucces<ERCCorporateHandler.Response>))]
        public async Task<IActionResult> ValidateTOTP([FromBody] ValidateTOTPModel Model)
        {
            ActiveResponseSucces<ERCCorporateHandler.Response> response = new ActiveResponseSucces<ERCCorporateHandler.Response>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.ValidateTOTP,
                                                                        _config["ERCCorporate:BaseUrl"].ToString(),
                                                                        _config["ERCCorporate:ValidateTOTP"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 4);
            response.LogId = Model.SignOnRq.LogId;
            response.RequestDateTime = Model.SignOnRq.DateTime;
            if (result.httpResult == null || result.httpResult.severity == "failure")
            {
                if (response.Status != null && response.Status.Code != "")
                {
                    response.Status = new Status { Severity = Severity.Error, Code = response.Status.Code, StatusMessage = response.Status.StatusMessage };
                }
                else
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = "ERROR" };
                }
            }
            else
            {
                var data = JsonConvert.DeserializeObject<ERCCorporateHandler.Response>(result.httpResult.httpResponse);
                response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                response.Content = data;
                return Ok(response);
            }

            return BadRequest(response);
        }
    }
}