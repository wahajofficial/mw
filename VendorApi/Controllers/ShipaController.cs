﻿using System.Net.Http;
using System.Threading.Tasks;
using DBHandler.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using DBHandler.Repositories;
using Microsoft.Extensions.Configuration;
using DBHandler.Enum;
using VendorApi.Helper;
using Swashbuckle.AspNetCore.Annotations;
using ShipaHandler.StatusUpdate;
using ShipaHandler.AWBValidation;
using ShipaHandler.OrderManagement;
using ShipaHandler;

namespace VendorApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShipaController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private readonly ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;

        public ShipaController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs)
        {
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
        }

        [Route("UpdateStatus")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<StatusUpdateModel>", typeof(ActiveResponseSucces<ShipaHandler.AWBValidation.RootObject>))]
        public async Task<IActionResult> UpdateStatus([FromBody] StatusUpdateModel Model)
        {
            ShipaHandler.AWBValidation.RootObject response = new ShipaHandler.AWBValidation.RootObject();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["Retail:BaseUrl"].ToString(),
                                                                        _config["Retail:ShipaStatusUpdate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler,5);
            response = JsonConvert.DeserializeObject<ShipaHandler.AWBValidation.RootObject>(result.httpResult.httpResponse);
            if (response.response.message == "Success")
            {
                return Ok(response);
            }
            else
            {
                return BadRequest(response);
            }
        }

        [Route("AWBValidation")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<StatusUpdateModel>", typeof(ActiveResponseSucces<ShipaHandler.AWBValidation.RootObject>))]
        public async Task<IActionResult> AWBValidation([FromBody] AWBValidationModel Model)
        {
            ShipaHandler.AWBValidation.RootObject response = new ShipaHandler.AWBValidation.RootObject();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["Retail:BaseUrl"].ToString(),
                                                                        _config["Retail:AWBValidation"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler,5);
            // response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            // response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;
            response = JsonConvert.DeserializeObject<ShipaHandler.AWBValidation.RootObject>(result.httpResult.httpResponse);
            if (response.response.message == "Success")
            {
                return Ok(response);
            }
            else
            {
                return BadRequest(response);
            }
        }

        [Route("OrderManagement")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ResponseObject>", typeof(ActiveResponseSucces<ResponseObject>))]
        public async Task<IActionResult> OrderManagement([FromBody] OrderManagementModel Model)
        {
           
            ActiveResponseSucces<ResponseObject> response = new ActiveResponseSucces<ResponseObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObject,
                                                                        _config["Shipa:BaseUrl"].ToString(),
                                                                        _config["Shipa:OrderManagement"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler,
                                                                        6);

            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                ResponseObject obj = JsonConvert.DeserializeObject<ResponseObject>(result.httpResult.httpResponse);
                response.Content = obj;
                if (result != null && result.httpResult?.severity == "Success" && obj.status == "success")
                {
                    response.Status = new DBHandler.Helper.Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return Ok(response);
                }
                else
                {
                    response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
                    return BadRequest(response);
                }

            }

            return BadRequest(response);
        }

        [Route("TrackOrder")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ResponseObject>", typeof(ActiveResponseSucces<ShipaHandler.TrackOrder.ResponseRootObject>))]
        public async Task<IActionResult> TrackOrder([FromBody] ShipaHandler.TrackOrder.TrackOrderModel Model)
        {

            ActiveResponseSucces<ShipaHandler.TrackOrder.ResponseRootObject> response = new ActiveResponseSucces<ShipaHandler.TrackOrder.ResponseRootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObject,
                                                                        _config["Shipa:BaseUrl"].ToString(),
                                                                        _config["Shipa:OrderManagement"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler,
                                                                        6);

            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                ShipaHandler.TrackOrder.ResponseRootObject obj = JsonConvert.DeserializeObject<ShipaHandler.TrackOrder.ResponseRootObject>(result.httpResult.httpResponse);
                response.Content = obj;
                if (result != null && result.httpResult?.severity == "Success" && obj.status == "success")
                {
                    response.Status = new DBHandler.Helper.Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return Ok(response);
                }
                else
                {
                    response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
                    return BadRequest(response);
                }

            }

            return BadRequest(response);
        }

        [Route("CancelOrder")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ResponseObject>", typeof(ActiveResponseSucces<ShipaHandler.TrackOrder.ResponseRootObject>))]
        public async Task<IActionResult> CancelOrder([FromBody] ShipaHandler.CancelOrder.CancelOrderModel Model)
        {

            ActiveResponseSucces<ShipaHandler.TrackOrder.ResponseRootObject> response = new ActiveResponseSucces<ShipaHandler.TrackOrder.ResponseRootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObject,
                                                                        _config["Shipa:BaseUrl"].ToString(),
                                                                        _config["Shipa:OrderManagement"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler,
                                                                        6);

            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                ShipaHandler.TrackOrder.ResponseRootObject obj = JsonConvert.DeserializeObject<ShipaHandler.TrackOrder.ResponseRootObject>(result.httpResult.httpResponse);
                response.Content = obj;
                if (result != null && result.httpResult?.severity == "Success" && obj.status == "success")
                {
                    response.Status = new DBHandler.Helper.Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return Ok(response);
                }
                else
                {
                    response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
                    return BadRequest(response);
                }

            }

            return BadRequest(response);
        }
    }
}