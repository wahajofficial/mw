﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net.Http;
using CoreHandler;
using DBHandler.Helper;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using DBHandler.Repositories;
using DBHandler.Model;
using Microsoft.Extensions.Configuration;
using DBHandler.Model.Dtos;
using DBHandler.Enum;
using HttpHandler;
using VendorApi.Helper;
using CommonModel;
using Newtonsoft.Json.Linq;
using IMTFHandler;
using Swashbuckle.AspNetCore.Annotations;

namespace VendorApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IMTFController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private readonly ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;


        public IMTFController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs)
        {
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
        }

        [Route("IncomingFinTrx")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>", typeof(ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>))]
        public async Task<IActionResult> IncomingFinTrx([FromBody] IMTFHandler.IncomingFinTrx.IncomingFinTrxModel Model)
        {
            ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject> response = new ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                    _config["IMTF:BaseUrl"].ToString(),
                                                                    _config["IMTF:transaction"].ToString(),
                                                                    level,
                                                                    _logs,
                                                                    _userChannels,
                                                                    this,
                                                                    _config,
                                                                    httpHandler,3);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<IMTFHandler.CommonResponse.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.responseDataObject.responseCode != null && data.responseDataObject.responseCode == "SUC")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);

                }
                else
                {
                    if (data == null)
                    {
                        response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = "FAIL" };
                    }
                    else
                    {
                        response.Status = new Status { Severity = Severity.Error, Code = data.responseDataObject.responseCode, StatusMessage = data.responseDataObject.responseMessage };
                        response.Content = data;
                    }
                }
            }
            return BadRequest(response);
        }
       
        [Route("InternalFinTrx")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject> ", typeof(ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>))]
        public async Task<IActionResult> InternalFinTrx([FromBody] IMTFHandler.InternalFinTrx.InternalFinTrxModel Model)
        {
            ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject> response = new ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                    _config["IMTF:BaseUrl"].ToString(),
                                                                    _config["IMTF:transaction"].ToString(),
                                                                    level,
                                                                    _logs,
                                                                    _userChannels,
                                                                    this,
                                                                    _config,
                                                                    httpHandler, 3);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<IMTFHandler.CommonResponse.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.responseDataObject.responseCode != null && data.responseDataObject.responseCode == "SUC")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                    
                }
                if (data == null)
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = "FAIL" };
                }
                else
                {
                    response.Status = new Status { Severity = Severity.Error, Code = data.responseDataObject.responseCode, StatusMessage = data.responseDataObject.responseMessage };
                    response.Content = data;
                }
            }
            return BadRequest(response);
        }
       
        [Route("OutgoingFinTrx")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>", typeof(ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>))]
        public async Task<IActionResult> OutgoingFinTrx([FromBody] IMTFHandler.OutgoingFinTrx.OutgoingFinTrxModel Model)
        {
            ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject> response = new ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                    _config["IMTF:BaseUrl"].ToString(),
                                                                    _config["IMTF:transaction"].ToString(),
                                                                    level,
                                                                    _logs,
                                                                    _userChannels,
                                                                    this,
                                                                    _config,
                                                                    httpHandler, 3);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<IMTFHandler.CommonResponse.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.responseDataObject.responseCode != null && data.responseDataObject.responseCode == "SUC")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);

                }
                if (data == null)
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = "FAIL" };
                }
                else
                {
                    response.Status = new Status { Severity = Severity.Error, Code = data.responseDataObject.responseCode, StatusMessage = data.responseDataObject.responseMessage };
                    response.Content = data;
                }
            }
            return BadRequest(response);
        }

        [Route("ScreeningLegalPersonModel")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>", typeof(ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>))]
        public async Task<IActionResult> ScreeningLegalPersonModel([FromBody] IMTFHandler.ScreeningLegalPerson.ScreeningLegalPersonModel Model)
        {
            ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject> response = new ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObject,
                                                                    _config["IMTF:BaseUrl"].ToString(),
                                                                    _config["IMTF:customer"].ToString(),
                                                                    level,
                                                                    _logs,
                                                                    _userChannels,
                                                                    this,
                                                                    _config,
                                                                    httpHandler, 3);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<IMTFHandler.CommonResponse.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.responseDataObject.responseCode != null && data.responseDataObject.responseCode == "SUC")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);

                }
                else
                {
                    if (data == null)
                    {
                        response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = "FAIL" };
                    }
                    else
                    {
                        response.Status = new Status { Severity = Severity.Error, Code = data.responseDataObject.responseCode, StatusMessage = data.responseDataObject.responseMessage};
                        response.Content = data;
                    }
                }
            }
            return BadRequest(response);
        }


        [Route("ScreeningNaturalPersonModel")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>", typeof(ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>))]
        public async Task<IActionResult> ScreeningNaturalPersonModel([FromBody] IMTFHandler.ScreeningNaturalPerson.ScreeningNaturalPersonModel Model)
        {
            ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject> response = new ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObject,
                                                                    _config["IMTF:BaseUrl"].ToString(),
                                                                    _config["IMTF:customer"].ToString(),
                                                                    level,
                                                                    _logs,
                                                                    _userChannels,
                                                                    this,
                                                                    _config,
                                                                    httpHandler, 3);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<IMTFHandler.CommonResponse.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.responseDataObject.responseCode != null && data.responseDataObject.responseCode == "SUC")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);

                }
                if (data == null)
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = "FAIL" };
                }
                else
                {
                    response.Status = new Status { Severity = Severity.Error, Code = data.responseDataObject.responseCode, StatusMessage = data.responseDataObject.responseMessage };
                    response.Content = data;
                }
            }
            return BadRequest(response);
        }


        [Route("CustUpdtNatPerson")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>", typeof(ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>))]
        public async Task<IActionResult> CustUpdtNatPerson([FromBody] IMTFHandler.CustUpdtNatPerson.CustUpdtNatPersonModel Model)
        {
            ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject> response = new ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObject,
                                                                    _config["IMTF:BaseUrl"].ToString(),
                                                                    _config["IMTF:customer"].ToString(),
                                                                    level,
                                                                    _logs,
                                                                    _userChannels,
                                                                    this,
                                                                    _config,
                                                                    httpHandler, 3);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<IMTFHandler.CommonResponse.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.responseDataObject.responseCode != null && data.responseDataObject.responseCode == "SUC")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);

                }
                else
                {
                    if (data == null)
                    {
                        response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = "FAIL" };
                    }
                    else
                    {
                        response.Status = new Status { Severity = Severity.Error, Code = data.responseDataObject.responseCode, StatusMessage = data.responseDataObject.responseMessage };
                        response.Content = data;
                    }
                }
            }
            return BadRequest(response);
        }


        [Route("CardTransaction")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject> ", typeof(ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>))]
        public async Task<IActionResult> CardTransaction([FromBody] IMTFHandler.CardTransaction.CardTransactionModel Model)
        {
            ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject> response = new ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObject,
                                                                    _config["IMTF:BaseUrl"].ToString(),
                                                                    _config["IMTF:transaction"].ToString(),
                                                                    level,
                                                                    _logs,
                                                                    _userChannels,
                                                                    this,
                                                                    _config,
                                                                    httpHandler, 3);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<IMTFHandler.CommonResponse.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.responseDataObject.responseCode != null && data.responseDataObject.responseCode == "SUC")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);

                }
                if (data == null)
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = "FAIL" };
                }
                else
                {
                    response.Status = new Status { Severity = Severity.Error, Code = data.responseDataObject.responseCode, StatusMessage = data.responseDataObject.responseMessage };
                    response.Content = data;
                }
            }
            return BadRequest(response);
        }


        [Route("MT202")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject> ", typeof(ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>))]
        public async Task<IActionResult> MT202([FromBody] IMTFHandler.MT202.MT202Model Model)
        {
            ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject> response = new ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObject,
                                                                    _config["IMTF:BaseUrl"].ToString(),
                                                                    _config["IMTF:transaction"].ToString(),
                                                                    level,
                                                                    _logs,
                                                                    _userChannels,
                                                                    this,
                                                                    _config,
                                                                    httpHandler, 3);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<IMTFHandler.CommonResponse.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.responseDataObject.responseCode != null && data.responseDataObject.responseCode == "SUC")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);

                }
                if (data == null)
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = "FAIL" };
                }
                else
                {
                    response.Status = new Status { Severity = Severity.Error, Code = data.responseDataObject.responseCode, StatusMessage = data.responseDataObject.responseMessage };
                    response.Content = data;
                }
            }
            return BadRequest(response);
        }

        [Route("MT210")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject> ", typeof(ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>))]
        public async Task<IActionResult> MT210([FromBody] IMTFHandler.MT210.MT210Model Model)
        {
            ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject> response = new ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObject,
                                                                    _config["IMTF:BaseUrl"].ToString(),
                                                                    _config["IMTF:transaction"].ToString(),
                                                                    level,
                                                                    _logs,
                                                                    _userChannels,
                                                                    this,
                                                                    _config,
                                                                    httpHandler, 3);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<IMTFHandler.CommonResponse.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.responseDataObject.responseCode != null && data.responseDataObject.responseCode == "SUC")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);

                }
                if (data == null)
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = "FAIL" };
                }
                else
                {
                    response.Status = new Status { Severity = Severity.Error, Code = data.responseDataObject.responseCode, StatusMessage = data.responseDataObject.responseMessage };
                    response.Content = data;
                }
            }
            return BadRequest(response);
        }


        [Route("BillPaymentScreening")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject> ", typeof(ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>))]
        public async Task<IActionResult> BillPaymentScreening([FromBody] IMTFHandler.BillPaymentScreening.BillPaymentScreeningModel Model)
        {
            ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject> response = new ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObject,
                                                                    _config["IMTF:BaseUrl"].ToString(),
                                                                    _config["IMTF:transaction"].ToString(),
                                                                    level,
                                                                    _logs,
                                                                    _userChannels,
                                                                    this,
                                                                    _config,
                                                                    httpHandler, 3);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<IMTFHandler.CommonResponse.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.responseDataObject.responseCode != null && data.responseDataObject.responseCode == "SUC")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);

                }
                if (data == null)
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = "FAIL" };
                }
                else
                {
                    response.Status = new Status { Severity = Severity.Error, Code = data.responseDataObject.responseCode, StatusMessage = data.responseDataObject.responseMessage };
                    response.Content = data;
                }
            }
            return BadRequest(response);
        }

        [Route("Sal_Internal_ExternalCustomer")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject> ", typeof(ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>))]
        public async Task<IActionResult> Sal_Internal_ExternalCustomer([FromBody] IMTFHandler.Sal_Internal_ExternalCustomer.Sal_Internal_ExternalCustomerModel Model)
        {
            ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject> response = new ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObject,
                                                                    _config["IMTF:BaseUrl"].ToString(),
                                                                    _config["IMTF:transaction"].ToString(),
                                                                    level,
                                                                    _logs,
                                                                    _userChannels,
                                                                    this,
                                                                    _config,
                                                                    httpHandler, 3);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<IMTFHandler.CommonResponse.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.responseDataObject.responseCode != null && data.responseDataObject.responseCode == "SUC")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);

                }
                if (data == null)
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = "FAIL" };
                }
                else
                {
                    response.Status = new Status { Severity = Severity.Error, Code = data.responseDataObject.responseCode, StatusMessage = data.responseDataObject.responseMessage };
                    response.Content = data;
                }
            }
            return BadRequest(response);
        }

        [Route("Sal_External_InternalCustomer")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject> ", typeof(ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>))]
        public async Task<IActionResult> Sal_External_InternalCustomer([FromBody] IMTFHandler.Sal_External_InternalCustomer.Sal_External_InternalCustomerModel Model)
        {
            ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject> response = new ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObject,
                                                                    _config["IMTF:BaseUrl"].ToString(),
                                                                    _config["IMTF:transaction"].ToString(),
                                                                    level,
                                                                    _logs,
                                                                    _userChannels,
                                                                    this,
                                                                    _config,
                                                                    httpHandler, 3);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<IMTFHandler.CommonResponse.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.responseDataObject.responseCode != null && data.responseDataObject.responseCode == "SUC")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);

                }
                if (data == null)
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = "FAIL" };
                }
                else
                {
                    response.Status = new Status { Severity = Severity.Error, Code = data.responseDataObject.responseCode, StatusMessage = data.responseDataObject.responseMessage };
                    response.Content = data;
                }
            }
            return BadRequest(response);
        }

        [Route("Sal_Internal_InternalCustomer")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject> ", typeof(ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>))]
        public async Task<IActionResult> Sal_Internal_InternalCustomer([FromBody] IMTFHandler.Sal_Internal_InternalCustomer.Sal_Internal_InternalCustomerModel Model)
        {
            ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject> response = new ActiveResponseSucces<IMTFHandler.CommonResponse.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObject,
                                                                    _config["IMTF:BaseUrl"].ToString(),
                                                                    _config["IMTF:transaction"].ToString(),
                                                                    level,
                                                                    _logs,
                                                                    _userChannels,
                                                                    this,
                                                                    _config,
                                                                    httpHandler, 3);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<IMTFHandler.CommonResponse.RootObject>(result.httpResult.httpResponse);
                if (data != null && data.responseDataObject.responseCode != null && data.responseDataObject.responseCode == "SUC")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);
                }
                if (data == null)
                {
                    response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = "FAIL" };
                }
                else
                {
                    response.Status = new Status { Severity = Severity.Error, Code = data.responseDataObject.responseCode, StatusMessage = data.responseDataObject.responseMessage };
                    response.Content = data;
                }
            }
            return BadRequest(response);
        }
    }
}