﻿using DBHandler.Model;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace VendorApi.MyAuth
{
    public class BasicAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        private UserManager<ApplicationUser> _userManager;
        private SignInManager<ApplicationUser> _signInManager;
      //  private string a="Invalid";            
        public BasicAuthenticationHandler(IOptionsMonitor<AuthenticationSchemeOptions> options, ILoggerFactory logger, UrlEncoder encoder,
            ISystemClock clock, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager) : base(options, logger, encoder, clock)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        protected override async Task HandleChallengeAsync(AuthenticationProperties properties)
        {
            Response.Headers["WWW-Authenticate"] = $"Basic realm=\"VendorApi\", charset=\"UTF-8\"";
           // Context.Response.Body.WriteAsync(Encoding.ASCII.GetBytes(a),0, Encoding.ASCII.GetBytes(a).Length);
            await base.HandleChallengeAsync(properties);
        }

        private void NewMethod()
        {
             Task.FromResult(Response);
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            AuthenticateResult result = null;
            if (!Request.Headers.ContainsKey("Authorization"))
                result= AuthenticateResult.Fail("Missing Authorization Header");


            try
            {
                var authHeader = AuthenticationHeaderValue.Parse(Request.Headers["Authorization"]);
                var credentialBytes = Convert.FromBase64String(authHeader.Parameter);
                var credentials = Encoding.UTF8.GetString(credentialBytes).Split(':');
                var username = credentials[0];
                var password = credentials[1];
                //user = await _userService.Authenticate(username, password);
                var user = await _userManager.FindByNameAsync(username);
                if (user == null)
                {
                    result= AuthenticateResult.Fail("Invalid Username or Password");
                }
                DateTime nowTime = DateTime.Now;
                if (user.LockoutEnd != null)
                {
                    DateTime? lockTime = DateTime.Parse(user.LockoutEnd.ToString());

                    if (lockTime > nowTime)
                    {
                      
                        result= AuthenticateResult.Fail($"Account is lockedout until {lockTime}");
                       // a = $"Account is lockedout until {lockTime}";
                       // Context.Response.Body.WriteAsync(Encoding.ASCII.GetBytes(a), 0, Encoding.ASCII.GetBytes(a).Length);
                    }
                }
                var _password = user.Id + password;
                var _result = await _signInManager.CheckPasswordSignInAsync(user, _password, true);
                if (_result.Succeeded)
                {
                    var claims = new[] {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
              //  new Claim(ClaimTypes.Name, user.Username),
            };
                    var identity = new ClaimsIdentity(claims, Scheme.Name);
                    var principal = new ClaimsPrincipal(identity);
                    var ticket = new AuthenticationTicket(principal, Scheme.Name);

                    result= AuthenticateResult.Success(ticket);
                }
                else
                {
                    result= AuthenticateResult.Fail("Invalid Username or Password");
                }
               
               

            }
            catch(Exception ex)
            {
                result= AuthenticateResult.Fail("Invalid Authorization Header");
            }
         
            return await Task.FromResult(result);

        }
      
    }
}
