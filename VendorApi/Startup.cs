﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;
using DBHandler.Enum;
using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Repositories;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Filters;
using VendorApi.MyAuth;

namespace VendorApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<VendorMWContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            //services.AddDbContext<TestMWContext>(options => options.UseSqlServer(Configuration.GetConnectionString("TestConnection")));
            services.AddIdentity<ApplicationUser, IdentityRole>(options =>
            {

                options.User.RequireUniqueEmail = false;
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 8;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = true;
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;
            })
              .AddEntityFrameworkStores<VendorMWContext>()
              .AddDefaultTokenProviders();

            #region Cors

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader();
                        //.AllowCredentials();
                    });
            });
            #endregion

            services.AddScoped<IDocumentsRepository, DocumentsRepository>();
            services.AddScoped<ILogRepository, LogRepository>();
            services.AddScoped<IUserChannelsRepository, UserChannelsRepository>();
            services.AddScoped<ITransactionImageRepository, TransactionImageRepository>();

            services.AddScoped<ILinkAccountRepository, LinkAccountRepository>();

            services.AddAuthentication("BasicAuthentication")
              .AddScheme<AuthenticationSchemeOptions, BasicAuthenticationHandler>("BasicAuthentication", o =>
               {


               });
            services.AddMvc(option => option.EnableEndpointRouting = false).AddNewtonsoftJson();
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });
            services.Configure<IISServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });
            services.Configure<KestrelServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });


            //services.Configure<IISServerOption=s>(options =>
            //{
            //    options.AutomaticAuthentication = false;
            //});
            //  services.AddSession();
            services.AddHttpClient();

            #region Swagger

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "Vendor API", Version = "v1", Description = "Restfull Api", Contact = new Microsoft.OpenApi.Models.OpenApiContact { Name = "", Email = "" } });
                //  c.SwaggerDoc("AppAdministration", new Info { Title = "App Administration API", Version = "v1.0" });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);

                c.EnableAnnotations();
                c.OperationFilter<SecurityRequirementsOperationFilter>();
                c.OperationFilter<AppendAuthorizeToSummaryOperationFilter>();
                c.CustomSchemaIds(x => x.FullName);
            });
            services.Configure<FormOptions>(x => x.MultipartBodyLengthLimit = 5368709120);
            services.AddSingleton<IFileProvider>(
             new PhysicalFileProvider(
                 Path.Combine(Directory.GetCurrentDirectory(), "Documents")));


            //net
            //services.AddSwaggerGen(c =>
            //{
            //    c.SwaggerDoc("v1", new Info
            //    {
            //        Version = "v1",
            //        Title = "API",
            //        Description = "Test API with ASP.NET Core 3.0",
            //        TermsOfService = "None",
            //        Contact = new Contact()
            //        {
            //            Name = "Dotnet Detail",
            //            Email = "dotnetdetail@gmail.com",
            //            Url = "www.dotnetdetail.net"
            //        },
            //        License = new License
            //        {
            //            Name = "ABC",
            //            Url = "www.dotnetdetail.net"
            //        },
            //    });

            //});

        }
        #endregion



        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseSwagger();

            ////Enable middleware to serve swagger - ui(HTML, JS, CSS, etc.), 
            //// specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                //c.EnableAnnotations();
            });
            //app.UseWhen(x => (x.Request.Path.StartsWithSegments("/api", StringComparison.OrdinalIgnoreCase)),
            // builder =>
            // {
            //     builder.UseMiddleware<AuthenticationMiddleware>();
            // });
            //app.UseDeveloperExceptionPage();
            app.UseExceptionHandler(a => a.Run(async context =>
            {
                var starttime = DateTime.Now;
                var routeData = context.GetRouteData().Values;
                string currentController = routeData["controller"]?.ToString();
                string currentAction = routeData["action"]?.ToString();
                var feature = context.Features.Get<IExceptionHandlerPathFeature>();
                var logId = Guid.NewGuid().ToString();
                ActiveResponseSucces<GloalException> response = new ActiveResponseSucces<GloalException>();
                response.Status = new Status { Severity = Severity.Error, StatusMessage = "Response is not as expected in " + currentAction + " function.", Code = "ERROR-01" };
                response.Content = null;
                response.LogId = logId;
                response.RequestDateTime = DateTime.Now;
                context.Response.ContentType = "application/json";

                var requestResponse = JsonConvert.SerializeObject(response);
                Log model = new Log();
                model.Application = "Global Handler Exception";
                model.FunctionName = currentAction;
                model.ControllerName = currentController;
                model.DeviceID = "Exception DeviceID";
                model.IP = "Exception ip";
                model.StartTime = starttime;
                model.EndTime = DateTime.Now;
                model.Exception = feature.Error.ToString();
                model.Message = feature.Error.Message;
                model.Level = Level.Error.ToString();
                model.Status = "Exception status";
                model.UserID = "Exception userid";
                model.RequestParameters = "Exception Request Parameter";
                model.RequestResponse = requestResponse;
                model.LogId = logId;
                model.RequestDateTime = DateTime.Now;
                model.ErrorCode = "Error-01";
                var logs = context.RequestServices.GetRequiredService<ILogRepository>();
                logs.Add(model);
                logs.Save();
                await context.Response.WriteAsync(requestResponse);
            }));

            app.UseCors("AllowAll");
            //app.UseStatusCodePages(async context => {
            //    if (context.HttpContext.Response.StatusCode == 401)
            //    {
            //        // your redirect
            //       // context.HttpContext.Response.Redirect("/Home/Error");
            //    }
            //});
            app.UseStaticFiles();
            app.UseAuthentication();
            //app.UseRouting(routes =>
            //{
            //    routes.MapControllers();
            //});

            //  app.UseMiddleware<AuthenticationMiddleWare>();
            app.UseMvc();
            //app.UseSwagger();
            //app.UseSwaggerUI(c =>
            //{
            //    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Test API V1");
            //});
        }
    }
}
