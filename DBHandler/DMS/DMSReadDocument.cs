﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DMSHandler.DMSReadDocument
{
    public class DMSReadDocumentModel
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public RootObject RootObject { get; set; }
    }

    public class Metadata
    {
        public string key { get; set; }
        public string value { get; set; }
    }

    public class RootObject
    {
        public string documentId { get; set; }
        public int? version { get; set; }
    }

    public class ResponseObject
    {
        public string base64Representation { get; set; }
        public string documentType { get; set; }
        public string fileName { get; set; }
        public List<Metadata> metadata { get; set; }
    }
}
