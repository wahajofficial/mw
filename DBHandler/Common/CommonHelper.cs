﻿using CommonModel;
using DBHandler.Repositories;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;

namespace DBHandler.Common
{
  public  class CommonHelper
    {
        ILogRepository _log;

        private static IConfiguration _config;
        string key1 = "";


        public CommonHelper(ILogRepository log, IConfiguration config)
        {
            _log = log;
            _config = config;
             key1 = _config.GetValue<string>("GlobalSettings:EncryptionKey");
        }
        public  Dictionary<string, string> GetHeaderForJson()
        {
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Content-Type", "application/json");
            return headers;
        }

        public  Dictionary<string, string> GetHeaderForERCCorporate()
        {
            var headvalue = _log.GetHeaders("ERCCorporate",1);
            string UserName = "";
            string Password = "";
            Dictionary<string, string> headers = new Dictionary<string, string>();
            foreach (var row in headvalue)
            {
                if (row.HeaderName.Equals("Content-Type"))
                {
                    headers.Add("Content-Type", row.HeaderValue);
                }
                if (row.HeaderName.Equals("UserName"))
                {
                    UserName = row.HeaderValue;
                }
                if (row.HeaderName.Equals("Password"))
                {
                    Password = row.HeaderValue;
                }

            }
            headers.Add("Authorization", "Basic " + ConvertAuthorization(UserName, Password));


            return headers;
        }
        public Dictionary<string, string> GetHeaderForBankSmart()
        {
            var headvalue = _log.GetHeaders("BankSmart", 1);
            string UserName = "";
            string Password = "";
            Dictionary<string, string> headers = new Dictionary<string, string>();
            foreach (var row in headvalue)
            {
                if (row.HeaderName.Equals("Content-Type"))
                {
                    headers.Add("Content-Type", row.HeaderValue);
                }
                if (row.HeaderName.Equals("UserName"))
                {
                    UserName = DecryptString(key1,row.HeaderValue);
                }
                if (row.HeaderName.Equals("Password"))
                {
                    Password = DecryptString(key1,row.HeaderValue);
                }

            }
            headers.Add("Authorization", "Basic " + ConvertAuthorization(UserName, Password));


            return headers;
        }

        public Dictionary<string, string> GetHeaderForRailsBank()
        {
            //string key = _config.GetValue<string>("GlobalSettings:EncryptionKey");
            var headvalue = _log.GetHeaders("RailsBank", 1);
            Dictionary<string, string> headers = new Dictionary<string, string>();
            foreach (var row in headvalue)
            {
                if (row.HeaderName.Equals("Content-Type"))
                {
                    headers.Add("Content-Type", row.HeaderValue);
                }
                if (row.HeaderName.Equals("Authorization"))
                {
                    headers.Add("Authorization",DecryptString(key1, row.HeaderValue));

                }
            }


            return headers;
        }


        public Dictionary<string, string> GetHeaderForPostCoder()
        {
            var headvalue = _log.GetHeaders("PostCoder", 1);
            Dictionary<string, string> headers = new Dictionary<string, string>();
            foreach (var row in headvalue)
            {
                if (row.HeaderName.Equals("Authorization"))
                {
                    headers.Add("Authorization", DecryptString(key1, row.HeaderValue));

                }
            }


            return headers;
        }

        //GetHeaderForSendGrid
        public Dictionary<string, string> GetHeaderForSendGrid()
        {
            var headvalue = _log.GetHeaders("SendGrid", 1);
            Dictionary<string, string> headers = new Dictionary<string, string>();
            foreach (var row in headvalue)
            {
                if (row.HeaderName.Equals("Authorization"))
                {
                    headers.Add("Authorization", DecryptString(key1, row.HeaderValue));

                }
            }
            return headers;
        }

        public Dictionary<string, string > GetHeaderForSumsub(object body, string baseurl, string url, HttpMethod method,bool isMultiContent)
        {
            if(HttpMethod.Get ==method)
            {
                return SendGet(url);
            }
            var headvalue = _log.GetHeaders("Sumsub", 1);
            // Create the request body
            var requestBody = new HttpRequestMessage(method, baseurl);

            if (isMultiContent)
            {
                requestBody.Content = ((MultipartFormDataContent)body);
            }
            else
            {
                requestBody.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
            }
            Dictionary<string, string> headers = new Dictionary<string, string>();
            var ts = DateTimeOffset.UtcNow.ToUnixTimeSeconds();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            foreach (var row in headvalue)
            {
                if (row.HeaderName.Equals("X-App-Token"))
                {
                    headers.Add("X-App-Token", DecryptString(key1, row.HeaderValue));
                }
                else if (row.HeaderName.Equals("Key"))
                {                  
                    string signature = CreateSignature(ts, method, url, RequestBodyToBytes(requestBody), DecryptString(key1, row.HeaderValue));
                    headers.Add("X-App-Access-Sig", signature);
                }
            }

            headers.Add("X-App-Access-Ts", ts.ToString());

            return headers;
        }
        private Dictionary<string, string>  SendGet(string url)
        {
            var headvalue = _log.GetHeaders("Sumsub", 1);

            Dictionary<string, string> headers = new Dictionary<string, string>();

            long ts = DateTimeOffset.UtcNow.ToUnixTimeSeconds();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            headers.Add("X-App-Access-Ts", ts.ToString());

            foreach (var row in headvalue)
            {
                if (row.HeaderName.Equals("X-App-Token"))
                {
                    headers.Add("X-App-Token", DecryptString(key1, row.HeaderValue));
                }
                else if (row.HeaderName.Equals("Key"))
                {
                    string signature = CreateSignature(ts, HttpMethod.Get, url, null, DecryptString(key1, row.HeaderValue));
                    headers.Add("X-App-Access-Sig", signature);
                }
            }

            return headers;
        }
        //Create Applicant 
        private  byte[] RequestBodyToBytes(HttpRequestMessage requestBody)
        {
            return requestBody.Content == null ?
                new byte[] { } : requestBody.Content.ReadAsByteArrayAsync().Result;
        }

        private string CreateSignature(long ts, HttpMethod httpMethod, string path, byte[] body,string keyString)
        {
           
            var hmac256 = new HMACSHA256(Encoding.ASCII.GetBytes(keyString));

            byte[] byteArray = Encoding.ASCII.GetBytes(ts + httpMethod.Method + path);

            if (body != null)
            {
                // concat arrays: add body to byteArray
                var s = new MemoryStream();
                s.Write(byteArray, 0, byteArray.Length);
                s.Write(body, 0, body.Length);
                byteArray = s.ToArray();
            }

            var result = hmac256.ComputeHash(
                new MemoryStream(byteArray)).Aggregate("", (s, e) => s + String.Format("{0:x2}", e), s => s);


            return result;
        }

        public Dictionary<string, string> GetHeaderForTrunarrative()
        {
            var headvalue = _log.GetHeaders("Trunarrative", 1);
            string UserName = "";
            string Password = "";
            Dictionary<string, string> headers = new Dictionary<string, string>();
            foreach (var row in headvalue)
            {
                if (row.HeaderName.Equals("Content-Type"))
                {
                    headers.Add("Content-Type", DecryptString(key1,row.HeaderValue));
                }
                if (row.HeaderName.Equals("Username"))
                {
                    UserName = DecryptString(key1, row.HeaderValue);
                }
                if (row.HeaderName.Equals("Password"))
                {
                    Password = DecryptString(key1, row.HeaderValue);
                }
            }
            headers.Add("Authorization", "Basic " + ConvertAuthorization(UserName, Password));
            return headers;
        }
        public Dictionary<string, string> GetHeaderForRetail()
        {
            var headvalue = _log.GetHeaders("Retail", 1);
            string UserName = "";
            string Password = "";
            Dictionary<string, string> headers = new Dictionary<string, string>();
            foreach (var row in headvalue)
            {
                if (row.HeaderName.Equals("Content-Type"))
                {
                    headers.Add("Content-Type", row.HeaderValue);
                }
                if (row.HeaderName.Equals("UserName"))
                {
                    UserName = row.HeaderValue;
                }
                if (row.HeaderName.Equals("Password"))
                {
                    Password = row.HeaderValue;
                }

            }
            headers.Add("Authorization", "Basic " + ConvertAuthorization(UserName, Password));
            return headers;
        }

        public  Dictionary<string, string> GetHeaderJCREDIT(string token)
        {
            var headvalue = _log.GetHeaders("JCREDIT", 1);
            Dictionary<string, string> headers = new Dictionary<string, string>();
            foreach (var row in headvalue)
            {
                if (row.HeaderName.Equals("Content-Type"))
                {
                    headers.Add("Content-Type", row.HeaderValue);
                }
            }
            if (!string.IsNullOrEmpty(token))
            {
                headers.Add("Authorization", "Bearer " + token);
            }
            return headers;
        }

        public  Dictionary<string, string> GetHeaderODS(string token)
        {
            var headvalue = _log.GetHeaders("ODS", 1);
            Dictionary<string, string> headers = new Dictionary<string, string>();
            foreach (var row in headvalue)
            {
                if (row.HeaderName.Equals("Content-Type"))
                {
                    headers.Add("Content-Type", row.HeaderValue);
                }
            }
            if (!string.IsNullOrEmpty(token))
            {
                headers.Add("Authorization", "Bearer " + token);
            }
            return headers;
        }

        public  Dictionary<string, string> GetHeaderForCRMUpdateCase(string token)
        {
            var headvalue = _log.GetHeaders("CRMUpdateCase", 1);
            Dictionary<string, string> headers = new Dictionary<string, string>();
            foreach (var row in headvalue)
            {
                if (row.HeaderName.Equals("Content-Type"))
                {
                    headers.Add("Content-Type", row.HeaderValue);
                }
                if (row.HeaderName.Equals("Accept"))
                {
                    headers.Add("Accept", row.HeaderValue);
                }
                if (row.HeaderName.Equals("OData-MaxVersion"))
                {
                    headers.Add("OData-MaxVersion", row.HeaderValue);
                }
                if (row.HeaderName.Equals("OData-Version"))
                {
                    headers.Add("OData-Version", row.HeaderValue);
                }
            }
            if (!string.IsNullOrEmpty(token))
            {
                headers.Add("Authorization", "Bearer " + token);
            }
            return headers;
        }

        public  Dictionary<string, string> GetHeaderLiveChat()
        {
            var headvalue = _log.GetHeaders("LiveChat", 1);
            Dictionary<string, string> headers = new Dictionary<string, string>();
            foreach (var row in headvalue)
            {
                if (row.HeaderName.Equals("Content-Type"))
                {
                    headers.Add("Content-Type", row.HeaderValue);
                }
                if (row.HeaderName.Equals("Authorization"))
                {
                    headers.Add("Authorization", row.HeaderValue);
                }
            }
           
            return headers;
        }

        public  Dictionary<string, string> GetHeaderFireBase(IConfiguration _config)
        {
            var headvalue = _log.GetHeaders("FireBase", 1);
            Dictionary<string, string> headers = new Dictionary<string, string>();
            foreach (var row in headvalue)
            {
                if (row.HeaderName.Equals("Content-Type"))
                {
                    headers.Add("Content-Type", row.HeaderValue);
                }
                if (row.HeaderName.Equals("Authorization"))
                {
                    headers.Add("Authorization", string.Format("key={0}", row.HeaderValue));
                }
                if (row.HeaderName.Equals("Sender"))
                {
                    headers.Add("Sender", string.Format("id={0}", row.HeaderValue));
                }
            }
            return headers;
        }

        public  Dictionary<string, string> GetHeaderForShipa()
        {
            OAuthBase oAuthBase = new OAuthBase();
            string out1 = "";
            string out2 = "";
            string out3 = "";
            string URL = "";
            string SecretKey1 = "";
            string SecretKey2 = "";
            string SecretKey3 = "";
            string SecretKey4 = "";

            var headvalue = _log.GetHeaders("Shipa", 1);
            foreach (var row in headvalue)
            {
                if (row.HeaderName.Equals("URL"))
                {
                    URL = row.HeaderValue;
                }
                if (row.HeaderName.Equals("SecretKey1"))
                {
                    SecretKey1 = row.HeaderValue;
                }
                if (row.HeaderName.Equals("SecretKey2"))
                {
                    SecretKey2 = row.HeaderValue;
                }
                if (row.HeaderName.Equals("SecretKey3"))
                {
                    SecretKey3 = row.HeaderValue;
                }
                if (row.HeaderName.Equals("SecretKey4"))
                {
                    SecretKey4 = row.HeaderValue;
                }
            }
            string signature = oAuthBase.GenerateSignature(new System.Uri(URL),
          SecretKey1,
          SecretKey2,
          SecretKey3,
          SecretKey4,
          "POST", oAuthBase.GenerateTimeStamp(), oAuthBase.GenerateNonce(), out out1, out out2, out out3);
            signature = "OAuth realm=4344065," + out3;
            Dictionary<string, string> headers = new Dictionary<string, string>();
            foreach (var row in headvalue)
            {
                if (row.HeaderName.Equals("Content-Type"))
                {
                    headers.Add("Content-Type", row.HeaderValue);
                }
                if (row.HeaderName.Equals("Authorization"))
                {
                    headers.Add("Authorization", signature);
                }
                if (row.HeaderName.Equals("Accept"))
                {
                    headers.Add("Accept", row.HeaderValue);
                }
               
            }
            return headers;
        }

        public  Dictionary<string, string> GetHeader()
        {
            var headvalue = _log.GetHeaders("Header", 1);
            string UserName = "";
            string Password = "";
            Dictionary<string, string> headers = new Dictionary<string, string>();
            foreach (var row in headvalue)
            {
                if (row.HeaderName.Equals("Content-Type"))
                {
                    headers.Add("Content-Type", row.HeaderValue);
                }
                if (row.HeaderName.Equals("CHANNEL_ID"))
                {
                    headers.Add("CHANNEL_ID", row.HeaderValue);
                }
                if (row.HeaderName.Equals("UserName"))
                {
                    UserName = row.HeaderValue;
                }
                if (row.HeaderName.Equals("Password"))
                {
                    Password = row.HeaderValue;
                }

            }
            headers.Add("Authorization", "Basic " + ConvertAuthorization(UserName, Password));
            return headers;
        }

        public  Dictionary<string, string> GetHeaderForECSFin()
        {
            var headvalue = _log.GetHeaders("ECSFin", 1);
            string UserName = "";
            string Password = "";
            Dictionary<string, string> headers = new Dictionary<string, string>();
            foreach (var row in headvalue)
            {
                if (row.HeaderName.Equals("Content-Type"))
                {
                    headers.Add("Content-Type", row.HeaderValue);
                }
                if (row.HeaderName.Equals("CHANNEL_ID"))
                {
                    headers.Add("CHANNEL_ID", row.HeaderValue);
                }
                if (row.HeaderName.Equals("UserName"))
                {
                    UserName = row.HeaderValue;
                }
                if (row.HeaderName.Equals("Password"))
                {
                    Password = row.HeaderValue;
                }

            }
            headers.Add("Authorization", "Basic " + ConvertAuthorization(UserName, Password));


            return headers;
        }

        public  Dictionary<string, string> GetHeaderForIMTF()
        {
            var headvalue = _log.GetHeaders("IMTF", 1);
            string UserName = "";
            string Password = "";
            Dictionary<string, string> headers = new Dictionary<string, string>();
            foreach (var row in headvalue)
            {
                if (row.HeaderName.Equals("Content-Type"))
                {
                    headers.Add("Content-Type", row.HeaderValue);
                }
               
                if (row.HeaderName.Equals("UserName"))
                {
                    UserName = row.HeaderValue;
                }
                if (row.HeaderName.Equals("Password"))
                {
                    Password = row.HeaderValue;
                }

            }
            headers.Add("Authorization", "Basic " + ConvertAuthorization(UserName, Password));
            return headers;
        }

        public  Dictionary<string, string> GetHeaderForIMTF1()
        {
            var headvalue = _log.GetHeaders("IMTF", 2);
            string UserName = "";
            string Password = "";
            Dictionary<string, string> headers = new Dictionary<string, string>();
            foreach (var row in headvalue)
            {
                if (row.HeaderName.Equals("Content-Type"))
                {
                    headers.Add("Content-Type", row.HeaderValue);
                }
                if (row.HeaderName.Equals("UserName"))
                {
                    UserName = row.HeaderValue;
                }
                if (row.HeaderName.Equals("Password"))
                {
                    Password = row.HeaderValue;
                }

            }
            headers.Add("Authorization", "Basic " + ConvertAuthorization(UserName, Password));

            return headers;
        }

        public  Dictionary<string, string> GetHeaderForRMS(string identifier)
        {
            var headvalue = _log.GetHeaders("RMS", 1);
            
            Dictionary<string, string> headers = new Dictionary<string, string>();
            foreach (var row in headvalue)
            {
                if (identifier == "1")
                {
                    if (row.HeaderName.Equals("Content-Type"))
                    {
                        headers.Add("Content-Type", "application/json");
                    }
                    if (row.HeaderName.Equals("userName"))
                    {
                        headers.Add("userName", row.HeaderValue);
                    }
                    if (row.HeaderName.Equals("password"))
                    {
                        headers.Add("password", row.HeaderValue);
                    }
                    //if (row.HeaderName.Equals("Cookie"))
                    //{
                    //    headers.Add("Cookie", row.HeaderValue);
                    //}
                }
                else
                {
                    if (row.HeaderName.Equals("Content-Type"))
                    {
                        headers.Add("Content-Type", row.HeaderValue);
                    }
                    if (row.HeaderName.Equals("Accept"))
                    {
                        headers.Add("Accept", row.HeaderValue);
                    }
                    if (row.HeaderName.Equals("Cache-Control"))
                    {
                        headers.Add("Cache-Control", row.HeaderValue);
                    }
                    if (row.HeaderName.Equals("Connection"))
                    {
                        headers.Add("Connection", row.HeaderValue);
                    }
                    if (row.HeaderName.Equals("Accept-Encoding"))
                    {
                        headers.Add("Accept-Encoding", row.HeaderValue);
                    }
                    if (row.HeaderName.Equals("User-Agent"))
                    {
                        headers.Add("User-Agent", row.HeaderValue);
                    }
                    if (row.HeaderName.Equals("Postman-Token"))
                    {
                        headers.Add("Postman-Token", row.HeaderValue);
                    }
                }
            }
            return headers;
        }

        public  Dictionary<string, string> GetHeaderForCRM()
        {
            var headvalue = _log.GetHeaders("CRM", 1);

            Dictionary<string, string> headers = new Dictionary<string, string>();
            foreach (var row in headvalue)
            {
                if (row.HeaderName.Equals("Content-Type"))
                {
                    headers.Add("Content-Type", row.HeaderValue);
                }
                if (row.HeaderName.Equals("Accept"))
                {
                    headers.Add("Accept", row.HeaderValue);
                }
            }
                return headers;
        }

        public  Dictionary<string, string> GetHeaderForMoneyThor(string customerID)
        {
            var headvalue = _log.GetHeaders("MoneyThor", 1);

            Dictionary<string, string> headers = new Dictionary<string, string>();
            foreach (var row in headvalue)
            {
                if (row.HeaderName.Equals("Content-Type"))
                {
                    headers.Add("Content-Type", row.HeaderValue);
                }
                if (row.HeaderName.Equals("Customer"))
                {
                    headers.Add("Customer", customerID);
                }
            }
            return headers;
        }

        public  Dictionary<string, string> GetHeaderForMoneyThorJson(string customerID)
        {
            var headvalue = _log.GetHeaders("MoneyThor", 2);

            Dictionary<string, string> headers = new Dictionary<string, string>();
            foreach (var row in headvalue)
            {
                if (row.HeaderName.Equals("Content-Type"))
                {
                    headers.Add("Content-Type", row.HeaderValue);
                }
                if (row.HeaderName.Equals("Customer"))
                {
                    headers.Add("Customer", customerID);
                }
            }
            return headers;
        }

        public static string ConvertAuthorization(string username, string password)
        {
            return Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", username, password)));
        }
        

        //Symmetric Key 
        public static string DecryptString(string key, string cipherText)
        {
            //string key = _config.GetValue<string>("GlobalSettings:EncryptionKey");
            byte[] iv = new byte[16];
            byte[] buffer = Convert.FromBase64String(cipherText);

            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = iv;
                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream(buffer))
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader streamReader = new StreamReader((Stream)cryptoStream))
                        {
                            return streamReader.ReadToEnd();
                        }
                    }
                }
            }
        }

        public static string EncryptString(string key, string plainText)
        {
            byte[] iv = new byte[16];
            byte[] array;

            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = iv;

                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter streamWriter = new StreamWriter((Stream)cryptoStream))
                        {
                            streamWriter.Write(plainText);
                        }

                        array = memoryStream.ToArray();
                    }
                }
            }
            return Convert.ToBase64String(array);
        }
    }
}
