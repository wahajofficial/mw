﻿using DBHandler.Modal;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;


namespace DBHandler.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }


        public DbSet<LinkAccount> TLinkAccounts { get; set; }
        public DbSet<Product> TProducts { get; set; }
        public DbSet<Log> TLog { get; set; }
        public DbSet<AdditionalDoc> TAdditionalDocs { get; set; }
        public DbSet<User> TUsers { get; set; }
        public DbSet<IdCard> TIdCards { get; set; }


        public DbSet<TransactionImage> TransactionImages {get;set;}


        //JobDetail
        //
        //RegisterationOTP
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
    }
}
