﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DBHandler.Model;
using DBHandler.Model.Dtos;

namespace DBHandler.Repositories
{
    //LinkAccount
    public class LinkAccountRepository : Repository<LinkAccount>, ILinkAccountRepository
    {
        private readonly VendorMWContext _ctx;

        public LinkAccountRepository(VendorMWContext ctx) : base(ctx)
        {
            _ctx = ctx;
        }


        //public LinkAccount GetByAccountNo(string AccountNo, string UserId)
        //{
        //    return _un.linkAccountRepository.SingleOrDefault(s => s.AccountNo == AccountNo && s.CustomerId == UserId);
        //}

        //public IEnumerable<LibsharaWinner> GetAllWinners(IUnitOfWork _un)
        //{
        //    return _un.libsharaWinnerRepository.GetAll();
        //}
        public List<LinkAccount> GetUserWakalaAccount(string userId)
        {
            var acc = (from la in _ctx.TLinkAccounts
                       join p in _ctx.TProducts
on new { ProductId = la.ProductId } equals new { ProductId = p.ProductId }
                       where p.IsWakala == true && la.CustomerId == userId
                       && la.AccountStatus == "Active"
                       select la

                     );
            return acc.ToList();
        }
        public List<LinkAccount> GetUserFlexiAccount(string userId)
        {
            var acc = (from la in _ctx.TLinkAccounts
                       join p in _ctx.TProducts
on new { ProductId = la.ProductId } equals new { ProductId = p.ProductId }
                       where p.IsFlexi == true && la.CustomerId == userId
                       && la.AccountStatus == "Active"
                       select la

                     );
            return acc.ToList();
        }
        //public IQueryable<LinkAccount> GetAll(IUnitOfWork _un)
        //{
        //    return _un.linkAccountRepository.GetAll();
        //}

        //public LinkAccount GetByAccountId(int companyId, int accountId)
        //{
        //    return _un.linkAccountRepository.SingleOrDefault(s => s.CompanyId == companyId && s.AccountId == accountId);
        //}

        //public LinkAccount GetByProductAccountId(int companyId, int productId, int accountId, string customerID)
        //{
        //    return _un.linkAccountRepository.SingleOrDefault(s => s.CompanyId == companyId && s.ProductId == productId && s.AccountId == accountId && s.CustomerId == customerID);
        //}
        //public LinkAccount GetByProductAccountIdRA(int companyId, int productId, string customerID)
        //{
        //    return _un.linkAccountRepository.SingleOrDefault(s => s.CompanyId == companyId && s.ProductId == productId && s.CustomerId == customerID && s.Callstatus == "RA");
        //}
        //public LinkAccount GetByProductAccount(int productId, string Account)
        //{
        //    return _un.linkAccountRepository.SingleOrDefault(s => s.ProductId == productId && s.AccountNo == Account);
        //}

        //public LinkAccount GetByProductUnverifiedAccount(int companyId, int productId, string customerID)
        //{
        //    return _un.linkAccountRepository.SingleOrDefault(s => s.CompanyId == companyId && s.ProductId == productId && s.IsVerified == false && s.CustomerId == customerID);
        //}
        //public LinkAccount GetVerifiedAccount(int companyId, int productId, string customerID)
        //{
        //    return _un.linkAccountRepository.SingleOrDefault(s => s.CompanyId == companyId && s.ProductId == productId && s.IsVerified == false && s.CustomerId == customerID && s.Callstatus == "A" && (s.AccountNo == null || s.AccountNo == ""));
        //}
        ////TODO:Rameez 20/2/2018
        //public LinkAccount GetVerifiedAccountexisting(int companyId, int productId, string customerID)
        //{
        //    return _un.linkAccountRepository.SingleOrDefault(s => s.CompanyId == companyId && s.ProductId == productId && s.IsVerified == false && s.CustomerId == customerID);
        //}

        //public LinkAccount GetbyUserID(string customerID)
        //{
        //    return _un.linkAccountRepository.SingleOrDefault(s => s.IsVerified == true && s.CustomerId == customerID && (s.Callstatus == "A" || s.Callstatus == "R" || s.RimNo != null));
        //}
        //public LinkAccount GetStatusbyUserID(string customerID)
        //{
        //    return _un.linkAccountRepository.SingleOrDefault(s => s.CustomerId == customerID && (s.Callstatus == "A" || s.Callstatus == "R" || s.Callstatus == "RT" || s.Callstatus == "RA" || s.RimNo != null));
        //}
        //public List<LinkAccount> GetRimNobyUserID(string customerID)
        //{
        //    return _un.linkAccountRepository.Find(s => s.CustomerId == customerID && s.Callstatus == "A").ToList();
        //}
        //public LinkAccount GetAccountbyUserIdProductCode(int companyId, string customerId, string productCode)
        //{
        //    return _un.linkAccountRepository.SingleOrDefault(s => s.CompanyId == companyId && s.CustomerId == customerId && s.Callstatus == "A");
        //}
        //public LinkAccount GetAccountbyUserIdProductId(int companyId, string customerId, int productId)
        //{
        //    return _un.linkAccountRepository.SingleOrDefault(s => s.CompanyId == companyId && s.CustomerId == customerId && s.Callstatus == "A" && s.ProductId == productId);
        //}
        //public LinkAccount GetAcountDetails(int companyId, int productId, string customerID)
        //{
        //    return _un.linkAccountRepository.SingleOrDefault(s => s.CompanyId == companyId && s.ProductId == productId && s.IsVerified == false && s.CustomerId == customerID && s.Callstatus == "A" && (s.AccountNo != null || s.AccountNo != ""));
        //}
        //public LinkAccount GetByProductUnverifiedAccount(int companyId, int productId, int accountId, string customerID)
        //{
        //    return _un.linkAccountRepository.SingleOrDefault(s => s.CompanyId == companyId && s.ProductId == productId && s.IsVerified == false && s.CustomerId == customerID && s.AccountId == accountId);
        //}
        //public List<LinkAccount> GetByProductId(int companyId, string clientId, int productId)
        //{
        //    return _un.linkAccountRepository.Find(s => s.CompanyId == companyId && s.CustomerId == clientId && s.ProductId == productId);
        //}


        public List<PdfMonth> GetSignatureReportPerDay( DateTime startDate, DateTime EndDate)
        {

            var a = (from la in _ctx.TLinkAccounts
                     join tu in _ctx.TUsers on la.CustomerId equals tu.UserId
                     join ui in _ctx.TIdCards on tu.DetailsId equals ui.Id
                     where la.ProductId == 1 && !string.IsNullOrEmpty(la.AccountNo)
                     && (la.AccountDate.Value.Date >= startDate.Date && la.AccountDate.Value.Date <= EndDate.Date)
                     select new PdfDto
                     {
                         FullName = la.AccountTitle,
                         AccountNo = la.AccountNo,
                         CprNo = tu.IdNo,
                         AccountDate = la.AccountDate,
                         CprBack = ui.BackCardImage,
                         RimNo = la.RimNo,
                         IsExisting = tu.IsExisting == true ? "Yes" : "No",
                         Signature = (from td in _ctx.TAdditionalDocs where td.UserId == la.CustomerId select td.PassportSignature).FirstOrDefault()


                     } into t1
                     group t1 by t1.AccountDate.Value.Day into g
                     select new PdfMonth
                     {
                         Month = g.FirstOrDefault().AccountDate.Value.Day.ToString() + g.FirstOrDefault().AccountDate.Value.Month.ToString() + g.FirstOrDefault().AccountDate.Value.Year.ToString(),
                         AccountList = g.ToList()
                     }



                   ).OrderByDescending(x => x.Month).ToList();

            return a;
        }
        public List<PdfMonth> GetSignatureReport( DateTime dt)
        {

            var a = (from la in _ctx.TLinkAccounts
                     join tu in _ctx.TUsers on la.CustomerId equals tu.UserId
                     join ui in _ctx.TIdCards on tu.DetailsId equals ui.Id
                     where la.ProductId == 1 && !string.IsNullOrEmpty(la.AccountNo)

                     select new PdfDto
                     {
                         FullName = la.AccountTitle,
                         AccountNo = la.AccountNo,
                         CprNo = tu.IdNo,
                         AccountDate = la.AccountDate,
                         CprBack = ui.BackCardImage,
                         RimNo = la.RimNo,
                         IsExisting = tu.IsExisting == true ? "Yes" : "No",
                         Signature = (from td in _ctx.TAdditionalDocs where td.UserId == la.CustomerId select td.PassportSignature).FirstOrDefault()


                     } into t1
                     group t1 by t1.AccountDate.Value.Month into g
                     select new PdfMonth
                     {
                         Month = g.FirstOrDefault().AccountDate.Value.Month.ToString() + g.FirstOrDefault().AccountDate.Value.Year.ToString(),
                         AccountList = g.ToList()
                     }



                   ).OrderByDescending(x => x.Month).ToList();

            return a;
        }
        public IQueryable<LinkAccount> GetByProductCode(int companyId, string clientId, string productCode)
        {
            var result = (from l in _ctx.TLinkAccounts
                          join p in _ctx.TProducts on new { Company = l.CompanyId, ProductId = l.ProductId } equals new { Company = p.CompanyId, ProductId = p.ProductId }
                          where l.CompanyId == companyId && l.CustomerId == clientId && p.ShortCode == productCode && l.IsVerified == true
                          select l
            );

            return result;
        }

        //public LinkAccount GetAcountDetailsbyAccountNo(int companyId, int productId, string customerID, string AccountNo)
        //{
        //    return _un.linkAccountRepository.SingleOrDefault(s => s.CompanyId == companyId && s.ProductId == productId && s.CustomerId == customerID && s.AccountNo == AccountNo);

        //}



        public VendorMWContext PlutoContext
        {
            get { return Context as VendorMWContext; }
        }
    }

}
