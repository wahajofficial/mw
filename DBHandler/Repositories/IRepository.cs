﻿using DBHandler.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DBHandler.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        TEntity Get(decimal id);
        TEntity Get(string id);
        TEntity Get(int id);
        IEnumerable<TEntity> GetAll();
        IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);

        // This method was not in the videos, but I thought it would be useful to add.
        TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate);
        void UpdateRange(IEnumerable<TEntity> entities);
        void Add(TEntity entity);
        void AddRange(IEnumerable<TEntity> entities);
        void UpdateEntity(TEntity entity, dynamic obj);
        void Remove(TEntity entity);
        void RemoveRange(IEnumerable<TEntity> entities);

        // void RemoveRangeByid(Id id);

        void RemovebyId(decimal id);
        void RemovebyId(int id);
        void RemovebyId(string id);


        void Update(TEntity entity);

        bool Save();
    }
}
