﻿using DBHandler.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Repositories
{
   public class TransactionImageRepository : Repository<TransactionImage>,ITransactionImageRepository
    {
        private readonly VendorMWContext _ctx;
        public TransactionImageRepository(VendorMWContext ctx) : base(ctx)
        {
            _ctx = ctx;
        }
    }
}
