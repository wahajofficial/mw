﻿using DBHandler.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace DBHandler.Repositories
{
    public class VkeyRegisterdUserRepo: Repository<TVkeyRegisteredUser>,IVkeyRegisterdUserRepository
    {
        private VendorMWContext _context;

        public VkeyRegisterdUserRepo(VendorMWContext context):base(context)
        {
            _context = context;
        }

        public TVkeyRegisteredUser findByTs(string ts)
        {
            return _context.TVkeyRegisteredUser.FirstOrDefault(x => x.ts.Equals(ts));
        }
    }
}
