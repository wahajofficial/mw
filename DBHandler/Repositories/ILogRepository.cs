﻿using DBHandler.Enum;
using DBHandler.Helper;
using DBHandler.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
namespace DBHandler.Repositories
{
    public interface ILogRepository : IRepository<Log>
    {
       void Logs(Level Level, string response, HttpContext contaxt,Exception ex, DateTime Starttime, DateTime Endtime,
            string RequestParameter, string RequestResponse, string userid, string ChannelId, string logId = "", DateTime? RequestDateTime = null,
            System.Net.HttpStatusCode? code = null, string baseurl = "", string functionName = "", DateTime? ApiRequestStartTime = null,
            DateTime? ApiRequestEndTime = null, string clientIp = "", string currentController = "", string currentAction = "", string ErrorLog = "", string SuccessLog = "", string InfoLog = "", string SplunCollector = "", string SplunkBase = "", string SplunkAuth = "", string httpAction = "");
        ErrorMessages GetErrorMessage(string errorCode);
        bool GetTversion();
        bool GetLogId(string Id);

        List<TVendorApiHeaders> GetHeaders(string VendorId, int Type);

    }
}
