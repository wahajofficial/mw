﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBHandler.Helper
{
    public class ActiveResponse
    {
        public ActiveResponse()
        {
            code = ACTVETErroCode.Success;
            message = "Success";
        }

        public ACTVETErroCode code { get; set; }
        public string message { get; set; }
        public object content { get; set; }
        public string exceptionMessage { get; set; }
        public decimal? id { get; set; }
        public string LogId { get; set; }
        public DateTime RequestDateTime { get; set; }

        public void UpdateMessage()
        {
            
            message = content == null ? "No data found," : "Success";
            
            code = content == null ? ACTVETErroCode.Unclassified : ACTVETErroCode.Success;
        }

        public enum ACTVETErroCode
        {
            Success = 1,
            Unclassified = 0,
            InvalidmailFormat = 2,
            InvalidMobileNumberFormat = 3,
            NotFound = 4,
            AlreadyExist = 5,
            Expired = 6,
            Failed = 7,
            EmailNotVerified = 8,
            UserAppliedBefore = 9,
            UserNotFound=404,
            changepassword=704,
            samepassword=705,
            sameemail=600,
            norights=700,
            UserBlocked=420,
            UnAuthorise=401
        }
        public enum ReturnResponse
        {
            BadRequest = 404,
            Success = 200,
            Unauthorized = 401
        }
    }
}
