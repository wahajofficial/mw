﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CoreHandler;
using DBHandler.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using DBHandler.Repositories;
using DBHandler.Model;
using Microsoft.Extensions.Configuration;
using DBHandler.Model.Dtos;
using DBHandler.Enum;
using HttpHandler;

namespace DBHandler.Helper
{
    public class InternalResult
    {
        public Status Status { get; set; }
        public RequestResponse  httpResult { get; set; }
    }

    public class ActiveResponseSucces<T>
    {
        public DateTime? RequestDateTime { get; set; }
        public string LogId { get; set; }
        public Status Status { get; set; }
        public T Content { get; set; }
    }
    
    public class Status
    {
        public string Code { get; set; }
        public string Severity { get; set; }
        public string StatusMessage { get; set; }
    }

    public static class Severity
    {
        public readonly static string Success = "Success";
        public readonly static string Error = "Error";
        public readonly static string Exception = "Exception";

    }

    public class GloalException { }
   
}
