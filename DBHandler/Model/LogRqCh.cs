﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DBHandler.Model
{
    [Table("LogRqCh")]
    public class LogRqCh
    {
        [Key]
        public Guid ID { get; set; }
        public string LogID { get; set; }
        public DateTime? RequestDateTime { get; set; }
        public string ChannelID { get; set; }
        public string RequestData { get; set; }
        public  string ResponseData { get; set; }
        public string ControllerName { get; set; }
        public string FunctionName { get; set; }

    }
}
