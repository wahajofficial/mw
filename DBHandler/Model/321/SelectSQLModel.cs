﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model._321
{
    public class GetLoanValueDTO
    {
        public SignOnRq SignOnRq { get; set; }

        public string Cif { get; set; }
        public int CampaignId { get; set; }
        public bool LoanAccepted { get; set; }
    }
    public class ProcessLoanRequestDTO
    {
        public SignOnRq SignOnRq { get; set; }
        public int Amount { get; set; }        
        public int CamapaignId { get; set; }
        public string ClientId { get; set; }
    }
   
}
