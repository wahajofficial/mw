﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class AddUpdateDepositRateModel
    {

     
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = "ProductId is a mandatory field.")]
        public string ProductId { get; set; }
        [Required(ErrorMessage = "SerialId is a mandatory field.")]
        [Range(0, int.MaxValue, ErrorMessage = "Only positive number allowed")]
        public int SerialId { get; set; }
        [Required(ErrorMessage = "EffectiveDate is a mandatory field.")]
        public DateTime EffectiveDate { get; set; }
        [Required(ErrorMessage = "MinSlabAmount is a mandatory field.")]
        public decimal MinSlabAmount { get; set; }
        [Required(ErrorMessage = "MaxSlabAmount is a mandatory field.")]
        public decimal MaxSlabAmount { get; set; }
        [Required(ErrorMessage = "EffectiveRate is a mandatory field.")]
        public decimal EffectiveRate { get; set; }
        [Required(ErrorMessage = "PreMatureRate is a mandatory field.")]
        public decimal PreMatureRate { get; set; }
    }
    public class AddUpdateDepositRateResponse
    {
        public string ProductId { get; set; }
        public int SerialId { get; set; }
    }
}
