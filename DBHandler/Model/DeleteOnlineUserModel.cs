﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class DeleteOnlineUserModel
    {
       
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [Range(0, int.MaxValue, ErrorMessage = "Only positive number allowed")]
        public int SerialNo { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string ClientId { get; set; }
    }


    public class DeleteOnlineUserResponse
    {
        public int ID { get; set; }
    }
}
