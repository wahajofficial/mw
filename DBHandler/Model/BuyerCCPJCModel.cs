﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.BuyerCCPJCModel
{
    public class BuyerCCPJCModel
    {
       
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = "Buyer ID is required")]
        public string BuyerID { get; set; }
        [Required(ErrorMessage = "CCP Core ID is required")]
        public string CCPCoreID { get; set; }
    }
    public class BuyerCCPJCResponse
    {
        public string BuyClientID { get; set; }
        public string BuyClientName { get; set; }
        public decimal BuyTotalLimit { get; set; }
        public decimal BuyOutstandingLimit { get; set; }
        public decimal BuyAvailableLimit { get; set; }
        public List<SupplierJC> SupplierInfo { get; set; }

    }
    public class SupplierJC
    {
        public string SupplierClientID { get; set; }
        public string SupplierClientName { get; set; }
        public decimal SupplierTotalLimit { get; set; }
        public decimal SupplierOutstandingLimit { get; set; }
        public decimal SupplierAvailableLimit { get; set; }
        public decimal?  PastOutstandingAmount { get; set; }
    }
}
