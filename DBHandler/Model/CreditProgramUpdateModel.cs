﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.CreditProgramUpdateModel
{
    public class CreditProgramUpdateModel
    {

        
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = "Credit Program ID is required")]
        public string CreditProgramID { get; set; }
        [Required(ErrorMessage = "Credit Program Name is required")]
        public string CreditProgramName { get; set; }
        [Required(ErrorMessage = "Credit Program Limit is required")]
        [Range(1, 999999999999.99, ErrorMessage = "Credit Program Limit must be between 1 to 999,999,999,999.99")]
        public decimal CreditProgramLimit { get; set; }
        [Required(ErrorMessage = "Credit Program Expiry Date is required")]
        public DateTime? CreditProgramExpiryDate { get; set; }
    }
    public class CreditProgramUpdateResponse
    {
        public string CreditProgramID { get; set; }
    }
}