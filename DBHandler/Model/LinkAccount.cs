﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBHandler.Model
{
    [Table("t_LinkAccount")]
    public class LinkAccount
    {
        [Key]
        public string Guid { get; set; }

        public int CompanyId { get; set; }
        public int AccountId { get; set; }
        public string AccountTitle { get; set; }
        public int ProductId { get; set; }
        public string CustomerId { get; set; }
        public bool IsVerified { get; set; }
        public string RimNo { get; set; }
        public string AccountNo { get; set; }
        public string AccountKey { get; set; }
        public string AccountStatus { get; set; }
        public string Callstatus { get; set; }
        public string Remarks { get; set; }
        public string CallId { get; set; }
        public string AMLRemarks { get; set; }
        public DateTime? AccountDate { get; set; }

        public string AmlCheck { get; set; }
        public string AmlDone { get; set; }
        public double? Rate { get; set; }
        public double? Amount { get; set; }

        public string MtAccount { get; set; }
        public string MtId { get; set; }

    }
}
