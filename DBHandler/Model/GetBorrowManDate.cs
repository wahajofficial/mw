﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;


namespace DBHandler.Model.GetBorrowManDate
{
    public class GetBorrowManDateModel
    {

        public SignOnRq SignOnRq { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string AccountID { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [Range(0, int.MaxValue, ErrorMessage = "Only positive number allowed")]
        public int SerialID { get; set; }
    }

    public class GetBorrowManDateResponse
    {
        public string AccountID { get; set; }
        public int? SerialID { get; set; }
        public decimal MinLimit { get; set; }

        public decimal MaxLimit { get; set; }

        public string AuthGroupID1 { get; set; }
        public string AuthGroupID2 { get; set; }
        public string AuthGroupID3 { get; set; }
        public string AuthGroupID4 { get; set; }
        public string AuthGroupID5 { get; set; }
        public string AuthGroupID6 { get; set; }
        public string ManDateType { get; set; }
        public string Comments { get; set; }

    }


}
