﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.GetCorporateAccountDetailEBosModel
{
    public class GetCorporateAccountDetailEBosModel
    {
        public SignOnRq SignOnRq { get; set; }
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string AccountId { get; set; }

        [MaxLength(29, ErrorMessage = "- Character length should not be more than 29.")]
        public string IBAN { get; set; }
    }

    public class GetCorporateAccountDetailEBosModelResponse
    {

        public string ClientID { get; set; }

        public string ProductID { get; set; }


        public string Name { get; set; }


        public string Address { get; set; }

        public string CountryID { get; set; }


        public string StateID { get; set; }

        public string CityID { get; set; }


        public string StatementFrequency { get; set; }


        public string HoldMail { get; set; }


        public string ZakatExemption { get; set; }


        public string ContactPersonName { get; set; }


        public string Designation { get; set; }


        public string Phone1 { get; set; }

        public string Phone2 { get; set; }

        public string MobileNo { get; set; }

        public string Fax { get; set; }

        public string EmailID { get; set; }


        public string IntroducerAccountNo { get; set; }

        public string IntroducedBy { get; set; }

        public string IntroducerAddress { get; set; }

        public string IntroducerCityID { get; set; }

        public string IntroducerStateID { get; set; }

        public string IntroducerCountryID { get; set; }



        public string ModeOfOperation { get; set; }


        public string Reminder { get; set; }


        public string Notes { get; set; }

        public string NatureID { get; set; }


        public string RelationshipCode { get; set; }


        public string AllowCreditTransaction { get; set; }

        public string AllowDebitTransaction { get; set; }

        public string NotServiceCharges { get; set; }

        public string NotStopPaymentCharges { get; set; }

        public string NotChequeBookCharges { get; set; }


        public string TurnOver { get; set; }

        public string NoOfDrTrx { get; set; }
        public string NoOfCrTrx { get; set; }
        public decimal? DrThresholdLimit { get; set; }
        public decimal? CrThresholdLimit { get; set; }

        public string ProductCash { get; set; }

        public string ProductClearing { get; set; }

        public string ProductCollection { get; set; }

        public string ProductRemittance { get; set; }

        public string ProductCross { get; set; }

        public string ProductOthers { get; set; }
        public string ProductOthersDesc { get; set; }

        public string iban { get; set; }
        public string OpenDate { get; set; }
        public string ActivationDate { get; set; }

        public string status { get; set; }
    }
}
