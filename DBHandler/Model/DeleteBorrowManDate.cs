﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.DeleteBorrowManDate
{
	public class DeleteBorrowManDateModel
	{
		public SignOnRq SignOnRq { get; set; }

		[Required(ErrorMessage = " is a mandatory field.")]
		public string AccountID { get; set; }
		[Required(ErrorMessage = " is a mandatory field.")]
		[Range(0, int.MaxValue, ErrorMessage = "Only positive number allowed")]
		public int SerialID { get; set; }
	}

	public class DeleteBorrowManDateResponse
	{
		public int SerialID { get; set; }
	}
}
