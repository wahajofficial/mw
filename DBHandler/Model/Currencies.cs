﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBHandler.Model
{
    [Table("t_Currencies")]
    public class Currencies
    {
        [Key]
        public Int32 Id { get; set; }
        public int CompanyId  { get; set; }
        public string CurrencyId  { get; set; }
        public string Description { get; set; }
        public Int32 Rounding  { get; set; }
        public Int32 IsActive  { get; set; }
        public double Rate1  { get; set; }
        public double Rate2 { get; set; }
        public double Rate3  { get; set; }
        public double Rate4  { get; set; }
        public double Rate5  { get; set; }
        public double Rate6  { get; set; }
        public DateTime WorkingDate  { get; set; }
        public DateTime LastUpdatedOn { get; set; }
    }
}
