﻿using DBHandler.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using CommonModel;


namespace CoreHandler.Model
{
    public class TransferDto
    {
        public class TransferResponse
        {
            public string ReferenceNo { get; set; }
        }
        public class Balance
        {
            public decimal? TotalBalance { get; set; }
            public decimal? AvailableBalance { get; set; }
        }
        public class BalanceInquiry
        {
            public Account Account { get; set; }
            public Balance Balance { get; set; }
        }

        public class sendTransfer
        {
            [Required(ErrorMessage = " is a mandatory field.")]
            public SignOnRq SignOnRq { get; set; }



            [Required(ErrorMessage = "AccountId  is required")]
            public string AccountId { get; set; }
        }
    }
}
