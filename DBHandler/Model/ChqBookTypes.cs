﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model
{
    public class ChqBookTypes
    {
        public string TypeID { get; set; }
        public string Description { get; set; }
    }
}
