﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model
{
    public class GetCustomerIDExpiryResponse
    {

        public ExpiredCustomer ExpiredCustomer { get; set; }

        public List<ExpiredShareHolder> ExpiredShareHolder { get; set; }
        public List<ExpiredBenificalOwner> ExpiredBenificalOwner { get; set; }
        public List<ExpiredBoardDirector> ExpiredBoardDirector { get; set; }
        public List<ExpiredAuthSingatory> ExpiredAuthSingatory { get; set; }
    }

    public class ExpiredCustomer
    {
        public string ClientId { get; set; }
        public DateTime? TradeLicenseExpirydateCorp { get; set; }
        public DateTime? MOAMOU { get; set; }
        public DateTime? NicExpirydate { get; set; }
        public DateTime? PassportExpiry { get; set; }
        public string Email { get; set; }
        public string phone1 { get; set; }
        public string phone2 { get; set; }
    }
    public class ExpiredShareHolder
    {
        public string ClientId { get; set; }
        public string ShareHolderID { get; set; }
        public DateTime? ShareholdersIDExpirydate { get; set; }
        public string ShareholdersIDtype { get; set; }

    }
    public class ExpiredBenificalOwner
    {
        public string ClientId { get; set; }
        public string BeneficialOwnersID { get; set; }
        public DateTime? BeneficialIDExpirydate { get; set; }
        public string BeneficialIDtype { get; set; }
    }

    public class ExpiredBoardDirector
    {
        public string ClientId { get; set; }
        public string BoardDirectorIDNo { get; set; }
        public DateTime? BoardDirectorIDExpirydate { get; set; }
        public string BoardDirectorIDtype { get; set; }
    }
    public class ExpiredAuthSingatory
    {
        public string ClientId { get; set; }
        public string AuthorisedSignatoriesID { get; set; }
        public DateTime? AuthorisedsignatoryIDExpirydate1 { get; set; }
        public DateTime? AuthorisedsignatoryIDExpirydate2 { get; set; }
        public string AuthorisedsignatoryIDtype1 { get; set; }
    }
}