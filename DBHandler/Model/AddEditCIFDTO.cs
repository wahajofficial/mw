﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class AddEditCIFDTO
    {
        
        public SignOnRq SignOnRq { get; set; }

        public AddEditNosroCustomer Customer { get; set; }
    }

    public class AddEditNosroCustomer
    {
        public string ClientID { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string Name { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
       // [StringRangeAttribute(AllowableValues = new[] { "NOSTRO" }, ErrorMessage = "- Valid value is NOSTRO.")]
        public string CustomerSegment { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string GroupCode { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "Low", "Medium", "High" }, ErrorMessage = "- Valid value is either Low,Medium or High.")]
        public string RiskProfile { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string Phone { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string Email { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string Address { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "A", "P", "D" }, ErrorMessage = "- Valid value is either A,P or D.")]
        public string Status { get; set; }


    }

}
