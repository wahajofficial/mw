﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
namespace DBHandler.Model.EarlySettlementExecution
{
    public class EarlySettlementExecutionModel
    {
        public SignOnRq SignOnRq { get; set; }

        public string clientId { get; set; }
        public string facId { get; set; }
        public string prodId { get; set; }
        public string dealRefNo { get; set; }
        public DateTime settlementDate { get; set; }
        public int principalAmt { get; set; }
        public string CurrAccId { get; set; }
        public string PaymentSource { get; set; }
    }


    public class EarlySettlementExecutionResponse
    {
        public string EarlySettleRefNo { get; set; }
    }

    public class EarlySettlementExecutionResponseFinal
    {
        public EarlySettlementExecutionResponse EarlySettleRef { get; set; }
    }
}
