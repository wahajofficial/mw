﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DBHandler.Model
{
    [Table("t_Users")]
    public class User
    {
        [Key]
        public int Id { get; set; }
        public string UserId { get; set; }
        public string IdNo { get; set; }
        public string FullName { get; set; }
        public int CompanyId { get; set; }
        public int? OrgId { get; set; }
        public string Type { get; set; }
        public string Email { get; set; }
        public string CountryID { get; set; }
        public int? CityID { get; set; }
        public string MobileNumber { get; set; }
        public bool ResidentStatus { get; set; }
        public string OTPCode { get; set; }
        public bool OTPStatus { get; set; }
        public bool EmailOTPStatus { get; set; }

        public string DetailsId { get; set; }
        public string PassportId { get; set; }
        public string GuardianDetailsId { get; set; }
        public string GuardianPassportId { get; set; }
        public string ForensicID { get; set; }
        public string ForensicP { get; set; }
        public string GuardianFrorensicID { get; set; }
        public string GuardianForensicP { get; set; }

        public bool IsExisting { get; set; }
        public string RimNo { get; set; }
        public string KfhMobileNo { get; set; }
        public bool IsNew { get; set; }
        public DateTime? AccountDate { get; set; }
        public string DeviceID { get; set; }
        public string CurrentID { get; set; }
        public string DLID { get; set; }
        public string GDLID { get; set; }
        public DateTime OTPExpireTime { get; set; }
        public string FireBaseToken { get; set; }
        public string Platform { get; set; }

        public bool SyncContact { get; set; }
        public bool DiscoverMe { get; set; }

        public bool? ForgetPassMobileOtp { get; set; }
        public bool? ForgetPassEmailOtp { get; set; }

    }
}
