﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model
{
    public class CurrencyPairing
    {
        public string CurrencyPairID { get; set; }
        public string FromCurrencyID { get; set; }
        public string ToCurrencyID { get; set; }
        public string GLID { get; set; }
    }
}
