﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace DBHandler.Model
{
    [Table("t_ErrorMessages")]
    public class ErrorMessages
    {
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string LocalCode { get; set; }
        public bool isError { get; set; }
        public string data { get; set; }

        [NotMapped]
        public object obj { get; set; }
    }
}
