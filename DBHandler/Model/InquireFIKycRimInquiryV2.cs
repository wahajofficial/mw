﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class InquireFIKycRimInquiryV2
    {
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string ClientID { get; set; }
    }
}
