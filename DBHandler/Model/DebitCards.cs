﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class DebitCards
    {
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string AccountID { get; set; }
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        [MinLength(16, ErrorMessage = "- Valid Min length should be less than 16.")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string DebitCardToken { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string CardName { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string DebitCardCategory { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string CardType { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public DateTime? ExpiryDate { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string Status { get; set; }


        public DateTime? CreateDateTime { get; set; }
    }
}
