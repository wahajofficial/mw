﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model
{
    public class InquireNostroCIFRespnse
    {
        public string Name { get; set; }
        public string CustomerSegment { get; set; }
        public string GroupCode { get; set; }
        public string RiskProfile { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Status { get; set; }
        public bool? IsReplicate { get; set; }

    }
}
