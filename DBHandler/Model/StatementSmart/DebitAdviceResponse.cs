﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.StatementSmart
{
    public class DebitAdviceResponse
    {
        public string retStatus { get; set; }
        public string retCode { get; set; }
        public string PDFFileName { get; set; }
        public string PDFBase64String { get; set; }
    }
    public class DebitAdviceResponseOut
    {
        public string PDFFileName { get; set; }
        public string PDFBase64String { get; set; }
    }
}
