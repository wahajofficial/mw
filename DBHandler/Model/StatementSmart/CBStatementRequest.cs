﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.StatementSmart
{
    public class CBStatementRequest
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string RecordType { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(5, ErrorMessage = "- Character length should not be more than 5.")]
        public string MessageTypeID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string SequenceNumber { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string CBUAEReference { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public DateTime? CustConsObtainedOn { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string EntityId { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MinLength(23, ErrorMessage = "- Valid Min length should not be less than 23.")]
        [MaxLength(23, ErrorMessage = "- Character length should not be more than 23.")]
        public string IBAN { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string CustName { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(2, ErrorMessage = "- Character length should not be more than 2.")]
        public string IDType { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string IDNumber { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public DateTime? StatementStartDate { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public DateTime? StatementEndDate { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MinLength(6, ErrorMessage = "- Valid Min length should not be less than 6.")]
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string FutureUse { get; set; }
    }
}
