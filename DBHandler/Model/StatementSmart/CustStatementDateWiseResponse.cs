﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.StatementSmart
{
    public class CustStatementDateWiseResponse
    {
        public string retStatus { get; set; }
        public string retCode { get; set; }
        public string PDFFileName { get; set; }
        public string PDFBase64String { get; set; }
        public BankDetails BankDetails { get; set; }
        public AccountDetails AccountDetails { get; set; }
        public decimal? OpeningBalance { get; set; }
        public decimal? ClosingBalance { get; set; }
        public int? NoOfTodaysCrTrx { get; set; }
        public int? NoOfTodaysDrTrx { get; set; }
        public decimal? TodaysDebitAmount { get; set; }
        public decimal? TodaysCreditAmount { get; set; }
        public List<StatementRecords> StatementRecords { get; set; }
        public ReportTitleDetails statementDetails { get; set; }

    }
    public class CustStatementDateWiseResponseOut
    {
        public string PDFFileName { get; set; }
        public string PDFBase64String { get; set; }
        public BankDetails BankDetails { get; set; }
        public AccountDetails AccountDetails { get; set; }
        public decimal? OpeningBalance { get; set; }
        public decimal? ClosingBalance { get; set; }
        public int? NoOfTodaysCrTrx { get; set; }
        public int? NoOfTodaysDrTrx { get; set; }
        public decimal? TodaysDebitAmount { get; set; }
        public decimal? TodaysCreditAmount { get; set; }
        public List<StatementRecords> StatementRecords { get; set; }
        public ReportTitleDetails statementDetails { get; set; }
    }

    public class ReportTitleDetails
    {
        public string statementTitle { get; set; }
        public string statementDateRange { get; set; }
    }


    public class StatementRecords
    {
        public string TrxReferenceNo { get; set; }
        public string ChannelRefID { get; set; }
        public string narrationID { get; set; }
        public DateTime wDate { get; set; }
        public DateTime ValueDate { get; set; }
        public string ChequeID { get; set; }
        public string Description { get; set; }
        public string TrxType { get; set; }
        public decimal Amount { get; set; }
        public decimal ForeignAmount { get; set; }
        public decimal Balance { get; set; }

    }
}
