﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.StatementSmart
{
    public class CBStatementResponse
    {
        public string retStatus { get; set; }
        public string retCode { get; set; }
        public StatementLineDetail StatementLineDetail { get; set; }
        public StatementLineControl StatementLineControl { get; set; }
    }

    public class CBStatementResponseOut
    {
        public StatementLineDetail StatementLineDetail { get; set; }
        public StatementLineControl StatementLineControl { get; set; }
    }

    public class StatementLineDetail
    {
        public string RecordType { get; set; }
        public string MessageTypeID { get; set; }
        public string FutureUse { get; set; }
        public List<StatementLineMoreDetails> StatementLineMoreDetails { get; set; }

    }
    public class StatementLineControl
    {
        public string RecordType { get; set; }
        public string CBUAEReference { get; set; }
        public string SendingInstitutionReference { get; set; }
        public string OpeningBalanceDCMark { get; set; }
        public DateTime? OpeningBalanceDate { get; set; }
        public string OpeningBalanceCCY { get; set; }
        public decimal? OpeningBalance { get; set; }
        public string ClosingBalanceDCMark { get; set; }
        public DateTime? ClosingBalanceDate { get; set; }
        public string ClosingBalanceCCY { get; set; }
        public decimal? ClosingBalance { get; set; }
        public decimal? NumberOfDebits { get; set; }
        public decimal? TotalOfDebits { get; set; }
        public decimal? NumberOfCredits { get; set; }
        public decimal? TotalOfCredits { get; set; }
        public string RejectReason { get; set; }
        public string AccountStatus { get; set; }
        public string TitleOfAccount { get; set; }
        public string IBAN { get; set; }
        public string FutureUse { get; set; }
    }

    public class StatementLineMoreDetails
    {
        public string TXNType { get; set; }
        public DateTime ValueDate { get; set; }
        public DateTime PostingDate { get; set; }
        public string DrCrMask { get; set; }
        public decimal TrxAmount { get; set; }
        public string TrxNarrative { get; set; }
        public string TrxReference { get; set; }
        public string ChannelRefID { get; set; }
    }
}
