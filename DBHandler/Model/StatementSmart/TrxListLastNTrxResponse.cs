﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.StatementSmart
{
    public class TrxListLastNTrxResponse
    {
        public string retStatus { get; set; }
        public string retCode { get; set; }
        public AccountDetails AccountDetails { get; set; }
        public List<StatementRecords> StatementRecords { get; set; }
    }

    public class TrxListLastNTrxResponseOut
    {
        public AccountDetails AccountDetails { get; set; }
        public List<StatementRecords> StatementRecords { get; set; }

    }
}
