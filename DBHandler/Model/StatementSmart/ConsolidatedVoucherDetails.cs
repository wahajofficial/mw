﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.StatementSmart
{
    public class ConsolidatedVoucherDetails
    {
        public string ChannelRefID { get; set; }
        public decimal ScrollNo { get; set; }
        public decimal SerialNo { get; set; }
        public DateTime VoucherDate { get; set; }
        public string TrxDescription { get; set; }
        public decimal Amount { get; set; }
        public decimal LocalEquiv { get; set; }
    }
}
