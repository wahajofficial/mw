﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.StatementSmart
{
    public class BankDetails
    {
        public string OurBankID { get; set; }
        public string OurBankName { get; set; }
        public string OurBranchID { get; set; }
        public string OurBranchName { get; set; }
        public string OurBranchAddress { get; set; }
        public string OurBranchCity { get; set; }
        public string Country { get; set; }
        public string Phone1 { get; set; }
    }
}
