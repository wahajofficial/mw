﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.StatementSmart
{
    public class TrxAdviceDetails
    {
        public decimal SerialNo { get; set; }
        public string AccountID { get; set; }
        public string AccountName { get; set; }
        public string CurrencyID { get; set; }
        public DateTime ValueDate { get; set; }
        public string ChequeID { get; set; }
        public DateTime ChequeDate { get; set; }
        public string TrxType { get; set; }
        public string AccountType { get; set; }
        public string TrxDescription { get; set; }
        public string SupervisorID { get; set; }
        public decimal VoucherAmount { get; set; }
        public decimal LocalEqv { get; set; }
    }
}
