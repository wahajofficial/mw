﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.StatementSmart
{
    public class MonthlyStatementStatus
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        [MinLength(16, ErrorMessage = "- Valid Min length should not be less than 16.")]
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string AccountID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "C", "V", }, ErrorMessage = "- Valid values are C - Customer Monthly Statement or V - Monthly Consolidated VAT Advice")]
        public string Type { get; set; }
    }
}
