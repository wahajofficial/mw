﻿using CommonModel;
using DBHandler.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class UpdateCorporateClientV2Model
    {
        
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = "Customer data require")]
        public UpdateCorporateCustomerModelV2 Cust { get; set; }
        [Required(ErrorMessage = "SicCodeCorp data require")]
        public List<UpdateCorporateClientSicCodeCorpModelV2> SicCodeCorp { get; set; }
        public List<UpdateCorporateClientClientSellerDTOV2> ClientSeller { get; set; }
        public List<UpdateCorporateClientClientBuyerDTOV2> ClientBuyer { get; set; }
        public List<UpdateCorporateClientCompanyInFoV2> CompanyInFo { get; set; }

    }

    public class UpdateCorporateCustomerModelV2
    {
        public string OurBranchID { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(9, ErrorMessage = "- Character length should not be more than 9.")]
        public string ClientID { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string GroupCode { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string RManager { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        [StringRangeAttribute(AllowableValues = new[] { "Low", "Medium", "High" }, ErrorMessage = "- Valid value is either Low,Medium or High.")]
        public string RiskProfile { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string EntityNameCorp { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string EntityTypeCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [PastDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> YearofestablishmentCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string RegisteredAddressCorp { get; set; }

        // [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string ContactName1Corp { get; set; }

        // [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string ContactID1Corp { get; set; }

        // [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string IDTypeCorp { get; set; }

        // [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string Address1Corp { get; set; }

        // [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string PhoneNumberCorp { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string AlternatePhonenumberCorp { get; set; }

        //[Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        [Email(ErrorMessage = "- is not a valid email.")]
        public string EmailIDCorp { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string ContactName2Corp { get; set; }


        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string ContactID2Corp { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string IDType2 { get; set; }

        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string Address2Corp { get; set; }

        public decimal? PhoneNumber2Corp { get; set; }

        public decimal? AlternatePhonenumber2Corp { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        [Email(ErrorMessage = "- is not a valid email.")]
        public string EmailID2Corp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string BusinessActivity { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string SICCodesCorp { get; set; }


        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string IndustryCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string PresenceinCISCountryCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string DealingwithCISCountryCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string CountryofIncorporationCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [PastDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> TradeLicenseIssuedateCorp { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> TradeLicenseExpirydateCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string TradeLicenseIssuanceAuthorityCorp { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "TRNNoCorp must be numeric")]
        public string TRNNoCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string PEPCorp { get; set; }


        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string BlackListCorp { get; set; }

        [FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> KYCReviewDateCorp { get; set; }

        //fATCA Fields
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string FATCARelevantCorp { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string CRSRelevantCorp { get; set; }
        //[Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        [StringRangeAttribute(AllowableValues = new[] { "SUP", "XUP", "UPX" }, ErrorMessage = "- Valid value is either SUP,XUP or UPX.")]
        public string USAEntityCorp { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string FFICorp { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string FFICategoryCorp { get; set; }
        [MaxLength(21, ErrorMessage = "- Character length should not be more than 21.")]
        public string GIINNoCorp { get; set; }
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string GIINNACorp { get; set; }
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string SponsorNameCorp { get; set; }
        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string SponsorGIIN { get; set; }
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string NFFECorp { get; set; }
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string StockExchangeCorp { get; set; }
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string TradingSymbolCorp { get; set; }


        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string USAORUAECorp { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string TINAvailableCorp { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo1Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry1Corp { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo2Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry2Corp { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo3Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry3Corp { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo4Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry4Corp { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo5Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry5Corp { get; set; }


        [MaxLength(10, ErrorMessage = "- Character length should not be more than 10.")]
        [StringRangeAttribute(AllowableValues = new[] { "Reason A", "Reason B", "Reason C" }, ErrorMessage = "- Valid value is either Reason A,Reason B or Reason C.")]
        public string FATCANoReasonCorp { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string ReasonBCorp { get; set; }

        public string ReasonACorp { get; set; }
        public string CreateBy { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string CompanyWebsite { get; set; }


        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string CompCashIntensive { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string PubLimCompCorp { get; set; }


        //[Required(ErrorMessage = " is a mandatory field.")]
        // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string RegAddCityCorp { get; set; }


        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string RegAddCountryCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string RegAddStateCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string Add1CountryCorp { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string Add1StateCorp { get; set; }
        //[Required(ErrorMessage = " is a mandatory field.")]
        // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string Add1CityCorp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string Add2CountryCorp { get; set; }
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string Add2StateCorp { get; set; }
        // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string Add2CityCorp { get; set; }

        [PastDateAttribute(ErrorMessage = " is not a valid date")]
        public System.DateTime? VATRegistered { get; set; }

        //[Required(ErrorMessage = " is a mandatory field.")]
        public string JurEmiratesID { get; set; }
        //[Required(ErrorMessage = " is a mandatory field.")]
        public string JurTypeID { get; set; }
        //[Required(ErrorMessage = " is a mandatory field.")]
        public string JurAuthorityID { get; set; }
        // [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        [MinLength(3, ErrorMessage = "- Character length should not be less than 3.")]
        public string POBox { get; set; }


        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string JurOther { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string GroupID { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string ParentClientID { get; set; }
        public string LicenseType { get; set; }
        public string CountryID { get; set; }
        public DateTime? MOAMOU { get; set; }
        //public string ShareHolderAddress { get; set; }
        //public string ShareHolderCountryID { get; set; }
        //public string ResidenceShareHolderCountryID { get; set; }
        //public string ShareHolderStateID { get; set; }
        //public string ShareHolderCity { get; set; }
        public string SigningAuthority { get; set; }
        public string POAResidenceCountry { get; set; }
        public string POAAddress { get; set; }
        public string POACountry { get; set; }
        public string POAState { get; set; }
        public string POACity { get; set; }
        //public string DirectorAddress { get; set; }
        //public string DirectorCountry { get; set; }
        //public string DirectorState { get; set; }
        //public string DirectorCity { get; set; }
        //public string DirectoryResidenceCountry { get; set; }
        public string AuditedFinancials { get; set; }
        public decimal? Assets { get; set; }
        public decimal? Revenue { get; set; }
        public decimal? Profits { get; set; }
        public decimal? BusinessTurnOver { get; set; }
        public decimal? CompanyCapital { get; set; }
        public string CompanyCountry { get; set; }
        public decimal? ExpectedDebitTurnOver { get; set; }
        public decimal? ExpectedMaxDebitTurnOver { get; set; }
        public decimal? ExpectedCreditTurnOver { get; set; }
        public decimal? ExpectedMaxCreditTurnOver { get; set; }
        public string CompanyAddress { get; set; }
        public decimal? ExpectedCashVolume { get; set; }
        public decimal? ExpectedCashValue { get; set; }
        public decimal? ChequesVolume { get; set; }
        public decimal? ChequesValue { get; set; }
        public decimal? SalaryPaymentVolume { get; set; }
        public decimal? SalaryPaymentValue { get; set; }
        public decimal? IntraBankRemittancesVolume { get; set; }
        public decimal? IntraBankRemittancesValue { get; set; }
        public decimal? InternationalRemittancesVolume { get; set; }
        public decimal? InternationalRemittancesValue { get; set; }
        public decimal? DomesticRemittanceVolume { get; set; }
        public decimal? DomesticRemittanceValues { get; set; }
        public decimal? ExpectedCashDebitVolume { get; set; }
        public decimal? ExpectedCashDebitValue { get; set; }
        public decimal? ChequeDebitVolume { get; set; }
        public decimal? ChequeDebitValue { get; set; }
        public decimal? SalaryDVolume { get; set; }
        public decimal? SalaryDValue { get; set; }
        public decimal? IntraDVolume { get; set; }
        public decimal? IntraDValue { get; set; }
        public decimal? InternationalDVolume { get; set; }
        public decimal? InternationalDValue { get; set; }
        public decimal? DomesticDRemittanceValues { get; set; }
        public decimal? DomesticDRemittanceVolume { get; set; }
        public decimal? CreditTradeFinanceVolume { get; set; }
        public decimal? CreditTradeFinanceValue { get; set; }
        public decimal? TradeFinanceVolumeD { get; set; }
        public decimal? TradeFinanceValueD { get; set; }
        public decimal? NoOfStaff { get; set; }
        public string CompanyActivites { get; set; }
        public string BusinessNatureRelation { get; set; }
        public string RelationalCompanyStructure { get; set; }
        public string CompanyGeoGraphicCourage { get; set; }
        public string UBOBackground { get; set; }
        public string ProductAndServicesOffered { get; set; }
        public int? IsDisclosureActivity { get; set; }
        public string DisclosureDetails { get; set; }
        public int? IsSanctionCountry { get; set; }
        public string SanctionRemarks { get; set; }
        public int? IsOfficeInSactionCountry { get; set; }
        public string SanctionCountries { get; set; }
        public string BusinessNatureSanctionCountry { get; set; }
        public int? IsProductOrigninatedCountry { get; set; }
        public string OriginatedProductCountry { get; set; }
        public string OriginatedCountryRemarks { get; set; }
        public int? IsExportGoods { get; set; }
        public string ExportGoodsCountry { get; set; }
        public string ExportGoodsRemarks { get; set; }
        public int? isRecievedTransFromSanctionCountry { get; set; }
        public string TransactionSanctionCountry { get; set; }
        public string TransactionSanctionRemarks { get; set; }
        public string CorporateStructureChart { get; set; }
        public string VisitConducted { get; set; }
        public string SanctionAssestment { get; set; }
        public string PepAssessment { get; set; }
        public string AdverseMedia { get; set; }
        public string AccountOpeningPurpose { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "0", "1" }, ErrorMessage = "- Valid value is either o or 1.")]
        public int? IsVip { get; set; }

        //New Cust Fields

        public string POAName { get; set; }
        public string POANationality { get; set; }
        public string POADateofBirth { get; set; }
        public string POAPlaceOfBirth { get; set; }
        public string POAGender { get; set; }
        public string selectionOfPayments { get; set; }
        public Nullable<decimal> DepositValue { get; set; }
        public Nullable<decimal> PaymentValue { get; set; }
        public Nullable<decimal> LoansValue { get; set; }
        public string PlaceofIncorporation { get; set; }
        public string Designation1Corp { get; set; }
        public string Department1Corp { get; set; }
        public string IDExpiryDateCorp { get; set; }
        public string Designation2Corp { get; set; }
        public string Department2Corp { get; set; }
        public string IDExpiryDate2Corp { get; set; }
        public string DealingwithCISTextarea { get; set; }
        public string IsOfficeInSactionCountrylist { get; set; }
        public string IsProductOrigninatedCountrylist { get; set; }
        public string ExportGoodsCountrylist { get; set; }
        public string TransactionSanctionCountrylist { get; set; }
    }

    public class UpdateCorporateClientCompanyInFoV2
    {
        public string ClientID { get; set; }
        public decimal SerialID { get; set; }
        public string AccountNo { get; set; }
        public string BankName { get; set; }
        public string BankCountry { get; set; }
    }
    public class UpdateCorporateClientSicCodeCorpModelV2
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string SicCodeCorp { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "P", "S" }, ErrorMessage = "- Valid value is either P or S")]
        public string Type { get; set; }
    }
    public class UpdateCorporateClientClientBuyerDTOV2
    {
        public string ClientBuyerID { get; set; }
        public string Name { get; set; }
        public int? IsWebsite { get; set; }
        public string WebsiteURL { get; set; }
        public string Profile { get; set; }
        public string Country { get; set; }
        public decimal? TransactionVolume { get; set; }
        public decimal? TransactionValue { get; set; }
        public string AdverseInfo { get; set; }
    }
    public class UpdateCorporateClientClientSellerDTOV2
    {
        public string SellerID { get; set; }
        public string Name { get; set; }
        public int? IsWebsite { get; set; }
        public string Website { get; set; }
        public string Profile { get; set; }
        public string Country { get; set; }
        public decimal? TransactionVolume { get; set; }
        public decimal? TransactionValue { get; set; }
        public string AdverseInfo { get; set; }
    }
}
