﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model
{
    public class DebitCardCategory
    {
        public string CategoryID { get; set; }
        public string Description { get; set; }
    }
}
