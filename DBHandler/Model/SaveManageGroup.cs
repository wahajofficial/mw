﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.SaveManageGroup
{
	public class SaveManageGroupModel
	{
		public SignOnRq SignOnRq { get; set; }
		[Required(ErrorMessage = "GroupID is a mandatory field.")]
		[MaxLength(30, ErrorMessage = "- GroupID length should not be more than 10.")]
		[MinLength(3, ErrorMessage = "- GroupID length should not be less than 3.")]
		public string GroupID { get; set; }
		[Required(ErrorMessage = "ClientID is a mandatory field.")]
		[MaxLength(9, ErrorMessage = "- ClientID length should not be more than 10.")]
		public string ClientID { get; set; }
		[Required(ErrorMessage = "GroupName is a mandatory field.")]
		[MaxLength(30, ErrorMessage = "- GroupName length should not be more than 30.")]
		[MinLength(3, ErrorMessage = "- GroupName length should not be less than 3.")]
		public string GroupName { get; set; }
		[Required(ErrorMessage = "Action is a mandatory field.")]
		[StringRangeAttribute(AllowableValues = new[] { "A", "E" }, ErrorMessage = "- Valid value is either A or E.")]
		public string Action { get; set; }
	}

	public class SaveManageGroupResponse
	{
		public string GroupID { get; set; }
		public string ClientID { get; set; }
		public string GroupName { get; set; }
	}

}
