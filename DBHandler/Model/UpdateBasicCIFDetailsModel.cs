﻿using CommonModel;
using CoreHandler.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model
{
   public class UpdateAccountCIFNameModel
    {
        public SignOnRq SignOnRq { get; set; }
        public string accountId { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }
        public string accountTitle { get; set; }
    }
    public class UpdateAccountCIFNameResponse
    {
            public string accountId { get; set; }
            public string firstName { get; set; }
            public string middleName { get; set; }
            public string lastName { get; set; }
            public string accountTitle { get; set; }
    }



}
