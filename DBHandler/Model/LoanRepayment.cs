﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
namespace DBHandler.Model.LoanRepayment
{
    public class LoanRepaymentModel
    {

        public SignOnRq SignOnRq { get; set; }

        [Required(ErrorMessage = "ClientId is required")]
        public string ClientId { get; set; }
        [Required(ErrorMessage = "FacId is required")]
        public string FacId { get; set; }
        [Required(ErrorMessage = "ProdId is required")]
        public string ProdId { get; set; }
        [Required(ErrorMessage = "DealRefNo is required")]
        public string DealRefNo { get; set; }
        [Required(ErrorMessage = "RePaymentType is required")]
        [StringRangeAttribute(AllowableValues = new[] { "Partial", "Full" }, ErrorMessage = "- Valid value is either Partial or Full.")]
        public string RePaymentType { get; set; }
        [Required(ErrorMessage = "RepaymentAmt is required")]
        public decimal RepaymentAmt { get; set; }
        [Required(ErrorMessage = "CurrAccID is required")]
        public string CurrAccID { get; set; }
        [Required(ErrorMessage = "PaymentMode is required")]
        public string PaymentMode { get; set; }
        [Required(ErrorMessage = "PaymentSource is required")]
        public string PaymentSource { get; set; }
        [Required(ErrorMessage = "TransactionDate is required")]
        public DateTime TransactionDate { get; set; }
        [Required(ErrorMessage = "ForceDebit is required")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string ForceDebit { get; set; }
        public string RefNo { get; set; }
        public string FeeType { get; set; }
        public string ReversalType { get; set; }
        public string ReversalReason { get; set; }

    }

    public class LoanRepaymentResponse
    {
        public string RepayRefNo { get; set; }

    }
}
