﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.Host
{
    public class BalanceEnquiry
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(8, ErrorMessage = "- Character length should not be more than 8.")]
        public string ATMID { get; set; }
        public string AccountID { get; set; }
        public string Supervision { get; set; }
        public string RefNo { get; set; }
        public DateTime PHXDate { get; set; }
        public string MerchantType { get; set; }
        public string WithdrawlBranchID { get; set; }
        public string AckInstIDCode { get; set; }
        public string ProcCode { get; set; }
        public decimal SettlmntAmount { get; set; }
        public decimal ConvRate { get; set; }
        public string AcqCountryCode { get; set; }
        public string CurrCodeTran { get; set; }
        public string CurrCodeSett { get; set; }
        public string ForwardInstID { get; set; }
        public string MessageType { get; set; }
        public string OrgTrxRefNo { get; set; }
        public string POSEntryMode { get; set; }
        public string POSConditionCode { get; set; }
        public string POSPINCaptCode { get; set; }
        public string RetRefNum { get; set; }
        public string CardAccptID { get; set; }
        public string CardAccptNameLoc { get; set; }
        public string VISATrxID { get; set; }
        public string CAVV { get; set; }
        public string ResponseCode { get; set; }
        public string ExtendedData { get; set; }
    }
}
