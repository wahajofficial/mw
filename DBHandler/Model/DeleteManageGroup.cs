﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.DeleteManageGroup
{
	public class DeleteManageGroupModel
	{
		public SignOnRq SignOnRq { get; set; }
		[Required(ErrorMessage = "GroupID is a mandatory field.")]
		[MaxLength(30, ErrorMessage = "- GroupID length should not be more than 10.")]
		[MinLength(3, ErrorMessage = "- GroupID length should not be less than 3.")]
		public string GroupID { get; set; }
	}

	public class DeleteManageGroupResponse
	{
		public string GroupID { get; set; }
	}
}
