﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBHandler.Model
{
    [Table("t_Documents")]
    public class Documents
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 ID { get; set; }
        public string documentId { get; set; }
        public string OurBranchID { get; set; }
        public string ChannelID { get; set; }
        public string base64Representation  { get; set; }
        public string documentType { get; set; }
        public string fileName { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public int Version { get; set; }
        public string parent_path { get; set; }
        public string categoryName { get; set; }
    }

    public class DocumentSaveResponse
    {
        public string documentId { get; set; }
        public int Version { get; set; }
    }
}
