﻿
using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class InquireCorporateCifV2Model
    {
        
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string ClientId { get; set; }
    }
}
