﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model
{
    public class AddEditAccountResponse
    {
        public string AccountId { get; set; }
        public string IBAN { get; set; }
    }
}
