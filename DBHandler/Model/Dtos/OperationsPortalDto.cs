﻿using CommonModel;
using DBHandler.Model.OperationsPortal;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.Dtos
{
    public class GetGLPostingAccountDto
    {
        public SignOnRq signOnRq { get; set; }
        public GetGLPostingAccount data { get; set; }
    }

    public class inc_F2Dto
    {
        public SignOnRq signOnRq { get; set; }
        public inc_F2 data { get; set; }
    }

    public class inc_Get_BeforeDto
    {
        public SignOnRq signOnRq { get; set; }
        public inc_Get_Before data { get; set; }
    }
}