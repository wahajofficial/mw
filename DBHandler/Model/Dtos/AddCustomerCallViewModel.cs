﻿using DBHandler.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBHandler.Model.Dtos
{
    public class AddCustomerCallViewModel
    {
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Please Send Date.")]
        public DateTime CallDate { get; set; }

        [Required(ErrorMessage = "Please Send Gender.")]
        public string Gender { get; set; }

        [DataType(DataType.Time)]
        [Required(ErrorMessage = "Please Send Start Time.")]
        public string StartTime { get; set; }

       
        [Required(ErrorMessage = "Please Send Start Time.")]
        public int DateToSend { get; set; }


        [Required(ErrorMessage = "Please enter Product Id.")]
        [StringLength(8, ErrorMessage = "Please enter a valid Product Id.", MinimumLength = 5)]
        public string ProductId { get; set; }

        [Required(ErrorMessage = "Please enter Account Id.")]
        public int AccountId { get; set; }

        [Required(ErrorMessage = "Please enter Name.")]
        
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter NotificationID.")]

        public string NotificationID { get; set; }
        [Required(ErrorMessage = "Please enter DeviceID.")]

        public string DeviceID { get; set; }

        public string Platform { get; set; }

        public BaseClass BaseClass { get; set; }
    }
}
