﻿using CommonModel;
using DBHandler.Model.Host;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.Dtos
{
    public class CashWithDrawalDto
    {
        public SignOnRq signOnRq { get; set; }
        public CashWithDrawal data { get; set; }
    }

    public class PreAuthorizationDto
    {
        public SignOnRq signOnRq { get; set; }
        public PreAuthorization data { get; set; }
    }
    public class PurchaseRequestDto
    {
        public SignOnRq signOnRq { get; set; }
        public PurchaseRequest data { get; set; }
    }
    public class FullReversal
    {
        public SignOnRq signOnRq { get; set; }
        public FullReversalDto data { get; set; }
    }
    public class PartialReversal
    {        
        public SignOnRq signOnRq { get; set; }
        public PartialReversalDto data { get; set; }
    }
    public class BalanceEnquiryDto
    {
        public SignOnRq signOnRq { get; set; }
        public BalanceEnquiry data { get; set; }
    }
    public class AccountVerificationDto
    {
        public SignOnRq signOnRq { get; set; }
        public AccountVerification data { get; set; }
    }
    public class CreditTransactionDto
    {
        public SignOnRq signOnRq { get; set; }
        public CreditTransaction data { get; set; }
    }

    public class FileManagementMsgDto
    {
        public SignOnRq signOnRq { get; set; }
        public FileManagementMsg data { get; set; }
    }

    public class GenericHostDto
    {
        public SignOnRq signOnRq { get; set; }
        // public string ourBranchId { get; set; }
    }
}
