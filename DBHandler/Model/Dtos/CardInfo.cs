﻿using DBHandler.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.Dtos
{
    public class CardInfo
    {
        [Required(ErrorMessage = "Please Provide All Data")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Please Provide All Data")]
        public string CompleteAddress { get; set; }

        public string NotesToDriver { get; set; }

        public string UserCardName { get; set; }
        public BaseClass BaseClass { get; set; }
    }

    public class CardReceive
    {
        [Required(ErrorMessage = "Please Provide All Data")]
        public string TrackingId { get; set; }
        public BaseClass BaseClass { get; set; }

    }

    

    public enum Action
    {
        Pending,
        Delivered,
        Activate
    }
}
