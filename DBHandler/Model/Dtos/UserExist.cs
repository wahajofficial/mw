﻿using DBHandler.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.Dtos
{
    public class UserExist
    {
        public string UserName { get; set; }
        public BaseClass BaseClass { get; set; }
    }
}
