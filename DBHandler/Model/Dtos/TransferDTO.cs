﻿using DBHandler.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBHandler.Model.Dtos
{
    public class GetCurrency
    {
        public string CurrencyId { get; set; }
        public double Rate { get; set; }
        public double PRate { get; set; }
        public int Rounding { get; set; }
    }

    public class ConvertCurrency
    {
        public double Amount { get; set; }
        public double Rate { get; set; }
        public double LocalEq { get; set; }
    }

    public class GetCurrencyDto
    {
        [Display(Name = "From Currency")]
        [Required(ErrorMessage = "{0} is mandatory")]
        [StringLength(3, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string Currency { get; set; }

        [Display(Name = "Type")]
        [Required(ErrorMessage = "{0} is mandatory")]
        [StringLength(1, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        public string Type { get; set; }

        [Display(Name = "Device ID")]
        [Required(ErrorMessage = "{0} is mandatory")]
        public string DeviceID { get; set; }

        public BaseClass BaseClass { get; set; }
    }

    public class ConvertCurrencyDto
    {
        [Display(Name = "From Currency")]
        [Required(ErrorMessage = "{0} is mandatory")]
        [StringLength(3, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string FromCurrency { get; set; }

        [Display(Name = "To Currency")]
        [Required(ErrorMessage = "{0} is mandatory")]
        [StringLength(3, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string ToCurrency { get; set; }

        [Display(Name = "Amount")]
        [Required(ErrorMessage = "{0} is mandatory")]
        [Range(typeof(double), "0.1", "999999999999", ErrorMessage = "Plese enter a valid {0}")]
        public double Amount { get; set; }

        [Display(Name = "Type")]
        [Required(ErrorMessage = "{0} is mandatory")]
        [StringLength(1, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        public string Type { get; set; }

        [Display(Name = "Device ID")]
        [Required(ErrorMessage = "{0} is mandatory")]
        public string DeviceID { get; set; }

        public BaseClass BaseClass { get; set; }
    }

    public class ADHOCOTP
    {
        [Display(Name = "Transfer Type")]
        [Required(ErrorMessage = "{0} is mandatory")]
        public string TransferType { get; set; } // D for fawri , N for fawri+ , I for internal KFH account , O for own KFH account , S for international bank account

        [Display(Name = "From Account No.")]
        [Required(ErrorMessage = "{0} is mandatory")]
        public string FromAccountId { get; set; } // IBAN

        [Display(Name = "From Account Title")]
        [Required(ErrorMessage = "{0} is mandatory")]
        [StringLength(100, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string FromAccountTitle { get; set; } // full name

        [Display(Name = "From Currency")]
        [Required(ErrorMessage = "{0} is mandatory")]
        [StringLength(3, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string FromCurrency { get; set; }

        [Display(Name = "To Account No.")]
        [Required(ErrorMessage = "{0} is mandatory")]
        public string ToAccountId { get; set; } // IBAN

        
        [Display(Name = "To Currency")]
        [Required(ErrorMessage = "{0} is mandatory")]
        [StringLength(3, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string ToCurrency { get; set; }
        
        [Display(Name = "Amount")]
        [Required(ErrorMessage = "{0} is mandatory")]
        [Range(typeof(double), "0.1", "999999999999", ErrorMessage = "Plese enter a valid {0}")]
        public double Amount { get; set; } // amount to transfer

        [Display(Name = "Exchange Rate")]
        [Required(ErrorMessage = "{0} is mandatory")]
        public double ExRate { get; set; } // exchange rate - 1.00 if BHD

        [Display(Name = "Local Eq.")]
        [Required(ErrorMessage = "{0} is mandatory")]
        [Range(typeof(double), "0.1", "999999999999", ErrorMessage = "Plese enter a valid {0}")]
        public double LocalEq { get; set; } // amount in BHD

        
        [Display(Name = "Device ID")]
        [Required(ErrorMessage = "{0} is mandatory")]
        public string DeviceID { get; set; }


        public BaseClass BaseClass { get; set; }
        //[Display(Name = "Device ID")]
        //[Required(ErrorMessage = "{0} is mandatory")]


    }



    public class TransfersDto1
    {
        [Display(Name = "Transfer Type")]
        [Required(ErrorMessage = "{0} is mandatory")]
        public string TransferType { get; set; } // D for fawri , N for fawri+ , I for internal KFH account , O for own KFH account , S for international bank account

        [Display(Name = "From Account No.")]
        [Required(ErrorMessage = "{0} is mandatory")]
        public string FromAccountId { get; set; } // IBAN

        [Display(Name = "From Account Title")]
        [Required(ErrorMessage = "{0} is mandatory")]
        [StringLength(100, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string FromAccountTitle { get; set; } // full name

        [Display(Name = "From Currency")]
        [Required(ErrorMessage = "{0} is mandatory")]
        [StringLength(3, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string FromCurrency { get; set; }

        [Display(Name = "To Account No.")]
        [Required(ErrorMessage = "{0} is mandatory")]
        public string ToAccountId { get; set; } // IBAN

        [Display(Name = "To Account Title")]
        [StringLength(100, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        [Required(ErrorMessage = "{0} is mandatory")]
        public string ToAccountTitle { get; set; } // full name

        [Display(Name = "To Currency")]
        [Required(ErrorMessage = "{0} is mandatory")]
        [StringLength(3, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string ToCurrency { get; set; }

        [Display(Name = "Beneficiary ID")]
        [Required(ErrorMessage = "{0} is mandatory")]
        public string BeneficiaryId { get; set; } // beneficiary ID - if new add new beneficiary and get ben. id

        [Display(Name = "Amount")]
        [Required(ErrorMessage = "{0} is mandatory")]
        [Range(typeof(double), "0.1", "999999999999", ErrorMessage = "Plese enter a valid {0}")]
        public double Amount { get; set; } // amount to transfer

        [Display(Name = "Exchange Rate")]
        [Required(ErrorMessage = "{0} is mandatory")]
        public double ExRate { get; set; } // exchange rate - 1.00 if BHD

        [Display(Name = "Local Eq.")]
        [Required(ErrorMessage = "{0} is mandatory")]
        [Range(typeof(double), "0.1", "999999999999", ErrorMessage = "Plese enter a valid {0}")]
        public double LocalEq { get; set; } // amount in BHD

        [Display(Name = "Purpose of Transfer")]
        [StringLength(100, ErrorMessage = "{0} must be at least {2} characters long.")]
        public string PurposeOfTransfer { get; set; }

        public string Charges { get; set; }

        [Display(Name = "Device ID")]
        [Required(ErrorMessage = "{0} is mandatory")]
        public string DeviceID { get; set; }


       
        public bool IsPartial { get; set; }


        public BaseClass BaseClass { get; set; }
        //[Display(Name = "Device ID")]
        //[Required(ErrorMessage = "{0} is mandatory")]


    }

    public class TransfersDto
    {
        [Display(Name = "Transfer Type")]
        [Required(ErrorMessage = "{0} is mandatory")]
        public string TransferType { get; set; } // D for fawri , N for fawri+ , I for internal KFH account , O for own KFH account , S for international bank account

        [Display(Name = "From Account No.")]
        [Required(ErrorMessage = "{0} is mandatory")]
        public string FromAccountId { get; set; } // IBAN

        [Display(Name = "From Account Title")]
        [Required(ErrorMessage = "{0} is mandatory")]
        [StringLength(100, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string FromAccountTitle { get; set; } // full name

        [Display(Name = "From Currency")]
        [Required(ErrorMessage = "{0} is mandatory")]
        [StringLength(3, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string FromCurrency { get; set; }

        [Display(Name = "To Account No.")]
        [Required(ErrorMessage = "{0} is mandatory")]
        public string ToAccountId { get; set; } // IBAN

        [Display(Name = "To Account Title")]
        [StringLength(100, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        [Required(ErrorMessage = "{0} is mandatory")]
        public string ToAccountTitle { get; set; } // full name

        [Display(Name = "To Currency")]
        [Required(ErrorMessage = "{0} is mandatory")]
        [StringLength(3, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string ToCurrency { get; set; }

        [Display(Name = "Beneficiary ID")]
        [Required(ErrorMessage = "{0} is mandatory")]
        public string BeneficiaryId { get; set; } // beneficiary ID - if new add new beneficiary and get ben. id

        [Display(Name = "Amount")]
        [Required(ErrorMessage = "{0} is mandatory")]
        [Range(typeof(double), "0.1", "999999999999", ErrorMessage = "Plese enter a valid {0}")]
        public double Amount { get; set; } // amount to transfer

        [Display(Name = "Exchange Rate")]
        [Required(ErrorMessage = "{0} is mandatory")]
        public double ExRate { get; set; } // exchange rate - 1.00 if BHD

        [Display(Name = "Local Eq.")]
        [Required(ErrorMessage = "{0} is mandatory")]
        [Range(typeof(double), "0.1", "999999999999", ErrorMessage = "Plese enter a valid {0}")]
        public double LocalEq { get; set; } // amount in BHD

        [Display(Name = "Purpose of Transfer")]
        [StringLength(100, ErrorMessage = "{0} must be at least {2} characters long.")]
        public string PurposeOfTransfer { get; set; }
        [Required(ErrorMessage = "OTP is mandatory")]
        [Display(Name = "OTP Code")]
        public string OTPCode { get; set; }
        public string Charges { get; set; }
        public string CardNo { get; set; }
        public string CardToken { get; set; }
        public DateTime? TransferDate { get; set; }
        public bool IsPartial { get; set; }

        public BaseClass BaseClass { get; set; }
    }

    public class EftsDto
    {
        public string Type { get; set; }
        public string ReferenceId { get; set; }

        public string Amount { get; set; }

        public string SenderIBAN { get; set; }
        public string SenderTitle { get; set; }

        public string ReceiverIBAN { get; set; }
        public string ReceiverTitle { get; set; }
        public string ReceiverBankCode { get; set; }
        public string ReceiverBankEftsCode { get; set; }
        public string ReceiverBankSwiftCode { get; set; } 
        public string ValueDate { get; set; }
    }
}
