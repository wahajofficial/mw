﻿using CommonModel;
using DBHandler.Model.StatementSmart;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.Dtos
{
    public class CustomerStatementDto
    {
        public SignOnRq signOnRq { get; set; }
        public CustStatementDateWise data { get; set; }
    }
    public class TrxListLastNTrxDto
    {
        public SignOnRq signOnRq { get; set; }
        public TrxListLastNTrx data { get; set; }
    }
    public class CBStatementReqDto
    {
        public SignOnRq signOnRq { get; set; }
        public CBStatementRequest data { get; set; }
    }
    public class MonthlyStatementDto
    {
        public SignOnRq signOnRq { get; set; }
        public MonthlyStatement data { get; set; }
    }
    public class VATAdviceDto
    {
        public SignOnRq signOnRq { get; set; }
        public VATAdvice data { get; set; }
    }
    public class MonthlyVATAdviceDto
    {
        public SignOnRq signOnRq { get; set; }
        public MonthlyVATAdvice data { get; set; }
    }
    public class TrxAdviceDto
    {
        public SignOnRq signOnRq { get; set; }
        public TrxAdvice data { get; set; }
    }
    public class DebitAdviceDto
    {
        public SignOnRq signOnRq { get; set; }
        public DebitAdvice data { get; set; }
    }
    public class CreditAdviceDto
    {
        public SignOnRq signOnRq { get; set; }
        public CreditAdvice data { get; set; }
    }
    public class GetStatementDetailsDto
    {
        public SignOnRq signOnRq { get; set; }
        public GetStatementDetails data { get; set; }
    }
    public class MonthlyStatementStatusDto
    {
        public SignOnRq signOnRq { get; set; }
        public MonthlyStatementStatus data { get; set; }
    }
}
