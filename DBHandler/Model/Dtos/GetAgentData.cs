﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBHandler.Model.Dtos
{
    
        public class GetAgentData
        {
            public int? Id { get; set; }
            public string Name { get; set; }
            public string Gender { get; set; }
        }
    
}
