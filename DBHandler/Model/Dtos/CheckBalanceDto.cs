﻿using DBHandler.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.Dtos
{
    public class CheckBalanceDto
    {
        public string Id { get; set; }
        public decimal Amount { get; set; }
        public BaseClass BaseClass { get; set; }
    }
}
