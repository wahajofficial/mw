﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBHandler.Model.Dtos
{
    public class NonBahrainSignupDto
    {
        [Display(Name = "Email")]
        [Required(ErrorMessage = "Please enter Email address")]
        [EmailAddress(ErrorMessage = "Please enter a valid Email address")]
        [StringLength(200, ErrorMessage = "Email address must not be greator than 200 characters")]
        public string EmailId { get; set; }

        [Required(ErrorMessage = "Please enter Name.")]
        [StringLength(100, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string Name { get; set; }


        [Required(ErrorMessage = "Please enter Device Id.")]
        
        public string DeviceId { get; set; }

        [Required(ErrorMessage = "Please enter Mobile Number.")]
        //[RegularExpression("^[0]([0-9])+$", ErrorMessage = "Please enter a valid Mobile Number")]
        [StringLength(15, ErrorMessage = "Mobile Number must be at least {2} and at max {1} characters long.", MinimumLength = 11)]
        public string MobileNumber { get; set; }

        //[Required(ErrorMessage = "Please enter Nationality.")]
        //[StringLength(10, ErrorMessage = "Nationality must not be greator than 10 characters.")]
        //public string Nationality { get; set; }

        //[Required(ErrorMessage = "Please enter Date of Birth.")]
        //public DateTime DOB { get; set; }

        //[Required(ErrorMessage = "Please select Gender.")]
        //[StringLength(10, ErrorMessage = "Nationality must not be greator than 10 characters.")]
        //public string Gender { get; set; }
    }
}
