﻿using DBHandler.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.Dtos
{
    public class EmailVerification
    {
        public string Email { get; set; }
        public BaseClass BaseClass { get; set; }
    }
}
