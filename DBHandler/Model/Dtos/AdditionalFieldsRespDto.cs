﻿using DBHandler.Helper;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DBHandler.Model.Dtos
{//ToDO:Rameez
    public class AdditionalFieldsRespDto
    {
        [Required(ErrorMessage = "Please enter Product Id.")]
        [StringLength(8, ErrorMessage = "Please enter a valid Product Id.", MinimumLength = 5)]
        public string ProductId { get; set; }

        //public int AddressType { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string BlockNo { get; set; }
        public string Town { get; set; }
        public string PhoneNo { get; set; }
        public int EducationLevel { get; set; }
        //public string CorrespondenceLanguage { get; set; }
        public int Pep { get; set; }
        public int PepRelation { get; set; }
        public string PepRelative { get; set; }

        public int EmploymentType { get; set; }

        public string EmployerName { get; set; }
        public string MonthlyIncome { get; set; }
        public int Occupation { get; set; }
        public string JobTitle { get; set; }

        public string CompanyName { get; set; }
        public DateTime? DateOfIncorporation { get; set; }
        public string PlaceOfIncorporation { get; set; }
        public string CompanyInvolvement { get; set; }
        public string LiquidationQuestion { get; set; }
        public string LegalForm { get; set; }
        public string GuardianRelation { get; set; }
        public string PlaceOfBirth { get; set; }
        public string SourceofFund { get; set; }
        public string OtherDescription { get; set; }
        
        public string HomeType { get; set; }
        public string MinorEMployementType { get; set; }
        public string DeviceID { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
    public BaseClass BaseClass { get; set; }
    }
}
