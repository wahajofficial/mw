﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class GetPreAccountClosure
    {
       
        public SignOnRq SignOnRq { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string AccountId { get; set; }
    }
}
