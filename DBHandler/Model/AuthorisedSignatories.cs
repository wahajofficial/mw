﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace DBHandler.Model
{
    public class AuthorisedSignatoriesView
    {
        public decimal? AuthorisedSignatoriesID { get; set; }
        public string AuthorisedsignatoriesName { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailID { get; set; }
        public string CorrespondenceAddress { get; set; }
        public string AuthorisedsignatoryIDtype1 { get; set; }
        public string AuthorisedsignatoryIDNumber1 { get; set; }
        //[FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> AuthorisedsignatoryIDExpirydate1 { get; set; }
        public string AuthorisedsignatoryIDIssueCountry1 { get; set; }
        public string AuthorisedsignatoryIDtype2 { get; set; }
        public string AuthorisedsignatoryIDNumber2 { get; set; }
        [FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> AuthorisedsignatoryIDExpirydate2 { get; set; }
        public string AuthorisedsignatoryIDIssueCountry2 { get; set; }
        public string AuthorisedsignatoryVisaIssuedby { get; set; }
        public string AuthorisedsignatoryVisaNumber { get; set; }
        [FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> AuthorisedsignatoryVisaExpiry { get; set; }
        public string Remarks { get; set; }

        public string Blacklist { get; set; }
        public string PEP { get; set; }
        public string FATCARelevant { get; set; }
        public string CRSRelevant { get; set; }
        public string AreyouaUSCitizen { get; set; }
        public string AreyouaUSTaxResident { get; set; }
        public string WereyoubornintheUS { get; set; }
        public string USAORUAE { get; set; }
        public string TINAvailable { get; set; }
        public string TinNo1 { get; set; }
        public string TaxCountry1 { get; set; }
        public string TinNo2 { get; set; }
        public string TaxCountry2 { get; set; }
        public string TinNo3 { get; set; }
        public string TaxCountry3 { get; set; }
        public string TinNo4 { get; set; }
        public string TaxCountry4 { get; set; }
        public string TinNo5 { get; set; }
        public string TaxCountry5 { get; set; }
        public string FATCANoReason { get; set; }
        public string ReasonB { get; set; }

        public string IDGender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string IDNationality { get; set; }

        public string ASCity { get; set; }
        public string ASState { get; set; }
        public string ASCountry { get; set; }
        public string groupid { get; set; }
    }

    public class AuthorisedSignatories 
    {
        public decimal? AuthorisedSignatoriesID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string AuthorisedsignatoriesName { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        [Email(ErrorMessage = "- is not a valid email.")]
        public string EmailID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string CorrespondenceAddress { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string AuthorisedsignatoryIDtype1 { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string AuthorisedsignatoryIDNumber1 { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        //[FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> AuthorisedsignatoryIDExpirydate1 { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string AuthorisedsignatoryIDIssueCountry1 { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string AuthorisedsignatoryIDtype2 { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string AuthorisedsignatoryIDNumber2 { get; set; }

        [FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> AuthorisedsignatoryIDExpirydate2 { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string AuthorisedsignatoryIDIssueCountry2 { get; set; }

        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string AuthorisedsignatoryVisaIssuedby { get; set; }

        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string AuthorisedsignatoryVisaNumber { get; set; }

        [FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> AuthorisedsignatoryVisaExpiry { get; set; }

        [MaxLength(500, ErrorMessage = "- Character length should not be more than 500.")]
        public string Remarks { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string Blacklist { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string PEP { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string FATCARelevant { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string CRSRelevant { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string AreyouaUSCitizen { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string AreyouaUSTaxResident { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string WereyoubornintheUS { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string USAORUAE { get; set; }
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string TINAvailable { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo1 { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry1 { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo2 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry2 { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo3 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry3 { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo4 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry4 { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo5 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry5 { get; set; }

        [MaxLength(10, ErrorMessage = "- Character length should not be more than 10.")]
        [StringRangeAttribute(AllowableValues = new[] { "Reason A", "Reason B", "Reason C" }, ErrorMessage = "- Valid value is either Reason A,Reason B orReason C.")]
        public string FATCANoReason { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string ReasonB { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "M", "F", "T" }, ErrorMessage = "valid value for this filed is M,F or T")]
        [MaxLength(1, ErrorMessage = "Gender length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string IDGender { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public DateTime? DateOfBirth { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string IDNationality { get; set; }

       // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string ASCity { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string ASState { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string ASCountry { get; set; }
        public string GroupID { get; set; }
    }

    public class AuthorisedSignatoriesVoew
    {
        public decimal? AuthorisedSignatoriesID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string AuthorisedsignatoriesName { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        [Email(ErrorMessage = "- is not a valid email.")]
        public string EmailID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string CorrespondenceAddress { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string AuthorisedsignatoryIDtype1 { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string AuthorisedsignatoryIDNumber1 { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        //[FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> AuthorisedsignatoryIDExpirydate1 { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string AuthorisedsignatoryIDIssueCountry1 { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string AuthorisedsignatoryIDtype2 { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string AuthorisedsignatoryIDNumber2 { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> AuthorisedsignatoryIDExpirydate2 { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string AuthorisedsignatoryIDIssueCountry2 { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string AuthorisedsignatoryVisaIssuedby { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string AuthorisedsignatoryVisaNumber { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> AuthorisedsignatoryVisaExpiry { get; set; }

        [MaxLength(500, ErrorMessage = "- Character length should not be more than 500.")]
        public string Remarks { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string Blacklist { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string PEP { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string FATCARelevant { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string CRSRelevant { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string AreyouaUSCitizen { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string AreyouaUSTaxResident { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string WereyoubornintheUS { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string USAORUAE { get; set; }
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string TINAvailable { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo1 { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry1 { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo2 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry2 { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo3 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry3 { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo4 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry4 { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo5 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry5 { get; set; }

        [MaxLength(10, ErrorMessage = "- Character length should not be more than 10.")]
        [StringRangeAttribute(AllowableValues = new[] { "Reason A", "Reason B", "Reason C" }, ErrorMessage = "- Valid value is either Reason A,Reason B orReason C.")]
        public string FATCANoReason { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string ReasonB { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string GroupID { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "M", "F", "T" }, ErrorMessage = "valid value for this filed is M,F or T")]
        [MaxLength(1, ErrorMessage = "Gender length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string IDGender { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public DateTime? DateOfBirth { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string IDNationality { get; set; }

       // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string ASCity { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string ASState { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string ASCountry { get; set; }
    }
    


   
}
