﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
   public class GetCustomerIDExpiryDto
    {
        public SignOnRq SignOnRq { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public DateTime ExpiryDate { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string ExpiryColumnName { get; set; }
        public string Type { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "AS", "SH", "BO", "BD", "Customer", }, ErrorMessage = "- Valid value is either AS,SH,BO,BD or customer")]
        public string Module { get; set; }
    }
}
