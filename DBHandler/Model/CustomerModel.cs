﻿using DBHandler.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class CustomerModel
    {
        public CustomerCorporateRequest Cust { get; set; }

        [Required(ErrorMessage = "Shareholder require")]
        public List<ShareHolderView> ShareHolders { get; set; }

        [Required(ErrorMessage = "AuthorisedSignatories require")]
        public List<AuthorisedSignatoriesView> Authignatories { get; set; }

        [Required(ErrorMessage = "BoardDirectors require")]
        public List<BoardDirectorsView> BoardDirectors { get; set; }


        [Required(ErrorMessage = "BeneficialOwners require")]
        public List<BeneficialOwnersView> BeneficialOwners { get; set; }
    }
}
