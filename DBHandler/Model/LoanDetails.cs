﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.LoanDetail
{
    public class LoanDetailsModel
    {

        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = "ClientId is required")]
        public string ClientId { get; set; }
        [Required(ErrorMessage = "Facility Id is required")]
        public string FacilityID { get; set; }
        [Required(ErrorMessage = "Product Id is required")]
        public string ProductID { get; set; }
    }
    public class LoanDetailsResponse
    {
        public string DealID { get; set; }
        public decimal Amount { get; set; }
        public string CurrencyID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime MaturityDate { get; set; }
        public string InterestRateType { get; set; }
        public decimal InterestRate { get; set; }
        public string BaseRate { get; set; }
        public decimal Margin { get; set; }
        public Repayment Repayment { get; set; }
        public CurrPosition CurrentPosition { get; set; }
    }
    public class Repayment
    {
        public string FundingAccount { get; set; }
        public string DisbursmentAccount { get; set; }
        public decimal TotalInstallments { get; set; }
        public decimal PaidInstallments { get; set; }
        public decimal RemainingInstallments { get; set; }
        public string PaymentMethod { get; set; }
        public int PastDueAmount { get; set; }
        public int InterestRepaymentAmount { get; set; }
    }
    public class CurrPosition
    {
        public decimal OutstandingAmount { get; set; }
        public DateTime? CurrInstallmentPaymentDate { get; set; }
        public decimal CurrentInstallmentRate { get; set; }
        public decimal? CurrInstallmentAmount { get; set; }
    }
}
