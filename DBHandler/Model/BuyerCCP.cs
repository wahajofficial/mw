﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;


namespace DBHandler.Model.BuyerCCP
{

    public class BuyerCCPsModel
    {

        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = "ClientId is required")]
        public string ClientID { get; set; }
    }

    public class BuyerCCPsResponse
    {
        public string CreditProgramID { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string Name { get; set; }
        public decimal ApprovedLimit { get; set; }
        public decimal UtilizedLimit { get; set; }
        public decimal AvaialableLimit { get; set; }
        public string CurrencyID { get; set; }
    }
}

