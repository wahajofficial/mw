﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.AddUpdateBorrowManDate
{
	public class AddUpdateBorrowManDateModel
	{
		public SignOnRq SignOnRq { get; set; }


		[Required(ErrorMessage = " is a mandatory field.")]
		public string AccountID { get; set; }
		[Required(ErrorMessage = " is a mandatory field.")]
		[Range(0, int.MaxValue, ErrorMessage = "Only positive number allowed")]
		public int SerialID { get; set; }
		[Required(ErrorMessage = " is a mandatory field.")]
		public decimal MinLimit { get; set; }

		[Required(ErrorMessage = " is a mandatory field.")]
		public decimal MaxLimit { get; set; }

		public string AuthGroupID1 { get; set; }
		public string AuthGroupID2 { get; set; }
		public string AuthGroupID3 { get; set; }
		public string AuthGroupID4 { get; set; }
		//[StringRangeAttribute(AllowableValues = new[] { "A", "E", "I", "L" }, ErrorMessage = "- Valid value is either A,E,I or L.")]
		public string ManDateType { get; set; }
		public string Comments { get; set; }
		public string AuthGroupID5 { get; set; }
		public string AuthGroupID6 { get; set; }


	}

	public class AddUpdateBorrowManDateResponse
	{
		public int SerialID { get; set; }
	}
}
