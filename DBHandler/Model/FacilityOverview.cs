﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.FacilityOverview
{
    public class FacilityOverviewModel
    {
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = "ClientId is required")]
        public string ClientId { get; set; }
    }
    public class FacilityOverviewResponse
    {
        public string FacilityID { get; set; }
        public DateTime ReviewDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string SecuredType { get; set; }
        public string LimitType { get; set; }
        public decimal ApprovedLimit { get; set; }
        public decimal UtilizedLimit { get; set; }
        public decimal AvailableLimit { get; set; }
        public decimal NoOfLoanProducts { get; set; }
        public string CreditProgramID { get; set; }
        public string CreditProgramName { get; set; }
        public string Status { get; set; }
    }
}
