﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace DBHandler.Model
{
    public class VendorMWContext : Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityDbContext<ApplicationUser>
    {
        public VendorMWContext(DbContextOptions<VendorMWContext> options)
           : base(options)
        {
        }

        public DbSet<LogRqCh> TLogRqCh { get; set; }
        public DbSet<Log> TLog { get; set; }
        public DbSet<UserChannels> TUserChannels { get; set; }
        public DbSet<UsersFunctions> TUsersFunctions { get; set; }
        public DbSet<TransactionImage> TransactionImages { get; set; }
        public DbSet<LinkAccount> TLinkAccounts { get; set; }
        public DbSet<Product> TProducts { get; set; }
        public DbSet<AdditionalDoc> TAdditionalDocs { get; set; }
        public DbSet<User> TUsers { get; set; }
        public DbSet<IdCard> TIdCards { get; set; }
        public DbSet<Version> TVersion { get; set; }
        public DbSet<Currencies> TCurrencies { get; set; }
        public DbSet<Documents> TDocuments { get; set; }
        public DbSet<DocumentMetaData> TDocumentMetaData { get; set; }
        public DbSet<ErrorMessages> TErrorMessages { get; set; }
        public DbSet<TVendorApiHeaders> TVendorApiHeaders { get; set; }
        public DbSet<TVkeyRegisteredUser> TVkeyRegisteredUser { get; set; }
        //ProcedureModel
        public DbSet<ValidateChannelModel> ValidateChannelProcedure { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Documents>().HasKey(t => new { t.ID });
            modelBuilder.Entity<DocumentMetaData>().HasKey(t => new { t.ID});
            modelBuilder.Entity<ErrorMessages>().HasKey(t => new { t.ErrorCode});
            modelBuilder.Entity<TVendorApiHeaders>().ToTable("t_ApiHeaders");
            modelBuilder.Entity<TVkeyRegisteredUser>().ToTable("t_VkeyRegisteredUser").HasKey(x=>x.id);
            modelBuilder.Entity<ValidateChannelModel>().HasNoKey();
            //modelBuilder.Entity<LogRqCh>().HasNoKey();
        }
    }
}
