﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.SPLUNK.SplunkConnectorModelForLogRQ
{
   public  class SplunkConnectorModel
    {
        public long time { get; set; }
        public string host { get; set; }
        public string source { get; set; }
        public object  @event { get; set; }
    }
    public class RequestParameters
    {
        public string Example { get; set; }
        public string Example2 { get; set; }
    }

    public class Event
    {
        public object RequestParameters { get; set; }
        public object RequestResponse { get; set; }
        public int Channel { get; set; }
        public string LogId { get; set; }
        public DateTime RequestDateTime { get; set; }
        public string LocalServerIP { get; set; }
    }
}
