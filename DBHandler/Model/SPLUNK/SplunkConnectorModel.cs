﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.SPLUNK
{
   public  class SplunkConnectorModel
    {
        public long time { get; set; }
        public string host { get; set; }
        public string source { get; set; }
        public object  @event { get; set; }
    }
    public class RequestParameters
    {
        public string Example { get; set; }
        public string Example2 { get; set; }
    }

    public class Event
    {
        public string FunctionName { get; set; }
        public string ControllerName { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public object RequestParameters { get; set; }
        public object RequestResponse { get; set; }
        public int Channel { get; set; }
        public string LogId { get; set; }
        public DateTime RequestDateTime { get; set; }
        public string ErrorCode { get; set; }
        public string APIbaseURL { get; set; }
        public string APIFunctionName { get; set; }
        public string LocalServerIP { get; set; }
    }
}
