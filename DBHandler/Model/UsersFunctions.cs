﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBHandler.Model
{

    [Table("t_UsersFunctions")]
    public class UsersFunctions
    {
        [Key]
        public Int32 Id { get; set; }
        public string UserId { get; set; }
        public string functionName { get; set; }
    }
}
