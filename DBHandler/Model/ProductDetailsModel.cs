﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
   public class ProductDetailsModel
    {
       
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = "Client Id is required")]
        public string ClientID { get; set; }
        [Required(ErrorMessage = "Facility ID is required")]
        public string FacilityID { get; set; }
        [Required(ErrorMessage = "Product Type is required")]
        public string ProductType { get; set; }
    }


    public class ProductDetailsModelResponse
    {
        public string ProductTypeName { get; set; }
        public string ProductType { get; set; }
        public string CurrencyID { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? MaturityDate { get; set; }
        public decimal ApprovedLimit { get; set; }
        public decimal UtilizedLimit { get; set; }
        public decimal AvailableLimit { get; set; }
    }
}
