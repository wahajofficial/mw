﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.BalanceConfirmationResponse
{
    public class BalanceConfirmationResponse
    {
        public string letterDate { get; set; }
        public string letterReference { get; set; }
        public List<string> customerNameAddress { get; set; }
        public string requestDate { get; set; }
        public List<AccountsModel> accounts { get; set; }
    }
    public class AccountsModel
    {
        public string accountNumber { get; set; }
        public string accountType { get; set; }
        public string currency { get; set; }
        public string balance { get; set; }
    }
}
