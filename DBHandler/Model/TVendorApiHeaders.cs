﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBHandler.Model
{
    [Table("t_ApiHeaders")]
    public class TVendorApiHeaders
    {
        [Key]
        public int HeaderId { get; set; }
        public string HeaderName { get; set; }
        public string HeaderValue { get; set; }
        public string VendorId { get; set; }
        public int Type { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
}
