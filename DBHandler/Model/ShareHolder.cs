﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBHandler.Model
{
    public class ShareHolderView
    {
        public decimal? ShareHolderID { get; set; }
        public string ShareHolderName { get; set; }
        public string ShareHolderAddress { get; set; }
        public string ShareholderType { get; set; }
        public string ShareholdersIDtype { get; set; }
        public decimal? ShareholdersIDNo { get; set; }
        //[FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> ShareholdersIDExpirydate { get; set; }
        public string CountryofIncorporation { get; set; }
        public string BoardMember { get; set; }
        public string AuthorisedSignatory { get; set; }
        public string BeneficialOwner { get; set; }
        public string ResidentNonResident { get; set; }
        public string CountryofResidence { get; set; }
        public decimal? OwnershipPercentage { get; set; }
        public string Blacklist { get; set; }
        public string PEP { get; set; }
        public string FATCARelevant { get; set; }
        public string CRSRelevant { get; set; }
        public string USCitizen { get; set; }
        public string AreyouaUSTaxResident { get; set; }
        public string WereyoubornintheUS { get; set; }
        public string USAEntity { get; set; }
        public string FFI { get; set; }
        public string FFICategory { get; set; }
        public string GIINNo { get; set; }
        public string GIINNA { get; set; }
        public string SponsorName { get; set; }
        public string SponsorGIIN { get; set; }
        public string NFFE { get; set; }
        public string StockExchange { get; set; }
        public string TradingSymbol { get; set; }
        public string USAORUAE { get; set; }
        public string TINAvailable { get; set; }
        public string TinNo1 { get; set; }
        public string TaxCountry1 { get; set; }
        public string TinNo2 { get; set; }
        public string TaxCountry2 { get; set; }
        public string TinNo3 { get; set; }
        public string TaxCountry3 { get; set; }
        public string TinNo4 { get; set; }
        public string TaxCountry4 { get; set; }
        public string TinNo5 { get; set; }
        public string TaxCountry5 { get; set; }
        public string FATCANoReason { get; set; }
        public string ReasonB { get; set; }

        public string JurEmiratesId { get; set; }
        public string JurTypeId { get; set; }
        public string JurAuthorityId { get; set; }
    }

    public class ShareHolder
    {
        public decimal? ShareHolderID { get; set; }
        //public string EntityType { get; set; }

        public string ShareHolderName { get; set; }

        public string ShareHolderAddress { get; set; }

        public string ShareholderType { get; set; }

        public string ShareholdersIDtype { get; set; }

        public string ShareholdersIDNo { get; set; }

        public DateTime? ShareholdersIDExpirydate { get; set; }

        public string CountryofIncorporation { get; set; }
        public string BoardMember { get; set; }
        public string AuthorisedSignatory { get; set; }
        public string BeneficialOwner { get; set; }

        public string ResidentNonResident { get; set; }

        public string CountryofResidence { get; set; }

        public decimal? OwnershipPercentage { get; set; }
        public string Blacklist { get; set; }
        public string PEP { get; set; }
        public string FATCARelevant { get; set; }
        public string CRSRelevant { get; set; }
        public string USCitizen { get; set; }
        public string AreyouaUSTaxResident { get; set; }

        public string WereyoubornintheUS { get; set; }

        public string USAEntity { get; set; }
        public string FFI { get; set; }

        public string FFICategory { get; set; }

        public string GIINNo { get; set; }

        public string GIINNA { get; set; }

        public string SponsorName { get; set; }

        public string SponsorGIIN { get; set; }

        public string NFFE { get; set; }

        public string StockExchange { get; set; }

        public string TradingSymbol { get; set; }

        public string USAORUAE { get; set; }

        public string TINAvailable { get; set; }

        public string TinNo1 { get; set; }

        public string TaxCountry1 { get; set; }
        public string TinNo2 { get; set; }

        public string TaxCountry2 { get; set; }
        public string TinNo3 { get; set; }

        public string TaxCountry3 { get; set; }
        public string TinNo4 { get; set; }

        public string TaxCountry4 { get; set; }
        public string TinNo5 { get; set; }

        public string TaxCountry5 { get; set; }

        public string FATCANoReason { get; set; }

        public string ReasonB { get; set; }

        //[Required(ErrorMessage = " is a mandatory field.")]
        public string SHIDIssuanceCountry { get; set; }

        //[Required(ErrorMessage = " is a mandatory field.")]
        public string IDGender { get; set; }
        //[Required(ErrorMessage = " is a mandatory field.")]
        public DateTime? DateOfBirth { get; set; }
       // [Required(ErrorMessage = " is a mandatory field.")]
        public string IDNationality { get; set; }

       // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string SHCity { get; set; }
        public string SHState { get; set; }
        public string SHCountry { get; set; }

        public string JurEmiratesId { get; set; }
        public string JurTypeId { get; set; }
        public string JurAuthorityId { get; set; }

        public string POBox { get; set; }
        public string JurOther { get; set; }
        public string ParentShareHolderId { get; set; }
    }
    
}
