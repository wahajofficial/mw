﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class UpdateFXRatesModel
    {
      
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public List<Rate> Rate { get; set; }
    }
    public class Rate
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public string CurrencyID { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public decimal BuyingRate { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public decimal SellingRate { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public decimal MeanRate { get; set; }
    }
}
