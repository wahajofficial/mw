﻿using Microsoft.EntityFrameworkCore;
using DBHandler.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DBHandler.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        internal DbContext Context;

        public Repository(DbContext context)
        {
            Context = context;
        }

        public TEntity Get(int id)
        {
            // Here we are working with a DbContext, not PlutoContext. So we don't have DbSets 
            // such as Courses or Authors, and we need to use the generic Set() method to access them.
            //var h = Context.Set<TEntity>().Find(id);
            return Context.Set<TEntity>().Find(id);
        }

        public TEntity Get(decimal id)
        {
            // Here we are working with a DbContext, not PlutoContext. So we don't have DbSets 
            // such as Courses or Authors, and we need to use the generic Set() method to access them.
            var h = Context.Set<TEntity>().Find(id);
            return Context.Set<TEntity>().Find(id);
        }

        public TEntity Get(string id)
        {
            // Here we are working with a DbContext, not PlutoContext. So we don't have DbSets 
            // such as Courses or Authors, and we need to use the generic Set() method to access them.
            var h = Context.Set<TEntity>().Find(id);
            return Context.Set<TEntity>().Find(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            // Note that here I've repeated Context.Set<TEntity>() in every method and this is causing
            // too much noise. I could get a reference to the DbSet returned from this method in the 
            // constructor and store it in a private field like _entities. This way, the implementation
            // of our methods would be cleaner:
            // 
            // _entities.ToList();
            // _entities.Where();
            // _entities.SingleOrDefault();
            // 
            // I didn't change it because I wanted the code to look like the videos. But feel free to change
            // this on your own.
            return Context.Set<TEntity>().ToList();
        }

        public IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {

            // var h = Context.Set<TEntity>().Where(predicate).FirstOrDefault();
            return Context.Set<TEntity>().Where(predicate);
        }

        public TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate)
        {

            //var h= Context.Set<TEntity>().SingleOrDefault(predicate);
            return Context.Set<TEntity>().FirstOrDefault(predicate);
        }

        public virtual void Add(TEntity entity)
        {
            // var h=entity.Equals(entity.GetType().Name);
            Context.Set<TEntity>().Add(entity);
            // return entity;          
        }

        public class Myobject
        {
            public string Date { get; set; }
        }

        public void Update(TEntity entity)
        {

            Context.Set<TEntity>().Update(entity);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            Context.Set<TEntity>().AddRange(entities);
        }
        public void UpdateRange(IEnumerable<TEntity> entities)
        {
            Context.Set<TEntity>().UpdateRange();
        }
        public void UpdateEntity(TEntity entity, dynamic obj)
        {
            try
            {
                foreach (var d in (dynamic)(obj))
                {
                    var type = typeof(TEntity).GetProperty(d.Path.ToString()).PropertyType;
                    var valx = Convert.ChangeType(d.Value, type);
                    entity.GetType().GetProperty(d.Path).SetValue(entity, valx, null);
                }
                //        dbSet.Attach(entity);
                Context.Entry(entity).State = EntityState.Modified;
            }
            catch (Exception ex)

            {

            }
        }

        public void Remove(TEntity entity)
        {
            Context.Set<TEntity>().Remove(entity);
        }

        public void RemovebyId(decimal id)
        {
            var entities = Context.Set<TEntity>().Find(id);
            Context.Set<TEntity>().Remove(entities);

        }

        public void RemovebyId(int id)
        {
            var entities = Context.Set<TEntity>().Find(id);
            Context.Set<TEntity>().Remove(entities);

        }

        public void RemovebyId(string id)
        {
            var entities = Context.Set<TEntity>().Find(id);
            Context.Set<TEntity>().Remove(entities);

        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            Context.Set<TEntity>().RemoveRange(entities);
        }

        //public void RemoveRangeByid(Id ids)
        //{
        //    foreach (var id in ids.Ids)
        //    {
        //        var entities = Context.Set<TEntity>().Find(id);
        //        if (entities == null)
        //        {

        //        }
        //        else
        //        {
        //            Context.Set<TEntity>().RemoveRange(entities);
        //        }
        //    }


        //}

        public bool Save()
        {
            return Context.SaveChanges() >= 0;
        }


        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (Context != null)
                {
                    Context.Dispose();
                    Context = null;
                }
            }
        }
    }
}
