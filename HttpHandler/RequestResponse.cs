﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;

namespace HttpHandler
{
   public class RequestResponse
    {
        public string severity { get; set; }
        public string httpResponse { get; set; }
        public byte[] b { get; set; }
        public HttpStatusCode StatusCode { get; set; }
    }
    
}
