﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PostCoderHandler.GetEmailValidate
{
    public class RequestGetEmailValidate
    {/// <summary>
     /// The name of the product
     /// </summary>
     /// <example>Men's emailaddress shoes</example>
     /// <param name="warning">Required parameter: Example: </param>
     /// <return>Returns comment</return>
     /// <response code="200">Ok</response>
        public string emailaddress { get; set; }  
    }


    public class ResponseObj
    {
        public string warning { get; set; }
        public string state { get; set; }
        public bool valid { get; set; }
        public string score { get; set; }
        public string processtime { get; set; }
    }

    public class ErrorResponse
    {
        public string warning { get; set; }
        public string state { get; set; }
        public bool valid { get; set; }
        public string score { get; set; }
        public string processtime { get; set; }
    }

}
