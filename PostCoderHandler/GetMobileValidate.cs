﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PostCoderHandler.GetMobileValidate
{
    public class RequestGetMobileValidate
    {
        public string mobilevalidate { get; set; }
    }


    public class ResponseObj
    {
        public string warning { get; set; }
        public int stateid { get; set; }
        public string state { get; set; }
        public bool on { get; set; }
        public bool valid { get; set; }
        public string number { get; set; }
        public string type { get; set; }
        public string error{ get; set; }
    }

    public class ErrorResponse
    {
        public string warning { get; set; }
        public int stateid { get; set; }
        public string state { get; set; }
        public bool on { get; set; }
        public bool valid { get; set; }
        public string number { get; set; }
        public string type { get; set; }
    }
}
