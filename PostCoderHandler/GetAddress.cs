﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PostCoderHandler.GetAddress
{
    public class RequestAddress
    {
        public string Country { get; set; }
        public string Postcode { get; set; }
        public string Addresslines{ get; set; }
        public string Additionaldatafields { get; set; }

    }
    public class ResponseObj
    {
        //public string addressline1 { get; set; }
        //public string addressline2 { get; set; }
        //public string summaryline { get; set; }
        //public string organisation { get; set; }
        //public string buildingname { get; set; }
        //public string premise { get; set; }
        //public string street { get; set; }
        //public string dependentlocality { get; set; }
        //public string posttown { get; set; }
        //public string county { get; set; }
        //public string postcode { get; set; }
        public List<ResponseObject> responseObject { get; set; }
    }

    public class ResponseObject
    {
        public string addressline1 { get; set; }
        public string addressline2 { get; set; }
        public string summaryline { get; set; }
        public string organisation { get; set; }
        public string buildingname { get; set; }
        public string premise { get; set; }
        public string street { get; set; }
        public string dependentlocality { get; set; }
        public string posttown { get; set; }
        public string county { get; set; }
        public string postcode { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string grideasting { get; set; }
        public string gridnorthing { get; set; }
    }

    public class ErrorResponse
    {
        //public string error { get; set; }
    }
}
