﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using IntelliLock.Licensing;
using CommonModel;
using Newtonsoft.Json;
using DBHandler.Model;
using DBHandler.Helper;
using System.IO;
using System.Net.Http;
using System.Text;

namespace DigiBancMiddleware.Helper
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class ExpirationDateChecker
    {
        private readonly RequestDelegate _next;

        public ExpirationDateChecker(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            bool lock_enabled = EvaluationMonitor.CurrentLicense.ExpirationDate_Enabled;
            System.DateTime expiration_date = EvaluationMonitor.CurrentLicense.ExpirationDate;
            if (expiration_date < DateTime.Now)
            {
                string data = GetRequestBodyInString(context);

                if (data == "")
                {
                    await _next(context).ConfigureAwait(false);
                }
                else
                {
                  
                    DTO dto = new DTO();
                    var originalBodyStream = context.Response.Body;
                    using (var responseBody = new MemoryStream())
                    {

                        dto = JsonConvert.DeserializeObject<DTO>(data);
                        DBHandler.Helper.ActiveResponseSucces<string> res = new DBHandler.Helper.ActiveResponseSucces<string>()
                        {
                            LogId = dto.SignOnRq.LogId,
                            Content = null,
                            RequestDateTime = dto.SignOnRq.DateTime
                        };
                        context.Response.Body = responseBody;
                        res.Status = new DBHandler.Helper.Status
                        {
                            Severity = Severity.Error,
                            StatusMessage = "Application Expired",
                            Code = "",
                        };
                        context.Response.ContentType = "application/json";
                        context.Response.StatusCode = 404;
                        var requestResponse = JsonConvert.SerializeObject(res);
                        byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(requestResponse);
                        originalBodyStream.Write(b);
                        string req = requestResponse == null ? JsonConvert.SerializeObject(res.Status) : requestResponse;
                        DateTime starTime = DateTime.Now;
                        Exception excetionForLog = null;
                        await responseBody.CopyToAsync(originalBodyStream);

                    }
                }
       
            }
            else
            {


                await _next(context);

            }
        }
        private static string GetRequestBodyInString(HttpContext context)
        {
            string bodyStr = string.Empty;
            using (StreamReader reader = new StreamReader(context.Request.Body, Encoding.UTF8, true, 1024, true))
            {
                bodyStr = reader.ReadToEnd();
            }
            return bodyStr;
        }
        private static async Task MockRequestBody(HttpContext context, string jsonOriginalParamModel)
        {
            var requestContent = new StringContent(jsonOriginalParamModel, Encoding.UTF8);
            var stream = context.Request.Body;
            stream = await requestContent.ReadAsStreamAsync().ConfigureAwait(false);
            context.Request.Body = stream;
        }

    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class ExpirationDateCheckerExtensions
    {
        public static IApplicationBuilder UseExpirationDateChecker(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ExpirationDateChecker>();
        }
    }

}
