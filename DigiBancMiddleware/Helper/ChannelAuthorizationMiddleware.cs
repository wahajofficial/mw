﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using CommonModel;
using DBHandler.Enum;
using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Repositories;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Routing;

namespace DigiBancMiddleware.Helper
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class ChannelAuthorizationMiddleware
    {
        private readonly RequestDelegate _next;
        public static long count = 1;
        private IConfiguration _config;


        public ChannelAuthorizationMiddleware(RequestDelegate next/*, IConfiguration config*/)
        {
          //  _config = config;
            _next = next;
        }

        public async Task Invoke(HttpContext context, ILogRepository mainlog, IConfiguration config, IUserChannelsRepository _userChannels, UserManager<ApplicationUser> userManager)
        {
           _config = config;
            string data = GetRequestBodyInString(context);

            if (data == "")
            {
                await _next(context).ConfigureAwait(false);
            }
            else
            {
                await MockRequestBody(context, data);
                DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, mainlog);
                var authHeader = AuthenticationHeaderValue.Parse(context.Request.Headers["Authorization"]);
                var credentialBytes = Convert.FromBase64String(authHeader.Parameter);
                  var credentials = Encoding.UTF8.GetString(credentialBytes).Split(':');
                var username = credentials[0];
                var user = await userManager.FindByNameAsync(username);
                var id = "";
                if (user != null)
                {
                    id = user.Id;
                }
                var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme);
                identity.AddClaim(new Claim("Id", id));
                var principal = new ClaimsPrincipal(identity);
                context.User.AddIdentity(identity);
                DTO dto = new DTO();
                var originalBodyStream = context.Response.Body;
                using (var responseBody = new MemoryStream())
                {
                    try
                    {

                        dto = Utility.ReadFromHeaders(context);
                        var endpoint = context.GetEndpoint();
                        if (endpoint == null)
                        {
                            string clientip = Utility.GetIP(context.Request.HttpContext);
                            DBHandler.Helper.ActiveResponseSucces<string> res = new DBHandler.Helper.ActiveResponseSucces<string>()
                            {
                                LogId = dto.SignOnRq.LogId,
                                Content = null,
                                RequestDateTime = dto.SignOnRq.DateTime
                            };
                            context.Response.Body = responseBody;
                            ErrorMessages mes = mainlog.GetErrorMessage("ERROR-04");
                            var path = context.Features.Get<IExceptionHandlerPathFeature>();
                            res.Status = new DBHandler.Helper.Status
                            {
                                Severity = Severity.Error,
                                StatusMessage = mes.ErrorMessage,
                                Code = mes.ErrorCode
                            };
                            context.Response.ContentType = "application/json";
                            context.Response.StatusCode = 404;
                            var requestResponse = JsonConvert.SerializeObject(res);
                            byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(requestResponse);
                            originalBodyStream.Write(b);
                            string req = requestResponse == null ? JsonConvert.SerializeObject(res.Status) : requestResponse;
                            DateTime starTime = DateTime.Now;
                            Exception excetionForLog = null;
                            if (bool.Parse(_config["GlobalSettings:IsLogEnabled"]))
                            {
                                mainlog.Logs(Level.Error, res.Status.Code != null ? res.Status.Code.ToString() : "",
                            context,
                            excetionForLog,
                            starTime,
                            DateTime.Now,
                            data,
                            requestResponse,
                            id,
                            dto.SignOnRq == null ? "" : dto.SignOnRq.ChannelId,
                            dto.SignOnRq == null ? "" : dto.SignOnRq.LogId,
                            dto.SignOnRq == null ? DateTime.Now : dto.SignOnRq.DateTime, System.Net.HttpStatusCode.BadRequest, "", "", DateTime.Now, DateTime.Now, clientip, "", ""
                            , _config["ErrorLog"], _config["SuccessLog"], _config["InfoLog"], "", "", "");
                            }
                            await responseBody.CopyToAsync(originalBodyStream);
                        }
                        else
                        {
                            dto = Utility.ReadFromHeaders(context);
                            var controllerActionDescriptor = endpoint.Metadata.GetMetadata<ControllerActionDescriptor>();
                            DBHandler.Helper.ActiveResponseSucces<string> res = new DBHandler.Helper.ActiveResponseSucces<string>()
                            {
                                LogId = dto.SignOnRq.LogId,
                                Content = null,
                                RequestDateTime = dto.SignOnRq.DateTime
                            };
                            string chanelId = helper.ValidateChannel(id, controllerActionDescriptor.ControllerName, controllerActionDescriptor.ActionName);
                            if (string.IsNullOrEmpty(chanelId))
                            {
                                string clientip = Utility.GetIP(context.Request.HttpContext);
                                string currentController = "";
                                string currentAction = "";
                                var RouteInfo = context.Request.HttpContext.GetRouteData().Values;
                                if (RouteInfo.Keys.Count > 0)
                                {
                                    currentController = RouteInfo["controller"].ToString() == null ? "" : RouteInfo["controller"].ToString();
                                    currentAction = RouteInfo["action"].ToString() == null ? "" : RouteInfo["action"].ToString();
                                }

                                context.Response.Body = responseBody;
                                ErrorMessages mes = mainlog.GetErrorMessage("ERROR-01");
                                res.Status = new DBHandler.Helper.Status
                                {
                                    Severity = Severity.Error,
                                    StatusMessage = $"Channel {dto.SignOnRq.ChannelId} is not allowed access to {controllerActionDescriptor.ActionName} function.",
                                    Code = mes.ErrorMessage
                                };
                                context.Response.ContentType = "application/json";
                                context.Response.StatusCode = 401;
                                var requestResponse = JsonConvert.SerializeObject(res);
                                byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(requestResponse);
                                originalBodyStream.Write(b);
                                string req = requestResponse == null ? JsonConvert.SerializeObject(res.Status) : requestResponse;
                                DateTime starTime = DateTime.Now;
                                Exception excetionForLog = null;

                                if (bool.Parse(_config["GlobalSettings:IsLogEnabled"]))
                                {

                                    mainlog.Logs(Level.Error, res.Status.Code != null ? res.Status.Code.ToString() : "",
                                context,
                                excetionForLog,
                                starTime,
                                DateTime.Now,
                                data,
                                requestResponse,
                                id,
                                dto.SignOnRq == null ? "" : dto.SignOnRq.ChannelId,
                                dto.SignOnRq == null ? "" : dto.SignOnRq.LogId,
                                dto.SignOnRq == null ? DateTime.Now : dto.SignOnRq.DateTime, System.Net.HttpStatusCode.BadRequest, "", "", DateTime.Now, DateTime.Now, clientip, currentController, currentAction,
                                _config["ErrorLog"], _config["SuccessLog"], _config["InfoLog"]);
                                }
                                    await responseBody.CopyToAsync(originalBodyStream);
                                }
                            
                            else
                            {
                                context.Request.Headers.Add("ChannelId", chanelId);
                               // context.Request.Headers.Add(")
                                await _next(context).ConfigureAwait(false);
                            }

                        }

                    }
                    catch (Exception ex)
                    {
                       
                        string clientip = Utility.GetIP(context.Request.HttpContext);
                        string currentController = "";
                        string currentAction = "";
                        string LogId = Guid.NewGuid().ToString();
                        var RouteInfo = context.Request.HttpContext.GetRouteData().Values;
                        if (RouteInfo.Keys.Count > 0)
                        {
                            currentController = RouteInfo["controller"].ToString() == null ? "" : RouteInfo["controller"].ToString();
                            currentAction = RouteInfo["action"].ToString() == null ? "" : RouteInfo["action"].ToString();
                        }

                        context.Response.Body = responseBody;
                        ErrorMessages mes = mainlog.GetErrorMessage("ERROR-01");
                        DBHandler.Helper.ActiveResponseSucces<string> res = new DBHandler.Helper.ActiveResponseSucces<string>()
                        {
                            LogId = LogId,
                            Content = null,
                            RequestDateTime = DateTime.Now
                        };
                        res.Status = new DBHandler.Helper.Status
                        {
                            Severity = Severity.Error,
                            StatusMessage = "Request is not as expected.",
                            Code = "ERROR-01"
                        };
                        context.Response.ContentType = "application/json";
                        context.Response.StatusCode = 400;
                        var requestResponse = JsonConvert.SerializeObject(res);
                        byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(requestResponse);
                        originalBodyStream.Write(b);
                        string req = requestResponse == null ? JsonConvert.SerializeObject(res.Status) : requestResponse;
                        DateTime starTime = DateTime.Now;
                        Exception excetionForLog = ex;

                        if (bool.Parse(_config["GlobalSettings:IsLogEnabled"]))
                        {

                        mainlog.Logs(Level.Error, res.Status.Code != null ? res.Status.Code.ToString() : "",
                        context,
                        excetionForLog,
                        starTime,
                        DateTime.Now,
                        data,
                        requestResponse,
                        id,
                        dto.SignOnRq == null ? "" : dto.SignOnRq.ChannelId,
                        LogId,
                        dto.SignOnRq == null ? DateTime.Now : dto.SignOnRq.DateTime, System.Net.HttpStatusCode.BadRequest, "", "", DateTime.Now, DateTime.Now, clientip, currentController, currentAction
                        , _config["ErrorLog"], _config["SuccessLog"], _config["InfoLog"]);
                        }
                            await responseBody.CopyToAsync(originalBodyStream);
                        
                    }

                }

            }
        }

        private static string GetRequestBodyInString(HttpContext context)
        {
            string bodyStr = string.Empty;
            using (StreamReader reader = new StreamReader(context.Request.Body, Encoding.UTF8, true, 1024, true))
            {
                bodyStr = reader.ReadToEnd();
            }
            return bodyStr;
        }
        private static async Task MockRequestBody(HttpContext context, string jsonOriginalParamModel)
        {
            var requestContent = new StringContent(jsonOriginalParamModel, Encoding.UTF8);
            var stream = context.Request.Body;
            stream = await requestContent.ReadAsStreamAsync().ConfigureAwait(false);
            context.Request.Body = stream;
        }


    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class ChannelAuthorizationMiddlewareExtensions
    {
        public static IApplicationBuilder UseChannelAuthorizationMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ChannelAuthorizationMiddleware>();
        }
    }
}
