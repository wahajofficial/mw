﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using CommonModel;
using DBHandler.Enum;
using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace DigiBancMiddleware.Helper
{
    /// <summary>
    /// Check if  log id is uniqe.
    /// </summary>
    public class LogIdMiddleWare
    {
        private readonly RequestDelegate _next;

        public LogIdMiddleWare(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext, ILogRepository log, IConfiguration config, UserManager<ApplicationUser> userManager)
        {
            //if authentication is not basic  then this ke will be blank.
            if (httpContext.Request.Headers.ContainsKey("Authorization"))
            {

                var authHeader = AuthenticationHeaderValue.Parse(httpContext.Request.Headers["Authorization"]);
                var credentialBytes = Convert.FromBase64String(authHeader.Parameter);
                var credentials = Encoding.UTF8.GetString(credentialBytes).Split(':');
                var username = credentials[0];
                var user = await userManager.FindByNameAsync(username);
                var id = "";
                if (user != null)
                {
                    id = user.Id;
                }
                //userID//

                DTO dto = new DTO();
                var originalBodyStream = httpContext.Response.Body;
                string data = GetRequestBodyInString(httpContext);
                if (data == "")
                {
                    await _next(httpContext).ConfigureAwait(false);
                }
                else
                {
                    await MockRequestBody(httpContext, data);
                    if (Utility.IsTmsNotificationAction(httpContext.GetEndpoint(), config))
                    {
                        dto = Utility.ReadFromHeaders(httpContext);

                    }
                    else
                    {
                        dto = JsonConvert.DeserializeObject<DTO>(data);

                    }

                    DBHandler.Helper.ActiveResponseSucces<string> res = new DBHandler.Helper.ActiveResponseSucces<string>()
                    {
                        LogId = dto.SignOnRq.LogId,
                        Content = null,
                        RequestDateTime = dto.SignOnRq.DateTime
                    };
                    bool isEnable = config.GetValue<bool>("GlobalSettings:UniqueLogIdEnable");
                    if (isEnable)
                    {
                        if (log.GetLogId(dto.SignOnRq.LogId))
                        {
                            res.Status = new DBHandler.Helper.Status
                            {
                                Severity = Severity.Error,
                                StatusMessage = $"{dto.SignOnRq.LogId} - Log ID is invalid, this must be unique.",
                                Code = "ERROR-400"
                            };
                            using (var responseBody = new MemoryStream())
                            {
                                httpContext.Response.Body = responseBody;
                                httpContext.Response.ContentType = "application/json";
                                httpContext.Response.StatusCode = 400;
                                var requestResponse = JsonConvert.SerializeObject(res);
                                byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(requestResponse);
                                originalBodyStream.Write(b);

                                await responseBody.CopyToAsync(originalBodyStream);
                            }
                        }
                        else
                        {
                            await _next(httpContext);
                        }
                    }
                    else
                    {
                        await _next(httpContext);
                    }
                }
            }
            else
            {
                await _next(httpContext);
            }
        }

        private static string GetRequestBodyInString(HttpContext context)
        {
            string bodyStr = string.Empty;
            using (StreamReader reader = new StreamReader(context.Request.Body, Encoding.UTF8, true, 1024, true))
            {
                bodyStr = reader.ReadToEnd();
            }
            return bodyStr;
        }
        private static async Task MockRequestBody(HttpContext context, string jsonOriginalParamModel)
        {
            var requestContent = new StringContent(jsonOriginalParamModel, Encoding.UTF8);
            var stream = context.Request.Body;
            stream = await requestContent.ReadAsStreamAsync().ConfigureAwait(false);
            context.Request.Body = stream;
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class LogIdMiddleWareExtensions
    {
        public static IApplicationBuilder UseLogIdMiddleWare(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<LogIdMiddleWare>();
        }
    }
}
