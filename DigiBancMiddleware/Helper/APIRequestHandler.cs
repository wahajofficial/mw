﻿using DBHandler.Enum;
using DBHandler.Model;
using DBHandler.Repositories;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DBHandler.Helper;
using HttpHandler;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using CommonModel;
using System.Text;
using System.Net.Http;
using DBHandler.Common;
using Microsoft.AspNetCore.Routing;
using System.IO;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace VendorApi.Helper
{
    public class APIRequestHandler
    {
        public static async Task<InternalResult> GetResponse(  SignOnRq SignOnRq,
                                                               object Model,
                                                               string baseurl,
                                                               string url,
                                                               Level level,
                                                               ILogRepository _logs,
                                                               IUserChannelsRepository _userChannels,
                                                               ControllerBase controllerBase,
                                                               IConfiguration _config,
                                                               HttpHandler.HttpHandler httpHandler,
                                                               int type = 1,
                                                               int isGet = 1,
                                                               string newBody = "",
                                                               string toke = "",
                                                               string StartTime = "")
        {
            DTO dto = new DTO();

            dto = Utility.ReadFromHeaders(controllerBase.HttpContext);
            SignOnRq = dto.SignOnRq;

            Exception excetionForLog = null;
            DBHandler.Helper.Status status;
            DateTime starTime = StartTime == "" ? DateTime.Now : DateTime.Parse(StartTime);
            DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, _logs);
            HttpContext ctx = controllerBase.Request?.HttpContext;
            var id = controllerBase.HttpContext.User.Claims.First().Value;
            RequestResponse httpResult = null;
            CommonHelper common = new CommonHelper(_logs,_config);
            string body = "";
            DateTime ApiRequestStartTime = DateTime.Now;
            DateTime ApiRequestEndTime = DateTime.Now;

            string httpMethods = "";

            status = ValidateModel(controllerBase, Model);
            if (status != null)
            {
                level = Level.Error;
                excetionForLog = new Exception(status.StatusMessage);
            }
            else
            {
                status = new Status() { Code = null };

                Dictionary<string, string> header = null;

                switch (type)
                {
                    case 1:
                        header = common.GetHeaderForJson();
                        break;
                    case 2:
                        header = common.GetHeaderForECSFin();
                        break;
                    case 3:
                        header = common.GetHeaderForIMTF();
                        break;
                    case 4:
                        header = common.GetHeaderForERCCorporate();
                        break;
                    case 5:
                        header = common.GetHeaderForRetail();
                        break;
                    case 6:
                        header = common.GetHeaderForShipa();
                        break;
                    case 7:
                        header = common.GetHeaderJCREDIT(toke);
                        break;
                    case 8:
                        header = common.GetHeaderFireBase(_config);
                        break;
                    case 9:
                        header = common.GetHeaderLiveChat();
                        break;
                    case 10:
                        break;
                    case 11:
                        header = common.GetHeaderODS(toke);
                        break;
                    case 12:
                        header = common.GetHeaderForIMTF1();
                        break;
                    case 13:
                        header = common.GetHeaderForRMS(toke);
                        break;
                    case 14:
                        header = common.GetHeaderForCRM();
                        break;
                    case 15:
                        header = common.GetHeaderForCRMUpdateCase(toke);
                        break;
                    case 16:
                        header = common.GetHeaderForMoneyThor(toke);
                        break;
                    case 17:
                        header = common.GetHeaderForMoneyThorJson(toke);
                        break;
                    case 18:
                        header = common.GetHeaderForJson();
                        break;
                    case 19:
                        header = common.GetHeaderForBankSmart();
                        break;
                    case 20:
                        header = common.GetHeaderForRailsBank();
                        break;
                    case 21:
                        header = common.GetHeaderForSendGrid();
                        break;
                    case 22:
                        if (isGet==2)
                             header = common.GetHeaderForSumsub(null, baseurl, url,HttpMethod.Get,false);
                        else if(isGet==36)
                            header = common.GetHeaderForSumsub(Model, baseurl, url, HttpMethod.Patch, false);
                        else
                            header = common.GetHeaderForSumsub(Model, baseurl, url, HttpMethod.Post, false);
                        break;
                    case 33:
                        header = common.GetHeaderForSumsub(Model, baseurl, url, HttpMethod.Post,true);
                        break;
                    case 34:
                        header = common.GetHeaderForTrunarrative();
                        break;
                    //case 23:
                    //    header = common.GetHeaderForAddIdDocumentSumsub(Model, baseurl, url);
                    //    break;
                    default:
                        header = common.GetHeader();
                        break;
                }

                body = JsonConvert.SerializeObject(Model);
                body = string.IsNullOrEmpty(newBody) ? body : newBody;
                ApiRequestStartTime = DateTime.Now;
                switch (isGet)
                {
                    case 2:
                        httpResult = await httpHandler.GetJsonData(baseurl, url, header, _config).ConfigureAwait(false);
                        httpMethods = "Get";
                        break;
                    case 3:
                        httpResult = await httpHandler.PutJsonData(baseurl, url, header, body, _config).ConfigureAwait(false);
                        httpMethods = "Put";
                        break;
                    case 33:
                        httpResult = await httpHandler.DeleteJsonData(baseurl, url, header, body, _config).ConfigureAwait(false);
                        httpMethods = "Delete";
                        break;
                    case 36:
                        httpResult = await httpHandler.PatchJsonData(baseurl, url, header, body, _config).ConfigureAwait(false);
                        httpMethods = "Patch";
                        break;
                    case 44:
                        header.Remove("Content-Type");
                        header.Add("Content-Type", "multipart/form-data; boundary=--------------------------845235239682787180694175");
                        httpResult = await httpHandler.PostFormData(baseurl, url, header, (Dictionary<string, string>)Model, _config).ConfigureAwait(false);
                        httpMethods = "PostFromData";
                        break;
                    case 55:
                        httpResult = await httpHandler.PostFormData(baseurl, url, header, (MultipartFormDataContent)Model, _config).ConfigureAwait(false);
                        httpMethods = "PostFromData";
                        break;
                    case 66:
                        httpResult = await httpHandler.PostFormData(baseurl, url, header, (Dictionary<string, string>)Model, _config).ConfigureAwait(false);
                        httpMethods = "PostFormData";
                        break;
                    case 77:
                        httpResult = await httpHandler.PostJsonDataDigest(baseurl, url, header, body, getTmsCredentials(_logs), _config).ConfigureAwait(false);
                        httpMethods = "PostJsonDataDigest";
                        break;
                    case 88:
                        httpResult = await httpHandler.GetJsonDataDigest(baseurl, url, header, getTmsCredentials(_logs), _config).ConfigureAwait(false);
                        httpMethods = "GetJsonDataDigest";
                        break;
                    case 99:
                        httpResult = await httpHandler.PutKafkaData(baseurl, url, body, _config).ConfigureAwait(false);
                        httpMethods = "PutFromData";
                        break;
                    case 111:
                        body = JsonConvert.SerializeObject(Model);
                        httpResult = new RequestResponse();
                        httpResult.httpResponse = newBody;
                        break;
                    case 222:
                        CommonModel.Twilio.SMS sMS =(CommonModel.Twilio.SMS) Model;
                        TwilioClient.Init(_config["TwilioAPI:SID"].ToString(), _config["TwilioAPI:AuthToken"].ToString());                        
                        var messageOptions = new CreateMessageOptions(new PhoneNumber(sMS.To));
                        messageOptions.From = new PhoneNumber(_config["TwilioAPI:From"].ToString());
                        messageOptions.Body = sMS.Message;
                        try
                        {
                            var message = MessageResource.Create(messageOptions);
                            httpResult = new RequestResponse() { StatusCode = System.Net.HttpStatusCode.OK, severity = "Success", httpResponse="" };
                        }
                        
                        catch (Exception ex)
                        {
                            httpResult = new RequestResponse() { StatusCode = System.Net.HttpStatusCode.BadRequest, severity = "Error", httpResponse = ex.ToString() };
                        }
                        httpMethods = "DLL";

                        break;
                    default:
                        if (type != 13)
                        {
                            httpResult = await httpHandler.PostJsonData(baseurl, url, header, body, false, _config).ConfigureAwait(false);
                            httpMethods = "Post";

                        }
                        else
                        {
                            httpResult = await httpHandler.PostJsonData(baseurl, url, header, body, true, _config).ConfigureAwait(false);
                            httpMethods = "Post";
                        }
                        break;
                }
                ApiRequestEndTime = DateTime.Now;
            }

            if (httpResult! != null && (httpResult.StatusCode != System.Net.HttpStatusCode.OK && httpResult.StatusCode != System.Net.HttpStatusCode.BadRequest))
            {
                status = new DBHandler.Helper.Status() { Severity = Severity.Error, Code = "ERROR-"+ ((int)httpResult.StatusCode), StatusMessage = httpResult.StatusCode.ToString()};
            }


            string severity = httpResult != null ? httpResult.severity : "";
            string response = httpResult == null ? JsonConvert.SerializeObject(status) : httpResult.httpResponse;
            System.Net.HttpStatusCode code = httpResult != null ? httpResult.StatusCode : System.Net.HttpStatusCode.OK;

            string clientip = Utility.GetIP(controllerBase.Request.HttpContext);
            string currentController = "";
            string currentAction = "";
            var RouteInfo = controllerBase.Request.HttpContext.GetRouteData().Values;
            if (RouteInfo.Keys.Count > 0)
            {
                currentController = RouteInfo["controller"].ToString() == null ? "" : RouteInfo["controller"].ToString();
                currentAction = RouteInfo["action"].ToString() == null ? "" : RouteInfo["action"].ToString();
            }
            if (bool.Parse(_config["GlobalSettings:IsLogEnabled"]))
            {


                //Task.Run(() =>
                _logs.Logs(level, severity,
                            controllerBase.Request.HttpContext,
                            excetionForLog,
                            starTime,
                            DateTime.Now,
                            body,
                            response,
                            id,
                            SignOnRq == null ? "" : SignOnRq.ChannelId,
                            SignOnRq == null ? "" : SignOnRq.LogId,
                            // Guid.NewGuid().ToString(),
                            SignOnRq == null ? DateTime.Now : SignOnRq.DateTime, code, baseurl, url, ApiRequestStartTime,
                            ApiRequestEndTime, clientip, currentController, currentAction, _config["ErrorLog"], _config["SuccessLog"], _config["InfoLog"], "","","",httpMethods);//);
            }

            return new InternalResult() { Status = status, httpResult = httpResult };
        }

        private static Dictionary<string, string> getTmsCredentials(ILogRepository _logs)
        {
            var credentials = _logs.GetHeaders("TMSCredentials", 3);
            Dictionary<string, string> credential = new Dictionary<string, string>();
            foreach (var item in credentials)
            {
                credential.Add(item.HeaderName, item.HeaderValue);
            }
            return credential;
        }

        public static DBHandler.Helper.Status ValidateModel(ControllerBase controllerBase, object Model)
        {
            DBHandler.Helper.Status status;

            bool isModelValid = controllerBase.TryValidateModel(Model);
            if (!isModelValid)
            {
                var errors = controllerBase.ModelState
                .Where(x => x.Value.Errors.Count > 0)
                .Select(x => new { x.Key, x.Value.Errors })
                .ToArray();

                string errormessage = "";
                foreach (var e in errors)
                {
                    errormessage += e.Key.ToUpper() + ":" + e.Errors[0].ErrorMessage + ",  ";
                }
                status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = errormessage, Code = "ERROR-02" };
                return status;
            }
            return null;
        }
    }
}
