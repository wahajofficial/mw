﻿using DBHandler.Helper;
using RailsBankHandler.SuspendCard;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Request
{
    public class SuspendCardRequestExample : IExamplesProvider<RailsBankHandler.SuspendCard.RequestSuspendCard>
    {
        public RequestSuspendCard GetExamples()
        {
            return new RequestSuspendCard()
            {
                card_id = "1234567ias-ddaa",
                    suspend_reason = "card-lost"
                
            };
        }
    }

    public class SuspendCardResponseExample : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.SuspendCard.ResponseObject>>
    {
        public ActiveResponseSucces<ResponseObject> GetExamples()
        {
            return new ActiveResponseSucces<ResponseObject>()
            {
                LogId = "1740ecf1-f54e-4fa3-8056-3889157b5bbd",
                Status = new Status()
                {
                    Code = "MSG-000000",
                    Severity = "Success",
                    StatusMessage = "Success"
                },
                Content = new RailsBankHandler.SuspendCard.ResponseObject()
                {
                    card_id = "bb8b2428-f94c-41df-8e82-a895ab4d6ac8",
                }
            };
        }
    }
}
