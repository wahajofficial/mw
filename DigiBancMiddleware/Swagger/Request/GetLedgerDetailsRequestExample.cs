﻿using RailsBankHandler.GetLedgers;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Request.GetLedgerDetailsRequestExample
{
    public class GetLedgerDetailsRequest : IExamplesProvider<RailsBankHandler.GetLedgers.RequestGetLegders>
    {
        public RequestGetLegders GetExamples()
        {
            return new RequestGetLegders() { ledger_id = "60238bb5-901d-487a-8787-364405bca218" };
        }
    }
}
