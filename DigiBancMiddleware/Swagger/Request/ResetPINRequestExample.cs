﻿using DBHandler.Helper;
using RailsBankHandler.ResetPIN;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Request
{
    public class ResetPINRequestExample : IExamplesProvider<RailsBankHandler.ResetPIN.RequestResetPIN>
    {
        public RequestResetPIN GetExamples()
        {
            return new RequestResetPIN()
            {
                card_id = "8698764f0-9089d"
            };
        }
    }


    public class ResetPINResponseExample : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.ResetPIN.ResponseObject>>
    {
        public ActiveResponseSucces<ResponseObject> GetExamples()
        {
            return new ActiveResponseSucces<ResponseObject>()
            {
                LogId = "1740ecf1-f54e-4fa3-8056-3889157b5bbd",
                Status = new Status()
                {
                    Code = "MSG-000000",
                    Severity = "Success",
                    StatusMessage = "Success"
                },
                Content = new RailsBankHandler.ResetPIN.ResponseObject()
                {
                    card_id = "bb8b2428-f94c-41df-8e82-a895ab4d6ac8",
                }
            };
        }
    }
}
