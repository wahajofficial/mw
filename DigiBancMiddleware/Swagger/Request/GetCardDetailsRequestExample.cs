﻿using DBHandler.Helper;
using RailsBankHandler.GetCards;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Request.GetCardDetailsRequestExample
{
    public class GetCardDetailsRequest : IExamplesProvider<RailsBankHandler.GetCards.RequestGetCards>
    {
        public RequestGetCards GetExamples()
        {
            return new RequestGetCards()
            {
                card_id = "602177f7-6e3e-4dac-b69b-2ef1a6f9ec71"
            };
        }

        public class GetCardDetailsResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.GetCards.ResponseObject>>
        {
            public ActiveResponseSucces<ResponseObject> GetExamples()
            {
                return new ActiveResponseSucces<ResponseObject>()
                {
                    LogId = "1740ecf1-f54e-4fa3-8056-3889157b5bbd",
                    Status = new Status()
                    {
                        Code = "MSG-000000",
                        Severity = "Success",
                        StatusMessage = "Success"
                    },
                    Content = new RailsBankHandler.GetCards.ResponseObject()
                    {
                        ledger_id = "6630b391-c5ce-46c1-9d23-a82a9e27f82d",
                        card_token = "string",
                        name_on_card = "some-chars",
                        card_carrier_type = "renewal",
                        card_status_reason = "abcdefghijklmnopqrstuvwxyz",
                        card_expiry_date = "abcdefghijklmnopqrstuvwxyz",
                        card_id = "6630b391-c5ce-46c1-9d23-a82a9e27f82d",
                        card_delivery_name = "short",
                        qr_code_content = "Any reasonably long string, e.g. description of something",
                        card_type = "virtual",
                        card_rules = new List<string>(){
                                "bb8b2428-f94c-41df-8e82-a895ab4d6ac8",
                              "753fa673-66b4-4c94-9ddb-f9f4b5c1e9a3",
                              "bb8b2428-f94c-41df-8e82-a895ab4d6ac8"
                        },

                        partner_product = "ExampleBank-Direct-Debit-1",
                        card_delivery_method = "standard-first-class",
                        card_design = "string",
                        truncated_pan = "1111",
                        card_delivery_address = new CardDeliveryAddress()
                        {
                            address_number = "13",
                            address_refinement = "Apartment 17",
                            address_region = "California",
                            address_iso_country = "GB",
                            address_street = "Long Street",
                            address_city = "Bratislava",
                            address_postal_code = "112233"
                        },
                        card_status = "card-status-activating",
                        card_programme = "some-chars"

                    }
                
                };
            }
        }
    }
}
