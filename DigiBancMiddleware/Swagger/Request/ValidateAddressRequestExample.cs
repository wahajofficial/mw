﻿using PostCoderHandler.GetAddress;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Request.ValidateAddressRequestExample
{
    public class ValidateAddressRequest : IExamplesProvider<PostCoderHandler.GetAddress.RequestAddress>
    {
        public RequestAddress GetExamples()
        {
            return new RequestAddress()
            {
                Country = "UK",
                Postcode = "NR14 7PZ",
                Addresslines = "0",
                Additionaldatafields = "longitude"
            };
        }
    }
}
