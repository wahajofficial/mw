﻿using DBHandler.Helper;
using RailsBankHandler.ActivateCard;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Request
{
    public class ActivateCardRequestExample : IExamplesProvider<RailsBankHandler.ActivateCard.RequestActivateCard>
    {
        public RequestActivateCard GetExamples()
        {
            return new RequestActivateCard()
            {
                card_id = "602178eb-6a99-49ee-a279-02e622721af0"
            };
        }

        public class ActiveCardResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.ActivateCard.ResponseObject>>
        {
            public ActiveResponseSucces<ResponseObject> GetExamples()
            {
                return new ActiveResponseSucces<ResponseObject>()
                {
                    LogId = "1740ecf1-f54e-4fa3-8056-3889157b5bbd",
                    Status = new Status()
                    {
                        Code = "MSG-000000",
                        Severity = "Success",
                        StatusMessage = "Success"
                    },
                    Content = new RailsBankHandler.ActivateCard.ResponseObject()
                    {
                        card_id = "bb8b2428-f94c-41df-8e82-a895ab4d6ac8",
                    }
                };
            }
        }
    }
}
