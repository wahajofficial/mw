﻿using RailsBankHandler.UpdtBeneficiaryAcct;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Request.UpdtBeneficiaryAcctRequestExample
{
    public class UpdtBeneficiaryAcctRequest : IExamplesProvider<RailsBankHandler.UpdtBeneficiaryAcct.RequestUpdtBeneficiaryAcct>
    {
        public RequestUpdtBeneficiaryAcct GetExamples()
        {
            return new RequestUpdtBeneficiaryAcct()
            {
                account_id = "602911b8-76e9-4ed3-9ae5-02bb92765734",
                beneficiary_id = "602911b8-00c2-494d-b242-422e71798054",
                
                    iban = "SK4402005678901234567890",
                    account_number = "45564658",
                    bank_country = "GB",
                    bank_code = "45564658",
                    bank_name = "Bank of America",
                    bic_swift = "SPSRSKBAXXX",
                    bank_code_type = "bsb",
                
            };
        }
    }
}
