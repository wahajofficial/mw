﻿using RailsBankHandler.GetLedgersClose;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Request.CloseLedgerRequestExample
{
    public class CloseLedgerRequest : IExamplesProvider<RailsBankHandler.GetLedgersClose.RequestGetLedgersClose>
    { 
        RequestGetLedgersClose IExamplesProvider<RequestGetLedgersClose>.GetExamples()
        {
            return new RequestGetLedgersClose()
            { ledger_id = "60238d5a-2deb-4ced-93b2-4555b35e76fa" };

        }
    }
}
