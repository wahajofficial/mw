﻿using CommonModel.Twilio;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Request.SendSMSRequestExample
{
    public class SendSMSRequest : IExamplesProvider<CommonModel.Twilio.SMS>
    {
        public SMS GetExamples()
        {
            return new SMS()
            {
                To = "+923333353980",
                Message = "Hello World"
            };
        }
    }
}
