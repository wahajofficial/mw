﻿using RailsBankHandler.BeneficiariesPost;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Request.AddBeneficiaryRequestExample
{
    public class AddBeneficiaryRequest : IExamplesProvider<RailsBankHandler.BeneficiariesPost.RequestBeneficiariessPost>
    {
        public RequestBeneficiariessPost GetExamples()
        {
            return new RequestBeneficiariessPost()
            {
                    holder_id = "",
                    person = new Person()
                    {
                        country_of_residence = new List<string>() { "USA"},
                        document_number = "000 000 000",
                        document_type = "passport",
                        income = new Income()
                        {
                            frequency = "annual",
                            currency = "EUR",
                            amount = "0"
                        },

                        pep_type = "direct",
                        pep_notes = "comment",
                        address_history = new List<object>()
                        {

                        },

                        pep = true,
                        email = "alice@short.com",
                        tin = "541571",
                        name = "Bob",
                        address = new Address()
                        {
                            address_iso_country = "GB"
                        },

                        social_security_number = "090606",
                        telephone = "0012345678912",
                        date_of_birth = "1981-02-03",
                        document_expiration_date = "2020-12-03",
                        tin_type = "type",
                        citizenship = new List<string>()
                        {
                            "USA"
                        },
                        nationality = new List<string>()
                        {
                            "Hungarian"
                        },

                        document_issuer = "USA",
                        country_of_birth = "USA"
                    },

                    asset_type = "eur",
                    uk_account_number = "45564658",
                    asset_class= "currency",
                    default_account = new DefaultAccount()
                    {
                        iban= "",
                        account_number= "45564658",
                        bank_country= "US",
                        bank_code= "45564658",
                        bank_name= "Bank of America",
                        account_type="checking",
                        bic_swift= "SPSRSKBAXXX",
                        asset_type= "eur",
                        asset_class="currency",
                        bank_code_type="bsb"
                    }
            };
        }
    }
}
