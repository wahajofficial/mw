﻿using PostCoderHandler.GetEmailValidate;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Request.ValidateEmailRequestExample
{
    public class ValidateEmailRequest : IExamplesProvider<PostCoderHandler.GetEmailValidate.RequestGetEmailValidate>
    {
        public RequestGetEmailValidate GetExamples()
        {
            return new RequestGetEmailValidate()
            {
                emailaddress = "sales@alliescomputing.com"
            };
        }
    }
}
