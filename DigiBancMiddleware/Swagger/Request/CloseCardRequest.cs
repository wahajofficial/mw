﻿using DBHandler.Helper;
using RailsBankHandler.CloseCard;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Request
{
    public class CloseCardRequest : IExamplesProvider<RailsBankHandler.CloseCard.RequestGetCards>
    {
        public RequestGetCards GetExamples()
        {
            return new RequestGetCards()
            { card_id = "ac886e26-ede2-4989-87ff-6c8dee3d1218" };
        }
    }

    public class CloseCardResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.CloseCard.ResponseObject>>
    {
        public ActiveResponseSucces<ResponseObject> GetExamples()
        {
            return new ActiveResponseSucces<ResponseObject>()
            {
                LogId = "1740ecf1-f54e-4fa3-8056-3889157b5bbd",
                Status = new Status()
                {
                    Code = "MSG-000000",
                    Severity = "Success",
                    StatusMessage = "Success"
                },
                Content = new RailsBankHandler.CloseCard.ResponseObject()
                {
                    close_reason = "card-lost",
                }
            };
        }
    }
}
