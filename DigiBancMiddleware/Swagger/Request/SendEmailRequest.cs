﻿using SendGridHandler.SendMail;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Request.SendEmailRequest
{
    public class SendEmailRequestExample : IExamplesProvider<SendGridHandler.SendMail.RequestSendMail>
    {
        public RequestSendMail GetExamples()
        {
            return new RequestSendMail()
            {
                   personalizations = new List<Personalization>()
                   {
                      new Personalization()
                      {
                          to = new List<To>()
                          {
                              new To()
                              {
                                  email = "john.doe@example.com",
                                  name = "John Doe"
                              }
                          },

                          subject = "Hello, World!",
                      }
                   },

                   content = new List<Content>()
                   {
                       new Content()
                       {
                           type = "text",
                           value = "Meeral"
                       }
                   },

                   from = new From()
                   {
                       email = "meeralk9@gmail.com",
                       name = "meeral khan"
                   },

                   reply_to = new ReplyTo()
                   {
                       email = "john.doe@example.com",
                       name = "John Doe"
                   },
                   
            };
        }
    }
}
