﻿using RailsBankHandler.GetBeneficiaryDets;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Request.GetBeneficiaryDetsRequestExample
{
    public class GetBeneficiaryDetsRequest : IExamplesProvider<RailsBankHandler.GetBeneficiaryDets.RequestGetBeneficiaryDets>
    {
        public RequestGetBeneficiaryDets GetExamples()
        {
            return new RequestGetBeneficiaryDets()
            {
                beneficiary_id = "602911b8-76e9-4ed3-9ae5-02bb92765734"
            };
        }
    }
}
