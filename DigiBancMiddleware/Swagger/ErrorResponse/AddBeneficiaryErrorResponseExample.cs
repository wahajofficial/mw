﻿using DBHandler.Helper;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.ErrorResponse.AddBeneficiaryErrorResponseExample
{
    public class AddBeneficiaryErrorResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.BeneficiariesPost.ErrorResponse>>
    {
        public ActiveResponseSucces<RailsBankHandler.BeneficiariesPost.ErrorResponse> GetExamples()
        {
            return new ActiveResponseSucces<RailsBankHandler.BeneficiariesPost.ErrorResponse>()
            {
                Status = new Status()
                {
                    Code = "ERROR-01",
                    Severity = "Error",
                    StatusMessage = ""
                },

                LogId = "495e2695-82ba-4427-8870-f9a03f31ce4d",
                Content = new RailsBankHandler.BeneficiariesPost.ErrorResponse()
                {
                    error = "invalid-data",
                    error_detail = "bank code must contain exactly 6 digits"

                }
            };
        }
    }
}
