﻿using DBHandler.Helper;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.ErrorResponse.GetAllBeneficiaryAcctsErrorResponseExample
{
    public class GetAllBeneficiaryAcctsErrorResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.GetAllBeneficiaryAccts.ErrorResponse>>
    {
        public ActiveResponseSucces<RailsBankHandler.GetAllBeneficiaryAccts.ErrorResponse> GetExamples()
        {
            return new ActiveResponseSucces<RailsBankHandler.GetAllBeneficiaryAccts.ErrorResponse>() {
                Status = new Status()
                {
                    Code = "ERROR-01",
                    Severity = "Error",
                    StatusMessage = ""
                },

                LogId = "495e2695-82ba-4427-8870-f9a03f31ce4d",
                Content = new RailsBankHandler.GetAllBeneficiaryAccts.ErrorResponse()
                {
                    error = "Invalid beneficiary id."
                }
            };
        }
    }
}
