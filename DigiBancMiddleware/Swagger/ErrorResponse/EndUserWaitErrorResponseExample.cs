﻿using DBHandler.Helper;
using RailsBankHandler.EndUsersPost;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.ErrorResponse.EndUserWaitErrorResponseExample
{
    public class EndUserWaitErrorResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.End.ErrorResponse>>
    {
        public ActiveResponseSucces<RailsBankHandler.End.ErrorResponse> GetExamples()
        {
            return new ActiveResponseSucces<RailsBankHandler.End.ErrorResponse>
            {
                Status = new Status()
                {
                    Code = "ERROR-01",
                    Severity = "Error",
                    StatusMessage = ""
                },

                LogId = "495e2695-82ba-4427-8870-f9a03f31ce4d",
                Content = new RailsBankHandler.End.ErrorResponse()
                {
                    error = "field-is-malformed"
                }
            };
        }
    }
}
