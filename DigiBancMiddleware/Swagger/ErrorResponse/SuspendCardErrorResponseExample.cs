﻿using DBHandler.Helper;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.ErrorResponse
{
    public class SuspendCardErrorResponseExample : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.SuspendCard.ErrorResponse>>
    {
        public ActiveResponseSucces<RailsBankHandler.SuspendCard.ErrorResponse> GetExamples()
        {
            return new ActiveResponseSucces<RailsBankHandler.SuspendCard.ErrorResponse>() {
                Status = new Status()
                {
                    Code = "ERROR-01",
                    Severity = "Error",
                    StatusMessage = ""
                },
                LogId = "495e2695-82ba-4427-8870-f9a03f31ce4d",
                Content = new RailsBankHandler.SuspendCard.ErrorResponse()
                {
                    error = "field-is-malformed",
                    detail = "field-is-malformed",
                    field = "items_per_page",
                    type = "path"

                }
            };
        }
    }
}
