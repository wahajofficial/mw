﻿using DBHandler.Helper;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.ErrorResponse.CloseLedgerErrorResponseExample
{
    public class CloseLedgerErrorResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.GetLedgersClose.ErrorResponse>>
    {
        public ActiveResponseSucces<RailsBankHandler.GetLedgersClose.ErrorResponse> GetExamples()
        {
            return new ActiveResponseSucces<RailsBankHandler.GetLedgersClose.ErrorResponse>()
            {
                Status = new Status()
                {
                    Code = "ERROR-01",
                    Severity = "Error",
                    StatusMessage = ""
                },

                LogId = "495e2695-82ba-4427-8870-f9a03f31ce4d",
                Content = new RailsBankHandler.GetLedgersClose.ErrorResponse()
                {
                    ledger_id = "60238d5a-2deb-4ced-93b2-4555b35e76fa",
                    error = "ledger-is-already-closed"
                }
            };
        }
    }
}
