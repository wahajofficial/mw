﻿using DBHandler.Helper;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.ErrorResponse
{
    public class ValidateMobNumErrorResponseExample : IExamplesProvider<ActiveResponseSucces<PostCoderHandler.GetMobileValidate.ErrorResponse>>
    {
        public ActiveResponseSucces<PostCoderHandler.GetMobileValidate.ErrorResponse> GetExamples()
        {
            return new ActiveResponseSucces<PostCoderHandler.GetMobileValidate.ErrorResponse>()
            {
                LogId = "1740ecf1-f54e-4fa3-8056-3889157b5bbd",
                Status = new Status()
                {
                    Code = "ERROR-01",
                    Severity = "Error",
                    StatusMessage = ""
                },
                Content = new PostCoderHandler.GetMobileValidate.ErrorResponse()
                {
                    warning = "Requires Phone Number Parameter",
                    state = "Test Key",
                    valid = false,
                    stateid = 0,
                    number = "",
                    on = false,
                    type = "UNKNOWN"
                }
            };
        }
    }
}
