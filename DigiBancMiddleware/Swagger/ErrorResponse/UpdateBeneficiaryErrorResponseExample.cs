﻿using DBHandler.Helper;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.ErrorResponse.UpdateBeneficiaryErrorResponseExample
{
    public class UpdateBeneficiaryErrorResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.UpdateBeneficiary.ErrorResponse>>
    {
        public ActiveResponseSucces<RailsBankHandler.UpdateBeneficiary.ErrorResponse> GetExamples()
        {
            return new ActiveResponseSucces<RailsBankHandler.UpdateBeneficiary.ErrorResponse>()
            {
                Status = new Status()
                {
                    Code = "ERROR-01",
                    Severity = "Error",
                    StatusMessage = ""
                },

                LogId = "495e2695-82ba-4427-8870-f9a03f31ce4d",
                Content = new RailsBankHandler.UpdateBeneficiary.ErrorResponse()
                {                   
                    error = "invalid-data",
                    error_detail = "bank code must contain exactly 6 digits"
                }
            };
        }
    }
}
