﻿using DBHandler.Helper;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.ErrorResponse.GetBeneficiaryDetsErrorResponseExample
{
    public class GetBeneficiaryDetsErrorResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.GetBeneficiaryDets.ErrorResponse>>
    {
        public ActiveResponseSucces<RailsBankHandler.GetBeneficiaryDets.ErrorResponse> GetExamples()
        {
            return new ActiveResponseSucces<RailsBankHandler.GetBeneficiaryDets.ErrorResponse>() {
                Status = new Status()
                {
                    Code = "ERROR-01",
                    Severity = "Error",
                    StatusMessage = ""
                },
                LogId = "495e2695-82ba-4427-8870-f9a03f31ce4d",
                Content = new RailsBankHandler.GetBeneficiaryDets.ErrorResponse()
                {
                    error = "Invalid beneficiary id",                    
                }
            };  
        }
    }
}
