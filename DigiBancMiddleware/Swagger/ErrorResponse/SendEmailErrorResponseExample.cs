﻿using DBHandler.Helper;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.ErrorResponse.SendEmailErrorResponseExample
{
    public class SendEmailErrorResponse : IExamplesProvider<ActiveResponseSucces<SendGridHandler.SendMail.ErrorResponse>>
    {
        public ActiveResponseSucces<SendGridHandler.SendMail.ErrorResponse> GetExamples()
        {
            return new ActiveResponseSucces<SendGridHandler.SendMail.ErrorResponse>()
            {
                Status = new Status()
                {
                    Code = "ERROR-01",
                    Severity = "Error",
                    StatusMessage = ""
                },

                LogId = "495e2695-82ba-4427-8870-f9a03f31ce4d",
                Content = new SendGridHandler.SendMail.ErrorResponse()
                {
                    errors = new List<SendGridHandler.SendMail.Error>()
                    {
                        new SendGridHandler.SendMail.Error()
                        {
                            message = "Does not contain a valid address.",
                            field = "personalizations.0.to.0.email",
                            help = "http://sendgrid.com/docs/API_Reference/Web_API_v3/Mail/errors.html#message.personalizations.to"
                        },

                        new SendGridHandler.SendMail.Error()
                        {
                            message = "The reply_to email does not contain a valid address.",
                            field = "reply_to.email",
                            help = "http://sendgrid.com/docs/API_Reference/Web_API_v3/Mail/errors.html#message.reply_to"
                        }
                    }
                }
            };
        }
    }
}
