﻿using DBHandler.Helper;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.ErrorResponse
{
    public class UpdtBeneficiaryAcctErrorResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.UpdtBeneficiaryAcct.ErrorResponse>>
    {
        public ActiveResponseSucces<RailsBankHandler.UpdtBeneficiaryAcct.ErrorResponse> GetExamples()
        {
            return new ActiveResponseSucces<RailsBankHandler.UpdtBeneficiaryAcct.ErrorResponse>() {
                LogId = "1740ecf1-f54e-4fa3-8056-3889157b5bbd",
                Status = new Status()
                {
                    Code = "ERROR-01",
                    Severity = "Error",
                    StatusMessage = ""
                },
                Content = new RailsBankHandler.UpdtBeneficiaryAcct.ErrorResponse()
                {
                    error = "invalid-data",
                    error_detail = "bank code must contain exactly 6 digits"
                }
            };
        }
    }
}
