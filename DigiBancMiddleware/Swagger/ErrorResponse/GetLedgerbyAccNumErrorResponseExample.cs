﻿using DBHandler.Helper;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.ErrorResponse.GetLedgerbyAccNumErrorResponseExample
{
    public class GetLedgerbyAccNumErrorResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.GetLedgerbyAcctNum.ErrorResponse>>
    {
        public ActiveResponseSucces<RailsBankHandler.GetLedgerbyAcctNum.ErrorResponse> GetExamples()
        {
            return new ActiveResponseSucces<RailsBankHandler.GetLedgerbyAcctNum.ErrorResponse>()
            {
                Status = new Status()
                {
                    Code = "ERROR-01",
                    Severity = "Error",
                    StatusMessage = ""
                },

                LogId = "495e2695-82ba-4427-8870-f9a03f31ce4d",
                Content = new RailsBankHandler.GetLedgerbyAcctNum.ErrorResponse()
                {
                    error = "field-is-malformed",
                    detail = "field-is-malformed",
                    field = "items_per_page",
                    type = "query"
                }
            };
        }
    }
}
