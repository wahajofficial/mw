﻿using DBHandler.Helper;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.ErrorResponse.AddEndUserErrorResponseExample
{
    public class AddEndUserErrorResponseExample : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.EndUsersPost.ErrorResponse>>
    {
        public ActiveResponseSucces<RailsBankHandler.EndUsersPost.ErrorResponse> GetExamples()
        {
            return new ActiveResponseSucces<RailsBankHandler.EndUsersPost.ErrorResponse>()
            {
                Status = new Status()
                {
                    Code = "ERROR-01",
                    Severity = "Error",
                    StatusMessage = ""
                },
                LogId = "495e2695-82ba-4427-8870-f9a03f31ce4d",
                Content = new RailsBankHandler.EndUsersPost.ErrorResponse()
                {
                    error = "field-is-malformed",
                    detail = "field-is-malformed",
                    path = new List<string>()
                {
                    "person","country_of_residence"
                },
                    type = "body"
                }

            };
        }
    }
}
