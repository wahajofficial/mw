﻿using DBHandler.Helper;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.ErrorResponse.GetBeneficiaryAcctErrorResponseExample
{
    public class GetBeneficiaryAcctErrorResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.GetBeneficiaryAcct.ErrorResponse>>
    {
        public ActiveResponseSucces<RailsBankHandler.GetBeneficiaryAcct.ErrorResponse> GetExamples()
        {
            return new ActiveResponseSucces<RailsBankHandler.GetBeneficiaryAcct.ErrorResponse>()
            {
                Status = new Status()
                {
                    Code = "ERROR-01",
                    Severity = "Error",
                    StatusMessage = ""
                },

                LogId = "495e2695-82ba-4427-8870-f9a03f31ce4d",
                Content = new RailsBankHandler.GetBeneficiaryAcct.ErrorResponse()
                {
                    error = "Invalid beneficiary_id."
                }
            };
        }
    }
}
