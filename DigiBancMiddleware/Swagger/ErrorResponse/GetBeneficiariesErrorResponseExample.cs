﻿using DBHandler.Helper;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.ErrorResponse.GetBeneficiariesErrorResponseExample
{
    public class GetBeneficiariesErrorResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.GetBeneficiaries.ErrorResponse>>
    {
        public ActiveResponseSucces<RailsBankHandler.GetBeneficiaries.ErrorResponse> GetExamples()
        {
            return new ActiveResponseSucces<RailsBankHandler.GetBeneficiaries.ErrorResponse>() {
                Status = new Status()
                {
                    Code = "ERROR-01",
                    Severity = "Error",
                    StatusMessage = ""
                },
                LogId = "495e2695-82ba-4427-8870-f9a03f31ce4d",
                Content = new RailsBankHandler.GetBeneficiaries.ErrorResponse()
                {
                    error = "field-is-malformed",
                    detail = "field-is-malformed",
                    field = "items_per_page",
                    type = "query"
                }
            };
        }
    }
}
