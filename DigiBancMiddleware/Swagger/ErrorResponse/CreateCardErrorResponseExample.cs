﻿using DBHandler.Helper;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.ErrorResponse.CreateCardErrorResponseExample
{
    public class CreateCardErrorResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.CardPost.ErrorResponse>>
    {
        public ActiveResponseSucces<RailsBankHandler.CardPost.ErrorResponse> GetExamples()
        {
            return new ActiveResponseSucces<RailsBankHandler.CardPost.ErrorResponse>()
            {
                Status = new Status()
                {
                    Code = "ERROR-01",
                    Severity = "Error",
                    StatusMessage = ""
                },
                LogId = "495e2695-82ba-4427-8870-f9a03f31ce4d",
                Content = new RailsBankHandler.CardPost.ErrorResponse()
                {
                    error = "card-programme-not-found-on-specified-product",
                }
            };
        }
    }
}
