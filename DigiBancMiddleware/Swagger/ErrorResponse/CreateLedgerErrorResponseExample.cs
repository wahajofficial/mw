﻿using DBHandler.Helper;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.ErrorResponse.CreateLedgerErrorResponseExample
{
    public class CreateLedgerErrorResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.LegdersPost.ErrorResponse>>
    {
        public ActiveResponseSucces<RailsBankHandler.LegdersPost.ErrorResponse> GetExamples()
        {
            return new ActiveResponseSucces<RailsBankHandler.LegdersPost.ErrorResponse>()
            {
                Status = new Status()
                {
                    Code = "ERROR-01",
                    Severity = "Error",
                    StatusMessage = ""
                },

                LogId = "495e2695-82ba-4427-8870-f9a03f31ce4d",
                Content = new RailsBankHandler.LegdersPost.ErrorResponse()
                {
                    error = "Invalid holder id."
                }
            };
        }
    }
}
