﻿using DBHandler.Helper;
using RailsBankHandler.BeneficiariesPost;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Response.AddBeneficiaryResponseExample
{
    public class AddBeneficiaryResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.BeneficiariesPost.ResponseObj>>
    {
        public ActiveResponseSucces<ResponseObj> GetExamples()
        {
            return new ActiveResponseSucces<ResponseObj>()
            {
                LogId = "e12854c1-df13-4bfd-85fd-c37390944f65",
                Status = new Status()
                {
                    Code = "MSG-000000",
                    Severity = "Success",
                    StatusMessage = "Success"
                },
                Content = new RailsBankHandler.BeneficiariesPost.ResponseObj()
                {
                    beneficiary_id = "6035e622-85ef-4122-9e27-c9be7330a3c2"
                }
            };
        }
    }
}
