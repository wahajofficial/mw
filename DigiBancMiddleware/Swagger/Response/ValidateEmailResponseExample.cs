﻿using DBHandler.Helper;
using PostCoderHandler.GetEmailValidate;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Response.ValidateEmailResponseExample
{
    public class ValidateEmailResponse : IExamplesProvider<ActiveResponseSucces<PostCoderHandler.GetEmailValidate.ResponseObj>>
    {
        public ActiveResponseSucces<ResponseObj> GetExamples()
        {
            return new ActiveResponseSucces<ResponseObj>()
            {
                LogId = "1740ecf1-f54e-4fa3-8056-3889157b5bbd",
                Status = new Status()
                {
                    Code = "MSG-000000",
                    Severity = "Success",
                    StatusMessage = "Success"
                },
                Content = new PostCoderHandler.GetEmailValidate.ResponseObj()
                {
                    warning = "This is a test key do NOT use it live (always returns true)",
                    state = "Test Key",
                    valid = true,
                    score = "100",
                    processtime = "0",
                }
            };
        }
    }
}
