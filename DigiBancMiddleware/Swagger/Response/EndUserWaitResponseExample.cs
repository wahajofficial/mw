﻿using DBHandler.Helper;
using RailsBankHandler.End;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Response.EndUserWaitResponseExample
{
    public class EndUserWaitResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.End.ResponseObject>>
    {
        ActiveResponseSucces<ResponseObject> IExamplesProvider<ActiveResponseSucces<ResponseObject>>.GetExamples()
        {
            return new ActiveResponseSucces<RailsBankHandler.End.ResponseObject>()
            {
                LogId = "e12854c1-df13-4bfd-85fd-c37390944f65",
                Status = new Status()
                {
                    Code = "MSG-000000",
                    Severity = "Success",
                    StatusMessage = "Success"
                },
                Content = new RailsBankHandler.End.ResponseObject()
                {
                    //created_at
                    enduser_status = "enduser-status-ok",
                    //last_modified_at;
                    ledgers = new List<object>() { "ledger_id: 602cd214-75e5-4695-8c7d-a4a260832dee", "ledger_id: 602cd1ed-3214-40ad-896c-60e6a39d9701" },
                    beneficiaries = new List<object>() { },
                    enduser_meta = new RailsBankHandler.End.EnduserMeta()
                    {
                        foo = "bar",
                    },

                    screening_monitored_search = false,
                    enduser_id = "6023832f-abed-4981-95c1-29a351824515",
                    person = new RailsBankHandler.End.Person()
                    {
                        country_of_residence = new List<string>() { "US" },
                        document_number = "000 000 000",
                        document_type = "passport",

                        income = new Income()
                        {
                            frequency = "annual",
                            currency = "EUR",
                            amount = 100,
                        },
                        pep_type = "direct",
                        pep_notes = "comment",

                        address_history = new List<AddressHistory>()
                        {
                        new AddressHistory()
                        {
                         address_end_date = "2099-12-31",
                         address_start_date = "2000-01-01",
                         address_iso_country = "GB"},
                        },

                        pep = true,
                        date_onboarded = "2011-11-21",
                        email = "alice@short.com",
                        tin = "541571",
                        document_issue_date = "2000-01-01",
                        name = "Bob",

                        address = new Address()
                        {
                            address_region = "California",
                            address_iso_country = "GB",
                            address_number = "47",
                            address_postal_code = "123456",
                            address_refinement = "Apartment 42",
                            address_street = "Riverside Drive",
                            address_city = "Bratislava"
                        },

                        social_security_number = "090606",
                        telephone = "0012345678912",
                        date_of_birth = "1981-02-03",
                        document_expiration_date = "2020-12-03",
                        tin_type = "type",

                        citizenship = new List<string>() {"USA"},
                        nationality = new List<string>(){ "Hungarian"},
                        document_issuer = "USA",
                        country_of_birth = "USA"
                
                    }
                   
                }               
            };
        }
    }
}
