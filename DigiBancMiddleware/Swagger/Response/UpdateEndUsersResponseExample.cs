﻿using DBHandler.Helper;
using RailsBankHandler.EndUsersPost;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Response
{
    public class UpdateEndUsersResponseExample : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.EndUsersPost.ResponseObj>>
    {
        public ActiveResponseSucces<ResponseObj> GetExamples()
        {
            return new ActiveResponseSucces<ResponseObj>()
            {
                LogId = "1740ecf1-f54e-4fa3-8056-3889157b5bbd",
                Status = new Status()
                {
                    Code = "MSG-000000",
                    Severity = "Success",
                    StatusMessage = "Success"
                },
                Content = new RailsBankHandler.EndUsersPost.ResponseObj()
                {
                    enduser_id = "6023832f-abed-4981-95c1-29a351824515"
                }
            };
        }
    }
}
