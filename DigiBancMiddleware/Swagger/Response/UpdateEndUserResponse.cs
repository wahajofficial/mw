﻿using DBHandler.Helper;
using RailsBankHandler.EndUsersPost;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Response.UpdateEndUserResponse
{
    public class UpdateEndUserResponseExample : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.EndUsersPost.ResponseObj>>
    {
        public ActiveResponseSucces<RailsBankHandler.EndUsersPost.ResponseObj> GetExamples()
        {
            return new ActiveResponseSucces<ResponseObj>()
            {
                LogId = "1740ecf1-f54e-4fa3-8056-3889157b5bbd",
                //RequestDateTime = DateTime.Now(),
                Status = new Status()
                {
                    Code = "MSG-000000",
                    Severity = "Success",
                    StatusMessage = "Success"
                },
                Content = new RailsBankHandler.EndUsersPost.ResponseObj()
                {
                    enduser_id = "603e0c4a-69f0-47b6-9c65-50f50af1967d"
                }
            };
        }
    }
}
