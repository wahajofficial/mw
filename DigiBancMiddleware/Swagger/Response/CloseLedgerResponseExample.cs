﻿using DBHandler.Helper;
using RailsBankHandler.GetLedgersClose;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Response.CloseLedgerResponseExample
{
    public class CloseLedgerResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.GetLedgersClose.ResponseObject>>
    {
        ActiveResponseSucces<ResponseObject> IExamplesProvider<ActiveResponseSucces<ResponseObject>>.GetExamples()
        {
            return new ActiveResponseSucces<ResponseObject>()
            {
                LogId = "e12854c1-df13-4bfd-85fd-c37390944f65",
                Status = new Status()
                {
                    Code = "MSG-000000",
                    Severity = "Success",
                    StatusMessage = "Success"
                },
                Content = new RailsBankHandler.GetLedgersClose.ResponseObject()
                {
                    ledger_id = "ledger is closed"
                }
            };
        }
    }
}