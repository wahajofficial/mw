﻿using DBHandler.Helper;
using RailsBankHandler.GetBeneficiaryAcct;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Response.GetBeneficiaryAcctResponseExample
{
    public class GetBeneficiaryAcctResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.GetBeneficiaryAcct.ResponseObject>>
    {
        public ActiveResponseSucces<ResponseObject> GetExamples()
        {
            return new ActiveResponseSucces<ResponseObject>()
            {
                LogId = "fc8a69d1-bbcc-43d2-a30f-30e8c6f621c8",
                Status = new Status()
                {
                    Code = "MSG-000000",
                    Severity = "Success",
                    StatusMessage = "Success"
                },
                Content = new RailsBankHandler.GetBeneficiaryAcct.ResponseObject()
                {
                    iban = "SK4402005678901234567890",
                    account_number = "45564658",
                    bank_country = "GB",
                    bank_code = "45564658",
                    bank_name = "Bank of America",
                    account_type = "checking",
                    bic_swift = "SPSRSKBAXXX",
                    beneficiary_id = "602911b8-76e9-4ed3-9ae5-02bb92765734",
                    asset_type = "eur",
                    asset_class = "currency",
                    account_id = "602911b8-00c2-494d-b242-422e71798054",
                    bank_code_type = "bsb",

                }
            };
        }
    }
}
