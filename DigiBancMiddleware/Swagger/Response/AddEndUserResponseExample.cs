﻿using Swashbuckle.AspNetCore.Filters;
using RailsBankHandler.EndUsersPost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DBHandler.Helper;

namespace DigiBancMiddleware.Swagger.Response
{
    public class AddEndUserResponseExample : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.EndUsersPost.ResponseObj>>
    {
        public ActiveResponseSucces<RailsBankHandler.EndUsersPost.ResponseObj> GetExamples()
        {
            return new ActiveResponseSucces<RailsBankHandler.EndUsersPost.ResponseObj>()
            {
                LogId = "1740ecf1-f54e-4fa3-8056-3889157b5bbd",
                Status = new Status()
                {
                    Code = "MSG-000000",
                    Severity = "Success",
                    StatusMessage = "Success"
                },
                Content = new RailsBankHandler.EndUsersPost.ResponseObj()
                {
                    enduser_id = "603e0c4a-69f0-47b6-9c65-50f50af1967d",
                }

            };
        }
      }
    }
