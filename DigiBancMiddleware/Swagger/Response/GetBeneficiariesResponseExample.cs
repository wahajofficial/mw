﻿using DBHandler.Helper;
using RailsBankHandler.GetBeneficiaries;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Response.GetBeneficiariesResponseExample
{
    public class GetBeneficiariesResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.GetBeneficiaries.ResponseObject>>
    {
        public ActiveResponseSucces<ResponseObject> GetExamples()
        {
            return new ActiveResponseSucces<ResponseObject>() {
                LogId = "1740ecf1-f54e-4fa3-8056-3889157b5bbd",
                Status = new Status()
                {
                    Code = "MSG-000000",
                    Severity = "Success",
                    StatusMessage = "Success"
                },
                Content = new RailsBankHandler.GetBeneficiaries.ResponseObject()
                {
                    iban = "SK4402005678901234567890",
                    beneficiary_status = "beneficiary-status-ok",
                    bank_country = "US",
                    screening_monitored_search = false,
                    holder_id = "6028d7f6-c035-4e7f-b232-b1029c7947ba",
                    beneficiary_holder = new BeneficiaryHolder()
                    {
                        enduser_id = "6028d7f6-c035-4e7f-b232-b1029c7947ba",
                        person = new Person()
                        {
                            name = "Bob"
                        }
                    },

                    bank_name = "Bank of America",
                    person = new Person()
                    {
                        country_of_residence = new List<string>() {
                    "US"},

                        document_number = "000 000 000",
                        document_type = "passport",
                        income = new Income()
                        {
                            amount = 0,
                            frequency = "annual",
                            currency = "eur"
                        },
                        pep_type = "direct",
                        pep_notes = "comment",
                        pep = true,
                        date_onboarded = "2011-11-21",
                        email = "alice@short.com",
                        tin = "541571",
                        name = "Bob",
                        address = new Address()
                        {
                            address_iso_country = "GB"
                        },
                        social_security_number = "090606",
                        telephone = "0012345678912",
                        date_of_birth = "1981-02-03",
                        document_expiration_date = "2020-12-03",
                        tin_type = "type",
                        citizenship = new List<string>(){
                    "US"
                },
                        nationality = new List<string>(){
                    "Hungarian"
                },
                        document_issuer = "USA",
                        country_of_birth = "US"
                    },
                    bic_swift = "SPSRSKBAXXX",
                    beneficiary_id = "60290040-cef8-4cb6-a02b-17198ca358d1",
                    asset_type = "eur",
                    asset_class = "currency",
                    default_account = new DefaultAccount()
                    {
                        iban = "SK4402005678901234567890",
                        account_number = "45564658",
                        bank_country = "US",
                        bank_code = "45564658",
                        bank_name = "Bank of America",
                        account_type = "account-type/checking",
                        bic_swift = "SPSRSKBAXXX",
                        asset_type = "eur",
                        asset_class = "currency",
                        account_id = "60290040-9cd7-450e-bd96-8335b6f1f0ef",
                        bank_code_type = "bsb"
                    }
                }
            };
        }
    }
}
