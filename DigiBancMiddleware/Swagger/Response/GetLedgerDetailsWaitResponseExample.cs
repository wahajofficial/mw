﻿using DBHandler.Helper;
using RailsBankHandler.GetLedgersWait;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Response.GetLedgerDetailsWaitResponseExample
{
    public class GetLedgerDetailsWaitResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.GetLedgersWait.ResponseObject>>
    {
        public ActiveResponseSucces<ResponseObject> GetExamples()
        {
            return new ActiveResponseSucces<ResponseObject>()
            {
                LogId = "e12854c1-df13-4bfd-85fd-c37390944f65",
                Status = new Status()
                {
                    Code = "MSG-000000",
                    Severity = "Success",
                    StatusMessage = "Success"
                },
                Content = new RailsBankHandler.GetLedgersWait.ResponseObject()
                {
                    ledger_primary_use_types = new List<string>()
                    {
                        "ledger-primary-use-types-payments",
                        
                    },

                    ledger_id = "60238bb5-901d-487a-8787-364405bca218",
                    ledger_holder = new LedgerHolder()
                    {
                        foo = "bar"
                    },
                    ledger_who_owns_assets = "ledger-assets-owned-by-me",
                    partner_ref= "examplebank",
                    holder_id = "602392ca-b7dd-4b03-bcda-a47208a307fc",
                    partner_id = "58fe2ce0-3def-4e08-b778-c121c7f98334",
                    ledger_t_and_cs_country_of_jurisdiction = "GB",
                    ledger_status = "ledger-status-ok",
                    amount = "0",
                    partner_product = "ExampleBank-EUR-1",

                    partner = new Partner()
                    {
                        partner_ref = "examplebank",
                        partner_id = "58fe2ce0-3def-4e08-b778-c121c7f98334",
                        company = new Company()
                        {
                            name = "Example Bank"
                        }
                    },
                    asset_type = "eur",
                    asset_class = "currency",
                    ledger_type = "ledger-type-single-user",
                    ledger_meta = new LedgerMeta()
                    {
                        foo = "bar"
                    }
                },
            };
        }
    }
}
