﻿using DBHandler.Helper;
using RailsBankHandler.UpdateBeneficiary;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Response.UpdateBeneficiaryResponseExample
{
    public class UpdateBeneficiaryResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.UpdateBeneficiary.ResponseObj>>
    {
        public ActiveResponseSucces<ResponseObj> GetExamples()
        {
            return new ActiveResponseSucces<ResponseObj>()
            {
                LogId = "fc8a69d1-bbcc-43d2-a30f-30e8c6f621c8",
                Status = new Status()
                {
                    Code = "MSG-000000",
                    Severity = "Success",
                    StatusMessage = "Success"
                },
                Content = new RailsBankHandler.UpdateBeneficiary.ResponseObj()
                {
                    beneficiary_id = "6035e622-85ef-4122-9e27-c9be7330a3c2"
                }
            };
        }
    }
}
