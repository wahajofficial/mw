﻿using DBHandler.Helper;
using SendGridHandler.SendMail;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Response.SendEmailRequestExample
{
    public class SendEmailRequest : IExamplesProvider<ActiveResponseSucces<SendGridHandler.SendMail.ResponseObject>>
    {
        public ActiveResponseSucces<ResponseObject> GetExamples()
        {
            return new ActiveResponseSucces<ResponseObject>()
            {
                LogId = "fc8a69d1-bbcc-43d2-a30f-30e8c6f621c8",
                Status = new Status()
                {
                    Code = "MSG-000000",
                    Severity = "Success",
                    StatusMessage = "Success"
                },
                Content = new SendGridHandler.SendMail.ResponseObject()
                {
                    errors = null
                }
            };
        }
    }
}
