﻿using DBHandler.Helper;
using SumsubHandler.TokenGen;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.SumSub_Examples.TokenGenExample
{
    public class RequestTokenGenExample : IExamplesProvider<SumsubHandler.TokenGen.RequestTokenGen>
    {
        public RequestTokenGen GetExamples()
        {
            return new RequestTokenGen()
            {
                applicantId = "123"
            };
        }
    }

    public class ResponseTokenGenExample : IExamplesProvider<ActiveResponseSucces<SumsubHandler.TokenGen.ResponseTokenGen>>
    {
        ActiveResponseSucces<ResponseTokenGen> IExamplesProvider<ActiveResponseSucces<ResponseTokenGen>>.GetExamples()
        {
            return new ActiveResponseSucces<ResponseTokenGen>()
            {
                LogId = "5ac04be8-f4fd-4891-a653-328966815caf",
                Status = new Status()
                {
                    Code = "MSG-000000",
                    Severity = "Success",
                    StatusMessage = "Success"
                },

                Content = new ResponseTokenGen()
                {
                    token = "_act-c5dcd247-a633-4677-8400-f2b4ebe04ad1",
                    userId = "123"
                }
            };
        }
    }

    public class ErrorResponseTokenGenExample : IExamplesProvider<ActiveResponseSucces<SumsubHandler.TokenGen.ErrorResponse>>
    {
        public ActiveResponseSucces<SumsubHandler.TokenGen.ErrorResponse> GetExamples()
        {
            return new ActiveResponseSucces<SumsubHandler.TokenGen.ErrorResponse>()
            {
                LogId = "5ac04be8-f4fd-4891-a653-328966815caf",
                Status = new Status()
                {
                    Code = "Error-01",
                    Severity = "Error",
                    StatusMessage = "Error"
                },

                Content = new SumsubHandler.TokenGen.ErrorResponse()
                {
                    Error = "Invalid Applicant ID"
                }
            };
        }
    }
}
