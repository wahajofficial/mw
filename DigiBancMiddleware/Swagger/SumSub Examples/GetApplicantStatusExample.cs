﻿using DBHandler.Helper;
using SumsubHandler.GetApplicantStatus;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.SumSub_Examples
{
    public class RequestGetApplicantStatusExample : IExamplesProvider<SumsubHandler.GetApplicantStatus.RequestGetApplicantStatus>
    {
        public RequestGetApplicantStatus GetExamples()
        {
            return new RequestGetApplicantStatus()
            {
                applicantId = "123"
            };
        }
    }

    public class ResponseGetApplicantStatusExample : IExamplesProvider<ActiveResponseSucces<SumsubHandler.GetApplicantStatus.ResponseGetApplicantStatus>>
    {
        public ActiveResponseSucces<ResponseGetApplicantStatus> GetExamples()
        {
            return new ActiveResponseSucces<ResponseGetApplicantStatus>()
            {
                LogId = "5ac04be8-f4fd-4891-a653-328966815caf",
                Status = new Status()
                {
                    Code = "MSG-000000",
                    Severity = "Success",
                    StatusMessage = "Success"
                },
                Content = new ResponseGetApplicantStatus()
                {
                    IDENTITY = new IDENTITY()
                    {
                        reviewResult = new ReviewResult()
                        {

                        },
                        country = "GBR",
                        idDocType = "PASSPORT",
                        imageIds = new List<string>()
                        {
                            "1791824894"
                        },
                        imageReviewResults = new ImageReviewResults()
                        { _1791824894 = new _1791824894()
                        {
                            
                        },
                        },
                        forbidden = false,
                        stepStatuses =null
                        
                    },
                    SELFIE = null
                }
            };
        }
    }

    public class ErrorResponseGetApplicantStatusExample : IExamplesProvider<ActiveResponseSucces<SumsubHandler.GetApplicantStatus.ErrorResponse>>
    {
        public ActiveResponseSucces<SumsubHandler.GetApplicantStatus.ErrorResponse> GetExamples()
        {
            return new ActiveResponseSucces<SumsubHandler.GetApplicantStatus.ErrorResponse>()
            {
                LogId = "5ac04be8-f4fd-4891-a653-328966815caf",
                Status = new Status()
                {
                    Code = "Error-01",
                    Severity = "Error",
                    StatusMessage = "Error"
                },

                Content = new SumsubHandler.GetApplicantStatus.ErrorResponse()
                {
                    description= "Invalid id 6079cf8d5a8c28000b80f42",
                    code= 401,
                    correlationId="req-bc1429be-05fc-47ea-8dbe-38089e87aa95"
                }
            };
        }
    }
}
