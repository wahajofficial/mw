﻿using DBHandler.Helper;
using SumsubHandler.CreateApplicant;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.SumSub_Examples.CreateApplicationExample
{
    public class RequestCreateApplication : IExamplesProvider<SumsubHandler.CreateApplicant.RequestCreateApplicant>
    {
        public RequestCreateApplicant GetExamples()
        {
            return new RequestCreateApplicant()
            {
                externalUserId = "USER_1615194077",
                sourceKey = "",
                email = "fahad@codebtech.com",
                lang = "",
                info = new Info()
                {
                    firstName = "Fahad",
                    lastName = "Ali",
                    middleName = "",
                    firstNameEn = "",
                    lastNameEn = "",
                    middleNameEn = "",
                    legalName = "",
                    gender = "",
                    dob = "",
                    placeOfBirth = "London",
                    countryOfBirth = "",
                    stateOfBirth = "",
                    country = "GBR",
                    nationality = "",
                    phone = "+449112081223",
                    addresses = new List<Addresses>()
                    {
                       new Addresses()
                       {
                            country="GBR",
             postCode="",
             town="",
             street="",
             subStreet="",
             state="",
             buildingName="",
             flatNumber="",
             buildingNumber="",
             startDate="",
             endDate=""

                 }
                    }
                },

                metadata = new List<Metadata>()
                {
                    new Metadata()
                    {
                         key= "keyFromClient", 
                         value ="valueFromClient"
                    }
                },

                fixedInfo = new fixedInfo()
                {
                    firstName = "Fahad",
                    lastName = "Ali",
                    middleName = "",
                    firstNameEn = "",
                    lastNameEn = "",
                    middleNameEn = "",
                    legalName = "",
                    gender = "",
                    dob = "",
                    placeOfBirth = "London",
                    countryOfBirth = "",
                    stateOfBirth = "",
                    country = "GBR",
                    nationality = "",
                    phone = "+449112081223",
                    addresses = new List<Addresses>()
                    {
                       new Addresses()
                       {
                            country="GBR",
             postCode="",
             town="",
             street="",
             subStreet="",
             state="",
             buildingName="",
             flatNumber="",
             buildingNumber="",
             startDate="",
             endDate=""

                 }
                    }
                }
            };
        }
    }

    public class ResponseCreateApplication : IExamplesProvider<ActiveResponseSucces<SumsubHandler.CreateApplicant.ResponseCreateApplicant>>
    {
        public ActiveResponseSucces<ResponseCreateApplicant> GetExamples()
        {
            return new ActiveResponseSucces<ResponseCreateApplicant>()
            {
                LogId = "5ac04be8-f4fd-4891-a653-328966815caf",
                Status = new Status()
                {
                    Code = "MSG-000000",
                    Severity = "Success",
                    StatusMessage = "Success"
                },

                Content = new ResponseCreateApplicant()
                {
                    id = "604752b54152840009a94832",
                    createdAt = "2021-03-09 10:49:25",
                    key = "XAVUZREYRNXTJS",
                    clientId = "codebtech",
                    inspectionId = "604752b54152840009a94833",
                    externalUserId = "USER_1615194055",
                    info = new Info()
                    {
                        firstName = "Fahad",
                        lastName = "Ali",
                        middleName = "",
                        firstNameEn = "Fahad",
                        lastNameEn = "Ali",
                        middleNameEn = "",
                        legalName = "",
                        gender = "",
                        dob = "",
                        placeOfBirth = "London",
                        countryOfBirth = "",
                        stateOfBirth = "",
                        country = "GBR",
                        nationality = "",
                        phone = "+449112081223"
                    },

                    fixedInfo = new fixedInfo()
                    {
                        firstName = "Fahad",
                        lastName = "Ali",
                        middleName = "",
                        firstNameEn = "",
                        lastNameEn = "",
                        middleNameEn = "",
                        legalName = "",
                        gender = "",
                        dob = "",
                        placeOfBirth = "London",
                        countryOfBirth = "",
                        stateOfBirth = "",
                        country = "GBR",
                        nationality = "",
                        phone = "+449112081223"
                    },
                    review = new Review()
                    {
                        reviewId = "YmZmG",
                        reprocessing = false,
                        levelName = "basic-kyc-level",
                        createDate = "2021-03-09 10:49:25+0000",
                        reviewStatus = "init",
                        notificationFailureCnt = 0,
                        priority = 0,
                        autoChecked = false

                    },
                    type = "individual",
                    email = "fahad@codebtech.com",

                        requiredIdDocs = new RequiredIdDocs()
                    {
                        docSets = new List<DocSet>()
                    {
                        new DocSet()
                        {
                            idDocSetType = "IDENTITY",
                            types = new List<string>()
                            {
                                "RESIDENCE_PERMIT", "DRIVERS" ,"ID_CARD", "PASSPORT"
                            },
                            subTypes = new List<string>()
                            {
                                "FRONT_SIDE","BACK_SIDE"
                            }
                        },

                        new DocSet()
                        {
                            idDocSetType = "SELFIE",
                            types = new List<string>()
                            {
                                "SELFIE"
                            },
                            videoRequired = "liveness"

                        }

                      }
                    }
                }
            };
        }
    }

    public class ErrorResponseCreateApplication : IExamplesProvider<ActiveResponseSucces<SumsubHandler.CreateApplicant.ErrorResponse>>
    {
        public ActiveResponseSucces<SumsubHandler.CreateApplicant.ErrorResponse> GetExamples()
        {
            return new ActiveResponseSucces<SumsubHandler.CreateApplicant.ErrorResponse>()
            {
                LogId = "5ac04be8-f4fd-4891-a653-328966815caf",
                Status = new Status()
                {
                    Code = "Error-01",
                    Severity = "Error",
                    StatusMessage = "Error"
                },

                Content = new SumsubHandler.CreateApplicant.ErrorResponse()
                {
                    description = "Applicant with external user id 'USER_1615194055' already exists: 604752b54152840009a94832",
                    code = 409,
                    correlationId = "req-cd15a46b-7bc1-49d1-b0ff-0cedb8d2f72f"
                }
            };
        }
    }
}
