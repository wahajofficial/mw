﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using DBHandler.Enum;
using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.DtoParameter;
using DBHandler.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Annotations;

namespace VendorApi.Controllers
{
    // [Authorize]

    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : Controller
    {
        private ILogRepository _log;
        private IConfiguration _config;
        private Exception excetionForLog;
        private Level level = Level.Info;

        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IUserChannelsRepository _userChannels;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserStore<ApplicationUser> _store;
        public static long count = 1;

        public AuthController(IUserChannelsRepository userChannels, ILogRepository log, IConfiguration config, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, VendorMWContext ctx)
        {
            _log = log;
            _config = config;
            _userManager = userManager;
            _signInManager = signInManager;
            _userChannels = userChannels;
            _store = new UserStore<ApplicationUser>(ctx);
        }

        /// <summary>
        /// This is to get version of the api of middleware
        /// </summary>
        /// <remarks>
        /// this will let you know which version of the api is running on the system currently
        /// </remarks>
        /// <param name="SignOnRq"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetVersion")]
        //[Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [SwaggerResponse(200, "Version information", typeof(DBHandler.Model.Version))]
        public async Task<IActionResult> GetVersion([FromBody] GetVersionModel Model)
        {
            DateTime starTime = DateTime.Now;
            IActionResult Result = null;
            ActiveResponseSucces<DBHandler.Model.Version> ResponseSuccess = new ActiveResponseSucces<DBHandler.Model.Version>();
            DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, _log);
            HttpContext ctx = Request.HttpContext;
            string id = HttpContext.User.Claims.First().Value;
            try
            {

                DBHandler.Model.Version version = _userChannels.GetAPIVersion();
                ResponseSuccess.LogId = Model.SignOnRq.LogId;
                ResponseSuccess.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                ResponseSuccess.Content = version;
                Result = Ok(ResponseSuccess);
            }
            catch (Exception ex)
            {
                level = Level.Error;
                //ResponseSuccess.Status.Code =Status.ACTVETErroCode.Failed;
                ResponseSuccess.Content = null;
                ResponseSuccess.LogId = Model.SignOnRq.LogId;
                ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                excetionForLog = ex;
                Result = BadRequest(ResponseSuccess);
            }

            ResponseSuccess.LogId = Model.SignOnRq.LogId;
            ResponseSuccess.RequestDateTime = Model.SignOnRq.DateTime;

            //var userID = HttpContext.User.Claims.First().Value;
            var userID = id;
            string res = JsonConvert.SerializeObject(Model);
            string req = JsonConvert.SerializeObject(Result);
            DateTime Endtime = DateTime.Now;
            HttpContext Ctx = Request.HttpContext;

            if (bool.Parse(_config["GlobalSettings:IsLogEnabled"]))
            {
                _log.Logs(level, ResponseSuccess.Status.Code.ToString(), Ctx, excetionForLog, starTime, Endtime, res, req, userID, Model.SignOnRq.ChannelId, ResponseSuccess.LogId, Model.SignOnRq.DateTime, System.Net.HttpStatusCode.InternalServerError, "", "", DateTime.Now, DateTime.Now, "", "", "", _config["ErrorLog"], _config["SuccessLog"], _config["InfoLog"], _config["SPLUNK:COLLECTOR"].ToString(), _config["SPLUNK:BASE_URL"].ToString(), _config["SPLUNK:AUTHKEY"].ToString());
            }
            return Result;
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [Route("register")]
        public async Task<IActionResult> register([FromBody] Registration Model)
        {
            DateTime starTime = DateTime.Now;
            IActionResult Result = null;
            ActiveResponseSucces<string> ResponseSuccess = new ActiveResponseSucces<string>();
            DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, _log);
            HttpContext ctx = Request.HttpContext;

            try
            {
                var id = HttpContext.User.Claims.First().Value;

                //return if channel not validated
                //if (!helper.ValidateChannel(id, Model.SignOnRq.ChannelId, ctx))
                //{
                //    level = Level.Error;
                //    ResponseSuccess.LogId = Model.SignOnRq.LogId;
                //    ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidChannel"].ToString(), Code = "ERROR-01" };
                //    Result = BadRequest(ResponseSuccess);
                //}
                //else
                //{

                //    //return BadRequest(ModelState);
                //    var userId = Guid.NewGuid().ToString();
                //    var password = userId + Model.Password;

                //    var user = new ApplicationUser { Id = userId, UserName = Model.Username, Email = Model.Email };
                //    var Userresult = await _userManager.CreateAsync(user, password);
                //    if (Userresult.Succeeded)
                //    {
                //        ResponseSuccess.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                //        ResponseSuccess.Content = "User Registered Successfully";
                //        Result = Ok(ResponseSuccess);
                //    }
                //    else
                //    {
                //        level = Level.Error;
                //        ResponseSuccess.Status = new Status { Severity = Severity.Success, Code = "ERROR=000001", StatusMessage = Severity.Error };
                //        ResponseSuccess.Content = "";

                //        Result = Ok(ResponseSuccess);

                //    }
                //}
            }
            catch (Exception ex)
            {
                level = Level.Error;
                //ResponseSuccess.Status.Code =Status.ACTVETErroCode.Failed;
                ResponseSuccess.Content = null;
                ResponseSuccess.LogId = Model.SignOnRq.LogId;
                ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = Severity.Exception, Code = "ERROR-03" };
                excetionForLog = ex;
                Result = BadRequest(ResponseSuccess);
            }

            ResponseSuccess.LogId = Model.SignOnRq.LogId;
            ResponseSuccess.RequestDateTime = Model.SignOnRq.DateTime;

            //var userID = HttpContext.User.Claims.First().Value;
            //var userID = id;
            string res = JsonConvert.SerializeObject(Model);
            string req = JsonConvert.SerializeObject(Result);
            DateTime Endtime = DateTime.Now;
            HttpContext Ctx = Request.HttpContext;
            if (bool.Parse(_config["GlobalSettings:IsLogEnabled"]))
            {
                _log.Logs(level, ResponseSuccess.Status.Code.ToString(), Ctx, excetionForLog, starTime, Endtime, res, req, null, Model.SignOnRq.ChannelId, ResponseSuccess.LogId, Model.SignOnRq.DateTime, System.Net.HttpStatusCode.InternalServerError, "", "", DateTime.Now, DateTime.Now, "", "", "", _config["ErrorLog"], _config["SuccessLog"], _config["InfoLog"], _config["SPLUNK:COLLECTOR"].ToString(), _config["SPLUNK:BASE_URL"].ToString(), _config["SPLUNK:AUTHKEY"].ToString());
            }


            return Result;
        
        }






        [HttpPost]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [Route("ChangePassword")]
        public async Task<IActionResult> ChangePassword([FromBody]  ChangePassword Model)
        {
            DateTime starTime = DateTime.Now;
            IActionResult Result = null;
            ActiveResponseSucces<string> ResponseSuccess = new ActiveResponseSucces<string>();
            DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, _log);
            HttpContext ctx = Request.HttpContext;
            var id = HttpContext.User.Claims.First().Value;

            try
            {
                ////if (!helper.ValidateChannel(id, Model.SignOnRq.ChannelId, ctx))
                ////{
                ////    level = Level.Error;
                ////    ResponseSuccess.LogId = Model.SignOnRq.LogId;
                ////    ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidChannel"].ToString(), Code = "ERROR-01" };
                ////    Result = BadRequest(ResponseSuccess);
                ////}
                ////else
                ////{
                ////    var findUser = _userManager.Users.SingleOrDefault(e => e.UserName == Model.UserName);
                ////    var newPassword = findUser.Id + Model.NewPassword;
                ////    findUser.LockoutEnabled = false;
                ////    findUser.LockoutEnd = null;
                ////    string hashedNewPassword = _userManager.PasswordHasher.HashPassword(findUser, newPassword);
                ////    await _store.SetPasswordHashAsync(findUser, hashedNewPassword);
                ////    await _store.UpdateAsync(findUser);
                ////}
            }
            catch (Exception ex)
            {
                level = Level.Error;
                //ResponseSuccess.Status.Code =Status.ACTVETErroCode.Failed;
                ResponseSuccess.Content = null;
                ResponseSuccess.LogId = Model.SignOnRq.LogId;
                ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = Severity.Exception, Code = "ERROR-03" };
                excetionForLog = ex;
                Result = BadRequest(ResponseSuccess);
            }

            ResponseSuccess.LogId = Model.SignOnRq.LogId;
            ResponseSuccess.RequestDateTime = Model.SignOnRq.DateTime;

            //var userID = HttpContext.User.Claims.First().Value;
            var userID = id;
            string res = JsonConvert.SerializeObject(Model);
            string req = JsonConvert.SerializeObject(Result);
            DateTime Endtime = DateTime.Now;
            HttpContext Ctx = Request.HttpContext;
            if (bool.Parse(_config["GlobalSettings:IsLogEnabled"]))
            {
                _log.Logs(level, ResponseSuccess.Status.Code.ToString(), Ctx, excetionForLog, starTime, Endtime, res, req, userID, Model.SignOnRq.ChannelId, ResponseSuccess.LogId, Model.SignOnRq.DateTime, System.Net.HttpStatusCode.InternalServerError, "", "", DateTime.Now, DateTime.Now, "", "", "", _config["ErrorLog"], _config["SuccessLog"], _config["InfoLog"], _config["SPLUNK:COLLECTOR"].ToString(), _config["SPLUNK:BASE_URL"].ToString(), _config["SPLUNK:AUTHKEY"].ToString());
            }

            return Result;
        }
    }
}