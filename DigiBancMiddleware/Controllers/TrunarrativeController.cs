﻿using DBHandler.Enum;
using DBHandler.Helper;
using DBHandler.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using VendorApi.Helper;

namespace DigiBancMiddleware.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrunarrativeController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private readonly ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;

        public TrunarrativeController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs)
        {
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
        }


        [Route("AccountScreening")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(201, "ActiveResponseSucces<TrunarrativeHandler.AccountScreening.AccountScreeningResponse>", typeof(ActiveResponseSucces<TrunarrativeHandler.AccountScreening.AccountScreeningResponse>))]
        [SwaggerResponse(400, "ActiveResponseSucces<TrunarrativeHandler.AccountScreening.AccountScreeningErrorResponse>", typeof(ActiveResponseSucces<TrunarrativeHandler.AccountScreening.AccountScreeningErrorResponse>))]
        public async Task<IActionResult> AccountScreening([FromBody] TrunarrativeHandler.AccountScreening.AccountScreeningRequest Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["TruRestAPI:BaseUrl"].ToString(),
                                                                        _config["TruRestAPI:AccountScreening"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 34);

            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;

            if (result.httpResult?.StatusCode == HttpStatusCode.OK)
            {
                response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                return CreatedAtAction(null, response);
            }

            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
            return BadRequest(response);
        }

        [Route("TransactionMonitoring")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(201, "ActiveResponseSucces<TrunarrativeHandler.TransactionMonitoring.TransactionMonitoringResponse>", typeof(ActiveResponseSucces<TrunarrativeHandler.TransactionMonitoring.TransactionMonitoringResponse>))]
        [SwaggerResponse(400, "ActiveResponseSucces<TrunarrativeHandler.TransactionMonitoring.TransactionMonitoringErrorResponse>", typeof(ActiveResponseSucces<TrunarrativeHandler.TransactionMonitoring.TransactionMonitoringErrorResponse>))]
        public async Task<IActionResult> TransactionMonitoring([FromBody] TrunarrativeHandler.TransactionMonitoring.TransactionMonitoringRequest Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["TruRestAPI:BaseUrl"].ToString(),
                                                                        _config["TruRestAPI:TransactionMonitoring"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 34);

            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;

            if (result.httpResult?.StatusCode == HttpStatusCode.OK)
            {
                response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                return CreatedAtAction(null, response);
            }

            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
            return BadRequest(response);
        }
    }
}
