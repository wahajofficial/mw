﻿using DBHandler.Enum;
using DBHandler.Helper;
using DBHandler.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using VendorApi.Helper;

namespace DigiBancMiddleware.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TwilioController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;
        private Exception myEx;
        private ControllerBase _controllerBase;
        private Exception excetionForLog;

        public TwilioController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs, ControllerBase controllerbase = null)
        {
            _controllerBase = controllerbase;
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
        }

        [Route("SendSMS")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(201, "ActiveResponseSucces<CommonModel.Twilio.SMS.ResponseObject>", typeof(ActiveResponseSucces<CommonModel.Twilio.ResponseObject>))]
        [SwaggerResponse(400, "ActiveResponseSucces<CommonModel.Twilio.SMS.ErrorResponse>", typeof(ActiveResponseSucces<CommonModel.Twilio.ErrorResponse>))]
        public async Task<IActionResult> SendSMS([FromBody] CommonModel.Twilio.SMS Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                       "",
                                                                        "",
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 10, 222);

           response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
           response.LogId = this.HttpContext.Request.Headers["LogId"];
           //var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
           //response.Content = obj;

            if (result.httpResult?.StatusCode == HttpStatusCode.OK)
                {
                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return CreatedAtAction(null, response);
                }
          
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = result.httpResult.httpResponse };
            //response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "Error" };
            return BadRequest(response);
        }
    }
}
