﻿using System.Threading.Tasks;
using DBHandler.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using VendorApi.Helper;
using Swashbuckle.AspNetCore.Annotations;
using DBHandler.Repositories;
using Microsoft.Extensions.Configuration;
using DBHandler.Enum;
using System.Net.Http;
using DBHandler.Model;
using System.Collections.Generic;


namespace DigiBancMiddleware.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DucontController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private readonly ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;
        private readonly IDocumentsRepository _documentsRepository;


        public DucontController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs, IDocumentsRepository DocumentsRepository)
        {
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
            _documentsRepository = DocumentsRepository;
        }

        [Route("SMS")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CommonModel.DucontSMS.ResponseRootObject>", typeof(ActiveResponseSucces<CommonModel.DucontSMS.ResponseRootObject>))]
        public async Task<IActionResult> SMS([FromBody] CommonModel.DucontSMS.DucontSMSModel Model)
        {
            ActiveResponseSucces<CommonModel.DucontSMS.ResponseRootObject> response = new ActiveResponseSucces<CommonModel.DucontSMS.ResponseRootObject>();
            Model.RootObject.userId = _config["SMS:UID"].ToString();
            Model.RootObject.password = _config["SMS:PWD"].ToString();
            Model.RootObject.channelId = _config["SMS:channelId"].ToString();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObject,
                                                                        _config["SMS:BaseUrl"].ToString(),
                                                                        _config["SMS:SMS"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 2, 1);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<CommonModel.DucontSMS.ResponseRootObject>(result.httpResult.httpResponse);
                if (data != null && data.responseCode != null && data.responseCode == "01")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                    response.Content = data;
                    return Ok(response);
                }
                else 
                {
                    response.Status = new Status { Severity = Severity.Error, Code = data.responseCode, StatusMessage = data.responseMessage};
                }
            }
            return BadRequest(response);
        }

        [Route("Email")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CommonModel.DucontEmail.ResponseRootObject>", typeof(ActiveResponseSucces<CommonModel.DucontEmail.ResponseRootObject>))]
        public async Task<IActionResult> Email([FromBody] CommonModel.DucontEmail.DucontEmailModel Model)
        {
            ActiveResponseSucces<CommonModel.DucontEmail.ResponseRootObject> response = new ActiveResponseSucces<CommonModel.DucontEmail.ResponseRootObject>();

            Model.RootObject.userId = _config["Email:UID"].ToString();
            Model.RootObject.password = _config["Email:PWD"].ToString();
            Model.RootObject.channelId = _config["Email:channelId"].ToString();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObject,
                                                                        _config["Email:BaseUrl"].ToString(),
                                                                        _config["Email:Email"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2, 1);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                var data = JsonConvert.DeserializeObject<CommonModel.DucontEmail.ResponseRootObject>(result.httpResult.httpResponse);
                if (data != null && data.responseCode != null && data.responseCode == "01")
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

                    response.Content = data;
                    return Ok(response);

                }
                else
                {
                    response.Status = new Status { Severity = Severity.Error, Code = data.responseCode, StatusMessage = data.responseMessage };

                }
            }
            return BadRequest(response);
        }
    }
}