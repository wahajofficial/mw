﻿using DBHandler.Enum;
using DBHandler.Helper;
using DBHandler.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RailsBankHandler.GetLedgers;
using RailsBankHandler.LegdersPost;
using RailsBankHandler.Process.CreateAccount;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using VendorApi.Helper;

namespace DigiBancMiddleware.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProcessController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private readonly ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;

        public ProcessController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs)
        {
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
        }

        [Route("CreateAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        //[ProducesResponseType(typeof(ActiveResponseSucces<RailsBankHandler.LegdersPost.ResponseObject>), statusCode: 201)]
        //[ProducesResponseType(typeof(ActiveResponseSucces<RailsBankHandler.Process.CreateAccount.ErrorResponse>), statusCode: 400)]
        public async Task<IActionResult> CreateAccount([FromBody] RailsBankHandler.LegdersPost.RequestLegdersPost Model)
        {
            ActiveResponseSucces<RailsBankHandler.Process.CreateAccount.CreateAccountResponse> response = new ActiveResponseSucces<RailsBankHandler.Process.CreateAccount.CreateAccountResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model.holder_id,
                                                                       _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                       _config["RailsBankAPI:GetEndUser"].ToString().Replace("{0}", Model.holder_id),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 20, 2);

            if (result.Status.Code == null)
            {
                var obj = JsonConvert.DeserializeObject<RailsBankHandler.Process.CreateAccount.CreateAccountResponse>(result.httpResult.httpResponse);
                response.Content = obj;
                if (obj != null && obj.ledgers != null && obj.ledgers.Count > 0)
                {
                    ActiveResponseSucces<object> response1 = new ActiveResponseSucces<object>();
                    List<object> getLedgResponse = new List<object>();
                    foreach (Ledger item in obj.ledgers)
                    {
                        InternalResult result1 = await APIRequestHandler.GetResponse(null, item,
                                                                           _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                           _config["RailsBankAPI:GetLedgerDetails"].ToString().Replace("{0}", item.ledger_id.ToString()),
                                                                           level,
                                                                           _logs,
                                                                           _userChannels,
                                                                           this,
                                                                           _config,
                                                                           httpHandler, 20, 2);

                        if (result.Status.Code == null)
                        {
                            if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
                            {
                                response1.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Ledger(s) already exist" };
                                getLedgResponse.Add(JsonConvert.DeserializeObject<object>(result1.httpResult.httpResponse));
                            }
                        }
                    }

                    response1.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
                    response1.LogId = this.HttpContext.Request.Headers["LogId"];
                    response1.Content = getLedgResponse;
                    return Ok(response1);
                }
                else
                {
                    ActiveResponseSucces<object> response2 = new ActiveResponseSucces<object>();
                    RequestLegdersPost requestLegdersPost = new RequestLegdersPost()
                    {
                        holder_id = Model.holder_id,
                        partner_product = Model.partner_product,
                        asset_class = Model.asset_class,
                        asset_type = Model.asset_type,
                        ledger_primary_use_types = Model.ledger_primary_use_types,
                        ledger_t_and_cs_country_of_jurisdiction = Model.ledger_t_and_cs_country_of_jurisdiction,
                        ledger_type = Model.ledger_type,
                        ledger_who_owns_assets = Model.ledger_who_owns_assets,
                        ledger_meta = Model.ledger_meta
                    };
                    InternalResult result2 = await APIRequestHandler.GetResponse(null, requestLegdersPost,
                                                                      _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                      _config["RailsBankAPI:CreateLedger"].ToString(),
                                                                      level,
                                                                      _logs,
                                                                      _userChannels,
                                                                      this,
                                                                      _config,
                                                                      httpHandler, 20);

                    response2.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
                    response2.LogId = this.HttpContext.Request.Headers["LogId"];
                    var data2 = JsonConvert.DeserializeObject<object>(result2.httpResult.httpResponse);
                    response2.Content = data2;
                    if (result2.Status.Code == null)
                    {
                        if (result2 != null && result2.httpResult?.StatusCode == HttpStatusCode.OK)
                        {
                            response2.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                            return CreatedAtAction(null, response2);
                        }
                    }

                    response2.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
                    return BadRequest(response2);
                }

            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
            return BadRequest(response);
        }
    }
}
