﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CoreHandler;
using DBHandler.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using DBHandler.Repositories;
using Microsoft.Extensions.Configuration;
using DBHandler.Enum;
using HttpHandler;
using VendorApi.Helper;
using CommonModel;
using Newtonsoft.Json.Linq;
using Swashbuckle.AspNetCore.Annotations;
using ERCCorporateHandler.ListOfWPSAccounts;
using ERCCorporateHandler;
//using RuleEngineHandler;
using CommonModel.RetailCustomerNoti;

namespace DigiBancMiddleware.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommonController : ControllerBase
    {

        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private readonly ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;

        public CommonController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs)
        {
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
        }

        [Route("SendFireBaseMessage")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CommonModel.RootObject>", typeof(ActiveResponseSucces<CommonModel.RootObject>))]
        public async Task<IActionResult> SendFireBaseMessage([FromBody] FireBaseMessage Model)
        {
            ActiveResponseSucces<CommonModel.RootObject> response = new ActiveResponseSucces<CommonModel.RootObject>();
            string Platform = "";

            RetailCustomerNotiModel m = new RetailCustomerNotiModel();
            m.RootObject = new CommonModel.RetailCustomerNoti.RootObject();
            m.RootObject.NotiContent = new NotiContent();

            m.RootObject.NotiContent.Heading = Model.title;
            m.RootObject.NotiContent.Description = Model.Body.TextToSend;
            m.RootObject.NotiContent.ShortDescription = Model.Body.TextToSend;
            m.RootObject.ChannelId = Model.SignOnRq.ChannelId;
            m.RootObject.Tag = "inbox";
            m.RootObject.Cif = Model.CIF;

            if (Model.SignOnRq.ChannelId != _config["GlobalSettings:RetailChannel"].ToString())
            {
                await RetailCustomerNoti(m);
            }

            if (string.IsNullOrEmpty(Model.deviceID))
            {
                CommonModel.GetDeviceToken.GetDeviceTokenModel getTokenModel = new CommonModel.GetDeviceToken.GetDeviceTokenModel();
                getTokenModel.SignOnRq = Model.SignOnRq;
                getTokenModel.RootObject = new CommonModel.GetDeviceToken.RootObject();
                getTokenModel.RootObject.CIF = Model.CIF;

                InternalResult deviceTokenResult = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                                    _config["Retail:BaseUrl"].ToString(),
                                                                                    _config["Retail:GetDeviceToken"].ToString(),
                                                                                    level,
                                                                                    _logs,
                                                                                    _userChannels,
                                                                                    this,
                                                                                    _config,
                                                                                    httpHandler, 5);
                response.LogId = Model.SignOnRq.LogId;
                response.RequestDateTime = Model.SignOnRq.DateTime;

                if (deviceTokenResult.Status != null && deviceTokenResult.Status.Severity == Severity.Error)
                {
                    response.Status = deviceTokenResult.Status;
                    return BadRequest(response);

                }

                var tokenResponse = JsonConvert.DeserializeObject<CommonModel.GetDeviceToken.ResponseRootObject>(deviceTokenResult.httpResult.httpResponse);
                if (tokenResponse.response.message != "Success")
                {
                    response.Status = new Status();
                    response.Status.Code = "ERR-001";
                    response.Status.StatusMessage = tokenResponse.response.message;
                    response.Status.Severity = Severity.Error;
                    return BadRequest(response);
                }
                else
                {
                    Model.deviceID = tokenResponse.response.content.message.firebaseToken;
                    Platform = tokenResponse.response.content.message.platForm;
                }
            }
            else
            {
                CommonModel.ValidateToken.ResponseRootObject validateresponse = new CommonModel.ValidateToken.ResponseRootObject();


                CommonModel.ValidateToken.ValidateTokenModel validateToeknModel = new CommonModel.ValidateToken.ValidateTokenModel();
                validateToeknModel.SignOnRq = Model.SignOnRq;
                validateToeknModel.RootObject = new CommonModel.ValidateToken.RootObject();
                validateToeknModel.RootObject.CIF = Model.CIF;
                validateToeknModel.RootObject.Token = Model.deviceID;

                InternalResult resultValidate = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, validateToeknModel.RootObject,
                                                                       _config["Retail:BaseUrl"].ToString(),
                                                                       _config["Retail:ValidateToken"].ToString(),
                                                                       level,
                                                                       _logs,
                                                                       _userChannels,
                                                                       this,
                                                                       _config,
                                                                       httpHandler, 5);
                validateresponse = JsonConvert.DeserializeObject<CommonModel.ValidateToken.ResponseRootObject>(resultValidate.httpResult.httpResponse);

                if (validateresponse.response.content.message == null )
                {
                    response.Status = new Status();
                    response.Status.Code = "ERR-001";
                    response.Status.StatusMessage = "Device not found.";
                    response.Status.Severity = Severity.Error;
                    return BadRequest(response);
                }
                else
                {
                    Platform = validateresponse.response.content.message.platForm;

                }
            }
            var data = new object();
            if (Platform == "A")
            {
                data = new
                {

                    to = Model.deviceID,
                    priority = "high",
                    notification=new
                    {
                        title = Model.title,
                        body = Model.Body.TextToSend
                    },

                    data = new
                    {
                        icon = "myicon",
                        page = "",
                        ConfrenceLink = "",
                        greeting = "",
                        message = Model.Body.TextToSend,
                        path = Model.Body.Path,
                        textToSend= Model.Body.TextToSend,
                        processRequest= Model.Body.ProcessRequest,
                        title = Model.title,
                        isReady = "false",
                        NotificationType = Model.typeID,
                        roomId = "",
                        notId = 10
                    }
                };
            }
            else
            {
                 data= new
                {
                    to = Model.deviceID,
                    notification = new
                    {
                        subtitle = Model.Body.TextToSend,
                        Title = "Live Chat"
                    },
                    priority = "high",
                    data = new
                    {
                        icon = "myicon",
                        page = "",
                        ConfrenceLink = "",
                        greeting = "",
                        message = Model.Body.TextToSend,
                        path = Model.Body.Path,
                        textToSend = Model.Body.TextToSend,
                        processRequest = Model.Body.ProcessRequest,
                        title = Model.title,
                        isReady = "false",
                        NotificationType = Model.typeID,
                        roomId = "",
                        notId = 10
                    }
                };
            }


            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, data,
                                                                        _config["GlobalSettings:FireBaseURL"].ToString(),
                                                                        _config["GlobalSettings:FireBaseAPI"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 8);
            response.LogId = Model.SignOnRq.LogId;
            response.RequestDateTime = Model.SignOnRq.DateTime;
            var respData = JsonConvert.DeserializeObject<CommonModel.RootObject>(result.httpResult.httpResponse);

            if (result.httpResult == null || result.httpResult.severity == "failure" || respData.success <= 0)
            {
                response.Status = new Status { Severity = Severity.Error, Code = "Err-0001", StatusMessage = "ERROR" };
                response.Content = respData;
            }
            else
            {
                response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                response.Content = respData;
                return Ok(response);
            }

            return BadRequest(response);
        }


        [Route("GetDeviceToken")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ShipaHandler.AWBValidation.RootObject>", typeof(ActiveResponseSucces<ShipaHandler.AWBValidation.RootObject>))]
        public async Task<IActionResult> GetDeviceToken([FromBody] CommonModel.GetDeviceToken.GetDeviceTokenModel Model)
        {
            ShipaHandler.AWBValidation.RootObject response = new ShipaHandler.AWBValidation.RootObject();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["Retail:BaseUrl"].ToString(),
                                                                        _config["Retail:GetDeviceToken"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 5);
            response = JsonConvert.DeserializeObject<ShipaHandler.AWBValidation.RootObject>(result.httpResult.httpResponse);
            if (response.response.message == "Success")
            {
                return Ok(response);
            }
            else
            {
                return BadRequest(response);
            }
        }

        [Route("ValidateToken")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CommonModel.ValidateToken.ResponseRootObject>", typeof(ActiveResponseSucces<CommonModel.ValidateToken.ResponseRootObject>))]
        public async Task<IActionResult> ValidateToken([FromBody] CommonModel.ValidateToken.ValidateTokenModel Model)
        {
            CommonModel.ValidateToken.ResponseRootObject response = new CommonModel.ValidateToken.ResponseRootObject();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["Retail:BaseUrl"].ToString(),
                                                                        _config["Retail:ValidateToken"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 5);
            response = JsonConvert.DeserializeObject<CommonModel.ValidateToken.ResponseRootObject>(result.httpResult.httpResponse);
            if (response.response.message != null)
            {
                return Ok(response);
            }
            else
            {
                return BadRequest(response);
            }
        }

        [Route("Receive")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CommonModel.ValidateToken.ResponseRootObject>", typeof(ActiveResponseSucces<CommonModel.ValidateToken.ResponseRootObject>))]
        public async Task<IActionResult> Receive([FromBody] CommonModel.ReceiveMessage.ReceiveMessageModel Model)
        {
            ActiveResponseSucces<CommonModel.ValidateToken.ResponseRootObject> response = new ActiveResponseSucces<CommonModel.ValidateToken.ResponseRootObject>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObject,
                                                                        _config["Retail:BaseUrl"].ToString(),
                                                                        _config["Retail:Receive"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 5);
            response.LogId = Model.SignOnRq.LogId;
            response.RequestDateTime = Model.SignOnRq.DateTime;
            if (result.Status != null && result.Status.Severity == Severity.Error)
            {
                response.Status = result.Status;
                return BadRequest(response);
            }
            else
            {
                var data = JsonConvert.DeserializeObject<CommonModel.ValidateToken.ResponseRootObject>(result.httpResult.httpResponse);

                if (result.httpResult != null && result.httpResult.severity == "failure")
                {
                    response.Status = new Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "Response not found." };
                    response.Content = data;
                    return BadRequest(response);
                }
                else
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                    response.Content = data;
                }

                return Ok(response);
            }
        }


        [Route("CardRequest")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CommonModel.CardRequest.ResponseRootObject>", typeof(ActiveResponseSucces<CommonModel.CardRequest.ResponseRootObject>))]
        public async Task<IActionResult> CardRequest([FromBody] CommonModel.CardRequest.CardRequestModel Model)
        {
            ActiveResponseSucces<CommonModel.CardRequest.ResponseRootObject> response = new ActiveResponseSucces<CommonModel.CardRequest.ResponseRootObject>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObject,
                                                                        _config["Retail:BaseUrl"].ToString(),
                                                                        _config["Retail:CardRequest"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 5);
            response.LogId = Model.SignOnRq.LogId;
            response.RequestDateTime = Model.SignOnRq.DateTime;
            if (result.Status != null && result.Status.Severity == Severity.Error)
            {
                response.Status = result.Status;
                return BadRequest(response);
            }
            else
            {
                var data = JsonConvert.DeserializeObject<CommonModel.CardRequest.ResponseRootObject>(result.httpResult.httpResponse);

                if (result.httpResult != null && result.httpResult.severity == "failure")
                {
                    response.Status = new Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "Response not found." };
                    response.Content = data;
                    return BadRequest(response);
                }
                else
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                    response.Content = data;
                }
                return Ok(response);
            }
        }

        /// <summary>
        /// Save customer 
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="signOnRq"></param>
        /// <returns></returns>
        //[Route("SaveCustomersInvitesOnRetail")]
        //[Authorize(AuthenticationSchemes = "BasicAuthentication")]
        //[HttpPost]
        //[SwaggerResponse(200, "ActiveResponseSucces<ActiveResponse>", typeof(ActiveResponseSucces<ActiveResponse>))]
        //public async Task<IActionResult> SaveCustomersInvitesOnRetail(CustomerDTOForRetailApi Model)
        //{
        //    ActiveResponseSucces<ActiveResponse> response = new ActiveResponseSucces<ActiveResponse>();
        //    InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.signOnRq, Model,
        //                                                                _config["Retail:BaseUrl"].ToString(),
        //                                                                _config["Retail:SaveCustomersInvitesOnRetail"].ToString(),
        //                                                                level,
        //                                                                _logs,
        //                                                                _userChannels,
        //                                                                this,
        //                                                                _config,
        //                                                                httpHandler, 5);

        //    response.Status = result.Status;
        //    response.LogId = Model.signOnRq?.LogId;
        //    response.RequestDateTime = Model.signOnRq?.DateTime;
        //    if (result.Status.Code == null)
        //    {
        //        response = JsonConvert.DeserializeObject<ActiveResponseSucces<ActiveResponse>>(result.httpResult.httpResponse);
        //        if (result.httpResult != null && result.httpResult.severity == "failure")
        //        {
        //            response.Status = new Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "Response not found." };
        //            response.Content = null;
        //            return BadRequest(response);
        //        }
        //        else
        //        {
        //            response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
        //            response.Content = response.Content;
        //            return Ok(response);
        //        }
        //    }

        //    return BadRequest(response);
        //}

        /// <summary>
        /// GetProspectId
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [Route("GetProspectId")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CommonModel.GetProspectId.ResponseRootObject>", typeof(ActiveResponseSucces<CommonModel.GetProspectId.ResponseRootObject>))]
        public async Task<IActionResult> GetProspectId(CommonModel.GetProspectId.GetProspectIdModel Model)
        {
            ActiveResponseSucces<CommonModel.GetProspectId.ResponseRootObject> response = new ActiveResponseSucces<CommonModel.GetProspectId.ResponseRootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.signOnRq, Model.RootObject,
                                                                        _config["Retail:BaseUrl"].ToString(),
                                                                        _config["Retail:GetProspectId"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 5);

            response.Status = result.Status;
            response.LogId = Model.signOnRq?.LogId;
            response.RequestDateTime = Model.signOnRq?.DateTime;
            if (result.Status == null  || result.Status.Code == null)
            {
                response.Content = JsonConvert.DeserializeObject<CommonModel.GetProspectId.ResponseRootObject>(result.httpResult.httpResponse);
                if (result.httpResult != null && result.httpResult.severity == "failure")
                {
                    response.Status = new Status() { Code = response.Content.response.content.code, Severity = Severity.Error, StatusMessage = response.Content.response.message };
                    response.Content = null;
                    return BadRequest(response);
                }
                else
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                    response.Content = response.Content;
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }



        [Route("RetailCustomerNoti")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CommonModel.RetailCustomerNoti.RetailCustomerNotiResponse>", typeof(ActiveResponseSucces<CommonModel.RetailCustomerNoti.RetailCustomerNotiResponse>))]
        public async Task<IActionResult> RetailCustomerNoti(CommonModel.RetailCustomerNoti.RetailCustomerNotiModel Model)
        {
            ActiveResponseSucces<CommonModel.RetailCustomerNoti.RetailCustomerNotiResponse> response = new ActiveResponseSucces<CommonModel.RetailCustomerNoti.RetailCustomerNotiResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.signOnRq, Model.RootObject,
                                                                        _config["Retail:BaseUrl"].ToString(),
                                                                        _config["Retail:RetailCustomerNoti"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 5);

            response.Status = result.Status;
            response.LogId = Model.signOnRq?.LogId;
            response.RequestDateTime = Model.signOnRq?.DateTime;
            if (result.Status == null || result.Status.Code == null)
            {
                response.Content = JsonConvert.DeserializeObject<CommonModel.RetailCustomerNoti.RetailCustomerNotiResponse>(result.httpResult.httpResponse);
                if (result.httpResult != null && result.httpResult.severity == "failure")
                {
                    response.Status = new Status() { Code = response.Content.response.content.code, Severity = Severity.Error, StatusMessage = response.Content.response.message };
                    response.Content = null;
                    return BadRequest(response);
                }
                else
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                    response.Content = response.Content;
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("CardRequestV2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CommonModel.CardRequestV2.ResponseRootObject>", typeof(ActiveResponseSucces<CommonModel.CardRequestV2.ResponseRootObject>))]
        public async Task<IActionResult> CardRequestV2([FromBody] CommonModel.CardRequestV2.CardRequestModel Model)
        {
            ActiveResponseSucces<CommonModel.CardRequestV2.ResponseRootObject> response = new ActiveResponseSucces<CommonModel.CardRequestV2.ResponseRootObject>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model.RootObject,
                                                                        _config["Retail:BaseUrl"].ToString(),
                                                                        _config["Retail:CardRequestV2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 5);
            response.LogId = Model.SignOnRq.LogId;
            response.RequestDateTime = Model.SignOnRq.DateTime;
            if (result.Status != null && result.Status.Severity == Severity.Error)
            {
                response.Status = result.Status;
                return BadRequest(response);
            }
            else
            {
                var data = JsonConvert.DeserializeObject<CommonModel.CardRequestV2.ResponseRootObject>(result.httpResult.httpResponse);

                if (result.httpResult != null && result.httpResult.severity == "failure")
                {
                    response.Status = new Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "Response not found." };
                    response.Content = data;
                    return BadRequest(response);
                }
                else
                {
                    response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                    response.Content = data;
                }
                return Ok(response);
            }
        }



    }
}