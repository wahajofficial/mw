﻿using DBHandler.Common;
using DBHandler.Enum;
using DBHandler.Helper;
using DBHandler.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Annotations;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using VendorApi.Helper;

namespace DigiBancMiddleware.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostCoderController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private  ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;
        private Exception myEx;
        private ControllerBase _controllerBase;
        private Exception excetionForLog;

        public PostCoderController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs, ControllerBase controllerbase = null)
        {
            _controllerBase = controllerbase;
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
        }

        /// <summary>
        /// Validate Email
        /// </summary>
        /// <remarks>
        /// Sample Request:
        ///
        ///     Following are parameter and values
        ///     
        ///        emailaddress: sales@alliescomputing.com
        ///     
        ///
        /// </remarks>
        [Route("ValidateEmail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpGet]
        [SwaggerResponse(200, "ActiveResponseSucces<PostCoderHandler.GetEmailValidate.ResponseObj>", typeof(ActiveResponseSucces<PostCoderHandler.GetEmailValidate.ResponseObj>))]
        [SwaggerResponse(400, "ActiveResponseSucces<PostCoderHandler.GetEmailValidate.ErrorResponse>", typeof(ActiveResponseSucces<PostCoderHandler.GetEmailValidate.ErrorResponse>))]
        public async Task<IActionResult> ValidateEmail([FromQuery] PostCoderHandler.GetEmailValidate.RequestGetEmailValidate Model)
        {
            CommonHelper common = new CommonHelper(_logs,_config);
            Dictionary<string, string>  headers = common.GetHeaderForPostCoder();
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            
            InternalResult result = await APIRequestHandler.GetResponse(null, 
                                                                        Model,
                                                                        _config["PostCoderAPI:BaseUrl"].ToString(),
                                                                        string.Format( _config["PostCoderAPI:ValidateEmail"].ToString(), headers["Authorization"],Model.emailaddress),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler,
                                                                        0,
                                                                        2);

            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<PostCoderHandler.GetEmailValidate.ResponseObj>(result.httpResult.httpResponse);
            var data = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = data;

            if (result.Status.Code == null)
            {
                if (result != null && obj.valid)
                {
                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return Ok(response);

                }
            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
            return BadRequest(response);
        }

        /// <summary>
        /// Validate Mob Num
        /// </summary>
        /// <remarks>
        /// Sample Request:
        ///
        ///     Following are parameter and values
        ///     
        ///        mobilevalidate : 07500123456
        ///     
        ///
        /// </remarks>
        [Route("ValidateMobNum")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpGet]
        [SwaggerResponse(200, "ActiveResponseSucces<PostCoderHandler.GetMobileValidate.ResponseObj>", typeof(ActiveResponseSucces<PostCoderHandler.GetMobileValidate.ResponseObj>))]
        [SwaggerResponse(400, "ActiveResponseSucces<PostCoderHandler.GetMobileValidate.ErrorResponse>", typeof(ActiveResponseSucces<PostCoderHandler.GetMobileValidate.ErrorResponse>))]
        public async Task<IActionResult> ValidateMobNum([FromQuery] PostCoderHandler.GetMobileValidate.RequestGetMobileValidate Model)
        {

            CommonHelper common = new CommonHelper(_logs, _config);
            Dictionary<string, string> headers = common.GetHeaderForPostCoder();

            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                            _config["PostCoderAPI:BaseUrl"].ToString(),
                                                                           string.Format(_config["PostCoderAPI:ValidateMobNum"].ToString(), headers["Authorization"], Model.mobilevalidate),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                            this,
                                                                            _config,
                                                                            httpHandler, 0, 2);
            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<PostCoderHandler.GetMobileValidate.ResponseObj>(result.httpResult.httpResponse);
            var data = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = data;

            if (result.Status.Code == null)
            {
                if (result != null && obj.valid)
                {
                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return Ok(response);
                }

            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
            return BadRequest(response);
        }

        /// <summary>
        /// Validate Address
        /// </summary>
        /// <remarks>
        /// Sample Request:
        ///
        ///     Following are parameter and values
        ///     
        ///        Country : UK
        ///        Postcode : NR14 7PZ
        ///        Addresslines : 0
        ///        Additionaldatafields : longitude
        ///     
        ///
        /// </remarks>
        [Route("ValidateAddress")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpGet]
        [SwaggerResponse(200, "ActiveResponseSucces<PostCoderHandler.GetAddress.ResponseObj>", typeof(ActiveResponseSucces<PostCoderHandler.GetAddress.ResponseObj>))]
        [SwaggerResponse(400, "ActiveResponseSucces<PostCoderHandler.GetAddress.ErrorResponse>", typeof(ActiveResponseSucces<PostCoderHandler.GetAddress.ErrorResponse>))]
        public async Task<IActionResult> ValidateAddress([FromQuery] PostCoderHandler.GetAddress.RequestAddress Model)
        {

            CommonHelper common = new CommonHelper(_logs,_config);
            Dictionary<string, string> headers = common.GetHeaderForPostCoder();

            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                            _config["PostCoderAPI:BaseUrl"].ToString(),
                                                                           string.Format(_config["PostCoderAPI:ValidateAddress"].ToString(), headers["Authorization"], Model.Country,Model.Postcode,Model.Addresslines,Model.Additionaldatafields),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                            this,
                                                                            _config,
                                                                            httpHandler, 0, 2);
            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<List<PostCoderHandler.GetAddress.ResponseObj>>(result.httpResult.httpResponse);
            var data = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);

            response.Content = data;
            if (result.Status.Code == null && obj.Count >0)
            {
                if (result != null )
                {
                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return Ok(response);
                }

            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "No data found" };
            return BadRequest(response);
        }
    }
}
