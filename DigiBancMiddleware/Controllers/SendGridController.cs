﻿using DBHandler.Enum;
using DBHandler.Helper;
using DBHandler.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using VendorApi.Helper;

namespace DigiBancMiddleware.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SendGridController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;
        private Exception myEx;
        private ControllerBase _controllerBase;
        private Exception excetionForLog;

        public SendGridController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs, ControllerBase controllerbase = null)
        {
            _controllerBase = controllerbase;
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
        }

        [Route("SendMail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(201, "ActiveResponseSucces<SendGridHandler.SendMail.ResponseObj>", typeof(ActiveResponseSucces<SendGridHandler.SendMail.ResponseObject>))]
        [SwaggerResponse(400, "ActiveResponseSucces<SendGridHandler.SendMail.ErrorResponse>", typeof(ActiveResponseSucces<SendGridHandler.SendMail.ErrorResponse>))]
        public async Task<IActionResult> SendMail([FromBody] SendGridHandler.SendMail.RequestSendMail Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["SendGridAPI:BaseUrl"].ToString(),
                                                                        _config["SendGridAPI:SendMail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 21);

            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;

            if (result.httpResult?.StatusCode == HttpStatusCode.Accepted)
            {
                response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                return CreatedAtAction(null, response);
            }
           
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };

            return BadRequest(response);
        }

    }
}
