﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PhoneUpdate
{
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.1-preview-30422-0661")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://cms.ws.eeft.com", ConfigurationName="PhoneUpdate.UpdatePhoneWSBean")]
    public interface UpdatePhoneWSBean
    {
        
        // CODEGEN: Generating message contract since the operation has multiple return values.
        [System.ServiceModel.OperationContractAttribute(Action="UpdatePhone", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Threading.Tasks.Task<PhoneUpdate.UpdatePhoneResponse> UpdatePhoneAsync(PhoneUpdate.UpdatePhoneRequest request);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.1-preview-30422-0661")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://core.ws.eeft.com")]
    public partial class ServiceHeader
    {
        
        private string serviceIDField;
        
        private string channelIDField;
        
        private string countryCodeField;
        
        private string txnSeqNumField;
        
        private string sourceDateField;
        
        private string sourceTimeField;
        
        private string userIDField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string ServiceID
        {
            get
            {
                return this.serviceIDField;
            }
            set
            {
                this.serviceIDField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public string ChannelID
        {
            get
            {
                return this.channelIDField;
            }
            set
            {
                this.channelIDField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=2)]
        public string CountryCode
        {
            get
            {
                return this.countryCodeField;
            }
            set
            {
                this.countryCodeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=3)]
        public string TxnSeqNum
        {
            get
            {
                return this.txnSeqNumField;
            }
            set
            {
                this.txnSeqNumField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=4)]
        public string SourceDate
        {
            get
            {
                return this.sourceDateField;
            }
            set
            {
                this.sourceDateField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=5)]
        public string SourceTime
        {
            get
            {
                return this.sourceTimeField;
            }
            set
            {
                this.sourceTimeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=6)]
        public string UserID
        {
            get
            {
                return this.userIDField;
            }
            set
            {
                this.userIDField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.1-preview-30422-0661")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://cms.ws.eeft.com")]
    public partial class Request_UpdatePhone
    {
        
        private string tokenCardNumberField;
        
        private string cardSeqNumField;
        
        private string functionField;
        
        private string phoneTypeField;
        
        private string seqNumField;
        
        private string priOrSecField;
        
        private string phoneNumField;
        
        private string privacyField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string TokenCardNumber
        {
            get
            {
                return this.tokenCardNumberField;
            }
            set
            {
                this.tokenCardNumberField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public string CardSeqNum
        {
            get
            {
                return this.cardSeqNumField;
            }
            set
            {
                this.cardSeqNumField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=2)]
        public string Function
        {
            get
            {
                return this.functionField;
            }
            set
            {
                this.functionField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=3)]
        public string PhoneType
        {
            get
            {
                return this.phoneTypeField;
            }
            set
            {
                this.phoneTypeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=4)]
        public string SeqNum
        {
            get
            {
                return this.seqNumField;
            }
            set
            {
                this.seqNumField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=5)]
        public string PriOrSec
        {
            get
            {
                return this.priOrSecField;
            }
            set
            {
                this.priOrSecField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=6)]
        public string PhoneNum
        {
            get
            {
                return this.phoneNumField;
            }
            set
            {
                this.phoneNumField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=7)]
        public string Privacy
        {
            get
            {
                return this.privacyField;
            }
            set
            {
                this.privacyField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.1-preview-30422-0661")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://cms.ws.eeft.com")]
    public partial class Response_UpdatePhone
    {
        
        private string tokenCardNumberField;
        
        private string cardSeqNumField;
        
        private string responseCodeField;
        
        private string responseTextField;
        
        private PhoneDetail[] phoneDetailField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string TokenCardNumber
        {
            get
            {
                return this.tokenCardNumberField;
            }
            set
            {
                this.tokenCardNumberField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public string CardSeqNum
        {
            get
            {
                return this.cardSeqNumField;
            }
            set
            {
                this.cardSeqNumField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=2)]
        public string ResponseCode
        {
            get
            {
                return this.responseCodeField;
            }
            set
            {
                this.responseCodeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=3)]
        public string ResponseText
        {
            get
            {
                return this.responseTextField;
            }
            set
            {
                this.responseTextField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PhoneDetail", Order=4)]
        public PhoneDetail[] PhoneDetail
        {
            get
            {
                return this.phoneDetailField;
            }
            set
            {
                this.phoneDetailField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.1-preview-30422-0661")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://cms.ws.eeft.com")]
    public partial class PhoneDetail
    {
        
        private string phnTypeField;
        
        private string seqNumField;
        
        private string priOrSecField;
        
        private string phnNumField;
        
        private string privacyField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string PhnType
        {
            get
            {
                return this.phnTypeField;
            }
            set
            {
                this.phnTypeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public string SeqNum
        {
            get
            {
                return this.seqNumField;
            }
            set
            {
                this.seqNumField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=2)]
        public string PriOrSec
        {
            get
            {
                return this.priOrSecField;
            }
            set
            {
                this.priOrSecField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=3)]
        public string PhnNum
        {
            get
            {
                return this.phnNumField;
            }
            set
            {
                this.phnNumField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=4)]
        public string Privacy
        {
            get
            {
                return this.privacyField;
            }
            set
            {
                this.privacyField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.1-preview-30422-0661")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="UpdatePhoneRequest", WrapperNamespace="http://cms.ws.eeft.com", IsWrapped=true)]
    public partial class UpdatePhoneRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://core.ws.eeft.com", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute(Namespace="http://core.ws.eeft.com")]
        public PhoneUpdate.ServiceHeader ServiceHeader;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://cms.ws.eeft.com", Order=1)]
        public PhoneUpdate.Request_UpdatePhone Request_UpdatePhone;
        
        public UpdatePhoneRequest()
        {
        }
        
        public UpdatePhoneRequest(PhoneUpdate.ServiceHeader ServiceHeader, PhoneUpdate.Request_UpdatePhone Request_UpdatePhone)
        {
            this.ServiceHeader = ServiceHeader;
            this.Request_UpdatePhone = Request_UpdatePhone;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.1-preview-30422-0661")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="UpdatePhoneResponse", WrapperNamespace="http://cms.ws.eeft.com", IsWrapped=true)]
    public partial class UpdatePhoneResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://core.ws.eeft.com", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute(Namespace="http://core.ws.eeft.com")]
        public PhoneUpdate.ServiceHeader ServiceHeader;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://cms.ws.eeft.com", Order=1)]
        public PhoneUpdate.Response_UpdatePhone Response_UpdatePhone;
        
        public UpdatePhoneResponse()
        {
        }
        
        public UpdatePhoneResponse(PhoneUpdate.ServiceHeader ServiceHeader, PhoneUpdate.Response_UpdatePhone Response_UpdatePhone)
        {
            this.ServiceHeader = ServiceHeader;
            this.Response_UpdatePhone = Response_UpdatePhone;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.1-preview-30422-0661")]
    public interface UpdatePhoneWSBeanChannel : PhoneUpdate.UpdatePhoneWSBean, System.ServiceModel.IClientChannel
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.1-preview-30422-0661")]
    public partial class UpdatePhoneWSBeanClient : System.ServiceModel.ClientBase<PhoneUpdate.UpdatePhoneWSBean>, PhoneUpdate.UpdatePhoneWSBean
    {
        
        /// <summary>
        /// Implement this partial method to configure the service endpoint.
        /// </summary>
        /// <param name="serviceEndpoint">The endpoint to configure</param>
        /// <param name="clientCredentials">The client credentials</param>
        static partial void ConfigureEndpoint(System.ServiceModel.Description.ServiceEndpoint serviceEndpoint, System.ServiceModel.Description.ClientCredentials clientCredentials);
        
        public UpdatePhoneWSBeanClient() : 
                base(UpdatePhoneWSBeanClient.GetDefaultBinding(), UpdatePhoneWSBeanClient.GetDefaultEndpointAddress())
        {
            this.Endpoint.Name = EndpointConfiguration.UpdatePhoneWSBeanPort.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public UpdatePhoneWSBeanClient(EndpointConfiguration endpointConfiguration) : 
                base(UpdatePhoneWSBeanClient.GetBindingForEndpoint(endpointConfiguration), UpdatePhoneWSBeanClient.GetEndpointAddress(endpointConfiguration))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public UpdatePhoneWSBeanClient(EndpointConfiguration endpointConfiguration, string remoteAddress) : 
                base(UpdatePhoneWSBeanClient.GetBindingForEndpoint(endpointConfiguration), new System.ServiceModel.EndpointAddress(remoteAddress))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public UpdatePhoneWSBeanClient(EndpointConfiguration endpointConfiguration, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(UpdatePhoneWSBeanClient.GetBindingForEndpoint(endpointConfiguration), remoteAddress)
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public UpdatePhoneWSBeanClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress)
        {
        }
        
        public System.Threading.Tasks.Task<PhoneUpdate.UpdatePhoneResponse> UpdatePhoneAsync(PhoneUpdate.UpdatePhoneRequest request)
        {
            return base.Channel.UpdatePhoneAsync(request);
        }
        
        public virtual System.Threading.Tasks.Task OpenAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginOpen(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndOpen));
        }
        
        public virtual System.Threading.Tasks.Task CloseAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginClose(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndClose));
        }
        
        private static System.ServiceModel.Channels.Binding GetBindingForEndpoint(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.UpdatePhoneWSBeanPort))
            {
                System.ServiceModel.BasicHttpBinding result = new System.ServiceModel.BasicHttpBinding();
                result.MaxBufferSize = int.MaxValue;
                result.ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max;
                result.MaxReceivedMessageSize = int.MaxValue;
                result.AllowCookies = true;
                return result;
            }
            throw new System.InvalidOperationException(string.Format("Could not find endpoint with name \'{0}\'.", endpointConfiguration));
        }
        
        private static System.ServiceModel.EndpointAddress GetEndpointAddress(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.UpdatePhoneWSBeanPort))
            {
                return new System.ServiceModel.EndpointAddress("http://10.13.139.63:9151/ITMWS/services/UpdatePhoneWSBeanPort");
            }
            throw new System.InvalidOperationException(string.Format("Could not find endpoint with name \'{0}\'.", endpointConfiguration));
        }
        
        private static System.ServiceModel.Channels.Binding GetDefaultBinding()
        {
            return UpdatePhoneWSBeanClient.GetBindingForEndpoint(EndpointConfiguration.UpdatePhoneWSBeanPort);
        }
        
        private static System.ServiceModel.EndpointAddress GetDefaultEndpointAddress()
        {
            return UpdatePhoneWSBeanClient.GetEndpointAddress(EndpointConfiguration.UpdatePhoneWSBeanPort);
        }
        
        public enum EndpointConfiguration
        {
            
            UpdatePhoneWSBeanPort,
        }
    }
}
