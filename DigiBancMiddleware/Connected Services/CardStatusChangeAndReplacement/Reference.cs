﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CardStatusChangeAndReplacement
{
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.1-preview-30422-0661")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://cms.ws.eeft.com", ConfigurationName="CardStatusChangeAndReplacement.StatusChangeWithReplacementWSBean")]
    public interface StatusChangeWithReplacementWSBean
    {
        
        // CODEGEN: Generating message contract since the operation has multiple return values.
        [System.ServiceModel.OperationContractAttribute(Action="StatusChangeWithReplacement", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Threading.Tasks.Task<CardStatusChangeAndReplacement.StatusChangeWithReplacementResponse> StatusChangeWithReplacementAsync(CardStatusChangeAndReplacement.StatusChangeWithReplacementRequest request);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.1-preview-30422-0661")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://core.ws.eeft.com")]
    public partial class ServiceHeader
    {
        
        private string serviceIDField;
        
        private string channelIDField;
        
        private string countryCodeField;
        
        private string txnSeqNumField;
        
        private string sourceDateField;
        
        private string sourceTimeField;
        
        private string userIDField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string ServiceID
        {
            get
            {
                return this.serviceIDField;
            }
            set
            {
                this.serviceIDField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public string ChannelID
        {
            get
            {
                return this.channelIDField;
            }
            set
            {
                this.channelIDField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=2)]
        public string CountryCode
        {
            get
            {
                return this.countryCodeField;
            }
            set
            {
                this.countryCodeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=3)]
        public string TxnSeqNum
        {
            get
            {
                return this.txnSeqNumField;
            }
            set
            {
                this.txnSeqNumField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=4)]
        public string SourceDate
        {
            get
            {
                return this.sourceDateField;
            }
            set
            {
                this.sourceDateField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=5)]
        public string SourceTime
        {
            get
            {
                return this.sourceTimeField;
            }
            set
            {
                this.sourceTimeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=6)]
        public string UserID
        {
            get
            {
                return this.userIDField;
            }
            set
            {
                this.userIDField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.1-preview-30422-0661")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://cms.ws.eeft.com")]
    public partial class Request_StatusChangeWithReplacement
    {
        
        private string tokenCardNumberField;
        
        private string cardSeqNumField;
        
        private string cardActFlagField;
        
        private string embossNameField;
        
        private string reasonCodeField;
        
        private string address1Field;
        
        private string address2Field;
        
        private string cityField;
        
        private string stateField;
        
        private string zipCodeField;
        
        private string aWBRefNumField;
        
        private string perHubLocationField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string TokenCardNumber
        {
            get
            {
                return this.tokenCardNumberField;
            }
            set
            {
                this.tokenCardNumberField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public string CardSeqNum
        {
            get
            {
                return this.cardSeqNumField;
            }
            set
            {
                this.cardSeqNumField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=2)]
        public string CardActFlag
        {
            get
            {
                return this.cardActFlagField;
            }
            set
            {
                this.cardActFlagField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=3)]
        public string EmbossName
        {
            get
            {
                return this.embossNameField;
            }
            set
            {
                this.embossNameField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=4)]
        public string ReasonCode
        {
            get
            {
                return this.reasonCodeField;
            }
            set
            {
                this.reasonCodeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=5)]
        public string Address1
        {
            get
            {
                return this.address1Field;
            }
            set
            {
                this.address1Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=6)]
        public string Address2
        {
            get
            {
                return this.address2Field;
            }
            set
            {
                this.address2Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=7)]
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=8)]
        public string State
        {
            get
            {
                return this.stateField;
            }
            set
            {
                this.stateField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=9)]
        public string ZipCode
        {
            get
            {
                return this.zipCodeField;
            }
            set
            {
                this.zipCodeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=10)]
        public string AWBRefNum
        {
            get
            {
                return this.aWBRefNumField;
            }
            set
            {
                this.aWBRefNumField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=11)]
        public string PerHubLocation
        {
            get
            {
                return this.perHubLocationField;
            }
            set
            {
                this.perHubLocationField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.1-preview-30422-0661")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://cms.ws.eeft.com")]
    public partial class Response_StatusChangeWithReplacement
    {
        
        private string tokenCardNumberField;
        
        private string cardSeqNumField;
        
        private string newTokenNumberField;
        
        private string newCardSeqNumField;
        
        private string responseCodeField;
        
        private string responseTextField;

        private string cardExpiryField;


        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string TokenCardNumber
        {
            get
            {
                return this.tokenCardNumberField;
            }
            set
            {
                this.tokenCardNumberField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public string CardSeqNum
        {
            get
            {
                return this.cardSeqNumField;
            }
            set
            {
                this.cardSeqNumField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=2)]
        public string NewTokenNumber
        {
            get
            {
                return this.newTokenNumberField;
            }
            set
            {
                this.newTokenNumberField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=3)]
        public string NewCardSeqNum
        {
            get
            {
                return this.newCardSeqNumField;
            }
            set
            {
                this.newCardSeqNumField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=4)]
        public string ResponseCode
        {
            get
            {
                return this.responseCodeField;
            }
            set
            {
                this.responseCodeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=5)]
        public string ResponseText
        {
            get
            {
                return this.responseTextField;
            }
            set
            {
                this.responseTextField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=6)]
        public string CardExpiry
        {
            get
            {
                return this.cardExpiryField;
            }
            set
            {
                this.cardExpiryField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.1-preview-30422-0661")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="StatusChangeWithReplacementRequest", WrapperNamespace="http://cms.ws.eeft.com", IsWrapped=true)]
    public partial class StatusChangeWithReplacementRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://core.ws.eeft.com", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute(Namespace="http://core.ws.eeft.com")]
        public CardStatusChangeAndReplacement.ServiceHeader ServiceHeader;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://cms.ws.eeft.com", Order=1)]
        public CardStatusChangeAndReplacement.Request_StatusChangeWithReplacement Request_StatusChangeWithReplacement;
        
        public StatusChangeWithReplacementRequest()
        {
        }
        
        public StatusChangeWithReplacementRequest(CardStatusChangeAndReplacement.ServiceHeader ServiceHeader, CardStatusChangeAndReplacement.Request_StatusChangeWithReplacement Request_StatusChangeWithReplacement)
        {
            this.ServiceHeader = ServiceHeader;
            this.Request_StatusChangeWithReplacement = Request_StatusChangeWithReplacement;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.1-preview-30422-0661")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="StatusChangeWithReplacementResponse", WrapperNamespace="http://cms.ws.eeft.com", IsWrapped=true)]
    public partial class StatusChangeWithReplacementResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://core.ws.eeft.com", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute(Namespace="http://core.ws.eeft.com")]
        public CardStatusChangeAndReplacement.ServiceHeader ServiceHeader;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://cms.ws.eeft.com", Order=1)]
        public CardStatusChangeAndReplacement.Response_StatusChangeWithReplacement Response_StatusChangeWithReplacement;
        
        public StatusChangeWithReplacementResponse()
        {
        }
        
        public StatusChangeWithReplacementResponse(CardStatusChangeAndReplacement.ServiceHeader ServiceHeader, CardStatusChangeAndReplacement.Response_StatusChangeWithReplacement Response_StatusChangeWithReplacement)
        {
            this.ServiceHeader = ServiceHeader;
            this.Response_StatusChangeWithReplacement = Response_StatusChangeWithReplacement;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.1-preview-30422-0661")]
    public interface StatusChangeWithReplacementWSBeanChannel : CardStatusChangeAndReplacement.StatusChangeWithReplacementWSBean, System.ServiceModel.IClientChannel
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.1-preview-30422-0661")]
    public partial class StatusChangeWithReplacementWSBeanClient : System.ServiceModel.ClientBase<CardStatusChangeAndReplacement.StatusChangeWithReplacementWSBean>, CardStatusChangeAndReplacement.StatusChangeWithReplacementWSBean
    {
        
        /// <summary>
        /// Implement this partial method to configure the service endpoint.
        /// </summary>
        /// <param name="serviceEndpoint">The endpoint to configure</param>
        /// <param name="clientCredentials">The client credentials</param>
        static partial void ConfigureEndpoint(System.ServiceModel.Description.ServiceEndpoint serviceEndpoint, System.ServiceModel.Description.ClientCredentials clientCredentials);
        
        public StatusChangeWithReplacementWSBeanClient() : 
                base(StatusChangeWithReplacementWSBeanClient.GetDefaultBinding(), StatusChangeWithReplacementWSBeanClient.GetDefaultEndpointAddress())
        {
            this.Endpoint.Name = EndpointConfiguration.StatusChangeWithReplacementWSBeanPort.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public StatusChangeWithReplacementWSBeanClient(EndpointConfiguration endpointConfiguration) : 
                base(StatusChangeWithReplacementWSBeanClient.GetBindingForEndpoint(endpointConfiguration), StatusChangeWithReplacementWSBeanClient.GetEndpointAddress(endpointConfiguration))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public StatusChangeWithReplacementWSBeanClient(EndpointConfiguration endpointConfiguration, string remoteAddress) : 
                base(StatusChangeWithReplacementWSBeanClient.GetBindingForEndpoint(endpointConfiguration), new System.ServiceModel.EndpointAddress(remoteAddress))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public StatusChangeWithReplacementWSBeanClient(EndpointConfiguration endpointConfiguration, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(StatusChangeWithReplacementWSBeanClient.GetBindingForEndpoint(endpointConfiguration), remoteAddress)
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public StatusChangeWithReplacementWSBeanClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress)
        {
        }
        
        public System.Threading.Tasks.Task<CardStatusChangeAndReplacement.StatusChangeWithReplacementResponse> StatusChangeWithReplacementAsync(CardStatusChangeAndReplacement.StatusChangeWithReplacementRequest request)
        {
            return base.Channel.StatusChangeWithReplacementAsync(request);
        }
        
        public virtual System.Threading.Tasks.Task OpenAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginOpen(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndOpen));
        }
        
        public virtual System.Threading.Tasks.Task CloseAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginClose(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndClose));
        }
        
        private static System.ServiceModel.Channels.Binding GetBindingForEndpoint(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.StatusChangeWithReplacementWSBeanPort))
            {
                System.ServiceModel.BasicHttpBinding result = new System.ServiceModel.BasicHttpBinding();
                result.MaxBufferSize = int.MaxValue;
                result.ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max;
                result.MaxReceivedMessageSize = int.MaxValue;
                result.AllowCookies = true;
                return result;
            }
            throw new System.InvalidOperationException(string.Format("Could not find endpoint with name \'{0}\'.", endpointConfiguration));
        }
        
        private static System.ServiceModel.EndpointAddress GetEndpointAddress(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.StatusChangeWithReplacementWSBeanPort))
            {
                return new System.ServiceModel.EndpointAddress("http://10.13.139.89:9080/ITMWS/services/StatusChangeWithReplacementWSBeanPort");
            }
            throw new System.InvalidOperationException(string.Format("Could not find endpoint with name \'{0}\'.", endpointConfiguration));
        }
        
        private static System.ServiceModel.Channels.Binding GetDefaultBinding()
        {
            return StatusChangeWithReplacementWSBeanClient.GetBindingForEndpoint(EndpointConfiguration.StatusChangeWithReplacementWSBeanPort);
        }
        
        private static System.ServiceModel.EndpointAddress GetDefaultEndpointAddress()
        {
            return StatusChangeWithReplacementWSBeanClient.GetEndpointAddress(EndpointConfiguration.StatusChangeWithReplacementWSBeanPort);
        }
        
        public enum EndpointConfiguration
        {
            
            StatusChangeWithReplacementWSBeanPort,
        }
    }
}
