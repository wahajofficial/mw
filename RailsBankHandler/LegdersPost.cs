﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RailsBankHandler.LegdersPost
{
    //public class RequestLegdersPost
    //{
    //    public RailsBankHandler.LegdersPost.Root RootObject { get; set; }
    //    //public string clientID { get; set; }
    //}

    public class ResponseObject
    {
        public string ledger_id { get; set; }
        public string error { get; set; }

    }
    public class LedgerMeta
    {
        public string foo { get; set; }
    }

    public class RequestLegdersPost
    {
        public List<string> ledger_primary_use_types { get; set; }
       // public string credit_details_id { get; set; }
        public string ledger_who_owns_assets { get; set; }
        public string holder_id { get; set; }
        public string ledger_t_and_cs_country_of_jurisdiction { get; set; }
        public string partner_product { get; set; }
        public string asset_type { get; set; }
        public string asset_class { get; set; }
        public string ledger_type { get; set; }
        public LedgerMeta ledger_meta { get; set; }
    }


    public class ErrorResponse
    {
        public string error { get; set; }
    }

}
