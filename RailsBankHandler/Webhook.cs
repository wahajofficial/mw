﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RailsBankHandler.Webhook
{
    public class WebhookRequest
    {
        public string webhook_url { get; set; }
        public string webhook_secret { get; set; }
    }

    public class WebhookResponse
    {
    }

    public class WebhookErrorResponse
    {
    }
}
