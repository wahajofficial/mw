﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RailsBankHandler.GetLedgersClose
{
    public class RequestGetLedgersClose
    {
        public string ledger_id { get; set; }
    }

    public class ResponseObject
    {
        public string ledger_id { get; set; }
        public string error { get; set; }
    }

    public class ErrorResponse
    {
        public string error { get; set; }
        public string ledger_id { get; set; }
    }

}
