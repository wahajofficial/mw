﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RailsBankHandler.GetPIN
{
    public class RequestGetPIN
    {
      
        public string card_id { get; set; }
    }

    public class ResponseObject
    {
        public string pin { get; set; }
        public string error { get; set; }

    }

    public class ErrorResponse
    {
        public string error { get; set; }
        public string detail { get; set; }
        public string field { get; set; }
        public string type { get; set; }
    }

}
