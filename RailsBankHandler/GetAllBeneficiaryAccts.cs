﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RailsBankHandler.GetAllBeneficiaryAccts
{
    public class RequestGetAllBeneficiaryAccts
    {
      
        public string beneficiary_id { get; set; }
   
    }
    public class ResponseObject
    {
        public string account_id { get; set; }
        public string error { get; set; }
    }

    public class ErrorResponse
    {
        public string error { get; set; }
       
    }

}
