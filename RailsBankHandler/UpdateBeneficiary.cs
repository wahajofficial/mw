﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RailsBankHandler.UpdateBeneficiary
{
    //public class RequestUpdateBeneficiary
    //{
      
    //    public RailsBankHandler.UpdateBeneficiary.Root RootObject { get; set; }
    //    public string beneficiary_id { get; set; }
    //}

    public class ResponseObj
    {
        public string beneficiary_id { get; set; }
        public string error { get; set; }
    }

    public class Income
    {
        public string frequency { get; set; }
        public string currency { get; set; }
        public string amount { get; set; }
    }

    public class Address
    {
        public string address_iso_country { get; set; }
    }

    public class Person
    {
        public List<string> country_of_residence { get; set; }
        public string document_number { get; set; }
        public string document_type { get; set; }
        public Income income { get; set; }
        public string pep_type { get; set; }
        public string pep_notes { get; set; }
        public List<object> address_history { get; set; }
        public bool pep { get; set; }
        public string date_onboarded { get; set; }
        public string email { get; set; }
        public string tin { get; set; }
        public string name { get; set; }
        public Address address { get; set; }
        public string social_security_number { get; set; }
        public string telephone { get; set; }
        public string date_of_birth { get; set; }
        public string document_expiration_date { get; set; }
        public string tin_type { get; set; }
        public List<string> citizenship { get; set; }
        public List<string> nationality { get; set; }
        public string document_issuer { get; set; }
        public string country_of_birth { get; set; }
    }

    public class DefaultAccount
    {
        public string iban { get; set; }
        public string account_number { get; set; }
        public string bank_country { get; set; }
        public string bank_code { get; set; }
        public string bank_name { get; set; }
        public string account_type { get; set; }
        public string bic_swift { get; set; }
        public string asset_type { get; set; }
        public string asset_class { get; set; }
        public string bank_code_type { get; set; }
    }

    public class RequestUpdateBeneficiary
    {
        public string holder_id { get; set; }
        public Person person { get; set; }
        public string asset_type { get; set; }
        public string uk_account_number { get; set; }
        public string asset_class { get; set; }
        public DefaultAccount default_account { get; set; }
        public string beneficiary_id { get; set; }
    }

    public class ErrorResponse
    {
        public string error { get; set; }
        public string error_detail { get; set; }
        public List<string> path { get; set; }
        public string type { get; set; }
    }
}
