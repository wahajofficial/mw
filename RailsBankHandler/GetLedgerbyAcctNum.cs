﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RailsBankHandler.GetLedgerbyAcctNum
{
    //public class RequestLedgerbyAcctNum
    //{
    //    public RailsBankHandler.GetLedgerbyAcctNum.Root RootObject { get; set; }
    //}

    public class RequestLedgerbyAcctNum
    {
        public string holder_id { get; set; }
        public string partner_product { get; set; }
        public string account_number { get; set; }
        public string items_per_page { get; set; }
        public string offset { get; set; }
        public string from_date { get; set; }
        public string to_date { get; set; }
        public string order { get; set; }
    }

    public class Person
    {
        public string name { get; set; }
    }


    public class LedgerHolder
    {
        public string enduser_id { get; set; }
        public Person Person { get; set; }
    }

    public class Company
    {
        public string name { get; set; }
    }

    public class Partner
    {
        public string partner_ref { get; set; }
        public Company company { get; set; }
        public string partner_id { get; set; }
    }

    public class LedgerMeta
    {
        public string foo { get; set; }
    }

    public class ResponseObject
    {
        public DateTime last_modified_at { get; set; }
        public List<string> ledger_primary_use_types { get; set; }
        public string ledger_id { get; set; }
        public LedgerHolder ledger_holder { get; set; }
        public string ledger_who_owns_assets { get; set; }
        public string partner_ref { get; set; }
        public string holder_id { get; set; }
        public string partner_id { get; set; }
        public string ledger_t_and_cs_country_of_jurisdiction { get; set; }
        public string ledger_status { get; set; }
        public int amount { get; set; }
        public DateTime created_at { get; set; }
        public string partner_product { get; set; }
        public Partner partner { get; set; }
        public string asset_type { get; set; }
        public string asset_class { get; set; }
        public string ledger_type { get; set; }
        public LedgerMeta ledger_meta { get; set; }
        public string error{ get; set; }
    }

    public class ErrorResponse
    {
        public string error { get; set; }
        public string detail { get; set; }
        public string field { get; set; }
        public string type { get; set; }
    }

  
}
