﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RailsBankHandler.GetLedgerEntries
{
    public class RequestGetLedgerEntries
    {
        public string ledger_id { get; set; }
        public int items_per_page { get; set; }
        public int offset { get; set; }
        public string from_date { get; set; }
        public string to_date { get; set; }
        public string order { get; set; }
        public string ledger_entry_event_type { get; set; }
    }
    public class ResponseGetLedgerEntries
    {
        public string ledger_entry_event_type { get; set; }
        public DateTime created_at { get; set; }
        public string ledger_entry_id { get; set; }
        public string transaction_id { get; set; }
        public string amount { get; set; }
        public string ledger_entry_type { get; set; }
    }

    public class ErrorResponseGetLedgerEntries
    {
      public string error { get; set; }
    }
}
