﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RailsBankHandler.Process.GetLedgersResponse
{
        public class Person
    {
        public string name { get; set; }
    }

    public class LedgerHolder
    {
        public string enduser_id { get; set; }
        public Person person { get; set; }
    }

    public class Company
    {
        public string name { get; set; }
    }

    public class Partner
    {
        public string partner_id { get; set; }
        public Company company { get; set; }
        public string partner_ref { get; set; }
    }

    public class LedgerMeta
    {
        public string foo { get; set; }
    }

    public class GetLedgResponse
    {
        public DateTime last_modified_at { get; set; }
        public List<string> ledger_primary_use_types { get; set; }
        public string ledger_id { get; set; }
        public LedgerHolder ledger_holder { get; set; }
        public string ledger_who_owns_assets { get; set; }
        public string partner_ref { get; set; }
        public int reserved_amount { get; set; }
        public string holder_id { get; set; }
        public string partner_id { get; set; }
        public string ledger_t_and_cs_country_of_jurisdiction { get; set; }
        public string ledger_status { get; set; }
        public int amount { get; set; }
        public DateTime created_at { get; set; }
        public string partner_product { get; set; }
        public Partner partner { get; set; }
        public string asset_type { get; set; }
        public string asset_class { get; set; }
        public int total_amount { get; set; }
        public string ledger_type { get; set; }
        public LedgerMeta ledger_meta { get; set; }
    }

}
