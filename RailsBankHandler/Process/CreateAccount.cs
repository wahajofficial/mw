﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RailsBankHandler.Process.CreateAccount
{
    public class Ledger
    {
        public string ledger_id { get; set; }
    }

    public class EnduserMeta
    {
        public string foo { get; set; }
    }

    public class Income
    {
        public int amount { get; set; }
        public string frequency { get; set; }
        public string currency { get; set; }
    }

    public class AddressHistory
    {
        public string address_iso_country { get; set; }
        public string address_start_date { get; set; }
        public string address_end_date { get; set; }
    }

    public class Address
    {
        public string address_refinement { get; set; }
        public string address_number { get; set; }
        public string address_street { get; set; }
        public string address_city { get; set; }
        public string address_postal_code { get; set; }
        public string address_region { get; set; }
        public string address_iso_country { get; set; }
    }

    public class Person
    {
        public List<string> country_of_residence { get; set; }
        public string document_number { get; set; }
        public string document_type { get; set; }
        public Income income { get; set; }
        public string pep_type { get; set; }
        public string pep_notes { get; set; }
        public List<AddressHistory> address_history { get; set; }
        public bool pep { get; set; }
        public string date_onboarded { get; set; }
        public string email { get; set; }
        public string tin { get; set; }
        public string document_issue_date { get; set; }
        public string name { get; set; }
        public Address address { get; set; }
        public string social_security_number { get; set; }
        public string telephone { get; set; }
        public string date_of_birth { get; set; }
        public string document_expiration_date { get; set; }
        public string tin_type { get; set; }
        public List<string> citizenship { get; set; }
        public List<string> nationality { get; set; }
        public string document_issuer { get; set; }
        public string country_of_birth { get; set; }
    }

    public class CreateAccountResponse
    {
        public DateTime last_modified_at { get; set; }
        public List<Ledger> ledgers { get; set; }
        public List<object> beneficiaries { get; set; }
        public EnduserMeta enduser_meta { get; set; }
        public bool screening_monitored_search { get; set; }
        public string enduser_id { get; set; }
        public Person person { get; set; }
        public DateTime created_at { get; set; }
        public string enduser_status { get; set; }
    }



















































    //RESPONSE
    //Response (in case Ledger already is created):
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    //public class Person
    //{
    //    public string name { get; set; }
    //}

    //public class LedgerHolder
    //{
    //    public string enduser_id { get; set; }
    //    public Person person { get; set; }
    //}

    //public class Company
    //{
    //    public string name { get; set; }
    //}

    //public class Partner
    //{
    //    public string partner_id { get; set; }
    //    public Company company { get; set; }
    //    public string partner_ref { get; set; }
    //}

    //public class CreateAccountResponse
    //{
    //    public DateTime last_modified_at { get; set; }
    //    public List<string> ledger_primary_use_types { get; set; }
    //    public string ledger_id { get; set; }
    //    public LedgerHolder ledger_holder { get; set; }
    //    public string ledger_who_owns_assets { get; set; }
    //    public string partner_ref { get; set; }
    //    public int reserved_amount { get; set; }
    //    public string holder_id { get; set; }
    //    public string partner_id { get; set; }
    //    public string ledger_t_and_cs_country_of_jurisdiction { get; set; }
    //    public string ledger_status { get; set; }
    //    public int amount { get; set; }
    //    public DateTime created_at { get; set; }
    //    public string partner_product { get; set; }
    //    public Partner partner { get; set; }
    //    public string asset_type { get; set; }
    //    public string asset_class { get; set; }
    //    public int total_amount { get; set; }
    //    public string ledger_type { get; set; }
    //    public LedgerMeta ledger_meta { get; set; }
    //}


    //Response (in case Ledger already is created):


}
