﻿using Microsoft.IdentityModel.Protocols;
using System;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace EncryptionUtility
{
    class Program
    {
        static void Main(string[] args)
        {
            var key = ConfigurationManager.AppSettings["SymmetricKey"];
            Console.WriteLine("Please enter a string for encryption");
            var str = Console.ReadLine();
            Console.WriteLine("Enter 0 for Encrypt and 1 for Decrypt:");
            string input = Console.ReadLine();
            var encryptedString = DBHandler.Common.CommonHelper.EncryptString(key, str);
            var decryptedString = "";
            //= DBHandler.Common.CommonHelper.DecryptString(key,str);
            if (input == "0")
            {
                decryptedString = DBHandler.Common.CommonHelper.DecryptString(key, encryptedString);
                Console.WriteLine($"encrypted string = {encryptedString}");
            }
            else if (input == "1")
            {
                decryptedString = DBHandler.Common.CommonHelper.DecryptString(key, str);
                Console.WriteLine($"decrypted string = {decryptedString}");
            }
            else
            {
                Console.WriteLine("Invalid Value...");
            }
            Console.ReadKey();
        }
    }
}
