﻿using System;
using System.Collections.Generic;

namespace TrunarrativeHandler.AccountScreening
{
    public class AccountScreeningRequest
    {
        public int accountStrategyId { get; set; }
        public string accountReference { get; set; }
        public Application Application { get; set; }
        public List<Person> person { get; set; }
    }

    public class Application
    {
        public string applicationDateTime { get; set; }
        public string ipAddress { get; set; }
    }

    public class ResidentialAddress
    {
        public string addressLine1 { get; set; }
        public string addressLine2 { get; set; }
        public string addressLine3 { get; set; }
        public string zipPostcode { get; set; }
        public string country { get; set; }
    }

    public class Person
    {
        public string role { get; set; }
        public bool isprimary { get; set; }
        public string clientReference { get; set; }
        public string dateOfBirth { get; set; }
        public string emailAddress { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }
        public string gender { get; set; }
        public string mobilePhoneNumber { get; set; }
        public List<ResidentialAddress> residentialAddress { get; set; }
    }

    public class AccountScreeningResponse
    {

    }
    public class AccountScreeningErrorResponse
    {

    }

}
