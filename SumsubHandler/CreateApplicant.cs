﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SumsubHandler.CreateApplicant
{
    public class RequestCreateApplicant
    {
        public string externalUserId { get; set; }
        public string sourceKey { get; set; }
        public string email { get; set; }
        public string lang { get; set; }
        public List<Metadata> metadata { get; set; }
        public Info info { get; set; }
        public fixedInfo fixedInfo { get; set; }
    }

    public class fixedInfo
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string middleName { get; set; }
        public string firstNameEn { get; set; }
        public string lastNameEn { get; set; }
        public string middleNameEn { get; set; }
        public string legalName { get; set; }
        public string gender { get; set; }
        public string dob { get; set; }
        public string placeOfBirth { get; set; }
        public string countryOfBirth { get; set; }
        public string stateOfBirth { get; set; }
        public string country { get; set; }
        public string nationality { get; set; }
        public string phone { get; set; }
        public List<Addresses> addresses{ get; set; }
    }

    public class ErrorResponse
    {
        public string description { get; set; }
        public int code { get; set; }
        public string correlationId { get; set; }
    }

    public class Metadata
    {
        public string key { get; set; }
        public string value { get; set; }
    }
    public class Addresses
    {
        public string country { get; set; }
        public string postCode { get; set; }
        public string town { get; set; }
        public string street { get; set; }
        public string subStreet { get; set; }
        public string state { get; set; }
        public string buildingName { get; set; }
        public string flatNumber { get; set; }
        public string buildingNumber { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
    }

    public class Info
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string middleName { get; set; }
        public string firstNameEn { get; set; }
        public string lastNameEn { get; set; }
        public string middleNameEn { get; set; }
        public string legalName { get; set; }
        public string gender { get; set; }
        public string dob { get; set; }
        public string placeOfBirth { get; set; }
        public string countryOfBirth { get; set; }
        public string stateOfBirth { get; set; }
        public string country { get; set; }
        public string nationality { get; set; }
        public string phone { get; set; }
        public List<Addresses> addresses{ get; set; }
    }

    public class DocSet
    {
        public string idDocSetType { get; set; }
        public List<string> types { get; set; }
        public List<string> subTypes { get; set; }
        public string videoRequired { get; set; }
    }

    public class RequiredIdDocs
    {
        public List<DocSet> docSets { get; set; }
    }

    public class Review
    {
        public string reviewId { get; set; }
        public bool reprocessing { get; set; }
        public string levelName { get; set; }
        public string createDate { get; set; }
        public string reviewStatus { get; set; }
        public int notificationFailureCnt { get; set; }
        public int priority { get; set; }
        public bool autoChecked { get; set; }
    }

    public class ResponseCreateApplicant
    {
        public string id { get; set; }
        public string createdAt { get; set; }
        public string key { get; set; }
        public string clientId { get; set; }
        public string inspectionId { get; set; }
        public string externalUserId { get; set; }
        public Info info { get; set; }
        public RequiredIdDocs requiredIdDocs { get; set; }
        public Review review { get; set; }
        public fixedInfo fixedInfo { get; set; }
        public string type { get; set; }
        public string email { get; set; }

    }

}
