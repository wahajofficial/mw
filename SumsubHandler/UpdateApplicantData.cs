﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SumsubHandler.UpdateApplicantData
{
    public class UpdateApplicantDataRequest
    {
        public string applicantId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string middleName { get; set; }
        public string dob { get; set; }
    }
    public class UpdateApplicantDataResponse
    {
    }
    public class UpdateApplicantDataErrorResponse
    {
    }
}
