﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SumsubHandler.AddIdDocument
{
    public class RequestAddIdDocument
    {
        public string applicantId { get; set; }
        public Metadata metadata{get;set;}
        public string documentData { get; set; }
        public string documentName { get; set; }
    }

    public class Metadata
    {
        public string idDocType { get; set; }
        public string country { get; set; }
    }

    public class ResponseAddIdDocument
    {
        public string idDocType { get; set; }
        public string country { get; set; }
    }

    public class ErrorResponseAddIdDocument
    {
        public string description { get; set; }
        public int code { get; set; }
        public string correlationId { get; set; }
    }
}
