﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ERCCorporateHandler
{
    public class ValidateTOTPModel
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public ValidateTOTP ValidateTOTP { get; set; }
    }

    public class ValidateTOTP
    {
        public string UserId { get; set; }
        public string Token { get; set; }
    }

    public class Response
    {
        public string Message { get; set; }
    }
}
