﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ERCCorporateHandler.UpdatedrawdownStatus
{
    public class UpdatedrawdownStatusModel
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public RootObject RootObject { get; set; }
    }

    public class RootObject
    {
        public string CaseId { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
    }

    public class ResponseRootObject
    {
        public bool Status { get; set; }
        public string Message { get; set; }
    }
}
