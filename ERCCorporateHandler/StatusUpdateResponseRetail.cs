﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ERCCorporateHandler.StatusUpdateResponseRetail
{
    public class Response
    {
        public int code { get; set; }
        public string message { get; set; }
        public CommonModel.ResponseObject content { get; set; }
        public object exceptionMessage { get; set; }
        public object id { get; set; }
    }

    public class RootObject
    {
        public Response response { get; set; }
        public string Message { get; set; }
        public string Code { get; set; }
    }
    
}
