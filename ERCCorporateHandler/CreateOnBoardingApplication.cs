﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ERCCorporateHandler.CreateOnBoardingApplication
{
    public  class CreateOnBoardingApplicationModel
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public RootObject RootObject { get; set; }
    }

    public class RootObject
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public string RMEmail { get; set; }
    }

    public class ResponseRootObject
    {
        public bool Status { get; set; }
        public string Message { get; set; }
    }
}
