﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Text;

namespace ERCCorporateHandler.InValidateSSOSession
{
    class InValidateSSOSession
    {
    }

    public class InValidateSSOSessionModel
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }
        public RootObject RootObject { get; set; }

    }

    public class RootObject
    {
        public string Reference_No { get; set; }
    }

    public class ResponseRootObject
    {
        public string Message { get; set; }
    }
}
