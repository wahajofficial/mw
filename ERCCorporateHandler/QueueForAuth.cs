﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ERCCorporateHandler.QueueForAuth
{

    public class QueueForAuthRequest
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public RootObjectRequest RootObjectRequest { get; set; }
    }

    public class RootObjectRequest
    {
        public string CIF { get; set; }
        public string AccountNumber  { get; set; }
        public string TranType  { get; set; }
        public string Remarks  { get; set; }
        public string CurrencyCode  { get; set; }
        public string coRelationID { get; set; }
        public string paymentChannel { get; set; }
    }

    public class RootObject
    {
        public string Message { get; set; }
    }
}
