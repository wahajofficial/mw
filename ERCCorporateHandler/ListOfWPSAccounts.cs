﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ERCCorporateHandler.ListOfWPSAccounts
{
    public class ListOfWPSAccountsModelRequest
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public RootObjectRequest RootObjectRequest { get; set; }
    }

    public class RootObjectRequest
    {
        public string CIF { get; set; }
    }

    public class RootObject
    {
        public int UserId { get; set; }
        public string CIF { get; set; }
        public string AccountNumber { get; set; }
        public string IBAN { get; set; }
    }

    public class RootObject2
    {
        public string Message { get; set; }
    }
}
