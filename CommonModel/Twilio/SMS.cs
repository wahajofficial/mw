﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonModel.Twilio
{
    public class SMS
    {
        public string To { get; set; }
        public string Message { get; set; }
    }

    public class ResponseObject
    {
        public string error { get; set; }
    }

    public class ErrorResponse
    {
        public string error { get; set; }
    }
}
