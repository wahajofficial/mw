﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace CommonModel
{
    public class StringRangeAttribute : ValidationAttribute
    {
        public string[] AllowableValues { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (AllowableValues.Contains(value?.ToString()) == true || value == null || value == "")
            {
                return ValidationResult.Success;
            }

            var msg = $"Please enter one of the allowed values: {string.Join(", ", (AllowableValues ?? new string[] { "No allowed values found" }))}.";
            return new ValidationResult(msg);
        }
    }

    public class FutureDateAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)// Return a boolean value: true == IsValid, false != IsValid
        {
            if (value != null)
            {
                DateTime d = Convert.ToDateTime(value);
                return d >= DateTime.Now; //Dates Greater than or equal to today are valid (true)
            }
            else
            {
                return true;
            }
        }
    }
    public class TradeExpiryDateAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)// Return a boolean value: true == IsValid, false != IsValid
        {

            List<DateTime> last30Days = Enumerable.Range(0, 30).Select(i => DateTime.Now.Date.AddDays(-i)).ToList();

            if (value != null)
            {
                DateTime d = Convert.ToDateTime(value);
                if (d < DateTime.Now)
                {
                    if (last30Days.Contains(d))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }
    }


    public class BirthdateDateAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)// Return a boolean value: true == IsValid, false != IsValid
        {
            if (value != null)
            {
                DateTime d = Convert.ToDateTime(value);
                return d <= DateTime.Now.AddYears(-18);  //Dates Greater than or equal to today are valid (true)
            }
            else
            {
                return true;
            }
        }
    }

    public class PastDateAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)// Return a boolean value: true == IsValid, false != IsValid
        {
            if (value != null)
            {
                DateTime d = Convert.ToDateTime(value);
                return d <= DateTime.Now; //Dates Greater than or equal to today are valid (true)
            }
            else
            {
                return true;
            }
        }
    }

    //[DisplayName("Square meters")]
    //[PosNumberNoZero(ErrorMessage = "need a positive number, bigger than 0")]
    //public string squaremeters { get; set; }
    public class PosNumberNoZeroAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }
            int getal;
            if (int.TryParse(value.ToString(), out getal))
            {

                if (getal == 0)
                    return false;

                if (getal > 0)
                    return true;
            }
            return false;

        }
    }
}
