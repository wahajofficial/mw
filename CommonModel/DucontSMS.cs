﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonModel.DucontSMS
{
    public class DucontSMSModel
    {
        public SignOnRq SignOnRq { get; set; }
        public RootObject RootObject { get; set; }
    }

  

    public class RootObject
    {
        public string userId { get; set; }
        public string password { get; set; }
        public string channelId { get; set; }
        public string senderId { get; set; }
        public string templateId { get; set; }
        public List<CommonModel.SMS.TemplateVariable> templateVariables { get; set; }
        public string languageId { get; set; }
        public string messageId { get; set; }
        public string priority { get; set; }
        public List<string> recipients { get; set; }
        public string statusCallbackURL { get; set; }
        public bool confirmDelivery { get; set; }
        public string validityPeriod { get; set; }
        public string body { get; set; }
        public int? messageCategory { get; set; } = 1;

    }

    public class ResponseRootObject
    {
        public string responseCode { get; set; }
        public string responseMessage { get; set; }
    }
}
