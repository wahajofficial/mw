﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CommonModel
{
    public class VendorProcessStatusModel
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }
        public VendorProcessStatus VendorProcessStatus { get; set; }
    }

    public class VendorProcessStatus
    {
        public string transReference { get; set; }
        public string channelID { get; set; }
        public string statusType { get; set; }
        public string cif { get; set; }
        public string statusCode { get; set; }
        public string statusDesc { get; set; }
        public string provCode { get; set; }
        public string provStatusCode { get; set; }
        public string provStatusDesc { get; set; }
        public string origRequestDoc { get; set; }
        public string origResponseDoc { get; set; }
        public string systemid { get; set; }
    }

    public class ResponseObject
    {
        public string Message { get; set; }
        public string Code { get; set; }
        public string referenceNo { get; set; }
    }
}
