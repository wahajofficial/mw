﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CommonModel.RetailCustomerNoti
{
    public class RetailCustomerNotiModel
    {
        public SignOnRq signOnRq { get; set; }
        public RootObject RootObject { get; set; }
    }


    public class NotiContent
    {
        public string Heading { get; set; }

        public string ShortDescription { get; set; }

        public string Description { get; set; }
    }

    public class RootObject
    {
        public string Tag { get; set; }
        public string Cif { get; set; }
        public string ChannelId { get; set; }
        public NotiContent NotiContent { get; set; }
    }
    public class Content
    {
        public string message { get; set; }
        public string code { get; set; }
    }

    public class Response
    {
        public int code { get; set; }
        public string message { get; set; }
        public string token { get; set; }
        public Content content { get; set; }
        public object exceptionMessage { get; set; }
        public object id { get; set; }
    }

    public class RetailCustomerNotiResponse
    {
        public Response response { get; set; }
    }
}