﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CommonModel.CCMGenerate
{
    public class CCMGenerateModel
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }
        public RootObject RootObject { get; set; }
    }

    public class Command
    {
        public string CommandName { get; set; }
        public string commandValue { get; set; }
    }

    public class DataDefinition
    {
        public string ModuleName { get; set; }
        public object item { get; set; }
        public string itemStr { get; set; }
    }

    public class RootObject
    {
        public List<Command> command { get; set; }
        public List<DataDefinition> dataDefinition { get; set; }
        public string workFlowDefinition { get; set; }
        public int responnseType { get; set; }
    }
}