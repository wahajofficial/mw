﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonModel.GetProspectId
{
    public class GetProspectIdModel
    {
            public SignOnRq signOnRq { get; set; }
            public RootObject RootObject{ get; set; }
    }

    public class RootObject
    {
        public string CIF { get; set; }
    }


    public class Content
    {
        public string message { get; set; }
        public string code { get; set; }
    }

    public class Response
    {
        public int code { get; set; }
        public string message { get; set; }
        public Content content { get; set; }
        public object exceptionMessage { get; set; }
        public object id { get; set; }
    }

    public class ResponseRootObject
    {
        public Response response { get; set; }
    }
}
