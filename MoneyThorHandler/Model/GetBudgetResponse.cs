﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyThorHandler.Model
{
    //public class GetBudgetResponse
    // {
    //     public Header header { get; set; }
    //     public GetBudgetPayload payload { get; set; }
    // }
    // public class GetBudgetPayload
    // {
    //     public List<Budget> budget { get; set; }
    // }

    // public class History
    // {

    // }

    // public class Budget
    // {
    //     public string key { get; set; }
    //     //public string name { get; set; }
    //     //public string currency { get; set; }
    //     //public decimal amount { get; set; }
    //     //public string direction { get; set; }
    //     //public string search_query { get; set; }
    //     //public string frequency { get; set; }
    //     //public string start_date { get; set; }
    //     //public int threshold { get; set; }
    //     //public object[] history { get; set; }
    // }

    public class BudgetResponse
    {
        public Header header { get; set; }
        public Budget payload { get; set; }
    }

    public class Header
    {
        public bool success { get; set; }
        public bool authenticated { get; set; }
        public string code { get; set; }
        public string message { get; set; }
    }
    public class CustomFieldResponse
    {
        public Header header { get; set; }
    }
    public class Budget
    {
        public string key { get; set; }
        public string name { get; set; }
        public string currency { get; set; }
        public double amount { get; set; }
        public string direction { get; set; }
        public string search_query { get; set; }
        public string frequency { get; set; }
        public string start_date { get; set; }
        public int threshold { get; set; }
        public Image image { get; set; }
    }


    public class Image
    {
        public string type { get; set; }
        public string data { get; set; }
    }

    public class RequestBudget
    {
        public string name { get; set; }
        public string currency { get; set; }
        public double amount { get; set; }
        public string direction { get; set; }
        public string search_query { get; set; }
        public string frequency { get; set; }
        public string start_date { get; set; }
        public int threshold { get; set; }
        public Image image { get; set; }
    }


    public class Payload
    {
        public Budget budget { get; set; }
        public List<List<object>> history { get; set; }
    }

    public class RootObject
    {
        public Header header { get; set; }
        public List<Payload> payload { get; set; }
    }
}
