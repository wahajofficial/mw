﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyThorHandler.Model
{
    public class synccustomFieldResponse
    {
        public string Key { get; set; }
        public List<CustomField> custom_fields { get; set; }
    }

    public class responsesynccustomField
    {
        public Header header { get; set; }
        public synccustomFieldResponse payload { get; set; }
    }
}
