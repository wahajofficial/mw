﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyThorHandler.Model.TipsByPattern
{

    public class Header
    {
        public bool success { get; set; }
        public bool authenticated { get; set; }
        public string message { get; set; }
        public int code { get; set; }
    }

    public class Target
    {
        public string type { get; set; }
    }

    public class Payload
    {
        public string tip_key { get; set; }
        public DateTime creation { get; set; }
        public string title { get; set; }
        public int height { get; set; }
        public string origin { get; set; }
        public string status { get; set; }
        public string running_from { get; set; }
        public string running_to { get; set; }
        public int priority { get; set; }
        public string latency_mode { get; set; }
        public int latency_days { get; set; }
        public string data { get; set; }
        public List<object> events { get; set; }
        public Target target { get; set; }
    }

    public class RootObject
    {
        public Header header { get; set; }
        public List<Payload> payload { get; set; }
    }

}
