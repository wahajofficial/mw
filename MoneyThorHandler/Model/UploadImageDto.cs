﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyThorHandler.Model
{
    public class UploadImageDto
    {
        public string key { get; set; }
        public List<CustomField> custom_fields { get; set; }  
    }
   
}
