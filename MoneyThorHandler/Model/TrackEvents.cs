﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyThorHandler.Model
{
    public class TrackEvents
    {
        public class Value
        {
            public string data { get; set; }
            public string reaction { get; set; }
            public bool? request_copy { get; set; }
            public string latency_event { get; set; }
            public string comment { get; set; }
        }

        public class Event
        {
            public string category { get; set; }
            public string action { get; set; }
            public string label { get; set; }
            public Value value { get; set; }
        }

        public class RootObject
        {
            public List<Event> events { get; set; }
        }
    }
}
