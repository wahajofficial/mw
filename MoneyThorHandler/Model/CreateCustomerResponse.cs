﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyThorHandler.Model
{
   public class CreateCustomerResponse
    {
        public Header header { get; set; }
        public CreateCustomerPayLoad payLoad { get; set; }

    }
    public class CreateCustomerPayLoad
    {
        public Source source { get; set; }
        public Customer customer { get; set; }
        public AccountResponse account { get; set; }

    }
    public class AccountResponse
    {
        public bool status { get; set; }
        public string account_Key { get; set; }
        //Account Number
        public string number { get; set; }
        //Account Name
        public string name { get; set; }
        //Account Description
        public string description { get; set; }
        //Account Currency
        public string currency { get; set; }
        public string type { get; set; }
        public List<CustomField> custom_fields { get; set; }

    }

}
