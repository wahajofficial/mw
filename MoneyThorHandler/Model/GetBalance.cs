﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyThorHandler.Model
{
    public class HeaderBalance
    {
        public bool success { get; set; }
        public string message { get; set; }
    }

    public class PayloadBalance
    {
        public string bank_name { get; set; }
        public string bank_full_name { get; set; }
        public string currency { get; set; }
        public List<List<object>> balances { get; set; }
    }

    public class RootObjectBalance
    {
        public HeaderBalance header { get; set; }
        public List<PayloadBalance> payload { get; set; }
    }
}
