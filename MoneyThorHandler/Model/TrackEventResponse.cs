﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyThorHandler.Model.TrackEventResponse
{
    public class Header
    {
        public bool success { get; set; }
        public bool authenticated { get; set; }
        public string message { get; set; }
    }

    public class RootObject
    {
        public Header header { get; set; }
    }
   
}
