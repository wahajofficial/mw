﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyThorHandler.Model
{
    public class CreateCsutomer
    {
        public Source source { get; set; }
        public Customer customer { get; set; }
        public Account account { get; set; }
    }

    public class TransactionDTO
    {
        public Source source { get; set; }
        public Customer customer { get; set; }
        public Account account { get; set; }
        public List<Transaction> transactions { get; set; }
    }

    public class Source
    {
        //Bank Name
        public string name { get; set; }
    }

    public class Customer
    {
        //Custoemr CIf 
        public string name { get; set; }
        public List<CustomField> custom_fields { get; set; }
    }

    public class Account
    {
        //Account Number
        public string number { get; set; }
        //Account Name
        public string name { get; set; }
        //Account Description
        public string description { get; set; }
        //Account Currency
        public string currency { get; set; }
        public string type { get; set; }
        public List<CustomField> custom_fields { get; set; }
    }
}
